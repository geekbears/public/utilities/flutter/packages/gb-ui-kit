# Template Screens

This widgets are usable to template common screens

## Loading

![Loading Template](images/template_screens/loading_template.png?raw=true)

Name: `LoadingTemplate`

## Login

![Login Template](images/template_screens/login_template.png?raw=true)

Name: `LoginTemplate`

## Signup

![Signup Template](images/template_screens/signup_template.png?raw=true)

Name: `SignUpTemplate`

## Error

![Error Template](images/template_screens/error_template.png?raw=true)

Name: `ErrorTemplate`

## Account

![Account Code Template](images/template_screens/confirm_account_code_template.png?raw=true)

Name: `AccountCodeConfirmationTemplate`

## Reset Password Request

![Reset Password Request Template](images/template_screens/reset_password_request_template.png?raw=true)

Name: `ResetPasswordRequestTemplate`

## Reset Password Code Input

![Reset Password Code Template](images/template_screens/reset_password_code_template.png?raw=true)

Name: `ResetPasswordCodeInputTemplate`

## Reset Password Set

![Reset Password Code Template](images/template_screens/reset_password_set_template.png?raw=true)

Name: `ResetPasswordSetTemplate`