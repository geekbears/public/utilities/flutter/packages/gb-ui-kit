## [2.7.0]
- Updates for flutter 3.29.0

## [2.6.3]
- Added 2 new APIs for SelectField:
    'optionsFooterBuilder'
    'optionsSeparatorBuilder'
    
    Make 'AnimatedFadeHeightSwitcher' to support slivers, same as AnimatedDisplayable

## [2.6.2]
- Improvements to focus management on SelectField

## [2.6.1]
- Fix SelectFieldOption.iconBuilder & Fix initialSelectField

## [2.6.0]
- Deprecate usage of TexField.maxWidth, it now relies on inputDecorationTheme.constraints?.maxWidth

## [2.5.16]
- Added SelectField.iconBuilder API to customize the select field icon to be used

## [2.5.15]
- Small fixes/improves in 'showAppModal' helper function
  - New support for 'actionsSpacing'
  - Fixed 'titlesActionsSpacing implementation

## [2.5.14]
- Added ResetPasswordCodeInputTemplate.codeFieldKeyboardType for customizing keyboard type

## [2.5.13]
- Small improvement in InputFeedbackText, allow text rapping when is wider than parent container
- Added SelectField.onTransitionEnds api

## [2.5.11]
- Provide more customization to SelectField

## [2.5.10]

- UI Fixes for CodeField (fix overflow issue)

## [2.5.9]

- Added new onSearchChange for SelectField
- Added possibility to customize ResetPasswordCodeInputTemplate.codeFieldLength

## [2.5.8]

- Small fixes to TextField text styling
- Support custom field padding on select field

## [2.5.6]

- Small improvements in TextField, provide a way to style labels
- New 'fieldAlignment' property on CodeFIeld
- Small improvements on ResetPasswordCode Input, ResetPasswordRequest, Reset password set template Templates
- More flexibility on Signup Template

## [2.5.5]

- Support customization for Outlined Button from theme
- Support for configuring ContainerButton "uppercase"

## [2.5.4]

- Allow to define custom loading indicator for `LoadingTemplate` widget

## [2.5.3]

- Fix Snackbar wring font size map after flutter 3.22.x upgrade

## [2.5.2]

- Added the autofillHints property to the TextField & PasswordField in order to support the autofill feature.

## [2.5.1]

- Allow to force errorStyle on inputs without the need of a message
- Allow to define a max width of fields in ForgotPasswordRequestTemplate

## [2.5.0]

- Upgrade to Flutter 3.22.0

## [2.4.1]

- Improve fallback of GBTheme.of

## [2.4.0]

- Allow PasswordField to implement decoration
  This allow flexibility on the way custom widgets are added based on obscure state.

## [2.3.6]

- Added more flexibility to login and reet password templates

## [2.3.5]

- Fixes for link button color theming

## [2.3.4]

- Pass borderWidth to OutlinedButton
- Add extraContent and inputs hint in ResetPasswordCodeInputTemplate
- Other improvements

## [2.3.3]

- Fixes and customizations in
  Login and Reset password templates

## [2.3.0]

- Upgrade for compatibility with Flutter 3.19.x

## [2.2.1]

- Add a new property [spaceBetweenFields] to customize the space between each input fields on SignupTemplate

## [2.2.0]

- Make compatible with Flutter 3.16.4
- Fix "useMaterial3" property being ignored

## [2.1.3]

- TimePicker widget improvements
  - Now we use the disabledTimeStyle when an hour is disabled completely before selecting it, so now it is painted as not available before scrolling to it.

## [2.1.2]

- Fix broken functionality on TimePicker

## [2.1.0]

- Flutter SDK has been upgraded to 3.13.0

## [2.0.2]

- CodeField: Avoid notifying change on initial value set

## [2.0.0]

- Flutter SDK has been upgraded to 3.10.5

## [1.11.11]

- Improves to CodeField widget

## [1.11.10]

- Support input bindings on some template screens
  - LoginTemplate
  - AccountCodeConfirmationTemplate
  - ResetPasswordCodeInputTemplate
  - ResetPasswordRequestTemplate

## [1.11.9]

- Auth template improvements
  - Add a flag to remove the space when there is no need to show a title and description

## [1.11.8]

- Added `textAlign` to ComposedActionText

## [1.11.7]

- Small improve in LinkButton

## [1.11.6]

- Login template changes

## [1.11.5]

- sign up template changes

## [1.11.4]

- Added 'keyboardType' prop to AccountCodeConfirmationTemplate

## [1.11.3]

- LoginTemplate has a new property called haveAnAccountBuilder which provide us the ability to define a custom builder for the have an account legend widget.

## [1.11.2]

- Added 'forceUppercase' to AccountCodeConfirmationTemplate

## [1.11.1]

- Fix ResetPasswordSetTemplate stateChange callback

## [1.11.0]

- Publishing updates

## [1.10.0]

- Improvements in login template
- Refactored some forgot password flow screens

## [1.9.9]

- Added RepaintBoundary to ScreenLoadingIndicator
- Prevents error
- AnimatedFadeHeightSwitcher improvements
  - Support curve in AnimatedFadeHeightSwitcher

## [1.9.6]

- Fix select-field search focus issue
-

## [1.9.5]

- Fixes in MultiSelectField

## [1.9.3]

- Select field improvements
- Small fixes in multi select
- Fixes in SelectField

## [1.9.0]

- Added `MultiSelectField` widget

## [1.8.2]

- Improves in SelectField
- Maybe fixes for CodeField:
  - https://geekbears.sentry.io/issues/3944496208/?project=4504407159537664&query=is%3Aunresolved&referrer=issue-stream

## [1.8.1]

- Fix accent color property on theme

## [1.8.0]

- Upgrade for flutter 3.7.3

## [1.7.27]

- Allow Time picker to receive 24 hour input format

## [1.7.26]

- Improves in login & signup template

## [1.7.24]

- TimePicker now dispatches the methods onChanged & onChangedParsed when it is built.
  Also some improvements has been implemented.

## [1.7.23]

- Improves to CodeField
  - Allow Paste functionality

## [1.7.22]

- Added GenericErrorRetry widget

## [1.7.18]

- Use email address keyword in some screen templates
- Disable autocorrect when using email keyboard type

## [1.7.17]

- Added `AnimatedFadeHeightSwitcher.visible` factory

## [1.7.16]

- TimePicker now is able to handle the blocking of time intervals along with allowing the time selection that is after the hours and/or minutes that we have indicated.

## [1.7.15]

- `TimePicker` widget has a new feature that allow us to block some time intervals using the new property `unavailableTimes` which receives a list of `UnavailableTime`.

## [1.7.14]

- Fix hintText in signup form

## [1.7.13]

- Improved debounced busy state on buttons
- Added `LoadingDebouncedBuilder` as a helper widget that provides more flexibility
- Added `ScreenLoadingIndicator` widget

## [1.7.11]

- Small improvements to login template

## [1.7.8]

- Small changes in ContainedButton (size)

## [1.7.7]

- Added `innerBoxWrapper` property to TexField 'onSubmitted' property to TextField
  Improvements to CodeField
  Allow templates to specify 'textfieldBoxWrapper'
  Added AppBar.systemOverlayStyle

## [1.7.5]

- Improvements in modals

## [1.7.4]

- Fixes in page templates:
  Login Template
  Reset Password Code Input
  Reset Password Request
  Reset Password Set

## [1.7.3]

- Update to `LoginTemplate` considering iPhone 14 aspect ration
- Fixes and improves to `AccountCodeConfirmationTemplate`

## [1.7.2]

- Fixes in 'SelectField'
- Added TextField.textAlignVertical property
- Fixes in modal "showAppModal"

## [1.7.1] - 14/10/22

- Type AnimatedStateListItemBuilder.animation as `Animation<double>`
- Fixes for AnimatableStateListView

## [1.7.0] - 06/10/22

- Upgrade Flutter version

## [1.4.20] - 29/09/22

- Added CodeField.forceUppercase property
- Added "titleColor", "messageColor" & "padding" props to `ErrorTemplate`

## [1.4.17] - 29/09/22

- Fixes in `AnimatableStateListView`

## [1.4.16] - 26/09/22

- Textfield:
  Add a condition to update the widget if the initial value changes by code

## [1.4.15] - 7/09/22

- Some modifications were applied to the TimePicker widget to improve its functionality

## [1.4.14] - 5/09/22

- Minor bug in `LoadingDebouncedSwitcher`

## [1.4.13] - 5/09/22

- Add support to username in signup page template

## [1.4.12] - 1/09/22

- Fixes in `AnimatableStateListView`
  Allow to pass custom scroll controller

## [1.4.11] - 30/08/22

- Improve `CodeField` widget
  - Allow to override input TextStyle
  - Set `textInputAction` that defaults to `TextInputAction.done`
  - Added `onSubmitted` property

## [1.4.10] - 29/08/22

- TimePicker widget has been integrated.

## [1.4.9] - 29/08/22

- Added 'AnimatableStateListView'
  - A useful widget to render animated list views
  - It handles insert and remove animations based on input data changes

## [1.4.8] - 16/08/22

- Added `AudioVisualizer` Widget

## [1.4.7] - 3/08/22

- Added `useRootNavigator` to `showAppModal` method

## [1.4.6] - 3/08/22

- Refactor `ErrorKeeper` => `GenericDataHandlerWidget`
  - `GenericDataHandlerWidget` improvements

## [1.4.5] - 1/08/22

- `ErrorKeeper` widget has been integrated along with `AnimatedPlaceholder` widget, also some improvements were applied to the `SelectableRadio` widget.

## [1.4.4] - 27_07/22

- The following properties has been added to the TextField widget:

  bool readOnly
  TextInputType? keyboardType
  List<TextInputFormatter>? inputFormatters
  TextCapitalization textCapitalization
  TextInputAction? textInputAction
  TextAlign textAlign

## [1.4.3] - 24/07/22

- Contained button fixed & autoroute upgrade

## [1.4.2] - 22/07/22

- Improve 'SelectField' initial value setup

## [1.4.1] - 20/07/22

- Upgrade dependencies
  - animated_bottom_navigation_bar
  - flutter_colorpicker
- Fixes in loading debounced & contained button

## [1.4.0] - 19/07/22

- Make it compatible with Flutter 3.0.x

## [1.3.14] - 19/07/22

- Added `fillColor`property to SelectField widget
- Added `maxWidth` property to input widgets
- Added `labelStyle` property to input widgets
- Added `titleStyle`& `optionTileStyle` properties for select field

## [1.3.10] - 14/07/22

- Apply link button color property if not null

## [1.3.9] - 11/07/22

- Added "Save" string to localizations

## [1.3.8] - 06/07/22

- Improve templates

  - LoginTemplate
  - ResetPasswordCodeInputTemplate
  - ResetPasswordSetTemplate
  - ResetPasswordCodeInputScreen
  - ResetPasswordRequestTemplate

  Now they allow more flexible layout and refactor some properties names

## [1.3.7] - 29/06/22

- Add a content padding property

## [1.3.6] - 29/06/22

- Fixed `Unhandled Exception: Bad state: No element`
  In `showSnackBarAlert` function

## [1.3.5] - 24/06/22

- Add a validation in Login Template to handle when the icon property is null

## [1.3.4] - 23/06/22

- Small improves in SignUp and Loading template

## [1.3.3] - 08/06/22

- Fixes typo in property `maxLength`
- Improve the way text color is populated from theme
- Allow a custom `textStyle` to be provided

## [1.3.2] - 07/06/22

- Added some params to the text field widget
  - ```dart
      final int? maxLenght;
      final int? maxLines;
      final int? minLines;
      final bool? expands;
    ```

## [1.3.1] - 21/05/22

- Added new properties for ScreenTemplates

  - ```dart
    /// The color applied to the haveAccount legend action color
    final Color? legendTextColor;

    /// The Style to apply to the haveAccount legend normal text
    final TextStyle? legendNormalStyle;

    /// The Style to apply to the haveAccount legend normal text
    final TextStyle? legendActionStyle;
    ```

  ```

  ```

- Fixes on Theme and `containedButtonThemeData` property not applying
- Breaking changes
  - Removed `actionColor` on `HaveAnAccountLegend` widget

## [1.2.17] - 02/05/22

- Background Focus theme data fixes
- Template Screens Improvements

## [1.2.16] - 02/05/22

- Updated Screens Templates, now they are more customizable
- Added new GBScaffold widget which supports, gradient

## [1.2.14] - 06/04/22

- Added more properties to `HaveAnAccountLegend` widget
  - ```
      bool upperCase;
      TextStyle? normalStyle;
      TextStyle? actionStyle;
    ```

## [1.2.13] - 06/04/22

- Added AddressStatesSelectField
- Changed searchFunctionality for select field

## [1.2.12] - 01/04/22

- Added `keywordType` support for `CodeField` widget
-

## [1.2.11] - 29/03/22

- Added Template Screens
- Name: LoadingTemplate
- Name: LoginTemplate
- Name: SignUpTemplate
- Name: ErrorTemplate
- Name: AccountCodeConfirmationTemplate
- Name: ResetPasswordRequestTemplate
- Name: ResetPasswordCodeInputTemplate
- Name: ResetPasswordSetTemplate

## [1.2.10] - 25/03/22

- Upgrade Example flutter version
- `CodeField` improvements
  - It now supports changing focus node when deleting text
- Added `AnimatedDisplayable` widget
- Added `AnimatedFadeHeightSwitcher` widget

## [1.2.9] - 25/03/22

- **SelectField**
  - Search filter capability

## [1.2.8] - 04/04/22

- **Textfield**

  - Allow to use the property prefixIcon & the property prefix.
  - Small changes were added to the colors in suffixIcon.

## [1.2.7] - 28/04/22

- **Modals**

  - Property `customCloseIcon` added to use a custom widget that will be used to close the modal.

  - `backgroundColor` property added.

  - `shape` property integrated.

- **Switcher**

  - `Switcher` widget added to handle the Switch behavior.

- **GBThemeData**

  - Small changes when using the `GBInputDecorationTheme` to apply the hintStyle and labelStyle properties from custom theme.

## [1.2.6] - 24/02/22

- Override button size in contained button

## [1.2.5] - 24/02/22

- Added support for ContainedButtonTheme
- Fixes and Cleanup

## [1.2.4] - 24/02/22

- Support for custom input decoration fill

## [1.2.3] - 24/02/22

- Refactor input fields border implementation

## [1.2.2] - 23/02/22

- Allow use gradient within ContainedButton widget.
- Allow use the InputBorder from the theme on TextField widget.

## [1.2.1] - 22/02/22

- Added snackbar and dialog modal

## [1.2.0] - 22/02/22

- Material 3 impl (beta?)
- Added FVM version declaration

## [1.1.2] - 04/02/22

- Selectfield issue fixed
- Clip on Select field container

## [1.1.1] - 23/12/21

- Small fix in text theme override

## [1.1.0] - 23/12/21

- Update theme to be compatible with flutter 2.8.1

## [1.0.22] - 12/12/21

- Added busy capability to ComposedText

## [1.0.21] - 17/11/21

- Disable scroll in Feedback text

## [1.0.20] - 17/11/21

- Added onChanged property to textfield

## [1.0.19] - 16/11/21

- Added Input Feedback Text

## [1.0.18] - 06/11/21

- Added disabled propertty to buttons

## [1.0.17] - 06/11/21

- Added missing properties to button factories

## [1.0.16] - 06/11/21

- Support debounceTime prop in button factory constructors

## [1.0.15] - 05/11/21

- Added debounce functionallity to buttons

## [1.0.14] - 05/11/21

- Added uppercase logic to buttons

## [1.0.13] - 05/11/21

## [1.0.12] - 05/11/21

- Fix composed action text

## [1.0.11] - 05/11/21

- Some fixes and improves

## [1.0.10] - 05/11/21

- Added composed action text

## [1.0.9] - 27/10/21

- Small changes in text theme styles

## [1.0.8] - 26/10/21

- Fixed busy indicator, inverse logic

## [1.0.7] - 26/10/21

- Add support for busy indicators on buttons

## [1.0.6] - 20/10/21

- Added support for TextEditingController for fields

## [1.0.5] - 20/10/21

- Fix null safety issue

## [1.0.4] - 20/10/21

- Allow text theme override

## [1.0.3] - 20/10/21

- Added border radius support for theme

## [1.0.2] - 20/10/21

- Fix GBTheme widget

## [1.0.1] - 16/10/21

- Added support for new progressIndicatorTheme property

## [1.0.0] - 06/08/21

- Null safety

## [0.0.2] - 06/08/21

- Added more components

## [0.0.1] - TODO: Add release date.

- TODO: Describe initial release.
