// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// WidgetbookGenerator
// **************************************************************************

import 'dart:core';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:widgetbook/widgetbook.dart';
import 'package:widgetbook_project/app_widget.dart';
import 'package:widgetbook_project/app_widget_theme.dart';
import 'package:widgetbook_project/presentation/buttons/contained_button.dart';
import 'package:widgetbook_project/presentation/inputs/select_field.dart';

void main() {
  runApp(const HotReload());
}

class HotReload extends StatelessWidget {
  const HotReload({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Widgetbook.material(
      appInfo: AppInfo(
        name: 'GBUI KIT App',
      ),
      supportedLocales: locales,
      localizationsDelegates: delegates,
      themes: [
        WidgetbookTheme(
          name: 'Light',
          data: getLightThemeData(),
        ),
      ],
      devices: [
        const Device(
          name: 'iPhone 12',
          resolution: Resolution(
            nativeSize: DeviceSize(
              height: 2532.0,
              width: 1170.0,
            ),
            scaleFactor: 3.0,
          ),
          type: DeviceType.mobile,
        ),
      ],
      frames: [
        const WidgetbookFrame(
          name: 'Widgetbook',
          allowsDevices: true,
        ),
        const WidgetbookFrame(
          name: 'None',
          allowsDevices: false,
        ),
      ],
      textScaleFactors: [
        1.0,
        2.0,
      ],
      categories: [
        WidgetbookCategory(
          name: 'use cases',
          folders: [
            WidgetbookFolder(
              name: 'presentation',
              widgets: [],
              folders: [
                WidgetbookFolder(
                  name: 'inputs',
                  widgets: [],
                  folders: [
                    WidgetbookFolder(
                      name: 'select_field',
                      widgets: [
                        WidgetbookComponent(
                          name: 'SelectField<dynamic>',
                          useCases: [
                            WidgetbookUseCase(
                              name: 'SelectField',
                              builder: (context) => selectFieldBasic(context),
                            ),
                          ],
                          isExpanded: true,
                        ),
                      ],
                      folders: [],
                      isExpanded: true,
                    ),
                  ],
                  isExpanded: true,
                ),
                WidgetbookFolder(
                  name: 'buttons',
                  widgets: [
                    WidgetbookComponent(
                      name: 'ContainedButton',
                      useCases: [
                        WidgetbookUseCase(
                          name: 'ContainedButton - Default',
                          builder: (context) => containedButtonDefault(context),
                        ),
                        WidgetbookUseCase(
                          name: 'ContainedButton.small',
                          builder: (context) => containedButtonSmall(context),
                        ),
                        WidgetbookUseCase(
                          name: 'ContainedButton.large',
                          builder: (context) => containedButtonLarge(context),
                        ),
                        WidgetbookUseCase(
                          name: 'ContainedButton.text',
                          builder: (context) => containedButtonText(context),
                        ),
                      ],
                      isExpanded: true,
                    ),
                  ],
                  folders: [],
                  isExpanded: true,
                ),
              ],
              isExpanded: true,
            ),
          ],
          widgets: [],
        ),
      ],
      deviceFrameBuilder: frameBuilder,
      localizationBuilder: localizationBuilder,
      scaffoldBuilder: scaffoldBuilder,
      themeBuilder: themeBuilder(),
    );
  }
}
