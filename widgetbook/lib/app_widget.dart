import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/generated/l10n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';
import 'package:widgetbook/widgetbook.dart';

@WidgetbookDeviceFrameBuilder()
// ignore: prefer_function_declarations_over_variables
DeviceFrameBuilderFunction frameBuilder = (
  BuildContext context,
  Device device,
  WidgetbookFrame frame,
  Orientation orientation,
  Widget child,
) {
  if (frame == WidgetbookFrame.defaultFrame()) {
    return WidgetbookDeviceFrame(
      orientation: orientation,
      device: device,
      child: child,
    );
  }

  if (frame == WidgetbookFrame.deviceFrame()) {
    final deviceInfo = mapDeviceToDeviceInfo(device);
    return DeviceFrame(
      orientation: orientation,
      device: deviceInfo,
      screen: child,
    );
  }

  return child;
};

@WidgetbookLocalizationBuilder()
// ignore: prefer_function_declarations_over_variables
LocalizationBuilderFunction localizationBuilder = (
  BuildContext context,
  List<Locale> supportedLocales,
  List<LocalizationsDelegate<dynamic>>? localizationsDelegates,
  Locale activeLocale,
  Widget child,
) {
  if (localizationsDelegates != null) {
    return Localizations(
      locale: activeLocale,
      delegates: localizationsDelegates,
      child: child,
    );
  }

  return child;
};

@WidgetbookScaffoldBuilder()
// ignore: prefer_function_declarations_over_variables
ScaffoldBuilderFunction scaffoldBuilder = (
  BuildContext context,
  WidgetbookFrame frame,
  Widget child,
) {
  if (frame.allowsDevices) {
    return Scaffold(
      body: child,
    );
  }

  return child;
};

@WidgetbookThemeBuilder()
ThemeBuilderFunction<CustomTheme> themeBuilder<CustomTheme>() => (
      BuildContext context,
      CustomTheme theme,
      Widget child,
    ) {
      if (theme is ThemeData) {
        return Theme(
          data: theme,
          child: child,
        );
      }
      // if (theme is CupertinoThemeData) {
      //   return CupertinoTheme(
      //     data: theme,
      //     child: child,
      //   );
      // }

      throw Exception(
        'You are using Widgetbook with custom theme data. '
        'Please provide an implementation for themeBuilder.',
      );
    };

@WidgetbookLocales()
const locales = <Locale>[
  Locale('en'),
  Locale('de'),
  Locale('fr'),
];

@WidgetbookLocalizationDelegates()
List<LocalizationsDelegate> delegates = [
  GBUiKitLocalizations.delegate,
  GlobalMaterialLocalizations.delegate,
  GlobalWidgetsLocalizations.delegate,
  GlobalCupertinoLocalizations.delegate,
];

@WidgetbookApp.material(
  name: 'GBUI KIT App',
  frames: [
    WidgetbookFrame(
      name: 'Widgetbook',
      allowsDevices: true,
    ),
    WidgetbookFrame(
      name: 'None',
      allowsDevices: false,
    ),
  ],
  devices: [Apple.iPhone12],
  textScaleFactors: [
    1,
    2,
    // 3,
  ],
  foldersExpanded: true,
  widgetsExpanded: true,
)
class Empty {}
