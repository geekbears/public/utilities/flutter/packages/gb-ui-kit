import 'package:flutter/cupertino.dart';
import 'package:flutter_gb_ui_kit/presentation/presentation.dart';
// import 'package:widgetbook/widgetbook.dart';
import 'package:flutter/material.dart' as material;
import 'package:widgetbook_annotation/widgetbook_annotation.dart';
import 'package:widgetbook/src/knobs/knobs.dart';

class _ExampleOption {
  final String value;

  const _ExampleOption({
    required this.value,
  });
}

List<SelectFieldOption<_ExampleOption>> _exampleOptions() {
  return const [
    SelectFieldOption(
      title: "Example key",
      value: _ExampleOption(
        value: "Example Value 1",
      ),
    ),
    SelectFieldOption(
      title: "Example key 2",
      value: _ExampleOption(
        value: "Example Value 2",
      ),
    ),
  ];
}

@WidgetbookUseCase(name: 'SelectField', type: SelectField)
Widget selectFieldBasic(BuildContext context) {
  String? label = context.knobs.nullableText(label: "label");
  bool enabled = context.knobs.boolean(label: "enabled", initialValue: true);
  String? unselectedString = context.knobs.nullableText(label: "unselectedString");
  Duration duration = ((value) {
    return Duration(milliseconds: (value as double).toInt());
  })(
    context.knobs.slider(
      label: "duration",
      description: "Duration of open/close animation",
      initialValue: 450.0,
      min: 100,
      max: 800,
    ),
  );
  // T? initialValue;
  // // ScrollController? scrollController;
  // // List<SelectFieldOption<T>> options;
  // // ValueChanged<T>? onSelected;
  bool closeOnSelect = context.knobs.boolean(label: "closeOnSelect");
  String? errorText = context.knobs.nullableText(label: "errorText");
  // // SelectTrailingBuilder? trailingBuilder;
  BorderRadius? borderRadius;
  bool? filled = context.knobs.boolean(
    label: "filled",
    initialValue: false,
  );
  bool enableSearch = context.knobs.boolean(
    label: "enableSearch",
    initialValue: false,
  );

  Color? fillColor = context.knobs.options(label: "fillColor", options: const [
    Option(label: "None", value: null),
    Option(
      label: "Red",
      value: material.Colors.red,
    ),
    Option(
      label: "Blue",
      value: material.Colors.blue,
    ),
  ]);

  bool overrideColor = context.knobs.boolean(
    label: "overrideColor",
    initialValue: false,
  );

  double maxWidth = context.knobs.slider(
    label: "maxWidth",
    initialValue: 343,
    min: 100,
    max: 420,
  );
  // List<InputFeedbackText>? feedback;

  return material.Center(
    child: material.Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SelectField<_ExampleOption>(
          label: label,
          enabled: enabled,
          unselectedString: unselectedString,
          duration: duration,
          options: _exampleOptions(),
          enableSearch: enableSearch,
          filled: filled,
          closeOnSelect: closeOnSelect,
          errorText: errorText,
          fillColor: fillColor,
          overrideColor: overrideColor,
          maxWidth: maxWidth,
        ),
      ],
    ),
  );
}
