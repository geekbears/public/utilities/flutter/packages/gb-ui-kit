import 'package:flutter/widgets.dart';
import 'package:flutter_gb_ui_kit/presentation/buttons/buttons.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';
import 'package:flutter/material.dart' as material;
import 'package:widgetbook/widgetbook.dart' hide WidgetbookUseCase;
import 'package:widgetbook_project/utils/utils.dart';

@WidgetbookUseCase(name: 'ContainedButton - Default', type: ContainedButton)
Widget containedButtonDefault(BuildContext context) {
  return _buildCommonScaffold(
    _buildWithCommonProperties(context, (context, properties) {
      return ContainedButton(
        onPressed: () {},
        borderRadius: properties.borderRadius,
        busy: properties.busy,
        busyColor: properties.busyColor,
        disabled: properties.disabled,
        height: properties.height,
        loadingDebounce: properties.loadingDebounce,
        padding: properties.padding,
        colors: properties.colors,
        child: Row(
          children: [
            const Icon(material.Icons.accessibility_new),
            RichText(
              text: const TextSpan(
                text: "Rich ",
                children: [
                  TextSpan(text: "Text"),
                ],
              ),
            ),
            const Icon(material.Icons.sports_martial_arts_rounded),
          ],
        ),
      );
    }),
  );
}

@WidgetbookUseCase(name: 'ContainedButton.small', type: ContainedButton)
Widget containedButtonSmall(BuildContext context) {
  return _buildCommonScaffold(
    _buildWithCommonProperties(context, (context, properties) {
      return ContainedButton.small(
        onPressed: () {},
        borderRadius: properties.borderRadius,
        busy: properties.busy,
        busyColor: properties.busyColor,
        disabled: properties.disabled,
        loadingDebounce: properties.loadingDebounce,
        padding: properties.padding,
        colors: properties.colors,
        child: Row(
          children: [
            const Icon(material.Icons.accessibility_new),
            RichText(
              text: const TextSpan(
                text: "Rich ",
                children: [
                  TextSpan(text: "Text"),
                ],
              ),
            ),
            const Icon(material.Icons.sports_martial_arts_rounded),
          ],
        ),
      );
    }),
  );
}

@WidgetbookUseCase(name: 'ContainedButton.large', type: ContainedButton)
Widget containedButtonLarge(BuildContext context) {
  return _buildCommonScaffold(
    _buildWithCommonProperties(context, (context, properties) {
      return ContainedButton.large(
        onPressed: () {},
        borderRadius: properties.borderRadius,
        busy: properties.busy,
        busyColor: properties.busyColor,
        disabled: properties.disabled,
        loadingDebounce: properties.loadingDebounce,
        padding: properties.padding,
        colors: properties.colors,
        child: Row(
          children: [
            const Icon(material.Icons.accessibility_new),
            RichText(
              text: const TextSpan(
                text: "Rich ",
                children: [
                  TextSpan(text: "Text"),
                ],
              ),
            ),
            const Icon(material.Icons.sports_martial_arts_rounded),
          ],
        ),
      );
    }),
  );
}

@WidgetbookUseCase(name: 'ContainedButton.text', type: ContainedButton)
Widget containedButtonText(BuildContext context) {
  return _buildCommonScaffold(
    _buildWithCommonProperties(context, (context, properties) {
      return ContainedButton.text(
        "Some Text here",
        onPressed: () {},
        borderRadius: properties.borderRadius,
        busy: properties.busy,
        busyColor: properties.busyColor,
        disabled: properties.disabled,
        loadingDebounce: properties.loadingDebounce,
        padding: properties.padding,
        colors: properties.colors,
      );
    }),
  );
}

/////
/////

Widget _buildCommonScaffold(Widget child) {
  return Builder(
    builder: (context) {
      return material.Center(
        child: material.Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            child,
          ],
        ),
      );
    },
  );
}

Widget _buildWithCommonProperties(BuildContext context, ButtonCommonBuilderFn builder) {
  BorderRadius borderRadius = borderPropertySlider(context, label: "borderRadius", initialValue: 15, min: 0, max: 30);
  bool busy = context.knobs.boolean(label: "busy", initialValue: false);
  Color busyColor = context.knobs.options(label: "busyColor", options: [
    const Option(
      label: "Red",
      value: material.Colors.red,
    ),
    const Option(
      label: "Blue",
      value: material.Colors.blue,
    ),
    const Option(
      label: "Yellow",
      value: material.Colors.yellow,
    ),
    const Option(
      label: "White",
      value: material.Colors.white,
    ),
  ]);
  List<material.WidgetStateProperty<Color>> colors = context.knobs.options(label: "colors", options: [
    Option(
      label: "Blue",
      value: [material.MaterialStateProperty.all(material.Colors.blue)],
    ),
    Option(
      label: "Red",
      value: [material.MaterialStateProperty.all(material.Colors.red)],
    ),
    Option(
      label: "Gradient Red - Blue",
      value: [
        material.MaterialStateProperty.all(material.Colors.red),
        material.MaterialStateProperty.all(material.Colors.blue)
      ],
    ),
  ]);
  bool disabled = context.knobs.boolean(label: "disabled", initialValue: false);
  double height = context.knobs.slider(label: "height", min: 18, max: 60, initialValue: 20);
  Duration loadingDebounce =
      durationPropertySlider(context, label: "loadingDebounce", initialValue: 400, min: 50, max: 1800);
  EdgeInsets padding = edgeInsetsPropertySlider(context, label: "padding", initialValue: 10, min: 0, max: 35);

  return builder(
    context,
    _CommonButtonProps(
      borderRadius: borderRadius,
      busy: busy,
      busyColor: busyColor,
      disabled: disabled,
      height: height,
      loadingDebounce: loadingDebounce,
      padding: padding,
      colors: colors,
    ),
  );
}

typedef ButtonCommonBuilderFn = Widget Function(BuildContext context, _CommonButtonProps properties);

class _CommonButtonProps {
  final BorderRadius borderRadius;
  final bool busy;
  final Color busyColor;
  final bool disabled;
  final double height;
  final Duration loadingDebounce;
  final EdgeInsets padding;
  final List<material.WidgetStateProperty<Color>> colors;

  const _CommonButtonProps({
    required this.borderRadius,
    required this.busy,
    required this.busyColor,
    required this.disabled,
    required this.height,
    required this.loadingDebounce,
    required this.padding,
    required this.colors,
  });
}
