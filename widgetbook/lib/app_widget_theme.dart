import 'package:flutter/material.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@WidgetbookTheme(name: 'Light', isDefault: true)
ThemeData getLightThemeData() => ThemeData(
      primarySwatch: Colors.green,
    );
