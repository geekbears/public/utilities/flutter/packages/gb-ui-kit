import 'package:flutter/cupertino.dart';
import 'package:widgetbook/widgetbook.dart';

BorderRadius borderPropertySlider(
  BuildContext context, {
  required String label,
  required double initialValue,
  double? min,
  double? max,
}) {
  return ((value) {
    return BorderRadius.circular(value);
  })(context.knobs.slider(
    label: "borderRadius",
    initialValue: initialValue,
    min: min,
    max: max,
  ));
}

Duration durationPropertySlider(
  BuildContext context, {
  required String label,
  required double initialValue,
  double? min,
  double? max,
}) {
  return ((double value) {
    return Duration(
      milliseconds: value.toInt(),
    );
  })(context.knobs.slider(label: label, initialValue: initialValue, min: min, max: max));
}

EdgeInsets edgeInsetsPropertySlider(
  BuildContext context, {
  required String label,
  required double initialValue,
  double? min,
  double? max,
}) {
  return ((value) {
    return EdgeInsets.all(value);
  })(context.knobs.slider(label: label, initialValue: initialValue, min: min, max: max));
}
