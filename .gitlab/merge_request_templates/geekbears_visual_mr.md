## Title
{Your title here}

## Description
{Your description here}

## How can it be tested?
  - {Your instructions here}

## Task links
  - [Task Name](https://clickup.com)

## Screenshots or GIF:
  - ### Device Name

- - - 

## Merge Request checklist: ⚠⚠

- [ ] Fetch latest changes from `dev` branch
- [ ] Merge `dev` branch into feature branch (if needed)
  - [ ] Resolve conflicts caused by merging (if needed)
- [ ] Run tests, and verify nothing's broken (if applicable)
  - [ ] Fix errors detected during testing
- [ ] Push all commits to remote branch

- - -

## Additional notes:

_Add notes to provide context to the maintainer of this repository:_

- 
