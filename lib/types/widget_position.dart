enum ButtonTemplatePosition {
  /// Places the widget below the code field
  BELOW_CODE_FIELD,

  /// Places the widget at the end of screen
  END_OF_SCREEN,
}
