import 'package:flutter/material.dart';

typedef WrapWidgetBuilder = Widget Function(BuildContext context, Widget defaultWidget);
