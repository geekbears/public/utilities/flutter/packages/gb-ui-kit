import 'package:flutter/material.dart';

import '../presentation.dart';

class DefaultBottomItem extends StatelessWidget {
  const DefaultBottomItem({
    Key? key,
    required this.item,
    required this.isSelected,
  }) : super(key: key);

  final BottomNavItem item;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    final gbtheme = GBTheme.of(context);
    final baseColor = gbtheme.textBlack;
    final Color highlightColor = gbtheme.accentColor;

    final Color? textColor = isSelected ? highlightColor : baseColor;
    return Container(
      // width: 200,
      // color: Colors.red,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            item.icon,
            size: 24,
            color: textColor,
          ),
          SizedBox(
            height: 8,
          ),
          if (item.label != null && item.label != "")
            GBText(
              item.label,
              style: TextStyle(
                color: textColor,
                fontSize: 14,
              ),
            ),
        ],
      ),
    );
  }
}
