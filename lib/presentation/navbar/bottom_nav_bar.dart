import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:flutter/material.dart' hide IndexedWidgetBuilder;
import 'package:flutter_gb_ui_kit/presentation/presentation.dart';

class BottomNavItem {
  final IconData? icon;
  final String? label;

  BottomNavItem({
    this.icon,
    this.label,
  });
}

class BottomNavBar extends StatefulWidget {
  final Color? activeColor;
  final Color? backgroundColor;
  final double? height;
  final GapLocation? gapLocation;
  final List<BottomNavItem>? items;
  final Widget Function(int index, bool selected)? itemBuilder;

  BottomNavBar({
    Color? activeColor,
    Color? backgroundColor,
    GapLocation? gapLocation,
    double height = 64,
    IndexedWidgetBuilder? itemBuilder,
    List<BottomNavItem>? items,
    Key? key,
  }) : this._internal(
          activeColor: activeColor,
          backgroundColor: backgroundColor,
          height: height,
          gapLocation: gapLocation,
          items: items,
          itemBuilder: _defaultBuilder(items),
          key: key,
        );

  const BottomNavBar._internal({
    required this.items,
    Key? key,
    this.activeColor,
    this.backgroundColor,
    this.gapLocation,
    this.height,
    this.itemBuilder,
  }) : super(key: key);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

IndexedWidgetBuilder Function(List<BottomNavItem>? items) _defaultBuilder = (items) {
  return (index, isSelected) {
    final BottomNavItem item = items![index];
    return DefaultBottomItem(
      item: item,
      isSelected: isSelected,
    );
  };
};

class _BottomNavBarState extends State<BottomNavBar> {
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    final scaffoldState = Scaffold.of(context);

    // Scaffold.of(context).widget.floatingActionButton
    GapLocation? gapLocation = widget.gapLocation;
    if (gapLocation == null && scaffoldState.hasFloatingActionButton) {
      if (scaffoldState.widget.floatingActionButtonLocation == FloatingActionButtonLocation.centerDocked ||
          scaffoldState.widget.floatingActionButtonLocation == FloatingActionButtonLocation.miniCenterDocked) {
        gapLocation = GapLocation.center;
      } else if (scaffoldState.widget.floatingActionButtonLocation == FloatingActionButtonLocation.endDocked ||
          scaffoldState.widget.floatingActionButtonLocation == FloatingActionButtonLocation.miniEndDocked) {
        gapLocation = GapLocation.end;
      } else if (scaffoldState.widget.floatingActionButtonLocation == FloatingActionButtonLocation.startDocked ||
          scaffoldState.widget.floatingActionButtonLocation == FloatingActionButtonLocation.miniStartDocked) {
        gapLocation = GapLocation.end;
      }
    }
    final GBThemeData? gbTheme = GBTheme.of(context);
    final ThemeData? theme = Theme.of(context);
    if (gbTheme == null || theme == null) {
      throw Exception(
          "GBThemeData can't be found on rendering tree, please make sure you use this widget inside GBTheme Widget");
    }
    final Color activeColor = widget.activeColor ?? theme.colorScheme.secondary;
    final Color splashColor = theme.primaryColor;
    final Color backgroundColor = widget.backgroundColor ?? gbTheme.backgroundBase;

    Widget navBar;

    if (widget.itemBuilder != null) {
      navBar = AnimatedBottomNavigationBar.builder(
        itemCount: widget.items?.length ?? 0,
        splashColor: splashColor,
        backgroundColor: backgroundColor,
        // tabBuilder: widget.itemBuilder,
        tabBuilder: widget.itemBuilder!,
        activeIndex: _index,
        onTap: _onTap,
        gapWidth: 0,
        gapLocation: gapLocation,
        notchMargin: 8,
        splashRadius: 45,
        // elevation: 1,
        height: widget.height,
      );
    } else {
      navBar = AnimatedBottomNavigationBar(
        height: widget.height,
        gapLocation: gapLocation,
        activeColor: activeColor,
        splashColor: activeColor.withAlpha(180),
        iconSize: 20,
        icons: widget.items!.map((e) => e.icon).toList() as List<IconData>,
        activeIndex: _index,
        onTap: _onTap,
      );
    }

    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: gbTheme.backgroundContrast,
            width: 2,
          ),
        ),
      ),
      child: navBar,
    );
    // return AnimatedBottomNavigationBar.builder(
    //   itemCount: 3,
    //   activeIndex: 0,
    //   onTap: (index) {},
    //   tabBuilder: (index, isActive) {
    //     return Column(
    //       children: [
    //         Icon(Icons.ac_unit_outlined),
    //         Text("sasa"),
    //       ],
    //     );
    //   },
    // );
  }

  _onTap(int index) {
    setState(() {
      _index = index;
    });
  }
}
