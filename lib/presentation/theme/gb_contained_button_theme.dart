// Copyright 2014 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'theme.dart';

/// A [ButtonStyle] that overrides the default appearance of
/// [TextButton]s when it's used with [ContainedButtonTheme] or with the
/// overall [Theme]'s [ThemeData.textButtonTheme].
///
/// The [style]'s properties override [TextButton]'s default style,
/// i.e.  the [ButtonStyle] returned by [TextButton.defaultStyleOf]. Only
/// the style's non-null property values or resolved non-null
/// [WidgetStateProperty] values are used.
///
/// See also:
///
///  * [ContainedButtonTheme], the theme which is configured with this class.
///  * [TextButton.defaultStyleOf], which returns the default [ButtonStyle]
///    for text buttons.
///  * [TextButton.styleFrom], which converts simple values into a
///    [ButtonStyle] that's consistent with [TextButton]'s defaults.
///  * [WidgetStateProperty.resolve], "resolve" a material state property
///    to a simple value based on a set of [WidgetState]s.
///  * [ThemeData.textButtonTheme], which can be used to override the default
///    [ButtonStyle] for [TextButton]s below the overall [Theme].
@immutable
class ContainedButtonThemeData extends TextButtonThemeData with Diagnosticable {
  /// Creates a [ContainedButtonThemeData].
  ///
  /// The [style] may be null.
  const ContainedButtonThemeData({
    required this.style,
    this.colors,
    this.uppercase = true,
  }) : super(
          style: style,
        );

  /// Overrides for [TextButton]'s default style.
  ///
  /// Non-null properties or non-null resolved [WidgetStateProperty]
  /// values override the [ButtonStyle] returned by
  /// [TextButton.defaultStyleOf].
  ///
  /// If [style] is null, then this theme doesn't override anything.
  @override
  final ButtonStyle style;

  final List<WidgetStateProperty<Color>>? colors;

  /// Defines if Default Button Text should be transformed to uppercase when using [ContainedButton.text] factory
  /// Defaults to [true]
  final bool uppercase;

  /// Linearly interpolate between two text button themes.
  static ContainedButtonThemeData? lerp(ContainedButtonThemeData? a, ContainedButtonThemeData? b, double t) {
    if (a == null && b == null) return null;
    return ContainedButtonThemeData(
      style: ButtonStyle.lerp(a?.style, b?.style, t)!,
      colors: b?.colors,
      uppercase: b?.uppercase ?? true,
    );
  }

  @override
  int get hashCode {
    return style.hashCode & colors.hashCode & uppercase.hashCode;
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    if (other.runtimeType != runtimeType) return false;
    return other is ContainedButtonThemeData &&
        other.style == style &&
        other.colors == colors &&
        other.uppercase == uppercase;
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<ButtonStyle>('style', style, defaultValue: null));
    properties.add(DiagnosticsProperty<List<WidgetStateProperty<Color>>>('colors', colors, defaultValue: null));
    properties.add(DiagnosticsProperty<bool>('uppercase', uppercase, defaultValue: null));
  }

  static ContainedButtonThemeData getDefaultTheme({
    required Color accentColor,
    required Color backgroundDisabled,
    required BorderRadius? buttonsRadius,
  }) {
    ButtonStyle _defaultButtonStyle = ButtonStyle(
      padding: WidgetStateProperty.all<EdgeInsets>(const EdgeInsets.all(14)),
      minimumSize: WidgetStateProperty.all<Size>(Size(18, 18)),
      backgroundColor: WidgetStateProperty.resolveWith<Color>((states) {
        if (states.contains(WidgetState.disabled)) {
          return backgroundDisabled;
        }
        return accentColor;
      }),
      shape: WidgetStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: buttonsRadius ?? BorderRadius.zero,
        ),
      ),
    );
    return ContainedButtonThemeData(
      style: _defaultButtonStyle,
      uppercase: true,
    );
  }
}

/// Overrides the default [ButtonStyle] of its [TextButton] descendants.
///
/// See also:
///
///  * [ContainedButtonThemeData], which is used to configure this theme.
///  * [TextButton.defaultStyleOf], which returns the default [ButtonStyle]
///    for text buttons.
///  * [TextButton.styleFrom], which converts simple values into a
///    [ButtonStyle] that's consistent with [TextButton]'s defaults.
///  * [ThemeData.textButtonTheme], which can be used to override the default
///    [ButtonStyle] for [TextButton]s below the overall [Theme].
class ContainedButtonTheme extends InheritedTheme {
  /// Create a [ContainedButtonTheme].
  ///
  /// The [data] parameter must not be null.
  const ContainedButtonTheme({
    Key? key,
    required this.data,
    required Widget child,
  }) : super(key: key, child: child);

  /// The configuration of this theme.
  final ContainedButtonThemeData data;

  /// The closest instance of this class that encloses the given context.
  ///
  /// If there is no enclosing [ContainedButtonTheme] widget, then
  /// [ThemeData.textButtonTheme] is used.
  ///
  /// Typical usage is as follows:
  ///
  /// ```dart
  /// TextButtonTheme theme = TextButtonTheme.of(context);
  /// ```
  static ContainedButtonThemeData of(BuildContext context) {
    final ContainedButtonTheme? buttonTheme = context.dependOnInheritedWidgetOfExactType<ContainedButtonTheme>();
    return buttonTheme?.data ?? GBTheme.of(context).containedButtonTheme;
  }

  @override
  Widget wrap(BuildContext context, Widget child) {
    return ContainedButtonTheme(data: data, child: child);
  }

  @override
  bool updateShouldNotify(ContainedButtonTheme oldWidget) => data != oldWidget.data;
}
