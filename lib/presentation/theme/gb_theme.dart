import 'package:flutter/material.dart';
import '../../constants/constants.dart';

import 'gb_theme_data.dart';

class GBTheme extends StatelessWidget {
  static final GBThemeData _kFallbackTheme = GBThemeData.fallback();

  final GBThemeData data;
  final Widget child;

  GBTheme({
    Key? key,
    GBThemeData? data,
    required this.child,
  })  : this.data = data ?? _kFallbackTheme,
        super(key: key);

  static GBThemeData of(BuildContext context) {
    _GBInheritedTheme? inheritedTheme;
    GBThemeData theme;
    try {
      inheritedTheme = context.dependOnInheritedWidgetOfExactType<_GBInheritedTheme>();
      if (inheritedTheme == null) {
        final _theme = Theme.of(context);
        if (_theme is GBThemeData) {
          return _theme;
        }
      }
    } catch (e) {}
    theme = inheritedTheme?.theme.data ?? GBThemeExtension.dataOf(context) ?? _kFallbackTheme;
    return theme;
  }

  static GBThemeData? maybeOf(BuildContext context) {
    _GBInheritedTheme? inheritedTheme;
    inheritedTheme = context.dependOnInheritedWidgetOfExactType<_GBInheritedTheme>();
    if (inheritedTheme == null) {
      final _theme = Theme.of(context);
      if (_theme is GBThemeData) {
        return _theme;
      }
    }
    return inheritedTheme?.theme.data;
  }

  @override
  Widget build(BuildContext context) {
    // Theme.of(context).th
    return Theme(
      data: data,
      child: child,
    );
  }

  static Color textDisabledOf(BuildContext context) =>
      GBThemeExtension.dataOf(context)?.textDisabled ?? DefaultColors.text_gray_disabled;
  static Color errorColorOf(BuildContext context) =>
      GBThemeExtension.dataOf(context)?.accentError ?? DefaultColors.accent_error_red;
  static Color accentColorOf(BuildContext context) =>
      GBThemeExtension.dataOf(context)?.colorScheme.secondary ?? DefaultColors.accent_accent_blue;
  static Color errorLightColorOf(BuildContext context) =>
      GBThemeExtension.dataOf(context)?.accentErrorLight ?? DefaultColors.accent_error_red_light;
  static Color backgroundDisabled(BuildContext context) =>
      GBThemeExtension.dataOf(context)?.backgroundDisabled ?? DefaultColors.background_disabled;
  static Color backgroundFocus(BuildContext context) =>
      GBThemeExtension.dataOf(context)?.backgroundFocus ?? DefaultColors.background_focus;
  static Color backgroundContrast(BuildContext context) =>
      GBThemeExtension.dataOf(context)?.backgroundContrast ?? DefaultColors.background_contrast;
}

class _GBInheritedTheme extends InheritedWidget {
  final GBTheme theme;

  _GBInheritedTheme({
    Key? key,
    required this.theme,
    required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_GBInheritedTheme oldWidget) {
    return oldWidget.theme.data != theme.data;
  }
}
