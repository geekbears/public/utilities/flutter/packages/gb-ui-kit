import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/presentation/theme/gb_contained_button_theme.dart';
import 'package:flutter_gb_ui_kit/presentation/theme/gb_input_decoration_theme.dart';

import '../../constants/colors.dart';

@immutable
class GBThemeData extends ThemeData with Diagnosticable {
  // final Color primaryColor;
  final Color accentColor;
  final Color accentError;
  final Color textDisabled;
  final Color textWhite;
  final Color backgroundBase;
  final Color accentErrorLight;
  final Color backgroundContrast;
  final Color backgroundFocus;
  final Color backgroundDisabled;
  final Color textBase;
  final Color textBlack;

  final BorderRadius? textFieldRadius;
  final double? textFieldBorderWidth;
  final BorderRadius? buttonsRadius;

  @override
  final GBInputDecorationTheme inputDecorationTheme;
  final ContainedButtonThemeData containedButtonTheme;

  factory GBThemeData({
    // @required this.primary,
    // @required this.accent,
    // @required this.accentError,
    // @required this.textGreyDisabled,
    // @required this.textWhite,
    // @required this.backgroundBase,
    // @required this.accentErrorLight,
    // @required this.backgroundContrast,
    // @required this.backgroundFocus,
    // @required this.backgroundDisabled,
    // @required this.textGreyBase,
    // @required this.textBlack,
    Color accentColor = DefaultColors.accent_accent_blue,
    Color accentError = DefaultColors.accent_error_red,
    Color accentErrorLight = DefaultColors.accent_error_red_light,
    Color backgroundBase = DefaultColors.background_base,
    Color backgroundContrast = DefaultColors.background_contrast,
    Color backgroundDisabled = DefaultColors.background_disabled,
    Color backgroundFocus = DefaultColors.background_focus,
    Color primaryColor = DefaultColors.primaryPrimaryYellow,
    Color textBase = DefaultColors.text_gray_base,
    Color textBlack = DefaultColors.text_text_black,
    Color textDisabled = DefaultColors.text_gray_disabled,
    Color textWhite = DefaultColors.text_text_white,
    TextTheme? textTheme,
    BorderRadius? textFieldRadius,
    double? textFieldBorderWidth,
    BorderRadius? buttonsRadius,
    GBInputDecorationTheme? inputDecorationTheme,
    ContainedButtonThemeData? containedButtonThemeData,
    OutlinedButtonThemeData? outlinedButtonThemeData,
    //
    bool? useMaterial3,
  }) {
    accentColor = accentColor;
    accentError = accentError;
    accentErrorLight = accentErrorLight;
    backgroundBase = backgroundBase;
    backgroundContrast = backgroundContrast;
    backgroundDisabled = backgroundDisabled;
    backgroundFocus = backgroundFocus;
    primaryColor = primaryColor;
    textBase = textBase;
    textBlack = textBlack;
    textDisabled = textDisabled;
    textWhite = textWhite;
    textFieldRadius = textFieldRadius;
    buttonsRadius = buttonsRadius;
    textFieldBorderWidth = textFieldBorderWidth;
    inputDecorationTheme = inputDecorationTheme;

    final theme = ThemeData(
      primaryColor: primaryColor,
      colorScheme: ColorScheme.fromSwatch().copyWith(
        primary: primaryColor,
        secondary: accentColor,
        error: accentError,
      ),
      outlinedButtonTheme: outlinedButtonThemeData,
      disabledColor: backgroundDisabled,
      useMaterial3: useMaterial3,
      // inputDecorationTheme: fInputDecorationTheme,
    );

    return GBThemeData.raw(
      accentColor: theme.colorScheme.secondary,
      accentError: accentError,
      accentErrorLight: accentErrorLight,
      backgroundBase: backgroundBase,
      backgroundContrast: backgroundContrast,
      backgroundDisabled: backgroundDisabled,
      backgroundFocus: backgroundFocus,
      textBase: textBase,
      textBlack: textBlack,
      textDisabled: textDisabled,
      textWhite: textWhite,
      textFieldRadius: textFieldRadius,
      textFieldBorderWidth: textFieldBorderWidth,
      buttonsRadius: buttonsRadius,
      containedButtonTheme: _containedButtonTheme(
        containedButtonThemeData,
        accentColor: accentColor,
        backgroundDisabled: backgroundDisabled,
        buttonsRadius: buttonsRadius,
      ),
      /* Default Theme */
      // GENERAL CONFIGURATION
      adaptationMap: theme.adaptationMap,
      applyElevationOverlayColor: theme.applyElevationOverlayColor,
      cupertinoOverrideTheme: theme.cupertinoOverrideTheme,
      extensions: theme.extensions,
      inputDecorationTheme: _getInputDecorationTheme(
        inputDecorationTheme,
        accentColor: accentColor,
        accentError: accentError,
        accentErrorLight: accentErrorLight,
        backgroundBase: backgroundBase,
        backgroundContrast: backgroundContrast,
        backgroundDisabled: backgroundDisabled,
        backgroundFocus: backgroundFocus,
        primaryColor: primaryColor,
        textBase: textBase,
        textBlack: textBlack,
        textDisabled: textDisabled,
        textWhite: textWhite,
        textFieldRadius: textFieldRadius,
      ),
      materialTapTargetSize: theme.materialTapTargetSize,
      pageTransitionsTheme: theme.pageTransitionsTheme,
      platform: theme.platform,
      scrollbarTheme: theme.scrollbarTheme,
      splashFactory: theme.splashFactory,
      useMaterial3: theme.useMaterial3,
      visualDensity: theme.visualDensity,
      // COLOR
      // [colorScheme] is the preferred way to configure colors. The other color
      // properties will gradually be phased out, see
      // https://github.com/flutter/flutter/issues/91772.
      canvasColor: theme.canvasColor,
      cardColor: theme.cardColor,
      colorScheme: theme.colorScheme,
      dialogBackgroundColor: theme.dialogBackgroundColor,
      disabledColor: theme.disabledColor,
      dividerColor: theme.dividerColor,
      focusColor: theme.focusColor,
      highlightColor: theme.highlightColor,
      hintColor: theme.hintColor,
      hoverColor: theme.hoverColor,
      indicatorColor: theme.indicatorColor,
      primaryColor: theme.primaryColor,
      primaryColorDark: theme.primaryColorDark,
      primaryColorLight: theme.primaryColorLight,
      scaffoldBackgroundColor: theme.scaffoldBackgroundColor,
      secondaryHeaderColor: theme.secondaryHeaderColor,
      shadowColor: theme.shadowColor,
      splashColor: theme.splashColor,
      unselectedWidgetColor: theme.unselectedWidgetColor,
      // TYPOGRAPHY & ICONOGRAPHY
      iconTheme: theme.iconTheme,
      primaryIconTheme: theme.primaryIconTheme,
      primaryTextTheme: theme.primaryTextTheme,
      textTheme: _getTextTheme(
        theme,
        textTheme,
      ),
      typography: theme.typography,
      // COMPONENT THEMES
      appBarTheme: theme.appBarTheme,
      badgeTheme: theme.badgeTheme,
      bannerTheme: theme.bannerTheme,
      bottomAppBarTheme: theme.bottomAppBarTheme,
      bottomNavigationBarTheme: theme.bottomNavigationBarTheme,
      bottomSheetTheme: theme.bottomSheetTheme,
      buttonBarTheme: theme.buttonBarTheme,
      buttonTheme: theme.buttonTheme,
      cardTheme: theme.cardTheme,
      checkboxTheme: theme.checkboxTheme,
      chipTheme: theme.chipTheme,
      dataTableTheme: theme.dataTableTheme,
      dialogTheme: theme.dialogTheme,
      dividerTheme: theme.dividerTheme,
      drawerTheme: theme.drawerTheme,
      dropdownMenuTheme: theme.dropdownMenuTheme,
      elevatedButtonTheme: theme.elevatedButtonTheme,
      expansionTileTheme: theme.expansionTileTheme,
      filledButtonTheme: theme.filledButtonTheme,
      floatingActionButtonTheme: theme.floatingActionButtonTheme,
      iconButtonTheme: theme.iconButtonTheme,
      listTileTheme: theme.listTileTheme,
      menuBarTheme: theme.menuBarTheme,
      menuButtonTheme: theme.menuButtonTheme,
      menuTheme: theme.menuTheme,
      navigationBarTheme: theme.navigationBarTheme,
      navigationDrawerTheme: theme.navigationDrawerTheme,
      navigationRailTheme: theme.navigationRailTheme,
      outlinedButtonTheme: _outlinedButton(
        theme.outlinedButtonTheme,
        borderRadius: buttonsRadius,
        secondary: theme.colorScheme.secondary,
      ),
      popupMenuTheme: theme.popupMenuTheme,
      progressIndicatorTheme: theme.progressIndicatorTheme,
      radioTheme: theme.radioTheme,
      segmentedButtonTheme: theme.segmentedButtonTheme,
      sliderTheme: theme.sliderTheme,
      snackBarTheme: theme.snackBarTheme,
      switchTheme: theme.switchTheme,
      tabBarTheme: theme.tabBarTheme,
      textButtonTheme: theme.textButtonTheme,
      textSelectionTheme: theme.textSelectionTheme,
      timePickerTheme: theme.timePickerTheme,
      toggleButtonsTheme: theme.toggleButtonsTheme,
      tooltipTheme: theme.tooltipTheme,
      actionIconTheme: theme.actionIconTheme,
      datePickerTheme: theme.datePickerTheme,
      searchBarTheme: theme.searchBarTheme,
      searchViewTheme: theme.searchViewTheme,
    );
  }
  // : ThemeData.dark();
  // : ThemeData.dark();

  GBThemeData.raw({
    // this.primaryColor,
    required this.accentColor,
    required this.accentError,
    required this.accentErrorLight,
    required this.backgroundBase,
    required this.backgroundContrast,
    required this.backgroundDisabled,
    required this.backgroundFocus,
    required this.textBase,
    required this.textBlack,
    required this.textDisabled,
    required this.textWhite,
    required this.textFieldRadius,
    required this.textFieldBorderWidth,
    required this.buttonsRadius,
    required this.containedButtonTheme,
    // Default Theme Data
    // GENERAL CONFIGURATION
    required super.adaptationMap,
    required super.applyElevationOverlayColor,
    required super.cupertinoOverrideTheme,
    required super.extensions,
    required this.inputDecorationTheme,
    required super.materialTapTargetSize,
    required super.pageTransitionsTheme,
    required super.platform,
    required super.scrollbarTheme,
    required super.splashFactory,
    required super.useMaterial3,
    required super.visualDensity,
    // COLOR
    // [colorScheme] is the preferred way to configure colors. The other color
    // properties will gradually be phased out, see
    // https://github.com/flutter/flutter/issues/91772.
    required super.canvasColor,
    required super.cardColor,
    required super.colorScheme,
    required super.dialogBackgroundColor,
    required super.disabledColor,
    required super.dividerColor,
    required super.focusColor,
    required super.highlightColor,
    required super.hintColor,
    required super.hoverColor,
    required super.indicatorColor,
    required super.primaryColor,
    required super.primaryColorDark,
    required super.primaryColorLight,
    required super.scaffoldBackgroundColor,
    required super.secondaryHeaderColor,
    required super.shadowColor,
    required super.splashColor,
    required super.unselectedWidgetColor,
    // TYPOGRAPHY & ICONOGRAPHY
    required super.iconTheme,
    required super.primaryIconTheme,
    required super.primaryTextTheme,
    required super.textTheme,
    required super.typography,
    // COMPONENT THEMES
    required super.actionIconTheme,
    required super.appBarTheme,
    required super.badgeTheme,
    required super.bannerTheme,
    required super.bottomAppBarTheme,
    required super.bottomNavigationBarTheme,
    required super.bottomSheetTheme,
    required super.buttonBarTheme,
    required super.buttonTheme,
    required super.cardTheme,
    required super.checkboxTheme,
    required super.chipTheme,
    required super.dataTableTheme,
    required super.datePickerTheme,
    required super.dialogTheme,
    required super.dividerTheme,
    required super.drawerTheme,
    required super.dropdownMenuTheme,
    required super.elevatedButtonTheme,
    required super.expansionTileTheme,
    required super.filledButtonTheme,
    required super.floatingActionButtonTheme,
    required super.iconButtonTheme,
    required super.listTileTheme,
    required super.menuBarTheme,
    required super.menuButtonTheme,
    required super.menuTheme,
    required super.navigationBarTheme,
    required super.navigationDrawerTheme,
    required super.navigationRailTheme,
    required super.outlinedButtonTheme,
    required super.popupMenuTheme,
    required super.progressIndicatorTheme,
    required super.radioTheme,
    required super.searchBarTheme,
    required super.searchViewTheme,
    required super.segmentedButtonTheme,
    required super.sliderTheme,
    required super.snackBarTheme,
    required super.switchTheme,
    required super.tabBarTheme,
    required super.textButtonTheme,
    required super.textSelectionTheme,
    required super.timePickerTheme,
    required super.toggleButtonsTheme,
    required super.tooltipTheme,
    // DEPRECATED (newest deprecations at the bottom)
    @Deprecated(
      'No longer used by the framework, please remove any reference to it. '
      'For more information, consult the migration guide at '
      'https://flutter.dev/docs/release/breaking-changes/toggleable-active-color#migration-guide. '
      'This feature was deprecated after v3.4.0-19.0.pre.',
    )
    Color? toggleableActiveColor,
    @Deprecated(
      'Use colorScheme.error instead. '
      'This feature was deprecated after v3.3.0-0.5.pre.',
    )
    Color? errorColor,
    @Deprecated(
      'Use colorScheme.background instead. '
      'This feature was deprecated after v3.3.0-0.5.pre.',
    )
    Color? backgroundColor,
    @Deprecated(
      'Use BottomAppBarTheme.color instead. '
      'This feature was deprecated after v3.3.0-0.6.pre.',
    )
    Color? bottomAppBarColor,
  }) : super.raw(
          inputDecorationTheme: inputDecorationTheme,
        );

  @override

  /// Creates a copy of this theme but with the given fields replaced with the new values.
  ///
  /// The [brightness] value is applied to the [colorScheme].
  GBThemeData copyWith({
    Color? accentColor,
    Color? accentError,
    Color? accentErrorLight,
    Color? backgroundBase,
    Color? backgroundContrast,
    Color? backgroundDisabled,
    Color? backgroundFocus,
    Color? textBase,
    Color? textBlack,
    Color? textDisabled,
    Color? textWhite,
    BorderRadius? textFieldRadius,
    double? textFieldBorderWidth,
    BorderRadius? buttonsRadius,
    ContainedButtonThemeData? containedButtonThemeData,
    /* Default Theme */
    // GENERAL CONFIGURATION
    Iterable<Adaptation<Object>>? adaptations,
    bool? applyElevationOverlayColor,
    NoDefaultCupertinoThemeData? cupertinoOverrideTheme,
    Iterable<ThemeExtension<dynamic>>? extensions,
    covariant GBInputDecorationTheme? inputDecorationTheme,
    MaterialTapTargetSize? materialTapTargetSize,
    PageTransitionsTheme? pageTransitionsTheme,
    TargetPlatform? platform,
    ScrollbarThemeData? scrollbarTheme,
    InteractiveInkFeatureFactory? splashFactory,
    VisualDensity? visualDensity,
    // COLOR
    ColorScheme? colorScheme,
    Brightness? brightness,
    // [colorScheme] is the preferred way to configure colors. The [Color] properties
    // listed below (as well as primarySwatch) will gradually be phased out, see
    // https://github.com/flutter/flutter/issues/91772.
    Color? canvasColor,
    Color? cardColor,
    Color? disabledColor,
    Color? dividerColor,
    Color? focusColor,
    Color? highlightColor,
    Color? hintColor,
    Color? hoverColor,
    Color? indicatorColor,
    Color? primaryColor,
    Color? primaryColorDark,
    Color? primaryColorLight,
    Color? scaffoldBackgroundColor,
    Color? secondaryHeaderColor,
    Color? shadowColor,
    Color? splashColor,
    Color? unselectedWidgetColor,
    // TYPOGRAPHY & ICONOGRAPHY
    IconThemeData? iconTheme,
    IconThemeData? primaryIconTheme,
    TextTheme? primaryTextTheme,
    TextTheme? textTheme,
    Typography? typography,
    // COMPONENT THEMES
    ActionIconThemeData? actionIconTheme,
    AppBarTheme? appBarTheme,
    BadgeThemeData? badgeTheme,
    MaterialBannerThemeData? bannerTheme,
    BottomAppBarTheme? bottomAppBarTheme,
    BottomNavigationBarThemeData? bottomNavigationBarTheme,
    BottomSheetThemeData? bottomSheetTheme,
    ButtonThemeData? buttonTheme,
    Object? cardTheme,
    CheckboxThemeData? checkboxTheme,
    ChipThemeData? chipTheme,
    DataTableThemeData? dataTableTheme,
    DatePickerThemeData? datePickerTheme,
    // TODO(QuncCccccc): Change the parameter type to DialogThemeData
    Object? dialogTheme,
    DividerThemeData? dividerTheme,
    DrawerThemeData? drawerTheme,
    DropdownMenuThemeData? dropdownMenuTheme,
    ElevatedButtonThemeData? elevatedButtonTheme,
    ExpansionTileThemeData? expansionTileTheme,
    FilledButtonThemeData? filledButtonTheme,
    FloatingActionButtonThemeData? floatingActionButtonTheme,
    IconButtonThemeData? iconButtonTheme,
    ListTileThemeData? listTileTheme,
    MenuBarThemeData? menuBarTheme,
    MenuButtonThemeData? menuButtonTheme,
    MenuThemeData? menuTheme,
    NavigationBarThemeData? navigationBarTheme,
    NavigationDrawerThemeData? navigationDrawerTheme,
    NavigationRailThemeData? navigationRailTheme,
    OutlinedButtonThemeData? outlinedButtonTheme,
    PopupMenuThemeData? popupMenuTheme,
    ProgressIndicatorThemeData? progressIndicatorTheme,
    RadioThemeData? radioTheme,
    SearchBarThemeData? searchBarTheme,
    SearchViewThemeData? searchViewTheme,
    SegmentedButtonThemeData? segmentedButtonTheme,
    SliderThemeData? sliderTheme,
    SnackBarThemeData? snackBarTheme,
    SwitchThemeData? switchTheme,
    // TODO(QuncCccccc): Change the parameter type to TabBarThemeData
    Object? tabBarTheme,
    TextButtonThemeData? textButtonTheme,
    TextSelectionThemeData? textSelectionTheme,
    TimePickerThemeData? timePickerTheme,
    ToggleButtonsThemeData? toggleButtonsTheme,
    TooltipThemeData? tooltipTheme,
    // DEPRECATED (newest deprecations at the bottom)
    @Deprecated(
      'Use a ThemeData constructor (.from, .light, or .dark) instead. '
      'These constructors all have a useMaterial3 argument, '
      'and they set appropriate default values based on its value. '
      'See the useMaterial3 API documentation for full details. '
      'This feature was deprecated after v3.13.0-0.2.pre.',
    )
    bool? useMaterial3,
    @Deprecated(
      'Use OverflowBar instead. '
      'This feature was deprecated after v3.21.0-10.0.pre.',
    )
    ButtonBarThemeData? buttonBarTheme,
    @Deprecated(
      'Use DialogThemeData.backgroundColor instead. '
      'This feature was deprecated after v3.27.0-0.1.pre.',
    )
    Color? dialogBackgroundColor,
  }) {
    cupertinoOverrideTheme = cupertinoOverrideTheme?.noDefault() as CupertinoThemeData?;
    return GBThemeData.raw(
      accentColor: accentColor ?? this.accentColor,
      accentError: accentError ?? this.accentError,
      accentErrorLight: accentErrorLight ?? this.accentErrorLight,
      backgroundBase: backgroundBase ?? this.backgroundBase,
      backgroundContrast: backgroundContrast ?? this.backgroundContrast,
      backgroundDisabled: backgroundDisabled ?? this.backgroundDisabled,
      backgroundFocus: backgroundFocus ?? this.backgroundFocus,
      textBase: textBase ?? this.textBase,
      textBlack: textBlack ?? this.textBlack,
      textDisabled: textDisabled ?? this.textDisabled,
      textWhite: textWhite ?? this.textWhite,
      textFieldRadius: textFieldRadius ?? this.textFieldRadius,
      buttonsRadius: buttonsRadius ?? this.buttonsRadius,
      textFieldBorderWidth: textFieldBorderWidth ?? this.textFieldBorderWidth,
      // containedButtonTheme: containedButtonThemeData ?? this.containedButtonTheme,
      containedButtonTheme: _containedButtonTheme(
        (containedButtonThemeData ?? this.containedButtonTheme),
        accentColor: accentColor ?? this.colorScheme.secondary,
        backgroundDisabled: backgroundDisabled ?? this.backgroundDisabled,
        buttonsRadius: buttonsRadius,
      ),

      /* DefaultTheme */
      // GENERAL CONFIGURATION
      adaptationMap: adaptations != null ? _createAdaptationMap(adaptations) : adaptationMap,
      applyElevationOverlayColor: applyElevationOverlayColor ?? this.applyElevationOverlayColor,
      cupertinoOverrideTheme: cupertinoOverrideTheme ?? this.cupertinoOverrideTheme,
      extensions: (extensions != null) ? _themeExtensionIterableToMap(extensions) : this.extensions,
      inputDecorationTheme: _getInputDecorationTheme(
        inputDecorationTheme ?? this.inputDecorationTheme,
        accentColor: accentColor ?? this.colorScheme.secondary,
        accentError: accentError ?? this.accentError,
        accentErrorLight: accentErrorLight ?? this.accentErrorLight,
        backgroundBase: backgroundBase ?? this.backgroundBase,
        backgroundContrast: backgroundContrast ?? this.backgroundContrast,
        backgroundDisabled: backgroundDisabled ?? this.disabledColor,
        backgroundFocus: backgroundFocus ?? this.backgroundFocus,
        primaryColor: primaryColor ?? this.primaryColor,
        textBase: textBase ?? this.textBase,
        textBlack: textBlack ?? this.textBlack,
        textDisabled: textDisabled ?? this.textDisabled,
        textWhite: textWhite ?? this.textWhite,
        textFieldRadius: textFieldRadius ?? this.textFieldRadius,
        textFieldBorderWidth: textFieldBorderWidth ?? this.textFieldBorderWidth,
      ),
      materialTapTargetSize: materialTapTargetSize ?? this.materialTapTargetSize,
      pageTransitionsTheme: pageTransitionsTheme ?? this.pageTransitionsTheme,
      platform: platform ?? this.platform,
      scrollbarTheme: scrollbarTheme ?? this.scrollbarTheme,
      splashFactory: splashFactory ?? this.splashFactory,
      // When deprecated useMaterial3 removed, maintain `this.useMaterial3` here
      // for == evaluation.
      useMaterial3: useMaterial3 ?? this.useMaterial3,
      visualDensity: visualDensity ?? this.visualDensity,
      // COLOR
      canvasColor: canvasColor ?? this.canvasColor,
      cardColor: cardColor ?? this.cardColor,
      colorScheme: (colorScheme ?? this.colorScheme).copyWith(brightness: brightness),
      disabledColor: disabledColor ?? this.disabledColor,
      dividerColor: dividerColor ?? this.dividerColor,
      focusColor: focusColor ?? this.focusColor,
      highlightColor: highlightColor ?? this.highlightColor,
      hintColor: hintColor ?? this.hintColor,
      hoverColor: hoverColor ?? this.hoverColor,
      indicatorColor: indicatorColor ?? this.indicatorColor,
      primaryColor: primaryColor ?? this.primaryColor,
      primaryColorDark: primaryColorDark ?? this.primaryColorDark,
      primaryColorLight: primaryColorLight ?? this.primaryColorLight,
      scaffoldBackgroundColor: scaffoldBackgroundColor ?? this.scaffoldBackgroundColor,
      secondaryHeaderColor: secondaryHeaderColor ?? this.secondaryHeaderColor,
      shadowColor: shadowColor ?? this.shadowColor,
      splashColor: splashColor ?? this.splashColor,
      unselectedWidgetColor: unselectedWidgetColor ?? this.unselectedWidgetColor,
      // TYPOGRAPHY & ICONOGRAPHY
      iconTheme: iconTheme ?? this.iconTheme,
      primaryIconTheme: primaryIconTheme ?? this.primaryIconTheme,
      primaryTextTheme: primaryTextTheme ?? this.primaryTextTheme,
      textTheme: textTheme ?? this.textTheme,
      typography: typography ?? this.typography,
      // COMPONENT THEMES
      actionIconTheme: actionIconTheme ?? this.actionIconTheme,
      appBarTheme: appBarTheme ?? this.appBarTheme,
      badgeTheme: badgeTheme ?? this.badgeTheme,
      bannerTheme: bannerTheme ?? this.bannerTheme,
      bottomAppBarTheme: bottomAppBarTheme ?? this.bottomAppBarTheme,
      bottomNavigationBarTheme: bottomNavigationBarTheme ?? this.bottomNavigationBarTheme,
      bottomSheetTheme: bottomSheetTheme ?? this.bottomSheetTheme,
      buttonTheme: buttonTheme ?? this.buttonTheme,
      cardTheme: cardTheme as CardThemeData? ?? this.cardTheme,
      checkboxTheme: checkboxTheme ?? this.checkboxTheme,
      chipTheme: chipTheme ?? this.chipTheme,
      dataTableTheme: dataTableTheme ?? this.dataTableTheme,
      datePickerTheme: datePickerTheme ?? this.datePickerTheme,
      dialogTheme: dialogTheme as DialogThemeData? ?? this.dialogTheme,
      dividerTheme: dividerTheme ?? this.dividerTheme,
      drawerTheme: drawerTheme ?? this.drawerTheme,
      dropdownMenuTheme: dropdownMenuTheme ?? this.dropdownMenuTheme,
      elevatedButtonTheme: elevatedButtonTheme ?? this.elevatedButtonTheme,
      expansionTileTheme: expansionTileTheme ?? this.expansionTileTheme,
      filledButtonTheme: filledButtonTheme ?? this.filledButtonTheme,
      floatingActionButtonTheme: floatingActionButtonTheme ?? this.floatingActionButtonTheme,
      iconButtonTheme: iconButtonTheme ?? this.iconButtonTheme,
      listTileTheme: listTileTheme ?? this.listTileTheme,
      menuBarTheme: menuBarTheme ?? this.menuBarTheme,
      menuButtonTheme: menuButtonTheme ?? this.menuButtonTheme,
      menuTheme: menuTheme ?? this.menuTheme,
      navigationBarTheme: navigationBarTheme ?? this.navigationBarTheme,
      navigationDrawerTheme: navigationDrawerTheme ?? this.navigationDrawerTheme,
      navigationRailTheme: navigationRailTheme ?? this.navigationRailTheme,
      outlinedButtonTheme: outlinedButtonTheme ?? this.outlinedButtonTheme,
      popupMenuTheme: popupMenuTheme ?? this.popupMenuTheme,
      progressIndicatorTheme: progressIndicatorTheme ?? this.progressIndicatorTheme,
      radioTheme: radioTheme ?? this.radioTheme,
      searchBarTheme: searchBarTheme ?? this.searchBarTheme,
      searchViewTheme: searchViewTheme ?? this.searchViewTheme,
      segmentedButtonTheme: segmentedButtonTheme ?? this.segmentedButtonTheme,
      sliderTheme: sliderTheme ?? this.sliderTheme,
      snackBarTheme: snackBarTheme ?? this.snackBarTheme,
      switchTheme: switchTheme ?? this.switchTheme,
      tabBarTheme: tabBarTheme as TabBarThemeData? ?? this.tabBarTheme,
      textButtonTheme: textButtonTheme ?? this.textButtonTheme,
      textSelectionTheme: textSelectionTheme ?? this.textSelectionTheme,
      timePickerTheme: timePickerTheme ?? this.timePickerTheme,
      toggleButtonsTheme: toggleButtonsTheme ?? this.toggleButtonsTheme,
      tooltipTheme: tooltipTheme ?? this.tooltipTheme,
      // DEPRECATED (newest deprecations at the bottom)
      buttonBarTheme: buttonBarTheme ?? this.buttonBarTheme,
      dialogBackgroundColor: dialogBackgroundColor ?? this.dialogBackgroundColor,
    );
  }

  static GBThemeData fallback() {
    return GBThemeData();
  }

  @override
  int get hashCode =>
      super.hashCode ^
      Object.hashAll([
        accentError,
        textDisabled,
        textWhite,
        backgroundBase,
        accentErrorLight,
        backgroundContrast,
        backgroundFocus,
        backgroundDisabled,
        textBase,
        textBlack,
        textFieldRadius,
        buttonsRadius,
        containedButtonTheme,
      ]);

  @override
  bool operator ==(Object other) {
    if (super != other) {
      return false;
    }
    return other is GBThemeData &&
        other.accentError == accentError &&
        other.textDisabled == textDisabled &&
        other.textWhite == textWhite &&
        other.backgroundBase == backgroundBase &&
        other.accentErrorLight == accentErrorLight &&
        other.backgroundContrast == backgroundContrast &&
        other.backgroundFocus == backgroundFocus &&
        other.backgroundDisabled == backgroundDisabled &&
        other.textBase == textBase &&
        other.textBlack == textBlack &&
        other.textFieldRadius == textFieldRadius &&
        other.buttonsRadius == buttonsRadius &&
        other.containedButtonTheme == containedButtonTheme;
  }

  static const OutlineInputBorder textFieldBaseOutlineBorder = OutlineInputBorder(
    borderRadius: BorderRadius.zero,
  );
  static const BorderSide textFieldBaseBorderSide = BorderSide(width: 1, style: BorderStyle.solid);
  static const BorderSide textFieldNoBorderSide = BorderSide(color: Colors.transparent, width: 0);

  static Map<Type, Adaptation<Object>> _createAdaptationMap(Iterable<Adaptation<Object>> adaptations) {
    final Map<Type, Adaptation<Object>> adaptationMap = <Type, Adaptation<Object>>{
      for (final Adaptation<Object> adaptation in adaptations) adaptation.type: adaptation
    };
    return adaptationMap;
  }
}

extension GBThemeExtension on Theme {
  static GBThemeData? dataOf(BuildContext context, {bool shadowThemeOnly = false}) {
    final ThemeData themeData = Theme.of(context);
    if (themeData.runtimeType != GBThemeData) {
      return null;
    }
    // TODO: Catch error when no of type
    return themeData as GBThemeData?;
    // final _InheritedTheme inheritedTheme = context.dependOnInheritedWidgetOfExactType<_InheritedTheme>();
    // if (shadowThemeOnly) {
    //   if (inheritedTheme == null || inheritedTheme.theme.isMaterialAppTheme)
    //     return null;
    //   return inheritedTheme.theme.data;
    // }

    // final MaterialLocalizations localizations = MaterialLocalizations.of(context);
    // final ScriptCategory category = localizations?.scriptCategory ?? ScriptCategory.englishLike;
    // final ThemeData theme = inheritedTheme?.theme?.data ?? _kFallbackTheme;
    // return ThemeData.localize(theme, theme.typography.geometryThemeFor(category));
  }
}

/// Gets default text theme style
/// if custom configured it will merge them
TextTheme _getTextTheme(ThemeData theme, TextTheme? textTheme) {
  return (textTheme ?? TextTheme())
      .merge(
        theme.textTheme.copyWith(
          displayLarge: theme.textTheme.displayLarge!.copyWith(
            fontSize: 34,
          ),
          displayMedium: theme.textTheme.displayMedium!.copyWith(
            fontSize: 24,
            fontWeight: FontWeight.w400,
          ),
          displaySmall: theme.textTheme.displaySmall!.copyWith(
            fontSize: 20,
            fontWeight: FontWeight.w400,
          ),
          headlineMedium: theme.textTheme.headlineMedium!.copyWith(
            fontSize: 19,
            fontWeight: FontWeight.w400,
          ),
          headlineSmall: theme.textTheme.headlineSmall!.copyWith(
            fontSize: 18,
            fontWeight: FontWeight.w400,
          ),
          titleLarge: theme.textTheme.titleLarge!.copyWith(
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
          titleMedium: theme.textTheme.titleMedium!.copyWith(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
          titleSmall: theme.textTheme.titleSmall!.copyWith(
            fontSize: 14,
            fontWeight: FontWeight.bold,
          ),
          bodyLarge: theme.textTheme.bodyLarge!.copyWith(
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
          bodyMedium: theme.textTheme.bodyMedium!.copyWith(
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),
          labelLarge: theme.textTheme.labelLarge!.copyWith(
            fontSize: 14,
            fontWeight: FontWeight.bold,
          ),
          bodySmall: theme.textTheme.bodySmall!.copyWith(
            fontSize: 12,
            fontWeight: FontWeight.w400,
          ),
          labelSmall: theme.textTheme.labelSmall!.copyWith(
            fontSize: 10,
            fontWeight: FontWeight.w400,
          ),
        ),
      )
      .merge(textTheme);
}

/// Gets default text theme style
/// if custom configured it will merge them
GBInputDecorationTheme _getInputDecorationTheme(
  GBInputDecorationTheme? customTheme, {
  required Color accentColor,
  required Color accentError,
  required Color accentErrorLight,
  required Color backgroundBase,
  required Color backgroundContrast,
  required Color backgroundDisabled,
  required Color backgroundFocus,
  required Color primaryColor,
  required Color textBase,
  required Color textBlack,
  required Color textDisabled,
  required Color textWhite,
  final BorderRadius? textFieldRadius,
  final double? textFieldBorderWidth,
}) {
  // final Color _errorColor = accentError;
  // final Color _errorLightColor = accentErrorLight;
  // final Color _accentColor = accentColor;
  // final Color _backgroundDisabled = backgroundDisabled;
  // final Color _backgroundFocus = backgroundFocus;
  // final Color _backgroundContrast = backgroundContrast;
  // final Color _borderColor = accentColor;
  // final Color _textDisabledColor = textDisabled;
  // final Color _textBaseColor = textBase;
  // final Color _textBlackColor = textBlack;
  // final BorderRadius? _borderRadius = textFieldRadius;
  if (customTheme != null && textFieldRadius != null) {
    debugPrint("WARNING - GB Theme: textField Radius will be overridden if provided in custom input theme");
  }

  final _defaultTheme = GBInputDecorationTheme(
    filled: true,
    alignLabelWithHint: false,
    isDense: true,
    border: GBThemeData.textFieldBaseOutlineBorder.copyWith(
      borderSide: GBThemeData.textFieldNoBorderSide.copyWith(
        width: textFieldBorderWidth,
      ),
      borderRadius: textFieldRadius,
    ),
    enabledBorder: GBThemeData.textFieldBaseOutlineBorder.copyWith(
      borderSide: GBThemeData.textFieldNoBorderSide.copyWith(
        width: textFieldBorderWidth,
      ),
      borderRadius: textFieldRadius,
    ),
    focusedBorder: GBThemeData.textFieldBaseOutlineBorder.copyWith(
      borderSide: GBThemeData.textFieldBaseBorderSide.copyWith(
        color: accentColor,
        width: textFieldBorderWidth,
      ),
      borderRadius: textFieldRadius,
    ),
    disabledBorder: GBThemeData.textFieldBaseOutlineBorder.copyWith(
      borderSide: GBThemeData.textFieldNoBorderSide.copyWith(
        width: textFieldBorderWidth,
      ),
      borderRadius: textFieldRadius,
    ),
    contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
    errorBorder: GBThemeData.textFieldBaseOutlineBorder.copyWith(
      borderSide: GBThemeData.textFieldNoBorderSide.copyWith(
        color: accentError,
        width: textFieldBorderWidth,
      ),
      borderRadius: textFieldRadius,
    ),
    focusedErrorBorder: GBThemeData.textFieldBaseOutlineBorder.copyWith(
      borderSide: GBThemeData.textFieldNoBorderSide.copyWith(
        color: accentError,
        width: textFieldBorderWidth,
      ),
      borderRadius: textFieldRadius,
    ),
    fillColor: backgroundContrast,
    errorFillColor: accentErrorLight,
    disabledFillColor: backgroundDisabled,
    focusColor: backgroundFocus,
  );
  return _defaultTheme.copyWith(
    filled: customTheme?.filled,
    labelStyle: customTheme?.labelStyle,
    hintStyle: customTheme?.hintStyle,
    alignLabelWithHint: customTheme?.alignLabelWithHint,
    isDense: customTheme?.isDense,
    border: customTheme?.border,
    enabledBorder: customTheme?.enabledBorder,
    focusedBorder: customTheme?.focusedBorder,
    disabledBorder: customTheme?.disabledBorder,
    contentPadding: customTheme?.contentPadding,
    errorBorder: customTheme?.errorBorder,
    fillColor: customTheme?.fillColor,
    focusColor: customTheme?.focusColor,
    errorFilledColor: customTheme?.errorFillColor,
    disabledFillColor: customTheme?.disabledFillColor,
    hoverColor: customTheme?.hoverColor,
    activeIndicatorBorder: customTheme?.activeIndicatorBorder,
    constraints: customTheme?.constraints,
    counterStyle: customTheme?.counterStyle,
    errorMaxLines: customTheme?.errorMaxLines,
    errorStyle: customTheme?.errorStyle,
    floatingLabelAlignment: customTheme?.floatingLabelAlignment,
    floatingLabelBehavior: customTheme?.floatingLabelBehavior,
    floatingLabelStyle: customTheme?.floatingLabelStyle,
    focusedErrorBorder: customTheme?.focusedErrorBorder,
    helperMaxLines: customTheme?.helperMaxLines,
    helperStyle: customTheme?.helperStyle,
    hintFadeDuration: customTheme?.hintFadeDuration,
    iconColor: customTheme?.iconColor,
    isCollapsed: customTheme?.isCollapsed,
    outlineBorder: customTheme?.outlineBorder,
    prefixIconColor: customTheme?.prefixIconColor,
    prefixStyle: customTheme?.prefixStyle,
    suffixIconColor: customTheme?.suffixIconColor,
    suffixStyle: customTheme?.suffixStyle,
  );
}

/// * [TextButton], [TextButtonTheme], [TextButtonThemeData],
///  * [ElevatedButton], [ElevatedButtonTheme], [ElevatedButtonThemeData],
///  * [OutlinedButton], [OutlinedButtonTheme], [OutlinedButtonThemeData]
///

ContainedButtonThemeData _containedButtonTheme(
  ContainedButtonThemeData? customTheme, {
  required Color accentColor,
  required Color backgroundDisabled,
  required BorderRadius? buttonsRadius,
}) {
  ContainedButtonThemeData _defaultButtonTheme = ContainedButtonThemeData.getDefaultTheme(
    accentColor: accentColor,
    backgroundDisabled: backgroundDisabled,
    buttonsRadius: buttonsRadius,
  );
  return ContainedButtonThemeData(
    style: (customTheme?.style ?? _defaultButtonTheme.style).merge(customTheme?.style),
    colors: customTheme?.colors,
    uppercase: customTheme?.uppercase ?? true,
  );
}

/// Convert the [extensionsIterable] passed to [ThemeData.new] or [copyWith]
/// to the stored [extensions] map, where each entry's key consists of the extension's type.
Map<Object, ThemeExtension<dynamic>> _themeExtensionIterableToMap(
    Iterable<ThemeExtension<dynamic>> extensionsIterable) {
  return Map<Object, ThemeExtension<dynamic>>.unmodifiable(<Object, ThemeExtension<dynamic>>{
    // Strangely, the cast is necessary for tests to run.
    for (final ThemeExtension<dynamic> extension in extensionsIterable)
      extension.type: extension as ThemeExtension<ThemeExtension<dynamic>>
  });
}

/**
 * Get default Outlined Theme
 */
OutlinedButtonThemeData _outlinedButton(
  OutlinedButtonThemeData outlinedButtonTheme, {
  required Color secondary,
  required BorderRadiusGeometry? borderRadius,
}) {
  final defaultStyle = ButtonStyle(
    minimumSize: WidgetStateProperty.all(
      Size(80, 48),
    ),
    side: WidgetStateProperty.all(
      BorderSide(
        color: secondary,
        style: BorderStyle.solid,
        width: 1.0,
      ),
    ),
    padding: WidgetStateProperty.all(
      const EdgeInsets.all(14),
    ),
    shape: WidgetStateProperty.all(
      RoundedRectangleBorder(
        borderRadius: borderRadius ?? BorderRadius.zero,
      ),
    ),
  ).merge(outlinedButtonTheme.style);
  return OutlinedButtonThemeData(
    style: defaultStyle,
  );
}
