import 'package:flutter/material.dart';

import '../../utils/utils.dart';

class AddressStateSelectField extends StatefulWidget {
  final Function(_AddressState? state)? onStateChange;
  final String? initialValue;
  final String? label;
  final bool enableSearch;

  /// Define a custom style to be used in the label of the field
  final TextStyle? labelStyle;

  /// Define max width constraints for this field
  /// by default is [343]
  final double maxWidth;

  /// Provide a custom text style to be used on the field title
  final TextStyle? titleStyle;

  /// Provide a custom text style to be used on the option list tiles
  final TextStyle? optionTileStyle;

  const AddressStateSelectField({
    Key? key,
    required this.onStateChange,
    this.initialValue,
    this.label,
    this.enableSearch = false,
    this.labelStyle,
    this.maxWidth = 343,
    this.optionTileStyle,
    this.titleStyle,
  }) : super(key: key);

  @override
  _AddressStateSelectFieldState createState() => _AddressStateSelectFieldState();
}

class _AddressStateSelectFieldState extends State<AddressStateSelectField> {
  String? _filteredText;

  List<_AddressState> get states => theStates.map((e) => _AddressState(value: e["value"], text: e["text"])).toList();

  List<_AddressState> get filteredStates => states
      .where((value) =>
          _filteredText == "" ||
          _filteredText == null ||
          value.text.toLowerCase().contains(_filteredText!.toLowerCase()))
      .toList();

  TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);

    return SelectField<_AddressState>(
      label: widget.label ?? localizations.state,
      labelStyle: widget.labelStyle,
      initialValue: widget.initialValue != null ? _ensureValue(widget.initialValue) : null,
      onSelected: (value) {
        widget.onStateChange?.call(value);
      },
      enableSearch: widget.enableSearch,
      maxWidth: widget.maxWidth,
      titleStyle: widget.titleStyle,
      optionTileStyle: widget.optionTileStyle,
      options: theStates.map((e) {
        return SelectFieldOption<_AddressState>(
          title: e["text"],
          value: _AddressState(
            text: e["text"],
            value: e["value"],
          ),
        );
      }).toList(),
    );
  }

  // Widget _buildFloating(BuildContext context) {
  //   return Stack(
  //     children: [
  //       Container(
  //         color: Colors.white,
  //         child: DropdownButtonFormField<String>(
  //           isExpanded: true,
  //           value: value,
  //           hint: GBText(
  //             widget.label ?? "Select an option",
  //             style: TextStyle(
  //               fontSize: 12,
  //             ),
  //           ),
  //           onChanged: _onFloatingOptionSelected,
  //           items: theStates
  //               .map<DropdownMenuItem<String>>(
  //                 (state) => DropdownMenuItem<String>(
  //                   value: state["text"],
  //                   child: GBText(
  //                     state["text"],
  //                     style: TextStyle(
  //                       fontSize: 16,
  //                       overflow: TextOverflow.ellipsis,
  //                     ),
  //                   ),
  //                 ),
  //               )
  //               .toList(),
  //           decoration: InputDecoration(
  //             contentPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
  //             fillColor: Colors.white,
  //             isCollapsed: true,
  //             enabledBorder: OutlineInputBorder(
  //               borderSide: BorderSide(
  //                 color: AppColors.grayE1,
  //                 width: 1.8,
  //               ),
  //             ),
  //             border: OutlineInputBorder(
  //               borderSide: BorderSide(color: AppColors.grayE1),
  //               borderRadius: BorderRadius.circular(5),
  //             ),
  //           ),
  //         ),
  //       ),
  //       Align(
  //         heightFactor: 1.5,
  //         alignment: AlignmentDirectional(-.9, -3),
  //         child: Container(
  //           color: Colors.white,
  //           padding: EdgeInsets.only(left: 2, right: 5),
  //           child: AppText(
  //             widget.label ?? "State",
  //             textStyle: AppTextStyle.light,
  //             style: TextStyle(fontSize: 12),
  //           ),
  //         ),
  //       ),
  //     ],
  //   );
  // }

  // CardSelect<_State> _buildCard(TreadsLocalizations localizations) {
  //   return CardSelect<_State>(
  //     options: filteredStates,
  //     onOptionSelected: _onCardOptionSelected,
  //     customListHeading: Container(
  //       padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
  //       child: TextField(
  //         controller: _controller,
  //         decoration: InputDecoration(
  //           hintText: localizations.typeToSearch,
  //         ),
  //         onChanged: (filterText) {
  //           setState(() {
  //             _filteredText = filterText;
  //           });
  //         },
  //       ),
  //     ),
  //     nameMapper: (i) => i.text,
  //     text: localizations.selectState,
  //   );
  // }

  _AddressState? _ensureValue(String? value) {
    // First check if provided value match as value
    var item;
    item = theStates.firstWhere((element) => element["text"] == value, orElse: () => null);
    if (item != null) {
      return _AddressState(value: item["value"], text: item["text"]);
    }
    // checks by text
    item = theStates.firstWhere((element) => element["value"] == value, orElse: () => null);
    if (item != null) {
      return _AddressState(value: item["value"], text: item["text"]);
    }
    return null;
  }

  // void _onCardOptionSelected(item) {
  //   widget.onStateChange?.call(item.value);
  //   setState(() {
  //     _filteredText = "";
  //     _controller.text = "";
  //   });
  // }

  // void _onFloatingOptionSelected(String? newValue) {
  //   widget.onStateChange?.call(newValue);
  //   setState(() {
  //     value = newValue;
  //   });
  // }
}

class _AddressState {
  final String value;
  final String text;

  _AddressState({
    required this.value,
    required this.text,
  });

  @override
  String toString() => '_State(value: $value, text: $text)';
}

// Values extracted from here: https://gist.github.com/iamjason/8f8f4bc00c13de86bcad#gistcomment-2247339

List theStates = [
  {"value": "AK", "text": "Alaska"},
  {"value": "AL", "text": "Alabama"},
  {"value": "AR", "text": "Arkansas"},
  // {"value": "AS", "text": "American Samoa"},
  {"value": "AZ", "text": "Arizona"},
  {"value": "CA", "text": "California"},
  {"value": "CO", "text": "Colorado"},
  {"value": "CT", "text": "Connecticut"},
  {"value": "DC", "text": "District of Columbia"},
  {"value": "DE", "text": "Delaware"},
  {"value": "FL", "text": "Florida"},
  {"value": "GA", "text": "Georgia"},
  // {"value": "GU", "text": "Guam"},
  {"value": "HI", "text": "Hawaii"},
  {"value": "IA", "text": "Iowa"},
  {"value": "ID", "text": "Idaho"},
  {"value": "IL", "text": "Illinois"},
  {"value": "IN", "text": "Indiana"},
  {"value": "KS", "text": "Kansas"},
  {"value": "KY", "text": "Kentucky"},
  {"value": "LA", "text": "Louisiana"},
  {"value": "MA", "text": "Massachusetts"},
  {"value": "MD", "text": "Maryland"},
  {"value": "ME", "text": "Maine"},
  {"value": "MI", "text": "Michigan"},
  {"value": "MN", "text": "Minnesota"},
  {"value": "MO", "text": "Missouri"},
  {"value": "MS", "text": "Mississippi"},
  {"value": "MT", "text": "Montana"},
  {"value": "NC", "text": "North Carolina"},
  {"value": "ND", "text": "North Dakota"},
  {"value": "NE", "text": "Nebraska"},
  {"value": "NH", "text": "New Hampshire"},
  {"value": "NJ", "text": "New Jersey"},
  {"value": "NM", "text": "New Mexico"},
  {"value": "NV", "text": "Nevada"},
  {"value": "NY", "text": "New York"},
  {"value": "OH", "text": "Ohio"},
  {"value": "OK", "text": "Oklahoma"},
  {"value": "OR", "text": "Oregon"},
  {"value": "PA", "text": "Pennsylvania"},
  {"value": "PR", "text": "Puerto Rico"},
  {"value": "RI", "text": "Rhode Island"},
  {"value": "SC", "text": "South Carolina"},
  {"value": "SD", "text": "South Dakota"},
  {"value": "TN", "text": "Tennessee"},
  {"value": "TX", "text": "Texas"},
  {"value": "UT", "text": "Utah"},
  {"value": "VA", "text": "Virginia"},
  // {"value": "VI", "text": "Virgin Islands"},
  {"value": "VT", "text": "Vermont"},
  {"value": "WA", "text": "Washington"},
  {"value": "WI", "text": "Wisconsin"},
  {"value": "WV", "text": "West Virginia"},
  {"value": "WY", "text": "Wyoming"}
];
