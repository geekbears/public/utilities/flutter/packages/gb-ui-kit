import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter/widgets.dart';

import '../../presentation.dart';

class PasswordField extends material.StatefulWidget {
  final String? label;

  /// Define a custom style to be used in the label of the field
  final material.TextStyle? labelStyle;
  final Iterable<String>? autofillHints;
  final String? hintText;
  final FocusNode? focusNode;
  final String? errorText;
  final bool enabled;
  final bool? filled;
  final TextEditingController? controller;
  final List<InputFeedbackText>? feedback;
  final Color? iconColor;
  final void Function(String value)? onChanged;

  /// Defines a custom builder widget that will wrap the TexField inner box
  /// This is useful to for example wrap your widget inside a Material elevation, etc
  final WrapWidgetBuilder? wrapInnerBox;

  ///
  /// Provides a way to override default input decoration styles
  /// by default if you only provide this value it will merge your style definition with default
  /// if you want to relay completely on your styling set [overrideDecoration] to `true`
  /// This function provides you access to default obscure icon
  final material.InputDecoration Function(BuildContext context, Widget icon)? decoration;

  /// Check [decoration] property for more info
  /// defaults to [false]
  final bool overrideDecoration;

  /// Provides a way to force error style (normally red colored)
  final bool forceErrorStyle;

  const PasswordField({
    Key? key,
    this.label,
    this.labelStyle,
    this.autofillHints = const [],
    this.hintText,
    this.focusNode,
    this.errorText,
    this.enabled = true,
    this.filled,
    this.controller,
    this.feedback,
    this.iconColor,
    this.onChanged,
    this.wrapInnerBox,
    this.decoration,
    this.overrideDecoration = false,
    this.forceErrorStyle = false,
  }) : super(key: key);

  @override
  PasswordFieldState createState() => PasswordFieldState();
}

class PasswordFieldState extends material.State<PasswordField> {
  bool _obscure = true;

  @override
  Widget build(BuildContext context) {
    final suffixIcon = material.IconButton(
      icon: AnimatedSwitcher(
        duration: const Duration(milliseconds: 300),
        child: _obscure
            ? Icon(
                GBIcons.icon_eye,
                key: ValueKey("eye"),
                color: widget.iconColor,
              )
            : Icon(
                GBIcons.icon_eye_closed,
                key: ValueKey("eye_closed"),
                color: widget.iconColor,
              ),
      ),
      onPressed: () {
        setState(() {
          _obscure = !_obscure;
        });
      },
    );
    return TextField(
      label: widget.label,
      labelStyle: widget.labelStyle,
      autofillHints: widget.autofillHints,
      hintText: widget.hintText,
      focusNode: widget.focusNode,
      errorText: widget.errorText,
      feedback: widget.feedback,
      enabled: widget.enabled,
      obscureText: _obscure,
      filled: widget.filled,
      controller: widget.controller,
      onChanged: widget.onChanged,
      innerBoxWrapper: widget.wrapInnerBox,
      decoration: widget.decoration?.call(context, suffixIcon),
      overrideDecoration: widget.overrideDecoration,
      suffixIcon: suffixIcon,
      forceErrorStyle: widget.forceErrorStyle,
      // child: child,
    );
  }
}
