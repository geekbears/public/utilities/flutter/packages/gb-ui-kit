// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter/services.dart';

import '../../../constants/constants.dart';
import '../../presentation.dart';

class CodeField extends material.StatefulWidget {
  final int length;

  /// Define a custom style to be used in the label of the field
  final material.TextStyle? labelStyle;
  final material.TextStyle? textStyle;
  final bool enabled;
  final bool? filled;
  final String? label;
  final String? errorText;
  final ValueChanged<String>? onChanged;
  final material.BorderRadius? borderRadius;
  final List<InputFeedbackText>? feedback;
  final material.TextInputType? keyboardType;
  final Function(String value)? onSubmitted;
  final material.TextInputAction textInputAction;

  /// {@macro flutter.widgets.editableText.inputFormatters}
  final List<TextInputFormatter>? inputFormatters;

  /// If true , this will set each char to uppercase, this is accomplished by adding a input-formatter
  final bool forceUppercase;

  /// Defines a custom builder widget that will wrap the TexField inner box
  /// This is useful to for example wrap your widget inside a Material elevation, etc
  final WrapWidgetBuilder? innerBoxWrapper;

  final String? value;

  /// Pass a text to be used as hintText
  final String? hintText;

  /// Provides a way to force error style (normally red colored)
  final bool forceErrorStyle;

  ///
  final AlignmentGeometry fieldAlignment;

  /// Customize the size of the input fields boxes
  final double inputBoxSize;

  const CodeField({
    Key? key,
    this.length = 6,
    this.labelStyle,
    this.textStyle,
    this.enabled = true,
    this.filled,
    this.label,
    this.errorText,
    this.onChanged,
    this.borderRadius,
    this.feedback,
    this.keyboardType,
    this.onSubmitted,
    this.textInputAction = material.TextInputAction.done,
    this.inputFormatters,
    this.forceUppercase = false,
    this.innerBoxWrapper,
    this.value,
    this.hintText,
    this.forceErrorStyle = false,
    this.fieldAlignment = Alignment.center,
    this.inputBoxSize = 45,
  }) : super(key: key);

  @override
  _CodeFieldState createState() => _CodeFieldState();
}

class _CodeFieldState extends material.State<CodeField> {
  List<FocusNode> _focusNodes = [];
  List<TextEditingController> _textControllers = [];

  int? _lastFocusedIndex;

  bool _hasFocus = false;

  bool _hasDelete = false;

  late String _value;

  bool get _hasError => widget.errorText != null && widget.errorText != "";
  bool get _hasLabel => widget.label != null && widget.label != "";
  bool get _hasFeedback => widget.feedback != null;

  @override
  void initState() {
    _setFocusNodes();
    _setInputControllers();
    _initStringValue();
    if (widget.value != null) {
      _setFieldValue(widget.value, false);
    }
    super.initState();
  }

  @override
  void didUpdateWidget(CodeField oldWidget) {
    if (oldWidget.length != widget.length) {
      _unsetFocusNodes();
      _setFocusNodes();
      _unsetTextEditingControllers();
      _setInputControllers();
      _initStringValue();
    }
    if (oldWidget.value != widget.value) {
      _setFieldValue(widget.value);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _unsetFocusNodes();
    super.dispose();
  }

  _initStringValue() {
    _value = [for (var i = 0; i < widget.length; i++) i].map((e) => " ").join();
  }

  _setFocusNodes() {
    for (int i = 0; i < widget.length; i++) {
      _focusNodes.add(FocusNode(
        onKeyEvent: (node, keyEvent) {
          if (keyEvent is! KeyDownEvent) return material.KeyEventResult.ignored;

          final isDelete = keyEvent.logicalKey.synonyms.contains(LogicalKeyboardKey.delete) ||
              keyEvent.logicalKey.synonyms.contains(LogicalKeyboardKey.backspace) ||
              keyEvent.logicalKey.keyId == LogicalKeyboardKey.backspace.keyId ||
              keyEvent.logicalKey.keyId == LogicalKeyboardKey.delete.keyId;

          if (isDelete) {
            bool jumped = _onDeleteJumpBackPressed(i);
            if (jumped) {
              return material.KeyEventResult.handled;
            }
          }

          final textController = _textControllers[i];

          // lets check if the corresponding textController already has a value
          if (textController.text.length > 0 && keyEvent.character != null && keyEvent.character != "" && !isDelete) {
            // then we will replace the contents with this char
            textController.text = keyEvent.character!;
            // and request to jump to next focus node
            int nextFocusIndex = i < widget.length - 1 ? i + 1 : i;
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              if (!mounted) return;
              _focusNodes[nextFocusIndex].requestFocus();
            });
          }
          return material.KeyEventResult.ignored;
        },
      )..addListener(_focusListener));
    }
  }

  _setInputControllers() {
    for (int i = 0; i < widget.length; i++) {
      _textControllers.add(TextEditingController());
    }
  }

  _unsetFocusNodes() {
    for (var i = 0; i < _focusNodes.length; i++) {
      _focusNodes[i].removeListener(_focusListener);
      _focusNodes[i].dispose();
    }
    _focusNodes = [];
  }

  _unsetTextEditingControllers() {
    for (var i = 0; i < _textControllers.length; i++) {
      _textControllers[i].dispose();
    }
    _textControllers = [];
  }

  _focusListener() {
    int? newFocusedNodeIndex;
    for (var focusNode in _focusNodes) {
      if (focusNode.hasFocus) {
        newFocusedNodeIndex = _focusNodes.indexOf(focusNode);
      }
      if (_lastFocusedIndex != null && _textControllers[_lastFocusedIndex!].text != "") {
        _textControllers[_lastFocusedIndex!].selection = TextSelection.fromPosition(TextPosition(offset: 1));
      }
    }

    bool newFocusState = newFocusedNodeIndex != null ? true : false;
    if (newFocusState != _hasFocus || _lastFocusedIndex != newFocusedNodeIndex) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        if (!mounted) return;
        setState(() {
          _hasFocus = newFocusState;
        });
      });
    }
    _lastFocusedIndex = newFocusedNodeIndex;
  }

  void _onFieldChanged(int index, String? value) {
    if (value != "" && value != null && value != " ") {
      int currentFocusedIndex = _lastFocusedIndex ?? 0;
      int nextFocusIndex = currentFocusedIndex < widget.length - 1 ? currentFocusedIndex + 1 : currentFocusedIndex;
      // _textControllers[currentFocusedIndex].text = value;
      _focusNodes[nextFocusIndex].requestFocus();
      _value = _value.replaceRange(index, index + 1, value);
    } else if (value == " ") {
      // remove space from textfield
      _textControllers[index].text = "";
    } else {
      _value = _value.replaceRange(index, index + 1, " ");
    }
    widget.onChanged?.call(_value.trim());
  }

  bool _onDeleteJumpBackPressed(int index) {
    // We will just jump back if there is an empty string on field
    // otherwise do nothing
    if (_textControllers[index].text != "") {
      return false;
    }
    // if (_textControllers[index].text.isEmpty || ) {

    int currentFocusedIndex = index;
    int nextFocusIndex = currentFocusedIndex != 0 ? currentFocusedIndex - 1 : currentFocusedIndex;
    material.WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (mounted) {
        _focusNodes[nextFocusIndex].requestFocus();
      }
    });
    return true;
    // }
  }

  @override
  Widget build(BuildContext context) {
    final GBThemeData? themeData = GBThemeExtension.dataOf(context);
    return material.Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (_hasLabel)
          material.Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: GBText.body2(
              "${widget.label}:",
              style: TextStyle(
                fontSize: 14,
              ).merge(widget.labelStyle),
            ),
          ),
        Align(
          alignment: widget.fieldAlignment,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              for (var i = 0; i < widget.length; i += 1) buildFieldBox(i != widget.length - 1, i, context, themeData),
            ],
          ),
        ),
        AnimatedDisplayable(display: _hasError, child: buildErrorText(themeData)),
        if (_hasFeedback) InputFeedbackContainer(feedback: widget.feedback!),
      ],
    );
  }

  material.Padding buildErrorText(GBThemeData? themeData) {
    return material.Padding(
      padding: const EdgeInsets.only(top: 2.0),
      child: GBText.caption(
        widget.errorText,
        style: TextStyle(
          // fontSize: 12,
          color: themeData?.accentError ?? DefaultColors.accent_error_red,
        ),
      ),
    );
  }

  Widget buildFieldBox(bool withPadding, int index, BuildContext context, GBThemeData? themeData) {
    final errorColor = themeData?.accentError ?? DefaultColors.accent_error_red;
    final errorLightColor = themeData?.accentErrorLight ?? DefaultColors.accent_error_red_light;
    final filled = (widget.filled ?? themeData?.inputDecorationTheme.filled ?? true);
    final backgroundFocusColor = filled ? themeData?.backgroundFocus ?? DefaultColors.background_focus : null;
    final backgroundContrastColor = filled ? themeData?.backgroundContrast ?? DefaultColors.background_contrast : null;
    final accentColor = themeData?.accentColor ?? DefaultColors.accent_accent_blue;
    final textDisabledColor = themeData?.textDisabled ?? DefaultColors.text_gray_disabled;
    final textBaseColor = themeData?.textBase ?? DefaultColors.text_text_black;
    final borderRadius = widget.borderRadius ?? themeData?.textFieldRadius;
    final errorBorder = themeData?.inputDecorationTheme.errorBorder ??
        TextField.baseOutlineBorder.copyWith(
          borderSide: TextField.baseBorderSide.copyWith(
            color: errorColor,
          ),
          borderRadius: borderRadius,
        );
    final focusedErrorBorder = themeData?.inputDecorationTheme.focusedErrorBorder ??
        TextField.baseOutlineBorder.copyWith(
          borderSide: TextField.baseBorderSide.copyWith(
            color: errorColor,
          ),
          borderRadius: borderRadius,
        );

    final textStyle = DefaultTextStyle.of(context)
        .style
        .merge(themeData?.textTheme.bodyLarge)
        .copyWith(
          // fontSize: 16,
          // fontWeight: FontWeight.bold,
          color: _lastFocusedIndex == index
              ? accentColor
              : !widget.enabled
                  ? textDisabledColor
                  : _hasError
                      ? errorColor
                      : textBaseColor,
        )
        .merge(widget.textStyle);

    final defaultBorder = themeData?.inputDecorationTheme.border ??
        TextField.baseOutlineBorder.copyWith(
          borderSide: TextField.noBorderSide,
          borderRadius: borderRadius,
        );

    double size = widget.inputBoxSize;

    final innerTextField = TextField(
      maxLength: 1,
      innerBoxWrapper: widget.innerBoxWrapper,
      // scrollPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      enabled: widget.enabled,
      controller: _textControllers[index],
      hintText: widget.hintText,
      onChanged: (value) {
        _onFieldChanged(index, value);
      },
      forceErrorStyle: widget.forceErrorStyle,
      focusNode: _focusNodes[index],
      textAlign: TextAlign.center,
      keyboardType: widget.keyboardType,
      textStyle: textStyle,
      decoration: material.InputDecoration(
        contentPadding: EdgeInsets.symmetric(
          vertical:
              ((size - ((textStyle.fontSize != null ? (textStyle.fontSize! * (textStyle.height ?? 1)) : 16))) / 2) -
                  defaultBorder.borderSide.width / 2,
        ),
        isDense: true,
        counter: SizedBox.shrink(),
        constraints: BoxConstraints(minHeight: size, minWidth: size, maxHeight: size, maxWidth: size),
        focusedBorder: _hasError
            ? focusedErrorBorder
            : themeData?.inputDecorationTheme.focusedBorder ??
                TextField.baseOutlineBorder.copyWith(
                  borderSide: TextField.baseBorderSide.copyWith(
                    color: accentColor,
                  ),
                  borderRadius: borderRadius,
                ),
        disabledBorder: themeData?.inputDecorationTheme.disabledBorder,
        enabledBorder: _hasError
            ? errorBorder
            : themeData?.inputDecorationTheme.enabledBorder ??
                TextField.baseOutlineBorder.copyWith(
                  borderSide: (_hasError)
                      ? TextField.baseBorderSide.copyWith(
                          color: errorColor,
                        )
                      : TextField.baseBorderSide,
                  borderRadius: borderRadius,
                ),
        border: defaultBorder,
        focusedErrorBorder: focusedErrorBorder,
        errorBorder: errorBorder,
        isCollapsed: themeData?.inputDecorationTheme.isCollapsed ?? true,
        hintMaxLines: 0,
        errorMaxLines: 0,
      ),
      textInputAction: widget.textInputAction,
      inputFormatters: [
        if (widget.forceUppercase) _UpperCaseTextFormatter(),
        ...widget.inputFormatters ?? [],
      ],
      onSubmitted: (value) {
        debugPrint("done: $_value");
        widget.onSubmitted?.call(_value);
      },
      textAlignVertical: TextAlignVertical.center,
    );

    final outerWidget = material.Container(
      child: Padding(
        padding: EdgeInsets.only(
          right: index != widget.length - 1 ? 16 : 0,
        ),
        child: material.Stack(
          children: [
            AnimatedContainer(
              duration: Duration(milliseconds: 300),
              width: size,
              height: size,
              decoration: BoxDecoration(
                color: (_hasError)
                    ? errorLightColor
                    : (_hasFocus)
                        ? backgroundFocusColor
                        : backgroundContrastColor,
                borderRadius: borderRadius,
              ),
            ),
            material.Container(
              width: size,
              height: size,
              child: HandlePasteBuilder(
                builder: (context) => innerTextField,
                onPaste: (String? value) {
                  _setFieldValue(value);
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(_focusNodes.elementAt(index));
                },
                node: _focusNodes.elementAt(index),
                shiftOffset: (offset, size) {
                  final left = offset.dx;
                  final top = size.height + 20;
                  //*The right does not indicates the width
                  final right = left + size.width;

                  final width = right - left;

                  var interval = (index) / (widget.length - 1);
                  final dx = interval * (width);
                  return Offset(dx, -top);
                },
              ),
            ),
          ],
        ),
      ),
    );

    return outerWidget;
  }

  showDialogMenu({required BuildContext context, required int index, required double size}) {
    var caption = GBText.caption(
      GBUiKitLocalizations.of(context).paste,
    );

    var backgroundBase = GBTheme.of(context).backgroundBase;

    var isDarkColor = backgroundBase.computeLuminance() < 0.5;

    var colorMenu = isDarkColor ? lighten(backgroundBase, 0.05) : darken(backgroundBase, 0.05);

    material.showMenu(
      context: context,
      position: buttonMenuPosition(context, index, size),
      color: colorMenu,
      items: [
        material.PopupMenuItem(
          onTap: () async {
            ClipboardData? cdata = await Clipboard.getData(Clipboard.kTextPlain);
            _setFieldValue(cdata?.text);
            FocusScope.of(context).unfocus();
          },
          child: material.Row(
            children: [
              Icon(
                material.Icons.paste,
                color: GBTheme.of(context).textTheme.bodySmall?.color,
              ),
              Expanded(
                child: Center(child: caption),
              ),
            ],
          ),
        ),
      ],
    );
  }

  RelativeRect buttonMenuPosition(BuildContext context, int index, double size) {
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    //*get the global position, from the widget local position
    final offset = renderBox.localToGlobal(Offset.zero);

    //*calculate the start point in this case, below the button
    final left = offset.dx;
    final top = offset.dy - size - 10;
    //*The right does not indicates the width
    final right = left + renderBox.size.width;

    final width = right - left;

    var interval = (index) / (widget.length - 1);
    final dx = interval * (width);

    return RelativeRect.fromLTRB(dx, top, width - dx, 0.0);
  }

  void _setFieldValue(String? value, [bool notify = true]) {
    // value.padRight(width)
    final valueToSet = (value ?? "")
        .trim()
        .padRight(_textControllers.length, " ")
        // .replaceAll(RegExp('[^A-Za-z0-9]'), '')
        .substring(0, widget.length);
    for (var i = 0; i < _textControllers.length; i++) {
      _textControllers.elementAt(i).text = valueToSet[i] == " " ? "" : valueToSet[i];
    }
    String _value = valueToSet.trim();
    if (notify) {
      WidgetsBinding.instance.scheduleFrameCallback((timeStamp) {
        widget.onChanged?.call(_value.trim());
      });
    }
    // FocusScope.of(context).unfocus();
  }
}

class _UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
