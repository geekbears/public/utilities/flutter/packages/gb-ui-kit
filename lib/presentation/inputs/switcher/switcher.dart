import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class Switcher extends StatefulWidget {
  const Switcher({
    Key? key,
    required this.value,
    this.duration,
    this.enabledColor,
    this.disabledColor,
    this.circleColor,
    this.onChanged,
    this.disabled = false,
  }) : super(key: key);

  final bool value;
  final Duration? duration;

  /// By default use `accentColor` from `GBTheme`
  final Color? enabledColor;

  /// By default use `backgroundDisabled` from `GBTheme`
  final Color? disabledColor;

  final Color? circleColor;
  final ValueChanged<bool>? onChanged;

  /// If `Switcher` can be used or not
  final bool disabled;

  @override
  State<Switcher> createState() => _SwitcherState();
}

class _SwitcherState extends State<Switcher> {
  late ValueNotifier<bool> _enabled;

  @override
  void initState() {
    _enabled = ValueNotifier<bool>(widget.value);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant Switcher oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.value != widget.value && !widget.disabled) {
      _enabled.value = widget.value;
    }
  }

  @override
  void dispose() {
    _enabled.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final themeData = GBTheme.of(context);
    final enabledColor = (widget.disabled) ? themeData.disabledColor : widget.enabledColor ?? themeData.accentColor;
    final disabledColor = (widget.disabled) ? themeData.disabledColor : widget.disabledColor ?? themeData.backgroundDisabled;
    return ValueListenableBuilder<bool>(
      valueListenable: _enabled,
      builder: (_, value, __) => GestureDetector(
        onTap: (!widget.disabled)
            ? () {
                widget.onChanged?.call(!value);
                _enabled.value = !value;
              }
            : null,
        child: AnimatedContainer(
          duration: widget.duration ??
              const Duration(
                milliseconds: 200,
              ),
          decoration: BoxDecoration(
            color: (!value) ? disabledColor : enabledColor,
            borderRadius: BorderRadius.circular(30),
          ),
          height: 24,
          width: 48,
          child: AnimatedAlign(
            alignment: (!value) ? Alignment.centerLeft : Alignment.centerRight,
            duration: widget.duration ??
                const Duration(
                  milliseconds: 200,
                ),
            child: Container(
              margin: const EdgeInsets.symmetric(
                horizontal: 4,
                vertical: 2,
              ),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: widget.circleColor ?? Colors.white,
              ),
              height: 18,
              width: 18,
            ),
          ),
        ),
      ),
    );
  }
}
