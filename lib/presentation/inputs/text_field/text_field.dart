import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter/services.dart';

import '../../presentation.dart';

class TextField extends material.StatefulWidget {
  static const material.OutlineInputBorder baseOutlineBorder = material.OutlineInputBorder(
    borderRadius: BorderRadius.zero,
  );
  static const BorderSide baseBorderSide = BorderSide(width: 1, style: BorderStyle.solid);
  static const BorderSide noBorderSide = BorderSide(color: material.Colors.transparent, width: 0);

  final String? label;

  /// Define a custom style to be used in the label of the field
  final material.TextStyle? labelStyle;
  final String? hintText;
  final FocusNode? focusNode;
  final String? errorText;
  final bool enabled;
  final bool readOnly;
  final Widget? suffix;
  final Widget? suffixIcon;
  final Widget? prefix;
  final Widget? prefixIcon;
  final bool obscureText;
  final bool? filled;
  final BorderRadius? borderRadius;
  final material.TextEditingController? controller;
  final List<InputFeedbackText>? feedback;
  final material.TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final material.TextCapitalization textCapitalization;
  final material.TextInputAction? textInputAction;
  final material.TextAlign textAlign;
  final int? maxLength;
  final int? maxLines;
  final int? minLines;
  final bool? expands;
  final material.TextStyle? textStyle;
  final void Function(String value)? onChanged;

  ///
  /// Provides a way to override default input decoration styles
  /// by default if you only provide this value it will merge your style definition with default
  /// if you want to relay completely on your styling set [overrideDecoration] to `true`
  final material.InputDecoration? decoration;

  /// Check [decoration] property for more info
  /// defaults to [false]
  final bool overrideDecoration;

  /// TextAlign
  final TextAlignVertical? textAlignVertical;

  /// Defines a custom builder widget that will wrap the TexField inner box
  /// This is useful to for example wrap your widget inside a Material elevation, etc
  final WrapWidgetBuilder? innerBoxWrapper;

  /// {@macro flutter.widgets.editableText.onSubmitted}
  ///
  /// See also:
  ///
  ///  * [TextInputAction.next] and [TextInputAction.previous], which
  ///    automatically shift the focus to the next/previous focusable item when
  ///    the user is done editing.
  final ValueChanged<String>? onSubmitted;

  /// {@macro flutter.widgets.editableText.scrollPadding}
  final EdgeInsets scrollPadding;

  /// {@macro flutter.widgets.editableText.autocorrect}
  final bool? autocorrect;

  /// Provides a way to modify the input [cursorHeight]
  final double? cursorHeight;

  /// Provides a way to force error style (normally red colored)
  final bool forceErrorStyle;

  /// {@macro flutter.widgets.editableText.autofillHints}
  final Iterable<String>? autofillHints;

  const TextField({
    Key? key,
    this.label,
    this.labelStyle,
    this.hintText,
    this.focusNode,
    this.errorText,
    this.enabled = true,
    this.readOnly = false,
    this.suffix,
    this.suffixIcon,
    this.prefix,
    this.prefixIcon,
    this.obscureText = false,
    this.filled,
    this.borderRadius,
    this.controller,
    this.feedback,
    this.keyboardType,
    this.inputFormatters,
    this.textCapitalization = material.TextCapitalization.none,
    this.textAlign = material.TextAlign.start,
    this.textInputAction,
    this.maxLength,
    this.maxLines = 1,
    this.minLines,
    this.expands,
    this.textStyle,
    this.onChanged,
    this.decoration,
    this.overrideDecoration = false,
    this.textAlignVertical,
    this.innerBoxWrapper,
    this.onSubmitted,
    this.scrollPadding = const EdgeInsets.all(10),
    this.autocorrect,
    this.cursorHeight,
    this.forceErrorStyle = false,
    this.autofillHints = const [],
  }) : super(key: key);

  @override
  _TextFieldState createState() => _TextFieldState();
}

class _TextFieldState extends material.State<TextField> {
  FocusNode? _focusNode;

  bool? _lastFocus;

  @override
  void initState() {
    super.initState();
    effectiveFocusNode.addListener(_focusListener);
  }

  @override
  void dispose() {
    effectiveFocusNode.removeListener(_focusListener);
    super.dispose();
  }

  _focusListener() {
    final newFocus = effectiveFocusNode.hasFocus;
    if (_lastFocus != newFocus) setState(() {});
    _lastFocus = newFocus;
  }

  ///NOTE: Sometimes we need display the input error style without an error meesage, due to this we implement [erroredInput]
  bool get _hasError => widget.errorText != null && widget.errorText != "" || widget.forceErrorStyle;
  bool get _hasFocus => effectiveFocusNode.hasFocus;

  FocusNode get effectiveFocusNode => widget.focusNode ?? _focusNode ?? (_focusNode = FocusNode());

  material.InputDecoration getInputDecoration(GBThemeData themeData) {
    final Color textBaseColor = themeData.textBase;
    final Color accentColor = themeData.accentColor;
    final Color errorColor = GBTheme.errorColorOf(context);
    final Color textDisabledColor = themeData.textDisabled;
    final Color? focusFillColor = themeData.inputDecorationTheme.focusColor;

    if (widget.overrideDecoration && widget.decoration != null) {
      return widget.decoration!;
    }

    return material.InputDecoration(
      counterText: widget.maxLength != null ? "" : null,
      hintText: widget.decoration?.hintText ?? widget.hintText,
      hintStyle: widget.decoration?.hintStyle ?? themeData.inputDecorationTheme.hintStyle,
      filled: widget.decoration?.filled ?? widget.filled ?? themeData.inputDecorationTheme.filled,
      border: widget.decoration?.border ?? themeData.inputDecorationTheme.border,
      enabledBorder: widget.decoration?.enabledBorder ??
          (_hasError ? themeData.inputDecorationTheme.errorBorder : themeData.inputDecorationTheme.enabledBorder),
      focusedBorder: widget.decoration?.focusedBorder ??
          (_hasError ? themeData.inputDecorationTheme.focusedErrorBorder : themeData.inputDecorationTheme.focusedBorder),
      focusedErrorBorder: widget.decoration?.focusedErrorBorder ?? themeData.inputDecorationTheme.focusedErrorBorder,
      disabledBorder: widget.decoration?.disabledBorder ?? themeData.inputDecorationTheme.disabledBorder,
      contentPadding: widget.decoration?.contentPadding ?? themeData.inputDecorationTheme.contentPadding,
      errorBorder: widget.decoration?.errorBorder ?? themeData.inputDecorationTheme.errorBorder,
      fillColor: widget.decoration?.fillColor ??
          (!widget.enabled
              ? themeData.inputDecorationTheme.disabledFillColor
              : _hasError
                  ? themeData.inputDecorationTheme.errorFillColor
                  : _hasFocus
                      ? focusFillColor
                      : themeData.inputDecorationTheme.fillColor),
      focusColor: focusFillColor,
      alignLabelWithHint: widget.decoration?.alignLabelWithHint ?? themeData.inputDecorationTheme.alignLabelWithHint,
      isDense: widget.decoration?.isDense ?? themeData.inputDecorationTheme.isDense,
      suffix: widget.decoration?.suffix ?? widget.suffix,
      prefix: widget.decoration?.prefix ?? widget.prefix,
      prefixIcon: widget.decoration?.prefixIcon ??
          (widget.prefixIcon != null
              ? material.IconTheme(
                  data: material.IconThemeData(
                    color: (!widget.enabled)
                        ? textDisabledColor
                        : (_hasError)
                            ? errorColor
                            : (effectiveFocusNode.hasFocus)
                                ? (_hasError)
                                    ? errorColor
                                    : accentColor
                                : textBaseColor,
                  ),
                  child: widget.prefixIcon!,
                )
              : null),
      suffixIcon: widget.decoration?.suffixIcon ??
          (widget.suffixIcon != null
              ? material.IconTheme(
                  data: material.IconThemeData(
                    color: (!widget.enabled)
                        ? textDisabledColor
                        : (_hasError)
                            ? errorColor
                            : (effectiveFocusNode.hasFocus)
                                ? (_hasError)
                                    ? errorColor
                                    : accentColor
                                : textBaseColor,
                  ),
                  child: widget.suffixIcon!,
                )
              : null),
    );
  }

  @override
  Widget build(BuildContext context) {
    final GBThemeData gbTheme = GBTheme.of(context);
    final Color textBaseColor = gbTheme.textBase;
    final Color errorColor = GBTheme.errorColorOf(context);
    final Color textDisabledColor = gbTheme.textDisabled;

    final TextStyle defaultTextStyle = DefaultTextStyle.of(context).style;
    final TextStyle inputStyle =
        (widget.textStyle ?? gbTheme.textTheme.bodyLarge ?? DefaultTextStyle.of(context).style).merge(
      TextStyle(
        // TODO: Inherit correct text color
        color: _hasError
            ? errorColor
            : !widget.enabled
                ? textDisabledColor
                : textBaseColor,
      ),
    );

    final innerTextField = material.TextField(
      key: widget.key,
      autocorrect:
          widget.autocorrect ?? {TextInputType.emailAddress, TextInputType.visiblePassword}.contains(widget.keyboardType)
              ? false
              : true,
      autofillHints: widget.autofillHints,
      expands: widget.expands ?? false,
      minLines: widget.minLines,
      maxLines: widget.maxLines,
      maxLength: widget.maxLength,
      maxLengthEnforcement: widget.maxLength != null ? MaxLengthEnforcement.enforced : null,
      enabled: widget.enabled,
      readOnly: widget.readOnly,
      focusNode: effectiveFocusNode,
      obscureText: widget.obscureText,
      keyboardType: widget.keyboardType,
      inputFormatters: widget.inputFormatters,
      textCapitalization: widget.textCapitalization,
      textInputAction: widget.textInputAction,
      textAlign: widget.textAlign,
      controller: widget.controller,
      cursorHeight: widget.cursorHeight,
      textAlignVertical: widget.textAlignVertical,
      onChanged: widget.onChanged,
      decoration: getInputDecoration(gbTheme),
      scrollPadding: widget.scrollPadding,
      style: inputStyle,
      onSubmitted: widget.onSubmitted,
    );
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: gbTheme.inputDecorationTheme.constraints?.maxWidth ?? 343),
      child: material.Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.label != null && widget.label != "" && !widget.overrideDecoration)
            material.Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: GBText.body2(
                "${widget.label}:",
                style: defaultTextStyle.merge(
                  widget.labelStyle ?? gbTheme.inputDecorationTheme.labelStyle,
                ),
              ),
            ),
          widget.innerBoxWrapper?.call(context, innerTextField) ?? innerTextField,
          AnimatedDisplayable(
            display: _hasError,
            child: material.Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: GBText.caption(
                widget.errorText,
                style: TextStyle(
                  color: errorColor,
                ),
              ),
            ),
          ),
          if (widget.feedback != null)
            InputFeedbackContainer(
              feedback: widget.feedback!,
            ),
        ],
      ),
    );
  }
}
