part of 'select_field.dart';

class MultiSelectField<T> extends SelectFieldBase<T> {
  /// Callback that is triggered each time the user selects an option
  final ValueChanged<List<T>>? onSelected;

  /// The option that should be pre-selected at the beginning
  final List<T>? initialValue;

  /// Provide a custom comparator that will be used to identify the relation
  /// between the initialValues and options provided
  final bool Function(List<T> initialValue, T valueCompared)? initialValueComparison;

  // Define a custom builder for the tile section, by default, material chips are build
  final Widget Function(
    BuildContext context,
    List<SelectFieldOption<T>> selected,
  )? titleBuilder;

  const MultiSelectField({
    super.key,
    super.label,
    super.labelStyle,
    super.enabled = true,
    super.unselectedString,
    super.duration = const Duration(milliseconds: 300),
    this.initialValue,
    super.scrollController,
    required super.options,
    this.onSelected,
    super.closeOnSelect = false,
    super.errorText,
    super.trailingBuilder,
    super.borderRadius,
    super.filled,
    super.enableSearch = false,
    super.fillColor,
    super.overrideColor = false,
    super.feedback,
    super.maxWidth = 343,
    super.titleStyle,
    super.optionTileStyle,
    this.titleBuilder,
    this.initialValueComparison,
    super.fieldPadding,
  }) : assert(
          overrideColor && fillColor != null || !overrideColor,
          "You must provide a color in order to set 'overrideColor' as true",
        );

  @override
  _MultiSelectFieldState<T> createState() => _MultiSelectFieldState<T>();
}

class _MultiSelectFieldState<T> extends State<MultiSelectField<T>> {
  List<SelectFieldOption>? _selectedFieldOptions = [];

  String get _title => widget.unselectedString ?? "Select";

  bool get _hasSelected => _selectedFieldOptions != null && _selectedFieldOptions!.isNotEmpty;

  @override
  void didUpdateWidget(MultiSelectField<T> oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (!listEquals(oldWidget.initialValue, widget.initialValue)) {
      _setInitialValue(widget.options);
    }

    if (oldWidget.options.isEmpty &&
        widget.options.isNotEmpty &&
        (_selectedFieldOptions == null || _selectedFieldOptions!.isEmpty) &&
        widget.initialValue != null) {
      _setInitialValue(widget.options);
    }
  }

  @override
  void initState() {
    if (widget.initialValue != null) {
      _setInitialValue(widget.options);
    }
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _SelectFieldCommonUI(
      selectField: widget,
      titleBuilder: (context) {
        return builtTitle(context);
      },
      onSelected: (option) {
        setState(() {
          if (_selectedFieldOptions?.contains(option) ?? false) {
            // _selectedFieldOptions =
            _selectedFieldOptions!.remove(option);
          } else if (_selectedFieldOptions != null) {
            _selectedFieldOptions!.add(option);
          } else {
            _selectedFieldOptions = [option];
          }
          _notifyChange();
        });
      },
      selected: _selectedFieldOptions ?? [],
    );
  }

  _notifyChange() {
    if (_selectedFieldOptions != null) {
      widget.onSelected?.call(
        _selectedFieldOptions!.map(
          (e) {
            return (e as SelectFieldOption<T>).value;
          },
        ).toList(),
      );
    }
  }

  _setInitialValue(List<SelectFieldOption<T>>? options) {
    try {
      _selectedFieldOptions = options?.where(
        (element) {
          if (widget.initialValueComparison != null && widget.initialValue != null) {
            return widget.initialValueComparison!(widget.initialValue!, element.value);
          } else {
            return widget.initialValue?.contains(element.value) ?? false;
          }
        },
      ).toList();
    } catch (e) {
      // throw Exception("Initial Value Provided os not valid as is not ");
      debugPrint("Initial value didn't math any value of options provided");
    }
  }

  material.Widget builtTitle(material.BuildContext context) {
    if (!_hasSelected) return Text(_title);
    return widget.titleBuilder?.call(context, _selectedFieldOptions as List<SelectFieldOption<T>>? ?? []) ??
        Wrap(
          children: (_selectedFieldOptions ?? []).map((option) {
            return material.Chip(
              label: Text(
                option.title,
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
              deleteIcon: Icon(material.Icons.close),
              onDeleted: () {
                setState(() {
                  _selectedFieldOptions?.remove(option);
                  _notifyChange();
                });
              },
            );
          }).toList(),
        );
  }
}
