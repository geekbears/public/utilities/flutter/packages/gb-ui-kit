part of "select_field.dart";

class _SelectFieldCommonUI<T> extends StatefulWidget {
  final SelectFieldBase<T> selectField;
  final List<SelectFieldOption<T>> selected;
  final void Function(SelectFieldOption<T> option) onSelected;
  final Widget Function(
    BuildContext context,
  ) titleBuilder;

  const _SelectFieldCommonUI({
    Key? key,
    required this.selectField,
    required this.selected,
    required this.onSelected,
    required this.titleBuilder,
  }) : super(key: key);

  @override
  State<_SelectFieldCommonUI<T>> createState() => __SelectFieldCommonUIState<T>();
}

class __SelectFieldCommonUIState<T> extends State<_SelectFieldCommonUI<T>> {
  bool _collapsed = true;

  // SelectFieldOption? _selectedFieldOption;
  material.TextEditingController? _searchController;
  List<SelectFieldOption<T>>? _options;

  bool get _hasLabel => widget.selectField.label != null && widget.selectField.label != "";
  bool get _hasError => widget.selectField.errorText != null && widget.selectField.errorText != "";
  bool get _hasFeedback => widget.selectField.feedback != null;

  final FocusNode _mainFocusNode = FocusNode(debugLabel: "SelectFieldMainFocusNode");

  void _searchListener() {
    if (_searchController?.text.isNotEmpty ?? false) {
      setState(() {
        _options = widget.selectField.options
            .where((element) => element.title.toLowerCase().contains(_searchController?.text.toLowerCase() ?? ''))
            .toList();
      });
    } else {
      setState(() => _options = widget.selectField.options);
    }
  }

  @override
  void didUpdateWidget(_SelectFieldCommonUI<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.selectField.collapsed != widget.selectField.collapsed) {
      _collapsed = widget.selectField.collapsed;
    }

    if (!widget.selectField.enabled && !_collapsed) {
      _collapsed = true;
    }

    if (oldWidget.selectField.options != widget.selectField.options &&
        listEquals(oldWidget.selectField.options, widget.selectField.options)) {
      _options = widget.selectField.options;
    }

    if (widget.selectField.enableSearch && _options == null) {
      _options = widget.selectField.options;
    }
    if (!oldWidget.selectField.enableSearch && widget.selectField.enableSearch) {
      _attachListener();
    } else if (oldWidget.selectField.enableSearch && !widget.selectField.enableSearch) {
      _detachListener();
    }
  }

  @override
  void initState() {
    _attachListener();
    super.initState();
    _collapsed = widget.selectField.collapsed;
    _mainFocusNode.addListener(
      () {
        setState(() {
          if (!_mainFocusNode.hasFocus && widget.selectField.closeOnUnfocus) {
            _setCollapsedState(context, true);
          }
        });
      },
    );
  }

  @override
  void dispose() {
    _detachListener();
    _mainFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final GBThemeData gbTheme = GBTheme.of(context);
    final TextStyle defaultTextStyle = DefaultTextStyle.of(context).style;
    return ConstrainedBox(
      constraints: gbTheme.inputDecorationTheme.constraints ??
          gbTheme.dropdownMenuTheme.inputDecorationTheme?.constraints ??
          BoxConstraints(
            maxWidth: widget.selectField.maxWidth ?? 343,
          ),
      child: AbsorbPointer(
        absorbing: !widget.selectField.enabled,
        child: GestureDetector(
          onTap: () {
            setState(() {
              _setCollapsedState(context, !_collapsed);
            });
          },
          child: Focus(
            focusNode: _mainFocusNode,
            onKeyEvent: (node, event) {
              switch (event) {
                case KeyDownEvent(:final logicalKey):
                  {
                    if (logicalKey == LogicalKeyboardKey.space) {
                      setState(() {
                        _setCollapsedState(context, !_collapsed);
                      });
                      return KeyEventResult.ignored;
                    }
                  }
              }
              return KeyEventResult.ignored;
            },
            child: material.Builder(
              builder: (context) {
                return material.Container(
                  clipBehavior: Clip.none,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (_hasLabel) _buildLabel(defaultTextStyle, gbTheme),
                      _buildExpandableField(context),
                      AnimatedDisplayable(display: _hasError, child: _buildErrorText()),
                      if (_hasFeedback) InputFeedbackContainer(feedback: widget.selectField.feedback!),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  void _setCollapsedState(BuildContext context, bool newState) {
    _collapsed = newState;
    if (!_collapsed) {
      _mainFocusNode.requestFocus();
    } else {
      material.FocusScope.of(context).unfocus();
    }
  }

  Widget _buildErrorText() {
    return material.Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: GBText.caption(
        widget.selectField.errorText,
        style: TextStyle(
          // fontSize: 12,
          color: DefaultColors.accent_error_red,
        ),
      ),
    );
  }

  Widget _buildLabel(material.TextStyle defaultTextStyle, GBThemeData gbTheme) {
    final style = defaultTextStyle.merge(
      widget.selectField.labelStyle ?? gbTheme.inputDecorationTheme.labelStyle,
    );
    return material.Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: DefaultTextStyle(
        child: GBText.body2(
          "${widget.selectField.label}:",
          context: context,
          style: style,
        ),
        style: style,
      ),
    );
  }

  Widget _buildExpandableField(BuildContext context) {
    final themeData = GBThemeExtension.dataOf(context);
    final redColor = themeData?.accentError ?? DefaultColors.accent_error_red;
    final redLightColor = themeData?.accentErrorLight ?? DefaultColors.accent_error_red_light;
    final accentColor = themeData?.accentColor ?? DefaultColors.accent_accent_blue;
    final backgroundContrast = themeData?.backgroundContrast ?? DefaultColors.background_contrast;
    final backgroundDisabled = themeData?.inputDecorationTheme.disabledFillColor ??
        themeData?.backgroundDisabled ??
        DefaultColors.background_disabled;
    final borderRadius = widget.selectField.borderRadius ?? themeData?.textFieldRadius;
    final _defaultBorder = TextField.baseBorderSide.copyWith(
      color: _hasError ? redColor : accentColor,
    );
    final defaultFillColor = widget.selectField.fillColor ?? themeData?.inputDecorationTheme.fillColor;
    return Column(
      children: [
        AnimatedContainer(
          duration: widget.selectField.duration,
          clipBehavior: Clip.hardEdge,
          constraints: BoxConstraints(
            maxWidth: themeData?.inputDecorationTheme.constraints?.maxWidth ?? 343,
          ),
          decoration: BoxDecoration(
            border: Border.fromBorderSide(
              !_mainFocusNode.hasFocus
                  ? themeData?.inputDecorationTheme.border?.borderSide ?? TextField.noBorderSide
                  : _hasError
                      ? (themeData?.inputDecorationTheme.errorBorder?.borderSide ?? _defaultBorder)
                      : (themeData?.inputDecorationTheme.focusedBorder?.borderSide ?? _defaultBorder),
            ),
            borderRadius: borderRadius,
            color: (widget.selectField.filled ?? themeData?.inputDecorationTheme.filled ?? true)
                ? _hasError
                    ? (widget.selectField.overrideColor ? widget.selectField.fillColor : redLightColor)
                    : widget.selectField.enabled
                        ? (defaultFillColor ?? backgroundContrast)
                        : (widget.selectField.overrideColor ? defaultFillColor : backgroundDisabled)
                : null,
          ),
          width: double.infinity,
          child: material.Column(
            children: [
              _buildFieldBox(themeData),
              _CollapsibleList<T>(
                selectField: widget.selectField,
                options: (widget.selectField.enableSearch) ? _options! : widget.selectField.options,
                collapsed: _collapsed,
                searchController: _searchController,
                parentContext: context,
                selected: widget.selected,
                onOptionSelected: (option) {
                  setState(() {
                    if (widget.selectField.closeOnSelect) _collapsed = true;
                    widget.onSelected(option as SelectFieldOption<T>);
                  });
                  material.FocusScope.of(context).unfocus();
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildFieldBox(GBThemeData? themeData) {
    final themeData = GBThemeExtension.dataOf(context);
    // final redLightColor = themeData?.accentErrorLight ?? DefaultColors.accent_error_red_light;
    // final backgroundDisabled = themeData?.backgroundDisabled ?? DefaultColors.background_disabled;
    final defaultStyle = themeData?.textTheme.bodyLarge ?? DefaultTextStyle.of(context).style;
    final disabledColor = themeData?.textDisabled ?? DefaultColors.text_gray_disabled;
    final defaultColor = defaultStyle.color ?? DefaultColors.text_gray_base;
    return material.Container(
      padding: widget.selectField.fieldPadding,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: DefaultTextStyle(
              style: defaultStyle
                  .copyWith(
                    fontSize: 16,
                    color: !widget.selectField.enabled || (_collapsed && widget.selected.isEmpty)
                        ? disabledColor
                        : defaultColor,
                  )
                  .merge(widget.selectField.titleStyle),
              child: Builder(
                builder: widget.titleBuilder,
              ),
            ),
          ),
          Align(
            child: _CollapseIcon(
              selectField: widget.selectField,
              collapsed: _collapsed,
            ),
            alignment: AlignmentDirectional.centerEnd,
          ),
        ],
      ),
    );
  }

  _attachListener() {
    if (widget.selectField.enableSearch && _searchController == null) {
      _searchController = material.TextEditingController();
      _options = widget.selectField.options;

      _searchController!.addListener(_searchListener);
    }
  }

  _detachListener() {
    _searchController?.removeListener(_searchListener);
    _searchController?.dispose();
    _searchController = null;
  }
}
