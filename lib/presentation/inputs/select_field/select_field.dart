// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' as material;
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import '../../../constants/colors.dart';
import '../../presentation.dart';

export 'select_field_option.dart';

part 'multi_select_field.dart';
part 'select_field_base.dart';
part 'select_field_common.dart';

typedef SelectTrailingBuilder = Widget Function(BuildContext context, int index, bool isSelected);

class SelectField<T> extends SelectFieldBase<T> {
  /// Callback that is triggered each time the user selects an option
  final ValueChanged<T>? onSelected;

  /// The option that should be pre-selected at the beginning
  final T? initialValue;

  const SelectField({
    super.key,
    super.label,
    super.labelStyle,
    super.enabled = true,
    super.unselectedString,
    super.duration = const Duration(milliseconds: 300),
    this.initialValue,
    super.scrollController,
    required super.options,
    this.onSelected,
    super.closeOnSelect = true,
    super.errorText,
    super.trailingBuilder,
    super.borderRadius,
    super.filled,
    super.enableSearch = false,
    super.fillColor,
    super.overrideColor = false,
    super.feedback,
    super.maxWidth,
    super.titleStyle,
    super.optionTileStyle,
    super.fieldPadding,
    super.onSearchChange,
    super.searchHintText,
    super.collapsed,
    super.autoFocusOnExpanded = true,
    super.onTransitionEnds,
    super.iconBuilder,
    super.closeOnUnfocus,
    super.optionsFooterBuilder,
    super.optionsSeparatorBuilder,
  }) : assert(
          overrideColor && fillColor != null || !overrideColor,
          "You must provide a color in order to set 'overrideColor' as true",
        );

  @override
  _SelectFieldState<T> createState() => _SelectFieldState<T>();
}

class _SelectFieldState<T> extends State<SelectField<T>> {
  SelectFieldOption? _selectedFieldOption;

  String get _title => _selectedFieldOption != null ? _selectedFieldOption!.title : widget.unselectedString ?? "Select";

  @override
  void didUpdateWidget(SelectField<T> oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.initialValue != widget.initialValue) {
      _setInitialValue(widget.options);
    }

    if (oldWidget.options.isEmpty &&
        widget.options.isNotEmpty &&
        _selectedFieldOption == null &&
        widget.initialValue != null) {
      _setInitialValue(widget.options);
    }
  }

  @override
  void initState() {
    if (widget.initialValue != null) {
      _setInitialValue(widget.options);
    }
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _SelectFieldCommonUI(
      selectField: widget,
      titleBuilder: (context) {
        return Text(_title);
      },
      onSelected: (option) {
        setState(() {
          _selectedFieldOption = option;
          _notifyChange();
        });
      },
      selected: [
        if (_selectedFieldOption != null) _selectedFieldOption!,
      ],
    );
  }

  _setInitialValue(List<SelectFieldOption<T>>? options) {
    try {
      _selectedFieldOption = options?.firstWhere(
        (element) => element.value == widget.initialValue,
      );
    } catch (e) {
      // throw Exception("Initial Value Provided os not valid as is not ");
      debugPrint("Initial value didn't math any value of options provided");
    }
  }

  _notifyChange() {
    if (_selectedFieldOption != null) {
      widget.onSelected?.call(
        _selectedFieldOption!.value,
      );
    }
  }
}

class _CollapsibleList<T> extends material.StatefulWidget {
  final bool collapsed;
  // final Widget child;
  final List<SelectFieldOption> options;
  final ValueChanged<SelectFieldOption> onOptionSelected;
  final material.TextEditingController? searchController;
  final BuildContext parentContext;
  final List<SelectFieldOption>? selected;
  final SelectFieldBase<T> selectField;

  const _CollapsibleList({
    super.key,
    required this.selectField,
    required this.parentContext,
    required this.onOptionSelected,
    required this.collapsed,
    required this.options,
    required this.selected,
    required this.searchController,
  });

  @override
  _CollapsibleListState createState() => _CollapsibleListState();
}

class _CollapsibleListState extends material.State<_CollapsibleList> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;
  material.FocusNode? _searchFieldFocusNode;

  bool _animationInProgress = false;

  ScrollController? _fallbackController;
  ScrollController? get _scrollController => widget.selectField.scrollController ?? _fallbackController;

  @override
  void initState() {
    try {
      _fallbackController = PrimaryScrollController.of(context);
    } catch (e) {}
    _animationController = AnimationController(vsync: this, duration: widget.selectField.duration);
    _animation = Tween<double>(begin: 0, end: 1).animate(_animationController);
    _searchFieldFocusNode = material.FocusNode();
    _animationController.addStatusListener((status) {
      ScrollableState? scrollable;
      try {
        scrollable = Scrollable.of(context);
      } catch (e) {}
      final bool isInProgress = status == AnimationStatus.forward || status == AnimationStatus.reverse;
      if (_animationInProgress != isInProgress) {
        setState(() {
          _animationInProgress = isInProgress;
        });
      }
      if (status == AnimationStatus.completed || status == AnimationStatus.dismissed) {
        widget.selectField.onTransitionEnds?.call(
            AnimationStatus.completed == status ? SelectFieldCollapseState.uncollapsed : SelectFieldCollapseState.collapsed);
      }
      if ((_scrollController != null || scrollable != null) &&
          mounted &&
          status == AnimationStatus.completed &&
          widget.selectField.autoFocusOnExpanded) {
        final ro = widget.parentContext.findRenderObject();
        if (_scrollController != null) {
          _scrollController?.position.ensureVisible(
            ro!,
            duration: Duration(milliseconds: 300),
          );
        } else if (scrollable != null) {
          scrollable.position.ensureVisible(
            ro!,
            duration: Duration(milliseconds: 300),
          );
        }
      }
    });
    super.initState();
  }

  @override
  void didUpdateWidget(_CollapsibleList oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.collapsed) {
      _animationController.reverse();
    } else {
      _animationController.forward();
    }

    if (oldWidget.collapsed && !widget.collapsed && widget.selectField.autoFocusOnExpanded) {
      _searchFieldFocusNode?.requestFocus();
    }
  }

  material.InputBorder get searchFilterBorder {
    GBThemeData themeData = GBTheme.of(context);
    GBInputDecorationTheme inputDecorationTheme = themeData.inputDecorationTheme;
    return material.UnderlineInputBorder(
      borderSide: (inputDecorationTheme.focusedBorder?.borderSide ?? TextField.baseBorderSide).copyWith(
        width: 1,
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    _searchFieldFocusNode?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final defaultTextStyle = DefaultTextStyle.of(context).style;
    final themeData = GBThemeExtension.dataOf(context);
    return material.Column(
      children: [
        AnimatedBuilder(
          animation: _animation,
          builder: (context, child) {
            return Align(
              alignment: AlignmentDirectional.topStart,
              heightFactor: _animation.value,
              child: _animationInProgress || (!_animationInProgress && !widget.collapsed) ? child : null,
            );
          },
          child: material.Column(
            children: [
              if (widget.selectField.enableSearch)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    hintText: widget.selectField.searchHintText ?? GBUiKitLocalizations.of(context).typeToFilter,
                    decoration: material.InputDecoration(
                      border: searchFilterBorder,
                      focusedBorder: searchFilterBorder,
                    ),
                    controller: widget.searchController,
                    focusNode: _searchFieldFocusNode,
                    onChanged: widget.selectField.onSearchChange,
                  ),
                ),
              material.LimitedBox(
                maxHeight: 288,
                child: material.Scrollbar(
                  child: material.LimitedBox(
                    maxHeight: 288,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Flexible(
                          child: CustomScrollView(
                            // physics: const AlwaysScrollableScrollPhysics(),
                            shrinkWrap: true,
                            slivers: [
                              AnimatableStateListView<SelectFieldOption>(
                                asSliver: true,
                                idMapper: (item) {
                                  return "${item.value}";
                                },
                                itemBuilder: (context, index, animation, item) {
                                  return buildListTile(item, defaultTextStyle, index, themeData);
                                },
                                separatorBuilder: widget.selectField.optionsSeparatorBuilder,
                                items: widget.selectField.options,
                              ),
                            ],
                          ),
                        ),
                        AnimatedFadeHeightSwitcher(
                          duration: material.Durations.medium4,
                          child: widget.selectField.optionsFooterBuilder != null
                              ? widget.selectField.optionsFooterBuilder!(context)
                              : SizedBox.shrink(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildListTile(SelectFieldOption option, TextStyle defaultTextStyle, int index, GBThemeData? themeData) {
    final bool isSelected = widget.selected?.contains(option) ?? false;
    final Color selectedColor = themeData?.accentColor ?? DefaultColors.accent_accent_blue;
    final Color? defaultTextColor = themeData?.textBlack ?? defaultTextStyle.color;
    final Color? fontColor = isSelected ? selectedColor : defaultTextColor;
    final Color backgroundFocusColor = themeData?.backgroundFocus ?? DefaultColors.background_focus;
    return material.Material(
      color: isSelected ? backgroundFocusColor : material.Colors.transparent,
      child: material.InkWell(
        onTap: () {
          widget.onOptionSelected(option);
        },
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
          child: material.Row(
            children: [
              Expanded(
                child: GBText.body(
                  option.title,
                  style: material.TextStyle(
                    color: fontColor,
                  ).merge(widget.selectField.optionTileStyle),
                ),
              ),
              IconTheme(
                data: IconThemeData(
                  size: 20,
                  color: fontColor,
                ),
                child: option.trailing ?? widget.selectField.trailingBuilder?.call(context, index, isSelected) ?? SizedBox(),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _CollapseIcon extends StatefulWidget {
  final bool collapsed;
  final SelectFieldBase selectField;

  _CollapseIcon({
    Key? key,
    required this.collapsed,
    required this.selectField,
  }) : super(key: key);

  @override
  __CollapseIconState createState() => __CollapseIconState();
}

class __CollapseIconState extends State<_CollapseIcon> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this, duration: widget.selectField.duration);
    _animation = Tween<double>(begin: 0, end: 1 * pi).animate(_animationController);
    super.initState();
  }

  @override
  void didUpdateWidget(_CollapseIcon oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.collapsed) {
      _animationController.reverse();
    } else {
      _animationController.forward();
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final defaultTExtStyle = DefaultTextStyle.of(context).style;
    final defaultIcon = Icon(
      material.Icons.keyboard_arrow_down,
      color: widget.selectField.enabled ? defaultTExtStyle.color : DefaultColors.text_gray_disabled,
    );
    return AnimatedBuilder(
      animation: _animation,
      child: widget.selectField.iconBuilder?.call(context, defaultIcon, widget.collapsed) ?? defaultIcon,
      builder: (context, child) {
        return Transform.rotate(
          angle: _animation.value,
          child: child,
        );
      },
    );
  }
}
