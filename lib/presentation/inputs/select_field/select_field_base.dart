part of "select_field.dart";

enum SelectFieldCollapseState { collapsed, uncollapsed }

abstract class SelectFieldBase<T> extends StatefulWidget {
  /// The label of this field
  final String? label;

  /// Define a custom style to be used in the label of the field
  final material.TextStyle? labelStyle;

  /// If false, all gesture detection will be disabled, this also affects color and transparency of the field
  final bool enabled;

  /// This text is used when no option have been selected
  final String? unselectedString;

  /// Modify the duration of the collapsing/uncollapsing animation
  final Duration duration;

  /// Wether if you would like to implement a custom controller to be used on the option list-view
  final ScrollController? scrollController;

  /// Provide the options to be used
  final List<SelectFieldOption<T>> options;

  // /// Callback that is triggered each time the user selects an option
  // final ValueChanged<T>? onSelected;

  /// If set to [true] this will auto-close the field when a user select an option of the dropdown
  final bool closeOnSelect;

  /// If you would like to show an error message this will render your message and change the background color of the field
  final String? errorText;

  /// This provides a useful builder to be used instead of the default one, useful if you would like to customize the style of the trailing icon on each option
  final SelectTrailingBuilder? trailingBuilder;

  /// Radius of the field
  final BorderRadius? borderRadius;

  /// If you would like to render this field with background color
  final bool? filled;
  final bool enableSearch;

  /// The [Color] to be used instead of the default one which is inherited from an [InputTheme]
  final Color? fillColor;

  /// By default, specifying a fillColor will only apply on default state of th field, but will not override when this field change to an error state or disabled
  /// this change that behavior
  final bool overrideColor;

  /// If you would like to provide feedback for this field you can do so
  final List<InputFeedbackText>? feedback;

  /// Define max width constraints for this field
  /// by default this going to fallback on Theme.decorationInputTheme.constraints
  final double? maxWidth;

  /// Provide a custom text style to be used on the field title
  final TextStyle? titleStyle;

  /// Provide a custom text style to be used on the option list tiles
  final TextStyle? optionTileStyle;

  /// Provide a way to customize default field content padding
  final EdgeInsets fieldPadding;

  // Provide a callback to that is going to be called on internal search text change
  final void Function(String searchText)? onSearchChange;

  // Provide a custom hintText to be used on filter field
  final String? searchHintText;

  // Define if collapsed state of field
  // Defaults to [true]
  final bool collapsed;

  // Configure if it should auto-focus when uncollapsed
  // Defaults to [true]
  final bool autoFocusOnExpanded;

  // This callback will be called whenever the transition between collapsed states ends
  final void Function(SelectFieldCollapseState state)? onTransitionEnds;

  // Provide a custom builder for the default icon(chevron)
  final Widget Function(BuildContext context, Widget defaultChild, bool expanded)? iconBuilder;

  // Configure if dropdown should be closed when loosing it's focus
  // defaults to [true]
  final bool closeOnUnfocus;

  // Provides a way to add a footer to the options menu
  // useful to add a legend or a custom menu entry that the user can interact with
  final Widget Function(BuildContext context)? optionsFooterBuilder;

  /// Called, as needed, to build list item separator.
  /// By default this just adds a divider
  final AnimatedStateListItemBuilder<T>? optionsSeparatorBuilder;

  const SelectFieldBase({
    Key? key,
    // this.initialValue,
    // this.onSelected,
    this.label,
    this.labelStyle,
    this.enabled = true,
    this.unselectedString,
    this.duration = const Duration(milliseconds: 300),
    this.scrollController,
    required this.options,
    this.closeOnSelect = true,
    this.errorText,
    this.trailingBuilder,
    this.borderRadius,
    this.filled,
    this.enableSearch = false,
    this.fillColor,
    this.overrideColor = false,
    this.feedback,
    this.maxWidth,
    this.titleStyle,
    this.optionTileStyle,
    this.fieldPadding = const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
    this.onSearchChange,
    this.searchHintText,
    this.collapsed = true,
    this.autoFocusOnExpanded = true,
    this.onTransitionEnds,
    this.iconBuilder,
    this.closeOnUnfocus = true,
    this.optionsFooterBuilder,
    this.optionsSeparatorBuilder,
  })  : assert(
          overrideColor && fillColor != null || !overrideColor,
          "You must provide a color in order to set 'overrideColor' as true",
        ),
        super(key: key);
}
