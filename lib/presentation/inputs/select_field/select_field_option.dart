// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/widgets.dart';

class SelectFieldOption<T> {
  final String title;
  final T value;
  final Widget? trailing;

  const SelectFieldOption({
    required this.title,
    required this.value,
    this.trailing,
  });

  @override
  bool operator ==(covariant SelectFieldOption<T> other) {
    if (identical(this, other)) return true;

    return other.title == title && other.value == value && other.trailing == trailing;
  }

  @override
  int get hashCode => title.hashCode ^ value.hashCode ^ trailing.hashCode;
}
