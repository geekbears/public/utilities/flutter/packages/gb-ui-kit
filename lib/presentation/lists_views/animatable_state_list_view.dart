// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gb_ui_kit/presentation/presentation.dart';

/// A useful widget to render animated list views, it handles insert and remove animations based on input data changes

class AnimatableStateListView<T> extends StatefulWidget {
  /// The array of elements that will be used to render data
  final Iterable<T> items;

  /// Specifies a different animation duration on item add/remove transition
  /// defaults to 400ms
  final Duration animationDuration;

  /// Specifies a callback that will be used to extract the id of each item,this will be used
  /// to keep track of item identity
  final IdMapper<T> idMapper;

  /// This property let's you pass a custom function that will be used to determine if the data
  /// have changed , since a deep equality check needs to be performed on the iterable it may be
  /// expensive letting the default comparator run, so here you can override it
  final bool Function(T previous, T current)? customComparator;

  ///
  /// Called, as needed, to build list item widgets.
  /// List items are only built when they're scrolled into view.
  final AnimatedStateListItemBuilder<T> itemBuilder;

  ///
  /// Called, as needed, to build list item separator.
  /// By default this just adds a divider
  final AnimatedStateListItemBuilder<T>? separatorBuilder;

  ///
  /// Control if this animated list should be renderer as a Sliver or normal AnimatedListView
  /// This could be helpful if you want to place the list with other slivers
  /// defaults to [false]
  final bool asSliver;

  ///
  /// Pass a custom scroll controller for the list-view, take into account
  /// that this is ignored if your are requesting an sliver
  final ScrollController? controller;

  const AnimatableStateListView({
    Key? key,
    required this.items,
    this.animationDuration = const Duration(milliseconds: 400),
    required this.idMapper,
    this.customComparator,
    required this.itemBuilder,
    this.separatorBuilder,
    this.asSliver = false,
    this.controller,
  })  : assert(controller == null || asSliver == false, "You cannot pass a controller if requesting as a sliver"),
        super(key: key);

  // final _IdMapperFunction<T> _mFn;

  @override
  State<AnimatableStateListView> createState() => _AnimatableStateListViewState<T>(
        itemBuilder: itemBuilder,
        idMapper: idMapper,
        separatorBuilder: separatorBuilder,
      );
}

class _AnimatableStateListViewState<T> extends State<AnimatableStateListView> {
  final GlobalKey<AnimatedListState> _listViewKey = GlobalKey();
  final GlobalKey<SliverAnimatedListState> _sliverViewKey = GlobalKey();
  int? _initialListCount;
  // final _ItemBuilderFunction<T> fn();
  final AnimatedStateListItemBuilder<T> itemBuilder;
  final AnimatedStateListItemBuilder<T>? separatorBuilder;
  final IdMapper<T> idMapper;

  _AnimatableStateListViewState({
    required this.itemBuilder,
    required this.idMapper,
    required this.separatorBuilder,
  });

  @override
  void initState() {
    if (widget.items.isNotEmpty) {
      _initialListCount = widget.items.length;
    }

    super.initState();
  }

  @override
  void didUpdateWidget(covariant AnimatableStateListView<T> oldWidget) {
    final bool isEqual = widget.customComparator?.call(oldWidget.items, widget.items) ??
        listEquals(oldWidget.items.toList(), widget.items.toList());
    if (!isEqual) {
      // // Remove item from the list
      List<String> _currentIds = widget.items.map((e) {
        final closure = idMapper;
        return closure(e);
      }).toList();
      // Add new item to the list
      List<String> _previousIds = oldWidget.items.map(
        (e) {
          return idMapper(e);
        },
      ).toList();

      List<T> _internalCurrentState = oldWidget.items.toList(growable: true);

      // if previous state was empty, we just add all new items
      if (_internalCurrentState.isEmpty && widget.items.isNotEmpty) {
        for (var i = 0; i < widget.items.length; i++) {
          _insertOnListState(i);
        }
      } else {
        // Let's create a mutable array of items that represent the current listView state, which begin with previous state
        for (var i = 0; i < max(_internalCurrentState.length, widget.items.length); i++) {
          // check if index is outbounding current length
          if (i >= widget.items.length) {
            _removeOnListState(i, _internalCurrentState.elementAt(i));
            _internalCurrentState.removeAt(i);
            i--;
            continue;
          } else if (i >= _internalCurrentState.length) {
            _insertOnListState(i);
            _internalCurrentState.insert(i, widget.items.elementAt(i));
            continue;
          }

          final T elementAtCurrentIndex = widget.items.elementAt(i);
          final T elementAtPreviousIndex = _internalCurrentState.elementAt(i);
          // let's check if we have a modified item
          if (idMapper(elementAtCurrentIndex) != idMapper(elementAtPreviousIndex)) {
            // if so, let's validate if this item was already in the previous state
            if (_previousIds.contains(idMapper(elementAtCurrentIndex))) {
              // if so , this means that an intermediary value was removed, let's remove
              _removeOnListState(i, elementAtPreviousIndex);
              // lets update the internalMirror
              _internalCurrentState.removeAt(i);
              // since we removed a value, let's re-iterate the same index
              i--;
            } else {
              _insertOnListState(i);
              _internalCurrentState.insert(i, elementAtCurrentIndex);
            }
          } // else nothing happened
        }
      }

      if (_initialListCount == null && widget.items.isNotEmpty) {
        _initialListCount = widget.items.length;
      }
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    GBThemeData gbTheme = GBTheme.of(context);

    if (widget.asSliver) {
      return SliverAnimatedList(
        key: _sliverViewKey,
        initialItemCount: _initialListCount ?? 0,
        itemBuilder: (context, index, animation) {
          if (index >= widget.items.length) return SizedBox.shrink();
          T item = widget.items.elementAt(index);
          return buildItem(context, index, animation, gbTheme, item);
        },
      );
    }

    return AnimatedList(
      key: _listViewKey,
      controller: widget.controller,
      initialItemCount: _initialListCount ?? 0,
      // shrinkWrap: _properties.shrinkWrap,
      // padding: _properties.padding,
      itemBuilder: (context, index, animation) {
        if (index >= widget.items.length) return SizedBox.shrink();
        T item = widget.items.elementAt(index);
        return buildItem(context, index, animation, gbTheme, item);
      },
    );
  }

  Widget buildItem(
    BuildContext context,
    int index,
    Animation<double> animation,
    GBThemeData gbTheme,
    T item,
  ) {
    if (item == null) return SizedBox.shrink();
    return Column(
      children: [
        _AnimatedItem(
          animation: animation.drive(
            CurveTween(
              curve: Curves.easeInOut,
            ),
          ),
          animate: false,
          child: itemBuilder(context, index, animation, item),
        ),
        if (index < widget.items.length - 1)
          separatorBuilder?.call(context, index, animation, item) ??
              Center(
                child: Divider(
                  height: 0,
                  color: gbTheme.backgroundFocus.withOpacity(.6),
                  indent: 16,
                  endIndent: 16,
                  // indent: 71,
                ),
              ),
      ],
    );
  }

  _insertOnListState(int index) {
    if (widget.asSliver) {
      (_sliverViewKey).currentState?.insertItem(
            index,
            duration: widget.animationDuration,
          );
    } else {
      (_listViewKey).currentState?.insertItem(
            index,
            duration: widget.animationDuration,
          );
    }
  }

  void _removeOnListState(int index, T item) {
    if (widget.asSliver) {
      _sliverViewKey.currentState?.removeItem(
        index,
        (context, animation) {
          GBThemeData gbTheme = GBTheme.of(context);
          return buildItem(
            context,
            index,
            animation,
            gbTheme,
            item,
          );
        },
        duration: widget.animationDuration,
      );
    } else {
      _listViewKey.currentState?.removeItem(
        index,
        (context, animation) {
          GBThemeData gbTheme = GBTheme.of(context);
          return buildItem(
            context,
            index,
            animation,
            gbTheme,
            item,
          );
        },
        duration: widget.animationDuration,
      );
    }
  }
}

class _AnimatedItem extends StatelessWidget {
  const _AnimatedItem({
    Key? key,
    required this.animation,
    required this.animate,
    required this.child,
  }) : super(key: key);

  final Animation<double> animation;
  final bool animate;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    Animation<Offset> offset;

    if (!animate) {
      offset = Tween<Offset>(
        begin: const Offset(-1, 0),
        end: const Offset(0, 0),
      ).animate(animation);
    } else {
      offset = Tween<Offset>(
        begin: const Offset(-1, 0),
        end: const Offset(0, 0),
      ).animate(animation);
    }

    return SizeTransition(
      axis: Axis.vertical,
      axisAlignment: -1,
      sizeFactor: animation,
      child: SlideTransition(
        position: offset,
        child: ScaleTransition(
          alignment: Alignment.topCenter,
          child: child,
          scale: animation.drive(
            Tween(
              begin: .6,
              end: 1,
            ),
          ),
        ),
      ),
      // child: SlideTransition(
      //   position: offset,
      //   child: child,
      // ),
    );
  }
}

// necessary class to preserve Function Type, fkin dart
// class _IdMapperFunction<T> {
//   final String Function(T item) _fn;
//   _IdMapperFunction(
//     this._fn,
//   );

//   String call(T item) {
//     return _fn(item);
//   }
// }

// class _ItemBuilderFunction<T> {
//   final AnimatedStateListItemBuilder<T> fn;
//   _ItemBuilderFunction(
//     this.fn,
//   );

//   Widget call(BuildContext context, int index, Animation animation, T item) {
//     return fn(context, index, animation, item);
//   }
// }

typedef AnimatedStateListItemBuilder<T> = Widget Function(
  BuildContext context,
  int index,
  Animation<double> animation,
  T item,
);

typedef IdMapper<T> = String Function(T item);
