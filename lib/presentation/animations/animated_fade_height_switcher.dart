// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/widgets.dart';
import 'package:sliver_tools/sliver_tools.dart';

// This widget helps switching child elements with a fade + height transition
// adding smoothness to the UI

enum AnimatedFadeHeightSwitcherEffect {
  size,
  fade,
}

class AnimatedFadeHeightSwitcher extends StatelessWidget {
  final Duration duration;
  final Widget child;
  final Curve curve;
  final double sizeAxisAlignment;
  final Axis sizeAxis;
  final Set<AnimatedFadeHeightSwitcherEffect> effects;
  final bool forSliver;

  const AnimatedFadeHeightSwitcher({
    Key? key,
    this.duration = const Duration(milliseconds: 350),
    required this.child,
    this.curve = Curves.easeOut,
    this.sizeAxisAlignment = -1,
    this.sizeAxis = Axis.vertical,
    this.effects = const {AnimatedFadeHeightSwitcherEffect.fade, AnimatedFadeHeightSwitcherEffect.size},
    this.forSliver = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (effects.isEmpty) {
      throw Exception("Please provide at least one effect for $this");
    }
    return AnimatedSwitcher(
      duration: duration,
      layoutBuilder: forSliver ? defaultSliverLayoutBuilder : AnimatedSwitcher.defaultLayoutBuilder,
      transitionBuilder: (child, animation) {
        Widget? _child;
        if (effects.contains(AnimatedFadeHeightSwitcherEffect.size)) {
          if (forSliver) {
            debugPrint("AnimatedFadeHeightSwitcherEffect ${AnimatedFadeHeightSwitcherEffect.size} not supported on sliver");
            //  _child = SliverFadeTransition(
            //   sizeFactor: animation.drive(
            //     CurveTween(curve: curve),
            //   ),
            //   axis: Axis.vertical,
            //   axisAlignment: -1,
            //   child: child,
            // );
          } else {
            _child = SizeTransition(
              sizeFactor: animation.drive(
                CurveTween(curve: curve),
              ),
              axis: Axis.vertical,
              axisAlignment: -1,
              child: child,
            );
          }
        }
        if (effects.contains(AnimatedFadeHeightSwitcherEffect.fade)) {
          if (forSliver) {
            _child = SliverFadeTransition(
              opacity: animation.drive(
                CurveTween(curve: curve),
              ),
              sliver: child,
            );
          } else {
            _child = FadeTransition(
              opacity: animation.drive(
                CurveTween(curve: curve),
              ),
              child: child,
            );
          }
        }
        return _child ?? child;
      },
      child: child,
    );
  }

  static Widget defaultSliverLayoutBuilder(Widget? currentChild, List<Widget> previousChildren) {
    return SliverStack(
      children: <Widget>[
        ...previousChildren,
        if (currentChild != null) currentChild,
      ],
    );
  }

  /// A helper UI that acts as a shortcut when implementing a widget that you want to either display or not
  /// performing this widget transition
  factory AnimatedFadeHeightSwitcher.visible({
    required bool visible,
    Duration duration = const Duration(milliseconds: 350),
    required Widget child,
    bool forSliver = false,
    Curve? curve,
    double sizeAxisAlignment = -1,
    Axis sizeAxis = Axis.vertical,
    Set<AnimatedFadeHeightSwitcherEffect> effects = const {
      AnimatedFadeHeightSwitcherEffect.fade,
      AnimatedFadeHeightSwitcherEffect.size
    },
  }) {
    return AnimatedFadeHeightSwitcher(
      child: visible ? child : const SizedBox.shrink(),
      duration: duration,
      curve: Curves.easeOut,
      sizeAxisAlignment: sizeAxisAlignment,
      forSliver: forSliver,
      sizeAxis: sizeAxis,
      effects: effects,
    );
  }
}
