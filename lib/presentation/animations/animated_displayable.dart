import 'package:flutter/widgets.dart';

import './animated_fade_height_switcher.dart';

// This widget helps displaying and hiding elements with a fade + height transition
// adding smoothness to the UI

class AnimatedDisplayable extends StatelessWidget {
  final bool display;
  final Duration duration;
  final Widget child;
  final bool forSliver;
  final Set<AnimatedFadeHeightSwitcherEffect> effects;

  AnimatedDisplayable({
    Key? key,
    required this.display,
    required this.child,
    this.duration = const Duration(milliseconds: 350),
    this.forSliver = false,
    this.effects = const {AnimatedFadeHeightSwitcherEffect.fade, AnimatedFadeHeightSwitcherEffect.size},
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedFadeHeightSwitcher(
      child: display ? child : SizedBox(height: 1),
      forSliver: forSliver,
    );
  }
}
