import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class AnimatedPlaceholder extends StatefulWidget {
  const AnimatedPlaceholder({
    Key? key,
    this.height = 16,
    this.width = 150,
    this.randomMaxWidth = 250,
    this.randomMaxHeight = 15,
    this.randomMinHeight = 10,
    this.randomMinWidth = 150,
    this.minHeightPlus = 10,
    this.minWidthPlus = 100,
    this.useRandomHeight = false,
    this.useRandomWidth = true,
    this.useFadeInRightAnimation = false,
    this.color,
    this.borderRadius,
  }) : super(key: key);

  final double height;
  final double width;
  final int randomMaxWidth;
  final int randomMaxHeight;

  /// The minimum random value applicable to the `height`
  final double randomMinHeight;

  /// The minimum random value applicable to the `width`
  final double randomMinWidth;

  /// The amount that will be added to the random width when it is minor than the `randomMinWidth`
  final double minWidthPlus;

  /// The ammount that will be added to the random height when it is minor than the `randomMinHeight`
  final double minHeightPlus;
  final bool useRandomHeight;
  final bool useRandomWidth;
  final bool useFadeInRightAnimation;
  final Color? color;
  final BorderRadius? borderRadius;

  @override
  State<AnimatedPlaceholder> createState() => _AnimatedPlaceholder();
}

class _AnimatedPlaceholder extends State<AnimatedPlaceholder> with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> opacity;
  late Animation<double> opacityOut;
  late Animation<Offset> position;
  late Random random;
  double randomHeight = 0;
  double randomWidth = 0;

  void _setSizes() {
    if (widget.useRandomHeight) {
      randomHeight = random.nextInt(widget.randomMaxHeight).toDouble();
      randomHeight = (randomHeight > widget.randomMinHeight) ? randomHeight : randomHeight + widget.minHeightPlus / 2;
    }

    if (widget.useRandomWidth) {
      randomWidth = random.nextInt(widget.randomMaxWidth).toDouble();
      randomWidth = (randomWidth > widget.randomMinWidth) ? randomWidth : randomWidth + widget.minWidthPlus / 2;
    }

    setState(() {});
  }

  void _listener() {
    if (_controller.status == AnimationStatus.completed) {
      _controller
        ..reset()
        ..forward();
      if (widget.useRandomHeight || widget.useRandomWidth) {
        _setSizes();
      }
    }
  }

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );

    opacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(0.0, 0.5),
    ));

    opacityOut = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(0.65, 0.95),
    ));

    final offset = (widget.useFadeInRightAnimation) ? Offset(30, 0) : Offset(0, 20);

    position = Tween<Offset>(
      begin: offset,
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(0.0, 0.5),
    ));

    random = Random();

    _controller.addListener(_listener);

    _setSizes();

    _controller.forward();

    super.initState();
  }

  @override
  void dispose() {
    _controller.removeListener(_listener);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return Opacity(
          opacity: opacity.value - opacityOut.value,
          child: Transform.translate(
            offset: position.value,
            child: child!,
          ),
        );
      },
      child: Container(
        height: (widget.useRandomHeight) ? randomHeight : this.widget.height,
        width: (widget.useRandomWidth) ? randomWidth : this.widget.width,
        decoration: BoxDecoration(
          color: widget.color ?? GBTheme.of(context).textDisabled.withOpacity(0.3),
          borderRadius: widget.borderRadius ?? BorderRadius.circular(8),
        ),
      ),
    );
  }
}
