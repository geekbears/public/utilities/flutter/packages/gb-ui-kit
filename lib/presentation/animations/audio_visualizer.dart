// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class MusicVisualizer extends StatelessWidget {
  /// Determines the number of lines that this widget will render
  final int linesCount;

  /// Set a list of colors to be picked based on the index of each rendered line
  final List<Color> colors;

  /// Define a number of duration that will be used to set each line rendered
  final List<int> durations;

  /// Define a maximum fractional height size
  final List<double> heights;

  /// Defines the with of each line rendered
  final double lineWidth;

  const MusicVisualizer({
    Key? key,
    this.linesCount = 10,
    this.colors = const [Colors.white, Colors.black],
    this.durations = const [900, 300, 800, 600, 400, 500],
    this.lineWidth = 10,
    this.heights = const [1, .6],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: new List<Widget>.generate(
        linesCount,
        (index) => _VisualComponent(
          duration: durations[index % durations.length],
          color: colors[index % colors.length],
          lineWidth: lineWidth,
          maxHeightFactor: heights[index % heights.length],
        ),
      ),
    );
  }
}

class _VisualComponent extends StatefulWidget {
  const _VisualComponent({
    Key? key,
    required this.duration,
    required this.color,
    required this.lineWidth,
    required this.maxHeightFactor,
  }) : super(key: key);
  final int duration;
  final Color color;
  final double lineWidth;
  final double maxHeightFactor;

  @override
  _VisualComponentState createState() => _VisualComponentState();
}

class _VisualComponentState extends State<_VisualComponent> with SingleTickerProviderStateMixin {
  late Animation<double> _animation;
  late AnimationController _animController;

  @override
  void initState() {
    super.initState();
    _animController = AnimationController(
      duration: Duration(
        milliseconds: widget.duration,
      ),
      vsync: this,
    );
    final curvedAnimation = CurvedAnimation(
      parent: _animController,
      curve: Curves.easeInCubic,
    );
    _animation = Tween<double>(begin: 0, end: widget.maxHeightFactor).animate(curvedAnimation)
      ..addListener(() {
        setState(() {});
      });
    _animController.repeat(reverse: true);
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      child: Container(
        width: widget.lineWidth,
        decoration: BoxDecoration(
          color: widget.color,
          borderRadius: BorderRadius.circular(
            5,
          ),
        ),
      ),
      heightFactor: _animation.value,
    );
  }
}
