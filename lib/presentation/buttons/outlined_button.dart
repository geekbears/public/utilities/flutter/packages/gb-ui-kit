import 'package:flutter/material.dart' as material;
import 'package:flutter/widgets.dart';

import '../../flutter_gb_ui_kit.dart';
import '../utils/loading_debounced_switcher.dart';
import 'button_progress_indicator.dart';

class OutlinedButton extends StatelessWidget {
  final Color? color;
  final Widget child;
  final EdgeInsets? padding;
  final bool? guessTextColor;
  final double? minWidth;
  final double? height;
  final void Function() onPressed;
  final material.BorderRadius? borderRadius;
  final bool busy;
  final Color? busyColor;
  final Duration? loadingDebounce;
  final bool disabled;
  final double? borderWidth;

  const OutlinedButton({
    Key? key,
    this.color,
    required this.child,
    this.padding,
    this.guessTextColor,
    this.minWidth = 80,
    this.height,
    required this.onPressed,
    this.borderRadius,
    this.busy = false,
    this.busyColor,
    this.loadingDebounce,
    this.disabled = false,
    this.borderWidth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GBThemeData theme = GBTheme.of(context);
    // if (theme == null) {
    //   throw Exception(
    //       "ThemeData can't be found on the rendering tree, please make sure you use this component Inside GBTheme Widget");
    // }
    final Color disabledColor = theme.backgroundDisabled;
    final Color? btnColor = disabled ? disabledColor : color;
    final borderRadius = this.borderRadius ?? theme.buttonsRadius;
    final outlinedButtonThemeStyle = theme.outlinedButtonTheme.style;
    return LoadingDebouncedBuilder(
      loading: busy,
      debounceTime: loadingDebounce,
      builder: (context, loadingState) {
        return material.OutlinedButton(
          onPressed: disabled ? null : onPressed,
          style: outlinedButtonThemeStyle?.copyWith(
            padding: padding != null ? material.WidgetStatePropertyAll(padding) : null,
            minimumSize: minWidth != null || height != null
                ? material.WidgetStatePropertyAll(
                    Size(
                      minWidth ?? double.minPositive,
                      height ?? double.minPositive,
                    ),
                  )
                : null,
            side: btnColor != null
                ? material.WidgetStateProperty.all(
                    material.BorderSide(
                      color: btnColor,
                      style: material.BorderStyle.solid,
                      width: borderWidth ?? 1.0,
                    ),
                  )
                : null,
            shape: borderRadius != null
                ? material.WidgetStatePropertyAll(
                    material.RoundedRectangleBorder(
                      borderRadius: borderRadius,
                    ),
                  )
                : null,
          ),
          // style: material.ButtonStyle(
          //   minimumSize: material.WidgetStateProperty.all(
          //     Size(minWidth ?? 80, height ?? 48),
          //   ),
          //   side: material.WidgetStateProperty.all(
          //     material.BorderSide(
          //       color: btnColor,
          //       style: material.BorderStyle.solid,
          //       width: borderWidth ?? 1.0,
          //     ),
          //   ),
          //   padding: material.WidgetStateProperty.all(
          //     padding ?? const material.EdgeInsets.all(14),
          //   ),
          //   shape: material.WidgetStateProperty.all(
          //     material.RoundedRectangleBorder(
          //       borderRadius: borderRadius ?? BorderRadius.zero,
          //     ),
          //   ),
          // ),
          child: LoadingDebouncedSwitcher(
            loading: busy,
            child: DefaultTextStyle(
              child: child,
              style: theme.textTheme.labelLarge!.copyWith(
                color: btnColor,
              ),
            ),
            loadingWidget: ButtonProgressIndicator(
              color: busyColor ?? btnColor,
            ),
            debounceTime: loadingDebounce,
          ),
        );
      },
    );
  }

  factory OutlinedButton.text(
    String text, {
    final Color? color,
    final double? minWidth,
    final double? height,
    final bool uppercase = true,
    bool busy = false,
    Color? busyColor,
    required void Function() onPressed,
    Duration? loadingDebounce,
    BorderRadius? borderRadius,
    bool? guessTextColor,
    EdgeInsets? padding,
    Key? key,
    bool disabled = false,
  }) {
    return OutlinedButton(
      color: color,
      child: GBText(uppercase ? text.toUpperCase() : text),
      minWidth: minWidth,
      height: height,
      onPressed: onPressed,
      busy: busy,
      busyColor: busyColor,
      loadingDebounce: loadingDebounce,
      borderRadius: borderRadius,
      guessTextColor: guessTextColor,
      padding: padding,
      key: key,
      disabled: disabled,
    );
  }

  factory OutlinedButton.large({
    Widget? child,
    String? text,
    final Color? color,
    final bool uppercase = true,
    bool busy = false,
    required void Function() onPressed,
    Color? busyColor,
    Duration? loadingDebounce,
    BorderRadius? borderRadius,
    bool? guessTextColor,
    EdgeInsets? padding,
    Key? key,
    bool disabled = false,
  }) {
    assert(child != null || text != null, "You must define either a child widget or  a text");
    if (text != null) {
      return OutlinedButton.text(
        text,
        color: color,
        minWidth: 340,
        height: 48,
        uppercase: uppercase,
        onPressed: onPressed,
        busy: busy,
        busyColor: busyColor,
        loadingDebounce: loadingDebounce,
        borderRadius: borderRadius,
        guessTextColor: guessTextColor,
        padding: padding,
        key: key,
        disabled: disabled,
      );
    }
    return OutlinedButton(
      color: color,
      child: child!,
      minWidth: 340,
      height: 48,
      onPressed: onPressed,
      busy: busy,
      busyColor: busyColor,
      loadingDebounce: loadingDebounce,
      borderRadius: borderRadius,
      guessTextColor: guessTextColor,
      padding: padding,
      key: key,
      disabled: disabled,
    );
  }

  static small({
    Widget? child,
    String? text,
    final Color? color,
    final bool uppercase = true,
    bool busy = false,
    Color? busyColor,
    required void Function() onPressed,
    Duration? loadingDebounce,
    BorderRadius? borderRadius,
    bool? guessTextColor,
    EdgeInsets? padding,
    Key? key,
    bool disabled = false,
  }) {
    assert(child != null || text != null, "You must define either a child widget or  a text");
    if (text != null) {
      return OutlinedButton.text(
        text,
        color: color,
        minWidth: 164,
        height: 48,
        uppercase: uppercase,
        onPressed: onPressed,
        busy: busy,
        busyColor: busyColor,
        loadingDebounce: loadingDebounce,
        borderRadius: borderRadius,
        guessTextColor: guessTextColor,
        padding: padding,
        key: key,
        disabled: disabled,
      );
    }
    return Container(
      constraints: BoxConstraints(maxWidth: 164),
      child: OutlinedButton(
        color: color,
        child: child!,
        minWidth: 164,
        height: 48,
        onPressed: onPressed,
        busy: busy,
        busyColor: busyColor,
        loadingDebounce: loadingDebounce,
        borderRadius: borderRadius,
        guessTextColor: guessTextColor,
        padding: padding,
        key: key,
        disabled: disabled,
      ),
    );
  }
}
