import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/presentation/theme/gb_contained_button_theme.dart';

import '../presentation.dart';
import '../utils/loading_debounced_switcher.dart';
import 'button_progress_indicator.dart';

class ContainedButton extends StatefulWidget {
  final WidgetStateProperty<Color?>? color;

  /// Colors used to apply a `LinearGradient`
  final List<WidgetStateProperty<Color?>>? colors;
  final Widget? child;
  final EdgeInsets? padding;
  final bool guessTextColor;
  final double? minWidth;
  final double? height;
  final void Function() onPressed;
  final BorderRadius? borderRadius;
  final bool busy;
  final Color? busyColor;
  final Duration? loadingDebounce;
  final bool disabled;

  const ContainedButton({
    Key? key,
    this.color,
    this.colors,
    this.child,
    this.padding,
    this.guessTextColor = true,
    this.minWidth,
    this.height,
    required this.onPressed,
    this.borderRadius,
    this.busy = false,
    this.busyColor,
    this.loadingDebounce,
    this.disabled = false,
  }) : super(key: key);

  @override
  State<ContainedButton> createState() => _ContainedButtonState();

  factory ContainedButton.text(
    String text, {
    final WidgetStateProperty<Color?>? color,
    final List<WidgetStateProperty<Color>>? colors,
    final double? minWidth,
    final double? height,
    final bool? uppercase,
    bool busy = false,
    Color? busyColor,
    required void Function() onPressed,
    Duration? loadingDebounce,
    BorderRadius? borderRadius,
    bool guessTextColor = true,
    EdgeInsets? padding,
    bool disabled = false,
    Key? key,
  }) {
    return ContainedButton(
      color: color,
      colors: colors,
      child: Builder(builder: (context) {
        final _uppercase = uppercase ?? ContainedButtonTheme.of(context).uppercase;
        return GBText(
          _uppercase ? text.toUpperCase() : text,
        );
      }),
      minWidth: minWidth,
      height: height,
      onPressed: onPressed,
      busy: busy,
      busyColor: busyColor,
      loadingDebounce: loadingDebounce,
      borderRadius: borderRadius,
      guessTextColor: guessTextColor,
      padding: padding,
      disabled: disabled,
      key: key,
    );
  }

  factory ContainedButton.large({
    Widget? child,
    String? text,
    final WidgetStateProperty<Color?>? color,
    final List<WidgetStateProperty<Color>>? colors,
    final bool? uppercase,
    bool busy = false,
    Color? busyColor,
    required void Function() onPressed,
    Duration? loadingDebounce,
    BorderRadius? borderRadius,
    bool guessTextColor = true,
    EdgeInsets? padding,
    bool disabled = false,
    Key? key,
  }) {
    assert(child == null || text == null);
    if (text != null) {
      return ContainedButton.text(
        text,
        color: color,
        colors: colors,
        minWidth: 340,
        height: 48,
        uppercase: uppercase,
        onPressed: onPressed,
        busy: busy,
        busyColor: busyColor,
        loadingDebounce: loadingDebounce,
        borderRadius: borderRadius,
        guessTextColor: guessTextColor,
        padding: padding,
        disabled: disabled,
        key: key,
      );
    }
    return ContainedButton(
      color: color,
      colors: colors,
      child: child,
      minWidth: 340,
      height: 48,
      onPressed: onPressed,
      busy: busy,
      busyColor: busyColor,
      borderRadius: borderRadius,
      guessTextColor: guessTextColor,
      padding: padding,
      disabled: disabled,
      key: key,
    );
  }

  static small({
    Widget? child,
    String? text,
    final WidgetStateProperty<Color?>? color,
    final List<WidgetStateProperty<Color>>? colors,
    final bool? uppercase,
    bool busy = false,
    Color? busyColor,
    required void Function() onPressed,
    Duration? loadingDebounce,
    BorderRadius? borderRadius,
    bool guessTextColor = true,
    EdgeInsets? padding,
    bool disabled = false,
    Key? key,
  }) {
    assert(child == null || text == null);
    if (text != null) {
      return ContainedButton.text(
        text,
        color: color,
        colors: colors,
        minWidth: 164,
        height: 48,
        uppercase: uppercase,
        onPressed: onPressed,
        busy: busy,
        busyColor: busyColor,
        loadingDebounce: loadingDebounce,
        borderRadius: borderRadius,
        guessTextColor: guessTextColor,
        padding: padding,
        disabled: disabled,
        key: key,
      );
    }
    return Container(
      constraints: BoxConstraints(maxWidth: 164),
      child: ContainedButton(
        color: color,
        colors: colors,
        child: child,
        minWidth: 164,
        height: 48,
        onPressed: onPressed,
        busy: busy,
        busyColor: busyColor,
        loadingDebounce: loadingDebounce,
        borderRadius: borderRadius,
        guessTextColor: guessTextColor,
        padding: padding,
        disabled: disabled,
        key: key,
      ),
    );
  }
}

class _ContainedButtonState extends State<ContainedButton> {
  bool _hovered = false;
  bool _focused = false;

  Set<WidgetState> get _states {
    Set<WidgetState> _states = Set<WidgetState>();
    if (_hovered) {
      _states.add(WidgetState.hovered);
    }
    if (_focused) {
      _states.add(WidgetState.focused);
    }
    if (widget.disabled) {
      _states.add(WidgetState.disabled);
    }
    return _states;
  }

  @override
  Widget build(BuildContext context) {
    final GBThemeData? themeData = GBTheme.of(context);
    if (themeData == null) {
      throw Exception(
          "ThemeData can't be found on the rendering tree, please make sure you use this component Inside GBTheme Widget");
    }
    if (widget.color != null && widget.colors != null) {
      throw Exception(
          'Just one property for the button color can be applied, it could be the property color or colors but not both');
    }

    final List<WidgetStateProperty<Color?>> _colors = widget.colors ??
        themeData.containedButtonTheme.colors ??
        [
          widget.color ??
              themeData.containedButtonTheme.style.backgroundColor ??
              WidgetStatePropertyAll(themeData.primaryColor),
        ];
    final WidgetStateProperty<Color?> btnColor = _colors.first;
    final double luminance = widget.guessTextColor ? btnColor.resolve(_states)?.computeLuminance() ?? 0 : 0;
    final Color? textColor = widget.guessTextColor
        ? luminance > .5
            ? themeData.textTheme.labelLarge!.color
            : themeData.textWhite
        : themeData.textTheme.labelLarge!.color;
    final BorderRadius? borderRadius = this.widget.borderRadius ?? themeData.buttonsRadius;
    final containedButtonThemeStyle = themeData.containedButtonTheme.style;
    return LoadingDebouncedBuilder(
      loading: widget.busy,
      debounceTime: widget.loadingDebounce,
      builder: (context, loadingState) {
        return DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: borderRadius,
            gradient: (_colors.length > 1)
                ? LinearGradient(
                    colors: (_colors as List<WidgetStateProperty<Color>>).map<Color>((e) {
                      return e.resolve(_states);
                    }).toList(),
                  )
                : null,
          ),
          child: TextButton(
            onPressed: widget.disabled || loadingState ? null : widget.onPressed,
            onFocusChange: (focus) {
              setState(() {
                _focused = focus;
              });
            },
            onHover: (hover) {
              setState(() {
                _hovered = hover;
              });
            },
            child: LoadingDebouncedSwitcher(
              loading: widget.busy,
              debounceTime: widget.loadingDebounce,
              loadingWidget: ButtonProgressIndicator(
                color: widget.busyColor ?? textColor,
              ),
              child: DefaultTextStyle(
                style: themeData.textTheme.labelLarge!.copyWith(
                  color: textColor,
                ),
                child: widget.child!,
              ),
            ),
            style: containedButtonThemeStyle.copyWith(
              shape: widget.borderRadius != null
                  ? WidgetStateProperty.all<OutlinedBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadiusDirectional.only(
                          topEnd: widget.borderRadius?.topRight != null
                              ? Radius.elliptical(widget.borderRadius!.topRight.x, widget.borderRadius!.topRight.y)
                              : Radius.zero,
                          bottomEnd: widget.borderRadius?.topRight != null
                              ? Radius.elliptical(
                                  widget.borderRadius!.bottomRight.x, widget.borderRadius!.bottomRight.y)
                              : Radius.zero,
                          topStart: widget.borderRadius?.topRight != null
                              ? Radius.elliptical(widget.borderRadius!.topLeft.x, widget.borderRadius!.topLeft.y)
                              : Radius.zero,
                          bottomStart: widget.borderRadius?.topRight != null
                              ? Radius.elliptical(widget.borderRadius!.bottomLeft.x, widget.borderRadius!.bottomLeft.y)
                              : Radius.zero,
                        ),
                      ),
                    )
                  : containedButtonThemeStyle.shape,
              minimumSize: widget.minWidth != null || widget.height != null
                  ? WidgetStatePropertyAll(
                      Size(widget.minWidth ?? double.minPositive, widget.height ?? double.minPositive))
                  : containedButtonThemeStyle.minimumSize,
              maximumSize: containedButtonThemeStyle.maximumSize,
              fixedSize: containedButtonThemeStyle.fixedSize,
              backgroundColor: _colors.length > 1
                  ? WidgetStateProperty.all<Color>(
                      Colors.transparent,
                    )
                  : _colors.first,
            ),
          ),
        );
      },
    );
  }
}
