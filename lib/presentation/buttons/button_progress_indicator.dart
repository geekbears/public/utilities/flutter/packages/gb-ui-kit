import 'package:flutter/material.dart';

class ButtonProgressIndicator extends StatelessWidget {
  final Color? color;
  const ButtonProgressIndicator({Key? key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 18,
      width: 18,
      child: FittedBox(
        fit: BoxFit.contain,
        child: CircularProgressIndicator(
          backgroundColor: this.color,
        ),
      ),
    );
  }
}
