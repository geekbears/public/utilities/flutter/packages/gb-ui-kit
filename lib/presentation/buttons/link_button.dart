import 'package:flutter/material.dart';

import '../presentation.dart';
import '../utils/loading_debounced_switcher.dart';
import 'button_progress_indicator.dart';

class LinkButton extends StatelessWidget {
  final void Function() onPressed;
  final Widget child;
  final double minWidth;
  final double height;
  final Color? color;
  final bool withInk;
  final bool busy;
  final Color? busyColor;
  final Duration? loadingDebounce;
  final bool disabled;

  const LinkButton({
    Key? key,
    required this.onPressed,
    required this.child,
    this.minWidth = 74,
    this.height = 56,
    this.color,
    this.withInk = true,
    this.busy = false,
    this.busyColor,
    this.loadingDebounce,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final baseStyle = theme.textTheme.labelLarge!;
    final Color defaultColor = color ?? theme.primaryColor;
    final bool isGBTheme = theme is GBThemeData;

    final ButtonStyle flatButtonStyle = (theme.textButtonTheme.style ??
        TextButton.styleFrom(
          minimumSize: Size(minWidth, height),
          maximumSize: Size(double.maxFinite, height),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          foregroundColor: defaultColor,
          // backgroundColor: baseStyle.color,
          padding: EdgeInsets.all(4),
          textStyle: baseStyle.copyWith(
            decoration: TextDecoration.underline,
          ),
        ).copyWith(
          overlayColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
            // if (states.contains(MaterialState.focused)) return Colors.red;
            // if (states.contains(MaterialState.hovered)) return Colors.green;
            // if (states.contains(MaterialState.pressed)) return Colors.blue;
            // return Colors.transparent; // Defer to the widget's default.
            if (withInk) {
              return defaultColor.withAlpha(40);
            }
            return Colors.transparent;
          }),
          surfaceTintColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
            // if (withInk) {
            // return defaultColor;
            return Colors.transparent;
            // }
            // if (states.contains(MaterialState.focused)) return Colors.red;
            // if (states.contains(MaterialState.hovered)) return Colors.green;
            // if (states.contains(MaterialState.pressed)) return Colors.blue;
            // return Colors.transparent; // Defer to the widget's default.
          }),
          backgroundColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
            // if (states.contains(MaterialState.focused)) return Colors.red;
            // if (states.contains(MaterialState.hovered)) return Colors.green;
            // if (states.contains(MaterialState.pressed)) return Colors.blue;
            return Colors.transparent; // Defer to the widget's default.
          }),
        ));

    return LoadingDebouncedBuilder(
      loading: busy,
      debounceTime: loadingDebounce,
      builder: (context, loadingState) {
        return TextButton(
          // minWidth: minWidth,
          // height: height,
          style: flatButtonStyle,
          onPressed: disabled || loadingState ? null : onPressed,
          // splashColor: Colors.transparent,
          // highlightColor: withInk ? null : Colors.transparent,
          // disabledColor: disabledColor,
          // color: color ?? defaultColor,
          // textColor: disabled ? disabledColor : defaultColor,
          child: LoadingDebouncedSwitcher(
            loading: busy,
            debounceTime: loadingDebounce,
            child: DefaultTextStyle(
              style: (flatButtonStyle.textStyle?.resolve(Set.from([])) ?? baseStyle).copyWith(
                color: defaultColor,
              ),
              child: child,
            ),
            loadingWidget: ButtonProgressIndicator(
              color: busyColor ?? defaultColor,
            ),
          ),
          // textTheme: ButtonTextTheme.normal,
        );
      },
    );
  }

  static text(
    String text, {
    required void Function() onPressed,
    Widget? prefix,
    Widget? trail,
    bool uppercase = true,
    bool busy = false,
    Color? busyColor,
    Duration? loadingDebounce,
    Color? color,
    double hegiht = 56,
    double minWidth = 74,
    bool withInk = true,
    bool disabled = false,
    Key? key,
  }) {
    return LinkButton(
      onPressed: onPressed,
      disabled: disabled,
      busy: busy,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (prefix != null)
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: prefix,
            ),
          Text(
            uppercase ? text.toUpperCase() : text,
          ),
          if (trail != null)
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: trail,
            ),
        ],
      ),
      busyColor: busyColor,
      loadingDebounce: loadingDebounce,
      color: color,
      height: hegiht,
      minWidth: minWidth,
      withInk: withInk,
      key: key,
    );
  }
}
