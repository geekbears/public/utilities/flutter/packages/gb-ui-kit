import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/presentation/buttons/button_progress_indicator.dart';

import '../../flutter_gb_ui_kit.dart';

class ComposedActionText extends StatefulWidget {
  final Function onAction;

  final bool uppercaseAction;

  final String normalText;
  final TextStyle? normalStyle;

  final String actionText;
  final TextStyle? actionStyle;
  final bool disabled;
  final bool busy;
  final TextAlign textAlign;

  const ComposedActionText({
    Key? key,
    required this.onAction,
    this.uppercaseAction = true,
    required this.normalText,
    this.normalStyle,
    required this.actionText,
    this.actionStyle,
    this.disabled = false,
    this.busy = false,
    this.textAlign = TextAlign.start,
  }) : super(key: key);

  @override
  State<ComposedActionText> createState() => _ComposedActionTextState();
}

class _ComposedActionTextState extends State<ComposedActionText> {
  late Color defaultNormalColor;
  late Color defaultActionColor;
  late Color disabledColor;

  Color? previousBackgroundColor;

  setDefaultColors() {
    final Color backgroundColor =
        Scaffold.maybeOf(context)?.widget.backgroundColor ?? GBTheme.maybeOf(context)?.backgroundBase ?? Colors.white;

    if (previousBackgroundColor != backgroundColor) {
      final lumen = backgroundColor.computeLuminance();
      defaultNormalColor = lumen > .5 ? GBTheme.of(context).textBlack : GBTheme.of(context).textWhite;
      defaultActionColor = lumen > .5 ? GBTheme.of(context).accentColor : GBTheme.of(context).textWhite;
    }

    disabledColor = GBTheme.maybeOf(context)?.disabledColor ??
        GBTheme.maybeOf(context)?.textDisabled ??
        Theme.of(context).disabledColor;

    previousBackgroundColor = backgroundColor;
  }

  @override
  Widget build(BuildContext context) {
    setDefaultColors();
    return RichText(
      textAlign: widget.textAlign,
      text: TextSpan(
        text: widget.normalText,
        style: TextStyle(
          color: defaultNormalColor,
        ).merge(widget.normalStyle),
        children: [
          if (!widget.busy)
            TextSpan(
              text: " " + (widget.uppercaseAction ? widget.actionText.toUpperCase() : widget.actionText),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  widget.onAction.call();
                },
              style: GBTheme.of(context)
                      .textTheme
                      .labelLarge
                      ?.copyWith(
                        color: widget.disabled ? disabledColor : defaultActionColor,
                      )
                      .merge(
                        widget.actionStyle,
                      ) ??
                  widget.actionStyle?.copyWith(
                    color: widget.actionStyle?.color ?? defaultActionColor,
                  ),
            )
          else
            WidgetSpan(
              child: Container(
                margin: EdgeInsets.only(left: 8),
                child: ButtonProgressIndicator(
                  color: defaultActionColor,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
