import 'package:flutter/material.dart';

import '../../flutter_gb_ui_kit.dart';

enum InputFeedbackType {
  simple,
  icon,
}

class InputFeedbackText extends StatefulWidget {
  final InputFeedbackType type;
  final bool active;
  final Color? color;
  final Widget? icon;
  final Widget text;

  const InputFeedbackText({
    required Key key,
    required this.type,
    required this.active,
    this.color,
    this.icon,
    required this.text,
  })  : assert(
          InputFeedbackType.icon != type || icon != null,
        ),
        super(key: key);

  @override
  State<InputFeedbackText> createState() => _InputFeedbackTextState();

  factory InputFeedbackText.check({
    required Key key,
    required bool active,
    required Widget text,
    Color? color,
  }) =>
      InputFeedbackText(
        key: key,
        type: InputFeedbackType.icon,
        icon: Icon(
          Icons.check,
        ),
        text: text,
        active: active,
        color: color,
      );

  factory InputFeedbackText.error({
    required Key key,
    required BuildContext context,
    required bool active,
    required Widget text,
    Color? color,
  }) =>
      InputFeedbackText(
        key: key,
        type: InputFeedbackType.icon,
        icon: Icon(
          Icons.close,
        ),
        text: text,
        active: active,
        color: color ?? GBTheme.of(context).colorScheme.error,
      );
}

class _InputFeedbackTextState extends State<InputFeedbackText> {
  double _colorLuminicence = 0;

  Color get _color {
    if (widget.color != null) {
      return widget.color!;
    }
    GBThemeData theme = GBTheme.of(context);
    final Color defaultColor = widget.color ?? theme.accentColor;
    return defaultColor;
  }

  Color get _contrastColor {
    return _colorLuminicence > .5 ? Colors.black : Colors.white;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      calculateLuminicence();
    });
  }

  @override
  void didUpdateWidget(InputFeedbackText oldWidget) {
    if (oldWidget.color != widget.color) {
      calculateLuminicence();
    }
    super.didUpdateWidget(oldWidget);
  }

  calculateLuminicence() {
    _colorLuminicence = _color.computeLuminance();
  }

  @override
  Widget build(BuildContext context) {
    GBThemeData theme = GBTheme.of(context);
    final double iconContainerSize = 16;

    return Row(
      children: [
        if (widget.type == InputFeedbackType.icon)
          AnimatedContainer(
            duration: Duration(milliseconds: 450),
            width: iconContainerSize,
            height: iconContainerSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                iconContainerSize / 2,
              ),
              border: Border.all(
                color: _color,
                width: 1,
              ),
              color: widget.active ? _color : Colors.transparent,
            ),
            child: Center(
              child: IconTheme(
                data: IconThemeData(
                  size: iconContainerSize - 4,
                  color: widget.active ? _contrastColor : _color,
                ),
                child: widget.active ? widget.icon! : SizedBox(),
              ),
            ),
          ),
        Flexible(
          fit: FlexFit.loose,
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: DefaultTextStyle(
              child: widget.text,
              style: (theme.textTheme.bodySmall ?? TextStyle()).copyWith(
                color: _color,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
