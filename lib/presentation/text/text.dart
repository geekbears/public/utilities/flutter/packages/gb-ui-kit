import 'package:flutter/widgets.dart';

import '../presentation.dart';

export 'composed_action_text.dart';
export 'input_feedback_container.dart';
export 'input_feedback_text.dart';

class GBText extends StatelessWidget {
  final String? text;
  final TextAlign? textAlign;
  final TextStyle? style;
  final int? maxLines;

  const GBText(
    this.text, {
    Key? key,
    this.textAlign,
    this.style,
    this.maxLines,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      this.text!,
      textAlign: textAlign,
      style: style,
      maxLines: maxLines,
    );
  }

  static Widget h1(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.displayLarge!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.displayLarge!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.displayLarge!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.displayLarge!.merge(style),
    );
  }

  static Widget h2(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.displayLarge!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.displayLarge!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.displayLarge!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.displayLarge!.merge(style),
    );
  }

  static Widget h3(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.displayMedium!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.displayMedium!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.displayMedium!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.displayMedium!.merge(style),
    );
  }

  static Widget h4(
    String? text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.headlineMedium!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.headlineMedium!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.headlineMedium!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.headlineMedium!.merge(style),
    );
  }

  static Widget h5(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.headlineSmall!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.headlineSmall!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.headlineSmall!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.headlineSmall!.merge(style),
    );
  }

  static Widget h6(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.titleLarge!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.titleLarge!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.titleLarge!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.titleLarge!.merge(style),
    );
  }

  static Widget subtitle(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.titleMedium!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.titleMedium!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.titleMedium!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.titleMedium!.merge(style),
    );
  }

  static Widget subtitle2(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.titleSmall!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.titleSmall!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.titleSmall!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.titleSmall!.merge(style),
    );
  }

  static Widget body(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.bodyLarge!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.bodyLarge!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.bodyLarge!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.bodyLarge!.merge(style),
    );
  }

  static Widget body2(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.bodyMedium!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.bodyMedium!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.bodyMedium!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.bodyMedium!.merge(style),
    );
  }

  static Widget small(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.bodySmall!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.bodySmall!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.bodySmall!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.bodySmall!.merge(style),
    );
  }

  static Widget button(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.labelLarge!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.labelLarge!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.labelLarge!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.labelLarge!.merge(style),
    );
  }

  static Widget caption(
    String? text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.bodySmall!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.bodySmall!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.bodySmall!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.bodySmall!.merge(style),
    );
  }

  static Widget overline(
    String text, {
    BuildContext? context,
    TextAlign? textAlign,
    Key? key,
    Color? color,
    TextStyle? style,
  }) {
    assert(color == null || style == null);
    if (context == null) {
      return Builder(
        builder: (context) {
          final GBThemeData themeData = GBTheme.of(context);
          return GBText(
            text,
            key: key,
            textAlign: textAlign,
            style: color != null
                ? themeData.textTheme.labelSmall!.merge(TextStyle(
                    color: color,
                  ))
                : themeData.textTheme.labelSmall!.merge(style),
          );
        },
      );
    }
    final GBThemeData themeData = GBTheme.of(context);
    return GBText(
      text,
      key: key,
      style: color != null
          ? themeData.textTheme.labelSmall!.merge(TextStyle(
              color: color,
            ))
          : themeData.textTheme.labelSmall!.merge(style),
    );
  }
}
