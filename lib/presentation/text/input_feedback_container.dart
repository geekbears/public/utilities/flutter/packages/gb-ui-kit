import 'package:flutter/material.dart';

import '../presentation.dart';

class InputFeedbackContainer extends StatefulWidget {
  final List<InputFeedbackText> feedback;

  InputFeedbackContainer({
    Key? key,
    required this.feedback,
  }) : super(key: key);

  @override
  _InputFeedbackContainerState createState() => _InputFeedbackContainerState();
}

class _InputFeedbackContainerState extends State<InputFeedbackContainer> {
  GlobalKey<AnimatedListState> _listKey = GlobalKey();

  @override
  void didUpdateWidget(covariant InputFeedbackContainer oldWidget) {
    if (hashL(oldWidget.feedback) != hashL(widget.feedback)) {
      Map<int, InputFeedbackText> toBeAdded = {};
      for (var i = 0; i < widget.feedback.length; i++) {
        int? containedAt;
        for (var j = 0; j < oldWidget.feedback.length; j++) {
          if (oldWidget.feedback[j].key == widget.feedback[i].key) {
            containedAt = j;
            break;
          }
        }

        if (containedAt == null) {
          toBeAdded.addAll({
            i: widget.feedback[i],
          });
          _listKey.currentState?.insertItem(
            i,
            duration: Duration(
              milliseconds: 400,
            ),
          );
        }
      }
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ConstrainedBox(
      constraints: theme.inputDecorationTheme.constraints ?? BoxConstraints(maxWidth: 343),
      child: AnimatedList(
        key: _listKey,
        initialItemCount: widget.feedback.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index, animation) {
          if (index + 1 > widget.feedback.length) {
            return SizedBox();
          }
          return Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
            ),
            child: SizeTransition(
              // alignment: Alignment.center,
              // scale: animation,
              sizeFactor: animation,
              axis: Axis.vertical,
              child: widget.feedback[index],
            ),
          );
        },
      ),
    );
  }

  hashL(List<InputFeedbackText> list) {
    return Object.hashAll(
      list.map((e) {
        final data = e.text.toString();
        return data;
      }),
    );
  }
}
