// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

/// A widget to be used to render a loading screen
/// this will be useful to render a transition when screen navigation is occurring and
/// a guard is processing async logic
class ScreenLoadingIndicator extends StatefulWidget {
  /// An stream that will be used to subscribe to the loading state
  final Stream<bool>? indicatorStream;

  /// A notifier to add an internal listener that will be used to act on loading state changes
  /// You must define a [valueMapper] for this
  final ValueNotifier<dynamic>? indicatorNotifier;

  /// Every time the [indicatorNotifier] triggers this callback will be processed to get the indicator/loading state
  final bool Function()? valueMapper;

  /// Defines the style of the builder
  final DefaultLoadingStyle style;

  final ScreenLoadingIndicatorType type;

  /// Provide your inner widget that will be shown when not loading
  final Widget child;

  /// Provide a transition duration
  final Duration duration;

  /// A function that wraps a new [child] with an animation that transitions
  /// the [child] in when the animation runs in the forward direction and out
  /// when the animation runs in the reverse direction. This is only called
  /// when a new [child] is set (not for each build), or when a new
  /// [transitionBuilder] is set. If a new [transitionBuilder] is set, then
  /// the transition is rebuilt for the current child and all previous children
  /// using the new [transitionBuilder]. The function must not return null.
  ///
  /// The default is [AnimatedSwitcher.defaultTransitionBuilder].
  ///
  /// The animation provided to the builder has the [duration] and
  /// [switchInCurve] or [switchOutCurve] applied as provided when the
  /// corresponding [child] was first provided.
  ///
  /// See also:
  ///
  ///  * [AnimatedSwitcherTransitionBuilder] for more information about
  ///    how a transition builder should function.
  final AnimatedSwitcherTransitionBuilder transitionBuilder;

  /// A function that wraps all of the children that are transitioning out, and
  /// the [child] that's transitioning in, with a widget that lays all of them
  /// out. This is called every time this widget is built. The function must not
  /// return null.
  ///
  /// The default is [AnimatedSwitcher.defaultLayoutBuilder].
  ///
  /// See also:
  ///
  ///  * [AnimatedSwitcherLayoutBuilder] for more information about
  ///    how a layout builder should function.
  final AnimatedSwitcherLayoutBuilder layoutBuilder;

  const ScreenLoadingIndicator({
    Key? key,
    this.indicatorStream,
    this.indicatorNotifier,
    this.valueMapper,
    this.type = ScreenLoadingIndicatorType.stack,
    required this.child,
    this.duration = const Duration(milliseconds: 500),
    this.transitionBuilder = AnimatedSwitcher.defaultTransitionBuilder,
    this.layoutBuilder = AnimatedSwitcher.defaultLayoutBuilder,
    this.style = const DefaultLoadingStyle(),
  })  : assert(
          indicatorStream != null || indicatorNotifier != null,
          "You must provide either a stream or value notifier",
        ),
        assert(
          indicatorNotifier == null || valueMapper != null,
          "If you are using a value notifier you must provider a function that will be called to get the state",
        ),
        super(key: key);

  static Widget defaultLoaderTransitionBuilder(Widget child, Animation<double> animation) {
    // return AnimatedBuilder(
    //   animation: animation,
    //   builder: (context, child) {
    //     return Opacity(opacity: animation.value,child: ,);
    //   },
    //   child: child,
    // );
    return Opacity(
      opacity: .8,
      child: child,
    );
  }

  static Widget defaultLoaderBuilder(DefaultLoadingStyle style) {
    return Builder(
      builder: (context) {
        return SizedBox.expand(
          child: Stack(
            children: [
              Opacity(
                opacity: style.overlayOpacity,
                child: Container(
                  color: style.overlayColor ?? GBTheme.backgroundContrast(context),
                ),
              ),
              Center(
                child: CircularProgressIndicator.adaptive(
                  valueColor: style.loadingColor != null ? AlwaysStoppedAnimation<Color>(style.loadingColor!) : null,
                  backgroundColor: style.loadingColor,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  State<ScreenLoadingIndicator> createState() => _ScreenLoadingIndicatorState();
}

class _ScreenLoadingIndicatorState extends State<ScreenLoadingIndicator> {
  // late AnimationController _controller;

  StreamSubscription? _streamSubscription;

  bool _showLoader = false;

  @override
  void didUpdateWidget(covariant ScreenLoadingIndicator oldWidget) {
    if (oldWidget.indicatorStream != widget.indicatorStream) {
      _subscribe();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    _subscribe();
    super.initState();
  }

  @override
  void dispose() {
    _streamSubscription?.cancel();
    if (widget.indicatorNotifier != null) {
      widget.indicatorNotifier!.removeListener(_notifierListener);
    }
    super.dispose();
  }

  _subscribe() {
    if (widget.indicatorStream != null) {
      if (_streamSubscription != null) {
        _streamSubscription!.cancel();
        _streamSubscription = null;
      }

      // _controller = AnimationController(
      //   duration: widget.duration,
      //   vsync: this,
      // );

      _streamSubscription = widget.indicatorStream!.listen((event) {
        if (!mounted) return;
        setState(() {
          _showLoader = event;
        });
      });
    } else {
      widget.indicatorNotifier!.addListener(_notifierListener);
    }
  }

  _notifierListener() {
    if (!mounted) return;
    setState(() {
      _showLoader = widget.valueMapper!();
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (widget.type) {
      case ScreenLoadingIndicatorType.stack:
        return Directionality(
          textDirection: TextDirection.ltr,
          child: Stack(
            children: [
              widget.child,
              Positioned.fill(
                child: RepaintBoundary(
                  key: ValueKey("switcher"),
                  child: AnimatedSwitcher(
                    key: ValueKey("loader-container"),
                    duration: widget.duration,
                    transitionBuilder: widget.transitionBuilder,
                    layoutBuilder: widget.layoutBuilder,
                    child: !_showLoader
                        ? SizedBox.shrink()
                        : RepaintBoundary(
                            child: ScreenLoadingIndicator.defaultLoaderBuilder(widget.style),
                          ),
                  ),
                ),
              ),
            ],
          ),
        );

      default:
        return RepaintBoundary(
          key: ValueKey("switcher"),
          child: AnimatedSwitcher(
            duration: widget.duration,
            transitionBuilder: widget.transitionBuilder,
            layoutBuilder: widget.layoutBuilder,
            child: _showLoader
                ? RepaintBoundary(
                    child: ScreenLoadingIndicator.defaultLoaderBuilder(widget.style),
                  )
                : widget.child,
          ),
        );
    }
  }
}

enum ScreenLoadingIndicatorType {
  switcher,
  stack,
}

class DefaultLoadingStyle {
  /// Color for the loading indicator
  final Color? loadingColor;

  /// Opacity for the overlay
  /// Defaults to [0.5]
  final double overlayOpacity;

  /// Color for the overlay
  final Color? overlayColor;

  /// If you want to completely customize the way the loader is show seg a custom builder
  /// this builder will provide the default widget so you can wrap it if needed
  final Widget? builder;

  const DefaultLoadingStyle({
    this.loadingColor,
    this.overlayOpacity = 0.5,
    this.overlayColor,
    this.builder,
  });
}
