import 'package:flutter/material.dart';

class OverlayCanvas extends StatefulWidget {
  final Widget child;

  OverlayCanvas({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  State<OverlayCanvas> createState() => _OverlayCanvasState();
}

class _OverlayCanvasState extends State<OverlayCanvas> {
  Offset _offset = Offset.zero;

  @override
  void initState() {
    debugPrint("Initializing overlay canvas");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        widget.child,
        Positioned(
          right: _offset.dx,
          bottom: _offset.dy,
          child: GestureDetector(
            onPanUpdate: (details) {
              setState(() {
                _offset = _offset.translate(-details.delta.dx, -details.delta.dy);
              });
            },
            child: Container(
              color: Colors.red,
              width: 20,
              height: 20,
            ),
          ),
        ),
      ],
    );
  }
}
