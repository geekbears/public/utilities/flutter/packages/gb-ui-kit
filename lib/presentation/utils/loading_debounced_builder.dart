import 'dart:async';

import 'package:flutter/widgets.dart';

///
/// This widget allows you to debounce a loading state so you can implement transition if state changes
/// too fast sometimes it doesn't have enough time to display a loading indicator
/// this widget tries to solve that issue
/// when changing to loading state transition will begin inmediatly, when loading state changes to false it will wait for the debounce time
/// before staring the transition

class LoadingDebouncedBuilder extends StatefulWidget {
  final bool loading;

  /// Defaults to 250 ms
  final Duration? debounceTime;
  final Widget Function(BuildContext context, bool loadingState) builder;

  const LoadingDebouncedBuilder({
    Key? key,
    required this.loading,
    this.debounceTime,
    required this.builder,
  }) : super(key: key);

  @override
  State<LoadingDebouncedBuilder> createState() => _LoadingDebouncedBuilderState();
}

class _LoadingDebouncedBuilderState extends State<LoadingDebouncedBuilder> {
  StreamController<bool> _loadingStreamController = StreamController();

  late bool _loading;

  @override
  void didUpdateWidget(covariant LoadingDebouncedBuilder oldWidget) {
    if (oldWidget.loading != widget.loading) {
      _loadingStreamController.add(widget.loading);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    // print("LoadingDebouncedSwitcher - init - $hashCode");
    _loading = widget.loading;
    _loadingStreamController.stream.asyncExpand<bool>((loading) async* {
      if (loading) {
        yield loading;
      } else {
        await Future.delayed(widget.debounceTime ?? const Duration(milliseconds: 450));
        yield loading;
      }
    })
        // .asyncExpand((event) => null)
        .listen((event) {
      if (!mounted) return;
      setState(() {
        _loading = event;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    // print("LoadingDebouncedSwitcher - dispose - $hashCode");
    _loadingStreamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // print("LoadingDebouncedSwitcher - build - loading:$_loading - $hashCode");
    return widget.builder(context, _loading);
  }
}
