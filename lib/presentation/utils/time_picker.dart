import 'dart:ui';
import 'package:collection/collection.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class UnavailableTime {
  const UnavailableTime({
    required this.hour,
    required this.disableMinutesFrom,
    required this.disableMinutesTo,
    required this.disableEntireHour,
  });

  /// Disables completely the hour and minutes.
  final bool disableEntireHour;

  /// The hour where the minutes will be disabled or
  /// the hour that will be disabled along with the minutes. **It should be in 24h format.**
  final int hour;

  /// Set the initial range where the minutes will be disabled.
  /// I.e. If we want disable 9:00 to 9:10 interval then we should set `disableMinutesFrom: 0` and `disableMinutesTo: 10`.
  final int disableMinutesFrom;

  /// Closes the range where the minutes will be disabled.
  /// I.e. If we want disable 9:00 to 9:10 then we should set `disableMinutesFrom: 0` and `disableMinutesTo: 10`.
  final int disableMinutesTo;
}

class PickedTime {
  const PickedTime({
    required this.hour,
    required this.minutes,
    required this.isAM,
  });

  final int hour;
  final int minutes;
  final bool isAM;

  @override
  String toString() {
    return "PickedTime(hour: $hour, minutes: $minutes, isAM: $isAM)";
  }
}

Future<void> _animateController(
  PageController controller,
  int offset, [
  Duration? duration,
  Curve? curve,
]) async {
  return controller.animateToPage(
    offset,
    duration: duration ??
        const Duration(
          milliseconds: 200,
        ),
    curve: curve ?? Curves.bounceIn,
  );
}

class TimePicker extends StatefulWidget {
  const TimePicker({
    Key? key,
    this.onChanged,
    this.onChangedParsed,
    this.initialHour,
    this.initialMinutes,
    this.initWithAM = true,
    this.availableAfterHours,
    this.availableAfterMinutes,
    this.unavailableTimes,
    this.unavailableLegend,
    this.activeTimeStyle,
    this.disabledTimeStyle,
  })  : assert(initialHour != 0),
        assert(initialHour == null || initialHour <= 24),
        super(key: key);

  /// Starts the picker in the specified hour
  final int? initialHour;

  /// Starts the picker in the specified minutes
  final int? initialMinutes;

  /// Starts the picker in the current deevice hour plus the specified hours but disabling the before hours,
  /// i.e. current time is 11:00 am and `availableAfterHours = 2`, the picker will start at 1:00 pm
  final int? availableAfterHours;

  /// Starts the picker in the current device minutes plus the specified minutes but disabling the before minute,
  /// i.e. current time is 11:40 am and `availableAfterMinutes = 20`, the picker will start at 12:00 am
  final int? availableAfterMinutes;

  /// Intervals of time that will be disabled.
  final List<UnavailableTime>? unavailableTimes;
  final String? unavailableLegend;
  final TextStyle? activeTimeStyle;
  final TextStyle? disabledTimeStyle;

  /// If the picker should start with `AM` instead of `PM`
  final bool initWithAM;
  final void Function(String time, bool isAvailableTime)? onChanged;
  final void Function(PickedTime time, bool isAvailableTime)? onChangedParsed;

  @override
  State<TimePicker> createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  int _selectedHour = 1;
  int _selectedMinutes = 0;
  late String _selectedDayTime;
  late DateTime? _currentDate;
  late int? _hourFromCurrentDate;
  late int? _minutesFromCurrentDate;
  late String _dayTimeFromCurrentDate;
  late final PageController _hourController;
  late final PageController _minutesController;
  late final PageController _dayTimeController;

  int? get _hourFromInput {
    if (widget.initialHour != null) {
      if (widget.initialHour! > 12) {
        return widget.initialHour! - 12;
      } else {
        return widget.initialHour;
      }
    }
    return null;
  }

  bool get _canDetermineDayTimeFromInputs {
    return widget.initialHour != null && widget.initialHour! > 12;
  }

  String get _dayTimeFromInput {
    return widget.initialHour! > 12 ? "PM" : "AM";
  }

  @override
  void initState() {
    if (widget.availableAfterHours != null || widget.availableAfterMinutes != null) {
      _currentDate = DateTime.now().add(
        Duration(
          hours: widget.availableAfterHours ?? 0,
          minutes: widget.availableAfterMinutes ?? 0,
        ),
      );
    } else {
      _currentDate = null;
    }

    if (_currentDate != null) {
      _formatDateTime(
        _currentDate!,
        (hour, minutes, timeOfDay) {
          _hourFromCurrentDate = hour;
          _minutesFromCurrentDate = minutes;
          _dayTimeFromCurrentDate = timeOfDay;
        },
      );
    } else {
      _hourFromCurrentDate = null;
      _minutesFromCurrentDate = null;
      _dayTimeFromCurrentDate = '';
    }

    _hourController = PageController(
      initialPage: (_hourFromCurrentDate != null)
          ? (_hourFromCurrentDate! - 1)
          : (widget.initialHour != null)
              ? _hourFromInput! - 1
              : 0,
      viewportFraction: 0.2,
    );
    _minutesController = PageController(
      initialPage: (_minutesFromCurrentDate != null) ? (_minutesFromCurrentDate!) : widget.initialMinutes ?? 0,
      viewportFraction: 0.2,
    );
    _dayTimeController = PageController(
      initialPage: (_dayTimeFromCurrentDate != "")
          ? (_dayTimeFromCurrentDate == "AM")
              ? 0
              : 1
          : _canDetermineDayTimeFromInputs
              ? (_dayTimeFromInput == "AM" ? 0 : 1)
              : (widget.initWithAM ? 0 : 1),
      viewportFraction: 0.2,
    );

    _selectedDayTime = (_dayTimeFromCurrentDate != "")
        ? _dayTimeFromCurrentDate
        : _canDetermineDayTimeFromInputs
            ? _dayTimeFromInput
            : (widget.initWithAM)
                ? "AM"
                : "PM";
    _selectedHour = _hourFromCurrentDate ?? _hourFromInput ?? _selectedHour;
    _selectedMinutes = _minutesFromCurrentDate ?? widget.initialMinutes ?? _selectedMinutes;

    final isUnavailable = _currentDate != null && _currentDate!.day != DateTime.now().day;

    widget.onChanged?.call(_formatSelectedTime(), !isUnavailable);
    widget.onChangedParsed?.call(_parsePickedTime(), !isUnavailable);

    super.initState();
  }

  @override
  void didUpdateWidget(TimePicker oldWidget) {
    if (widget.availableAfterHours != null || widget.availableAfterMinutes != null) {
      _currentDate = DateTime.now().add(
        Duration(
          hours: widget.availableAfterHours ?? 0,
          minutes: widget.availableAfterMinutes ?? 0,
        ),
      );

      _formatDateTime(
        _currentDate!,
        (hour, minutes, timeOfDay) {
          if (hour != _hourFromCurrentDate) {
            _hourFromCurrentDate = hour;
            _animateController(_hourController, hour - 1);
          }

          if (minutes != _minutesFromCurrentDate) {
            _minutesFromCurrentDate = minutes;
            _animateController(_minutesController, minutes);
          }

          if (timeOfDay != _dayTimeFromCurrentDate) {
            _dayTimeFromCurrentDate = timeOfDay;
            _animateController(_dayTimeController, (timeOfDay == "AM") ? 0 : 1);
          }
        },
      );
    } else {
      _currentDate = null;
      _hourFromCurrentDate = null;
      _minutesFromCurrentDate = null;
      _dayTimeFromCurrentDate = "";
    }
    super.didUpdateWidget(oldWidget);
  }

  bool get _hourIsBefore {
    if (_currentDate == null) return false;
    return _selectedHour < _hourFromCurrentDate!;
  }

  bool get _minutesAreBefore {
    if (_currentDate == null) return false;
    return _selectedMinutes < _minutesFromCurrentDate!;
  }

  bool get _currentDayTimeIsEqual {
    if (_currentDate == null) return false;
    return _selectedDayTime == _dayTimeFromCurrentDate;
  }

  void _formatDateTime(DateTime date, void Function(int, int, String) callback) {
    callback.call(
      date.hour > 12 ? date.hour - 12 : date.hour,
      date.minute,
      _currentDate!.hour < 12 ? "AM" : "PM",
    );
  }

  String _formatSelectedTime() {
    if (_selectedMinutes < 10) {
      return "$_selectedHour:0$_selectedMinutes $_selectedDayTime";
    }
    return "$_selectedHour:$_selectedMinutes $_selectedDayTime";
  }

  PickedTime _parsePickedTime() => PickedTime(
        hour: _selectedHour,
        minutes: _selectedMinutes,
        isAM: _selectedDayTime == "AM",
      );

  @override
  Widget build(BuildContext context) {
    final isUnavailableDate = _currentDate != null && _currentDate!.day != DateTime.now().day;
    return ClipRRect(
      clipBehavior: Clip.antiAlias,
      child: IgnorePointer(
        ignoring: isUnavailableDate,
        child: SizedBox(
          height: 200,
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _TimeSelector(
                    selectorType: _TimeSelectorType.hours,
                    activeTimeStyle: widget.activeTimeStyle,
                    disabledTimeStyle: widget.disabledTimeStyle,
                    controller: _hourController,
                    itemCount: 12,
                    currentHour: _selectedHour,
                    currentMinutes: _selectedMinutes,
                    currentTimeOfDay: _selectedDayTime,
                    availableAfter: (_currentDayTimeIsEqual)
                        ? (_selectedDayTime == "PM" && _hourFromCurrentDate == 12)
                            ? null
                            : _hourFromCurrentDate! - 1
                        : null,
                    unavailableTimes: widget.unavailableTimes,
                    desiredText: (i) {
                      return "${i + 1}";
                    },
                    onChanged: (hour, isAvailableTime) {
                      if (_currentDayTimeIsEqual && int.parse(hour) == _hourFromCurrentDate && _minutesAreBefore) {
                        _animateController(
                          _minutesController,
                          _minutesFromCurrentDate!,
                          const Duration(
                            milliseconds: 300,
                          ),
                        );
                      }
                      setState(() => _selectedHour = int.parse(hour));
                      widget.onChanged?.call(_formatSelectedTime(), isAvailableTime);
                      widget.onChangedParsed?.call(_parsePickedTime(), isAvailableTime);
                    },
                  ),
                  _TimeSelector(
                    selectorType: _TimeSelectorType.none,
                    activeTimeStyle: widget.activeTimeStyle,
                    disabledTimeStyle: widget.disabledTimeStyle,
                    controller: PageController(
                      viewportFraction: 0.2,
                    ),
                    itemCount: 1,
                    customWidth: 10,
                    unavailableTimes: widget.unavailableTimes,
                    currentHour: _selectedHour,
                    currentMinutes: _selectedMinutes,
                    currentTimeOfDay: _selectedDayTime,
                    handleIdleBehavior: false,
                    desiredText: (i) {
                      return ":";
                    },
                    onChanged: (value, isAvailableTime) {},
                  ),
                  _TimeSelector(
                    selectorType: _TimeSelectorType.minutes,
                    activeTimeStyle: widget.activeTimeStyle,
                    disabledTimeStyle: widget.disabledTimeStyle,
                    controller: _minutesController,
                    itemCount: 60,
                    currentHour: _selectedHour,
                    currentTimeOfDay: _selectedDayTime,
                    availableAfter: (_currentDayTimeIsEqual && _selectedHour == _hourFromCurrentDate)
                        ? _minutesFromCurrentDate!
                        : null,
                    unavailableTimes: widget.unavailableTimes,
                    desiredText: (i) {
                      return (i < 10) ? "0$i" : i.toString();
                    },
                    onChanged: (minutes, isAvailableTime) {
                      setState(() => _selectedMinutes = int.parse(minutes));
                      widget.onChanged?.call(_formatSelectedTime(), isAvailableTime);
                      widget.onChangedParsed?.call(_parsePickedTime(), isAvailableTime);
                    },
                  ),
                  _TimeSelector(
                    selectorType: _TimeSelectorType.timeOfDay,
                    activeTimeStyle: widget.activeTimeStyle,
                    disabledTimeStyle: widget.disabledTimeStyle,
                    controller: _dayTimeController,
                    itemCount: ["am", "pm"].length,
                    handleIdleBehavior: true,
                    unavailableTimes: widget.unavailableTimes,
                    currentMinutes: _selectedMinutes,
                    currentHour: _selectedHour,
                    availableAfter: (_dayTimeFromCurrentDate == "PM") ? 1 : null,
                    desiredText: (i) {
                      return ["am", "pm"][i].toUpperCase();
                    },
                    onChanged: (dayTime, isAvailable) {
                      if ((_currentDayTimeIsEqual || !_currentDayTimeIsEqual) && _hourIsBefore) {
                        _animateController(
                          _hourController,
                          _hourFromCurrentDate! - 1,
                          const Duration(
                            milliseconds: 300,
                          ),
                        );
                      }
                      if ((_currentDayTimeIsEqual || !_currentDayTimeIsEqual) && _hourIsBefore) {
                        _animateController(
                          _minutesController,
                          _minutesFromCurrentDate!,
                          const Duration(
                            milliseconds: 300,
                          ),
                        );
                      }
                      setState(() => _selectedDayTime = dayTime);
                      widget.onChanged?.call(_formatSelectedTime(), isAvailable);
                      widget.onChangedParsed?.call(_parsePickedTime(), isAvailable);
                    },
                  ),
                ],
              ),
              if (isUnavailableDate && widget.unavailableLegend != null)
                AnimatedOpacity(
                  opacity: (isUnavailableDate) ? 1 : 0,
                  duration: const Duration(
                    milliseconds: 400,
                  ),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                    child: Center(
                      child: GBText.body(
                        widget.unavailableLegend!,
                        style: GBTheme.of(context).textTheme.bodyLarge?.copyWith(
                              color: GBTheme.of(context).accentError,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

enum _TimeSelectorType {
  hours,
  minutes,
  timeOfDay,
  none,
}

class _TimeSelector extends StatefulWidget {
  const _TimeSelector({
    Key? key,
    required this.controller,
    required this.selectorType,
    required this.itemCount,
    required this.onChanged,
    required this.activeTimeStyle,
    required this.disabledTimeStyle,
    this.currentHour,
    this.currentMinutes,
    this.currentTimeOfDay,
    this.availableAfter,
    this.desiredText,
    this.customWidth,
    this.unavailableTimes,
    this.handleIdleBehavior = true,
  }) : super(key: key);

  final PageController controller;
  final _TimeSelectorType selectorType;
  final List<UnavailableTime>? unavailableTimes;
  final int itemCount;
  final int? currentHour;
  final int? currentMinutes;
  final String? currentTimeOfDay;
  final int? availableAfter;
  final double? customWidth;
  final bool handleIdleBehavior;
  final TextStyle? activeTimeStyle;
  final TextStyle? disabledTimeStyle;
  final void Function(String value, bool isAvailableTime) onChanged;
  final String Function(int index)? desiredText;

  @override
  State<_TimeSelector> createState() => _TimeSelectorState();
}

class _TimeSelectorState extends State<_TimeSelector> {
  double _currentPage = 0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      widget.controller.addListener(_controllerListener);
      setState(() => _currentPage = widget.controller.initialPage.toDouble());
    });
    super.initState();
  }

  @override
  void dispose() {
    widget.controller.removeListener(_controllerListener);
    widget.controller.dispose();
    super.dispose();
  }

  bool get _usingAfterProperty => widget.availableAfter != null;

  void _controllerListener() {
    setState(() => _currentPage = widget.controller.page ?? 0);
  }

  bool _checkScrollNotification(Object notification) {
    if (_usingAfterProperty) {
      if (notification is UserScrollNotification && notification.direction == ScrollDirection.idle) {
        if (_currentPage < widget.availableAfter!) {
          Future.delayed(
            Duration.zero,
            () {
              _animateController(widget.controller, widget.availableAfter!);
            },
          );
        }
      } else if (notification is ScrollEndNotification && _currentPage < widget.availableAfter!) {
        Future.delayed(
          Duration.zero,
          () {
            _animateController(widget.controller, widget.availableAfter!);
          },
        );
      }
    }
    return false;
  }

  bool _isBeforeAvailable(int i) {
    if (!_usingAfterProperty) return false;

    return i < widget.availableAfter!;
  }

  bool _isAvailableTime(int i) {
    if (widget.unavailableTimes?.isEmpty ?? true) return true;

    switch (widget.selectorType) {
      case _TimeSelectorType.hours:
        final unavailableTimes = widget.unavailableTimes!.where((e) {
          if (e.disableEntireHour) {
            return (widget.currentTimeOfDay == "PM") ? (e.hour - 13) == i : (e.hour - 1) == i;
          }

          if (e.hour == 12 && widget.currentTimeOfDay == "PM") {
            return (e.hour - 1) == _currentPage.round() && (e.hour - 1) == i;
          } else if (e.hour == 24 && widget.currentTimeOfDay == "AM") {
            return (e.hour - 13) == _currentPage.round() && (e.hour - 13) == i;
          }

          return (widget.currentTimeOfDay == "PM")
              ? (e.hour - 13) == _currentPage.round() && (e.hour - 13) == i
              : (e.hour - 1) == _currentPage.round() && (e.hour - 1) == i;
        }).toList();

        if (unavailableTimes.isNotEmpty) {
          final isUnavailableTime = unavailableTimes.singleWhereOrNull((e) {
            if ((widget.currentTimeOfDay == "AM" && e.hour == 12) ||
                (widget.currentTimeOfDay == "PM" && e.hour == 24)) {
              return false;
            }

            return e.disableEntireHour ||
                widget.currentMinutes! >= e.disableMinutesFrom && widget.currentMinutes! <= e.disableMinutesTo;
          });

          return isUnavailableTime == null;
        }

        return true;
      case _TimeSelectorType.minutes:
        final unavailableTimes = widget.unavailableTimes!.where(
          (e) {
            if (e.hour == 12 && widget.currentTimeOfDay == "PM") {
              return e.hour == widget.currentHour;
            } else if (e.hour == 24 && widget.currentTimeOfDay == "AM") {
              return (e.hour - 12) == widget.currentHour;
            }

            return (widget.currentTimeOfDay == "PM")
                ? (e.hour - 12) == widget.currentHour
                : e.hour == widget.currentHour;
          },
        );

        if (unavailableTimes.isNotEmpty) {
          if (unavailableTimes.any((e) {
            if ((widget.currentTimeOfDay == "AM" && e.hour == 12) || widget.currentTimeOfDay == "PM" && e.hour == 24) {
              return false;
            }

            return e.disableEntireHour;
          })) {
            return false;
          } else {
            final unavailableTime = unavailableTimes.singleWhereOrNull((e) {
              if ((widget.currentTimeOfDay == "AM" && e.hour == 12) ||
                  widget.currentTimeOfDay == "PM" && e.hour == 24) {
                return false;
              }

              return i >= e.disableMinutesFrom && i <= e.disableMinutesTo;
            });

            return unavailableTime == null;
          }
        }

        return true;
      case _TimeSelectorType.timeOfDay:
        final unavailableTimes = widget.unavailableTimes!.where(
          (e) {
            if (e.hour == 12 && _currentPage.round() == 1) {
              return e.hour == widget.currentHour;
            } else if (e.hour == 24 && _currentPage.round() == 0) {
              return (e.hour - 12) == widget.currentHour;
            }
            return (_currentPage.round() == 1) ? (e.hour - 12) == widget.currentHour : e.hour == widget.currentHour;
          },
        );

        if (unavailableTimes.isNotEmpty) {
          final unavailableTime = unavailableTimes.singleWhereOrNull((e) {
            if ((i == 0 && _currentPage.round() == 0 && e.hour == 12) ||
                i == 1 && _currentPage.round() == 1 && e.hour == 24) {
              return false;
            }

            if (i == 1 && _currentPage.round() == 1 && e.hour == 12 && widget.currentHour == 12) {
              return (e.disableEntireHour ||
                  (widget.currentMinutes! >= e.disableMinutesFrom && widget.currentMinutes! <= e.disableMinutesTo));
            } else if (i == 0 && _currentPage.round() == 0 && e.hour == 24 && widget.currentHour == 12) {
              return (e.disableEntireHour ||
                  (widget.currentMinutes! >= e.disableMinutesFrom && widget.currentMinutes! <= e.disableMinutesTo));
            }

            return (i == 1 &&
                    _currentPage.round() == 1 &&
                    e.hour - 12 == widget.currentHour &&
                    (e.disableEntireHour ||
                        (widget.currentMinutes! >= e.disableMinutesFrom &&
                            widget.currentMinutes! <= e.disableMinutesTo))) ||
                (i == 0 &&
                    _currentPage.round() == 0 &&
                    e.hour == widget.currentHour &&
                    (e.disableEntireHour ||
                        (widget.currentMinutes! >= e.disableMinutesFrom &&
                            widget.currentMinutes! <= e.disableMinutesTo)));
          });

          return unavailableTime == null;
        }

        return true;

      case _TimeSelectorType.none:
        final unavailableTimes = widget.unavailableTimes!.where(
          (e) {
            if (e.hour == 12 && widget.currentTimeOfDay == "PM") {
              return e.hour == widget.currentHour;
            } else if (e.hour == 24 && widget.currentTimeOfDay == "AM") {
              return (e.hour - 12) == widget.currentHour;
            }

            return (widget.currentTimeOfDay == "PM")
                ? (e.hour - 12) == widget.currentHour
                : e.hour == widget.currentHour;
          },
        );

        if (unavailableTimes.isNotEmpty) {
          final unavailableTime = unavailableTimes.singleWhereOrNull((e) {
            if ((widget.currentTimeOfDay == "AM" && e.hour == 12) || widget.currentTimeOfDay == "PM" && e.hour == 24) {
              return false;
            }

            if (e.hour == 12 && widget.currentHour == 12 && widget.currentTimeOfDay == "PM") {
              return (e.disableEntireHour ||
                  (widget.currentMinutes! >= e.disableMinutesFrom && widget.currentMinutes! <= e.disableMinutesTo));
            } else if (e.hour == 24 && widget.currentHour == (e.hour - 12) && widget.currentTimeOfDay == "AM") {
              return (e.disableEntireHour ||
                  (widget.currentMinutes! >= e.disableMinutesFrom && widget.currentMinutes! <= e.disableMinutesTo));
            }

            return (widget.currentTimeOfDay == "PM" &&
                    e.hour - 12 == widget.currentHour &&
                    (e.disableEntireHour ||
                        (widget.currentMinutes! >= e.disableMinutesFrom &&
                            widget.currentMinutes! <= e.disableMinutesTo))) ||
                (widget.currentTimeOfDay == "AM" &&
                    e.hour == widget.currentHour &&
                    (e.disableEntireHour ||
                        (widget.currentMinutes! >= e.disableMinutesFrom &&
                            widget.currentMinutes! <= e.disableMinutesTo)));
          });

          return unavailableTime == null;
        }

        return true;
      default:
        return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = GBTheme.of(context);
    final defaultStyle = theme.textTheme.displayMedium!;
    return SizedBox(
      width: widget.customWidth ?? 50,
      child: NotificationListener(
        onNotification: _checkScrollNotification,
        child: PageView.builder(
          controller: widget.controller,
          scrollDirection: Axis.vertical,
          onPageChanged: (i) {
            if (_usingAfterProperty && widget.unavailableTimes != null) {
              widget.onChanged.call(
                widget.desiredText?.call(i) ?? "${i + 1}",
                !_isBeforeAvailable(i) && _isAvailableTime(i),
              );
            } else if (_usingAfterProperty) {
              widget.onChanged.call(widget.desiredText?.call(i) ?? "${i + 1}", !_isBeforeAvailable(i));
            } else if (widget.unavailableTimes != null) {
              widget.onChanged.call(widget.desiredText?.call(i) ?? "${i + 1}", _isAvailableTime(i));
            } else {
              widget.onChanged.call(widget.desiredText?.call(i) ?? "${i + 1}", true);
            }
          },
          itemCount: widget.itemCount,
          itemBuilder: (context, i) {
            final percent = (_currentPage <= i) ? (_currentPage - i) + 1 : (i - _currentPage) + 1;
            final scale = percent.clamp(0.75, 0.95) * 1.2;
            final opacity = percent.clamp(0.3, 1.0);

            final _defaultStyle = defaultStyle.copyWith(
              fontSize: theme.textTheme.displayMedium!.fontSize! * scale,
              color: theme.accentColor,
            );

            final _activeStyle = widget.activeTimeStyle?.copyWith(
                  fontSize: (widget.activeTimeStyle?.fontSize ?? theme.textTheme.displayMedium!.fontSize!) * scale,
                ) ??
                _defaultStyle;

            final _disabledStyle = widget.disabledTimeStyle?.copyWith(
                  fontSize: (widget.disabledTimeStyle?.fontSize ?? theme.textTheme.displayMedium!.fontSize!) * scale,
                ) ??
                _defaultStyle.copyWith(
                  color: theme.disabledColor,
                );

            return Opacity(
              opacity: opacity,
              child: AnimatedDefaultTextStyle(
                duration: const Duration(
                  milliseconds: 50,
                ),
                style: (_usingAfterProperty && widget.unavailableTimes != null)
                    ? !_isBeforeAvailable(i) && _isAvailableTime(i)
                        ? _activeStyle
                        : _disabledStyle
                    : (_usingAfterProperty)
                        ? !_isBeforeAvailable(i)
                            ? _activeStyle
                            : _disabledStyle
                        : (widget.unavailableTimes != null)
                            ? _isAvailableTime(i)
                                ? _activeStyle
                                : _disabledStyle
                            : _activeStyle,
                child: Text(
                  widget.desiredText?.call(i) ?? "${i + 1}",
                  textAlign: TextAlign.center,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
