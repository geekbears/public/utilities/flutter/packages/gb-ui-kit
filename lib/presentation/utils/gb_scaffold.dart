import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class GBScaffold extends StatelessWidget {
  const GBScaffold({
    Key? key,
    this.appBar,
    this.body,
    this.backgroundColors,
    this.floatingActionButton,
    this.floatingActionButtonLocation,
    this.floatingActionButtonAnimator,
    this.persistentFooterButtons,
    this.drawer,
    this.onDrawerChanged,
    this.endDrawer,
    this.onEndDrawerChanged,
    this.bottomNavigationBar,
    this.bottomSheet,
    this.resizeToAvoidBottomInset,
    this.drawerScrimColor,
    this.drawerEdgeDragWidth,
    this.restorationId,
    this.beginGradient,
    this.endGradient,
    this.stopsGradient,
  }) : super(key: key);

  final PreferredSizeWidget? appBar;
  final List<Color>? backgroundColors;
  final FloatingActionButtonLocation? floatingActionButtonLocation;
  final FloatingActionButtonAnimator? floatingActionButtonAnimator;
  final DragStartBehavior drawerDragStartBehavior = DragStartBehavior.start;
  final Widget? body;
  final Widget? floatingActionButton;
  final Widget? drawer;
  final Widget? endDrawer;
  final Widget? bottomNavigationBar;
  final Widget? bottomSheet;
  final List<Widget>? persistentFooterButtons;
  final bool? resizeToAvoidBottomInset;
  final bool primary = true;
  final bool extendBody = false;
  final bool extendBodyBehindAppBar = false;
  final Color? drawerScrimColor;
  final bool drawerEnableOpenDragGesture = true;
  final bool endDrawerEnableOpenDragGesture = true;
  final String? restorationId;
  final double? drawerEdgeDragWidth;
  final List<double>? stopsGradient;
  final Alignment? beginGradient;
  final Alignment? endGradient;
  final void Function(bool)? onDrawerChanged;
  final void Function(bool)? onEndDrawerChanged;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: primary,
      extendBody: extendBody,
      extendBodyBehindAppBar: extendBodyBehindAppBar,
      drawerDragStartBehavior: drawerDragStartBehavior,
      endDrawerEnableOpenDragGesture: endDrawerEnableOpenDragGesture,
      appBar: appBar,
      backgroundColor: (backgroundColors != null) ? Colors.transparent : Theme.of(context).scaffoldBackgroundColor,
      body: (backgroundColors != null)
          ? Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: beginGradient ?? Alignment.topCenter,
                  end: endGradient ?? Alignment.bottomCenter,
                  colors: backgroundColors!,
                  stops: stopsGradient,
                ),
              ),
              child: body,
            )
          : body,
      floatingActionButton: floatingActionButton,
      floatingActionButtonLocation: floatingActionButtonLocation,
      floatingActionButtonAnimator: floatingActionButtonAnimator,
      persistentFooterButtons: persistentFooterButtons,
      drawer: drawer,
      onDrawerChanged: onDrawerChanged,
      endDrawer: endDrawer,
      onEndDrawerChanged: onEndDrawerChanged,
      bottomNavigationBar: bottomNavigationBar,
      bottomSheet: bottomSheet,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      drawerScrimColor: drawerScrimColor,
      drawerEdgeDragWidth: drawerEdgeDragWidth,
      restorationId: restorationId,
    );
  }
}
