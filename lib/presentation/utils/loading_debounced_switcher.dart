import 'package:flutter/widgets.dart';

import '../presentation.dart';

///
/// This widget allows you to switch between a loading state and a normal, when APIs act
/// too fast sometimes it doesn't have enough time to display a loading indicator
/// this widget tries to solve that issue
/// when changing to loading state transition will begin inmediatly, when loading state changes to false it will wait for the debounce time
/// before staring the transition

class LoadingDebouncedSwitcher extends StatelessWidget {
  final bool loading;
  final Duration duration;

  /// Defaults to 450 ms
  final Duration? debounceTime;
  final Widget loadingWidget;
  final Widget child;

  const LoadingDebouncedSwitcher({
    Key? key,
    required this.loading,
    this.duration = const Duration(milliseconds: 250),
    this.debounceTime,
    required this.loadingWidget,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print("LoadingDebouncedSwitcher - build - loading:$_loading - $hashCode");
    return LoadingDebouncedBuilder(
      loading: loading,
      debounceTime: debounceTime,
      builder: (context, state) {
        return AnimatedSwitcher(
          duration: duration,
          child: state ? loadingWidget : child,
        );
      },
    );
  }
}
