// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/presentation/presentation.dart';

class GenericErrorRetry extends StatelessWidget {
  const GenericErrorRetry({
    Key? key,
    this.errorTitle,
    this.titleStyle,
    required this.errorMessage,
    this.errorMessageStyle,
    this.onTryAgain,
    this.buttonText,
    this.buttonIsBusy = false,
    this.buttonUppercase = false,
    this.customCTA,
  }) : super(key: key);

  final String? errorTitle;
  final TextStyle? titleStyle;
  final String errorMessage;
  final TextStyle? errorMessageStyle;
  final void Function()? onTryAgain;
  final String? buttonText;
  final bool buttonIsBusy;
  final bool buttonUppercase;
  final Widget Function(BuildContext context)? customCTA;

  @override
  Widget build(BuildContext context) {
    final localizations = GBUiKitLocalizations.of(context);
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GBText(
            errorTitle ?? localizations.error,
            style: GBTheme.of(context)
                .textTheme
                .displayMedium
                ?.copyWith(
                  color: GBTheme.accentColorOf(context),
                )
                .merge(titleStyle),
          ),
          const SizedBox(
            height: 20,
          ),
          GBText.body(
            errorMessage,
            textAlign: TextAlign.center,
            style: DefaultTextStyle.of(context).style.merge(errorMessageStyle),
          ),
          const SizedBox(
            height: 30,
          ),
          customCTA?.call(context) ??
              (onTryAgain != null
                  ? ContainedButton.large(
                      onPressed: () => onTryAgain?.call(),
                      text: buttonText ?? localizations.tryAgain,
                      busy: buttonIsBusy,
                      uppercase: buttonUppercase,
                    )
                  : SizedBox.shrink()),
        ],
      ),
    );
  }
}
