// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../presentation.dart';

class HandlePasteBuilder extends StatefulWidget {
  const HandlePasteBuilder({
    Key? key,
    required this.builder,
    this.onTap,
    required this.onPaste,
    this.node,
    this.unfocusOnPaste = true,
    this.shiftOffset,
  }) : super(key: key);

  final Widget Function(BuildContext context) builder;
  final VoidCallback? onTap;
  final void Function(String? value) onPaste;
  final FocusNode? node;
  final bool unfocusOnPaste;
  final Offset Function(Offset primaryOffset, Size size)? shiftOffset;

  @override
  State<HandlePasteBuilder> createState() => _HandlePasteBuilderState();
}

class _HandlePasteBuilderState extends State<HandlePasteBuilder> {
  @override
  Widget build(BuildContext context) {
    return handlePasteWidget(
      context: context,
      child: widget.builder(context),
    );
  }

  Widget handlePasteWidget({
    required BuildContext context,
    required Widget child,
  }) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (widget.onTap != null) {
          return widget.onTap!();
        }
        FocusScope.of(context).requestFocus(widget.node);
      },
      onLongPress: () {
        showDialogMenu(
          context: context,
        );
      },
      onDoubleTap: () {
        showDialogMenu(
          context: context,
        );
      },
      child: IgnorePointer(
        child: child,
      ),
    );
  }

  showDialogMenu({
    required BuildContext context,
  }) {
    var caption = GBText.caption(
      GBUiKitLocalizations.of(context).paste,
    );

    var backgroundBase = GBTheme.of(context).backgroundBase;

    var isDarkColor = backgroundBase.computeLuminance() < 0.5;

    var colorMenu = isDarkColor ? lighten(backgroundBase, 0.05) : darken(backgroundBase, 0.05);

    showMenu(
      context: context,
      position: buttonMenuPosition(context),
      color: colorMenu,
      items: [
        PopupMenuItem(
          onTap: () async {
            ClipboardData? cdata = await Clipboard.getData(Clipboard.kTextPlain);
            String? copiedtext = cdata?.text;
            widget.onPaste.call(copiedtext);
            if (widget.unfocusOnPaste) FocusScope.of(context).unfocus();
          },
          child: Row(
            children: [
              Icon(
                Icons.paste,
                color: GBTheme.of(context).textTheme.bodySmall?.color,
              ),
              Expanded(
                child: Center(child: caption),
              ),
            ],
          ),
        ),
      ],
    );
  }

  RelativeRect buttonMenuPosition(
    BuildContext context,
  ) {
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    //*get the global position, from the widget local position
    final offset = renderBox.localToGlobal(Offset.zero);

    //*calculate the start point in this case, below the button
    final dx = widget.shiftOffset?.call(offset, renderBox.size).dx ?? 0;
    final dy = widget.shiftOffset?.call(offset, renderBox.size).dy ?? 0;
    final left = offset.dx + dx;
    final top = offset.dy + dy;
    final right = left + renderBox.size.width / 2;

    return RelativeRect.fromLTRB(left, top, right, 0);
  }
}
