import 'package:flutter/widgets.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class HaveAnAccountLegend extends StatelessWidget {
  final Function onAction;
  final bool alreadyHave;
  final bool upperCase;
  final String? signUpText;
  final String? signInText;
  final TextStyle? normalStyle;
  final TextStyle? actionStyle;

  const HaveAnAccountLegend({
    Key? key,
    required this.onAction,
    required this.alreadyHave,
    this.upperCase = true,
    this.signUpText,
    this.signInText,
    this.normalStyle,
    this.actionStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ComposedActionText(
      onAction: onAction,
      uppercaseAction: upperCase,
      normalStyle: normalStyle,
      actionStyle: actionStyle,
      normalText: alreadyHave ? "Already have an account? " : "Don’t have an account? ",
      actionText: alreadyHave ? signInText ?? "Sign In" : signUpText ?? "Sign-Up",
    );
  }
}
