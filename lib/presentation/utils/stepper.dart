import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class Stepper extends StatelessWidget {
  const Stepper({
    Key? key,
    required this.steps,
    this.dotsColor,
  }) : super(key: key);

  final List<Step> steps;
  final Color? dotsColor;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        const double dashWidth = 5;
        final maxWidth = constraints.maxWidth;
        final dotsCount = ((maxWidth / (2 * dashWidth)) / steps.length).floor();
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: List.generate(
            steps.length,
            (i) => Row(
              children: [
                steps[i],
                if (steps.last != steps[i])
                  ...List.generate(
                    dotsCount,
                    (i) {
                      return Padding(
                        padding: const EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: Container(
                          height: 2,
                          width: dashWidth,
                          decoration: BoxDecoration(
                            color: dotsColor ?? GBTheme.of(context).textWhite,
                            shape: BoxShape.circle,
                          ),
                        ),
                      );
                    },
                  ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class Step extends StatelessWidget {
  const Step({
    Key? key,
    required this.isActive,
    required this.isCompleted,
    this.title,
    this.completedColor,
    this.activeColor,
    this.inactiveColor,
    this.completedBorderColor,
  }) : super(key: key);

  final String? title;
  final bool isActive;
  final bool isCompleted;
  final Color? completedColor;
  final Color? activeColor;
  final Color? inactiveColor;
  final Color? completedBorderColor;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        AnimatedContainer(
          duration: const Duration(
            milliseconds: 200,
          ),
          height: (isCompleted)
              ? 40
              : (isActive)
                  ? 40
                  : 35,
          width: (isCompleted)
              ? 40
              : (isActive)
                  ? 40
                  : 35,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: (isCompleted)
                ? completedColor ?? GBTheme.of(context).textWhite
                : (isActive)
                    ? activeColor ?? GBTheme.of(context).accentColor
                    : inactiveColor ?? GBTheme.of(context).disabledColor,
            border: (isCompleted)
                ? Border.all(
                    width: 4,
                    color: completedBorderColor ?? GBTheme.of(context).accentColor,
                  )
                : null,
          ),
        ),
        if (title != null)
          GBText.body(
            title!,
            color: (isActive)
                ? activeColor ?? GBTheme.of(context).accentColor
                : (isCompleted)
                    ? completedColor ?? GBTheme.of(context).textWhite
                    : inactiveColor ?? GBTheme.of(context).disabledColor,
          ),
      ],
    );
  }
}
