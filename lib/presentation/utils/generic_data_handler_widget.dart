import 'package:flutter/material.dart';

import '../presentation.dart';

/// A widget that helps you handling loading, errors and data through the UI with animations
class GenericDataHandlerWidget extends StatelessWidget {
  const GenericDataHandlerWidget({
    Key? key,
    required this.hasError,
    required this.errorMessage,
    required this.isLoading,
    required this.loadingChild,
    required this.child,
    this.errorTitle,
    this.buttonText,
    this.customButton,
    this.titleStyle,
    this.messageStyle,
    this.onTryAgain,
    this.buttonUppercase = true,
    this.buttonIsBusy = false,
  }) : super(key: key);

  final bool hasError;
  final bool isLoading;
  final String? errorTitle;
  final String errorMessage;
  final String? buttonText;
  final bool buttonUppercase;
  final bool buttonIsBusy;
  final ContainedButton? customButton;
  final TextStyle? titleStyle;
  final TextStyle? messageStyle;

  /// The widget that will be shown when there is none error
  final Widget child;

  /// The diget that will be shown while loading
  final Widget loadingChild;
  final void Function()? onTryAgain;

  @override
  Widget build(BuildContext context) {
    return AnimatedFadeHeightSwitcher(
      child: (isLoading)
          ? loadingChild
          : (!hasError)
              ? child
              : GenericErrorRetry(
                  errorMessage: errorMessage,
                  errorTitle: errorTitle,
                  buttonIsBusy: buttonIsBusy,
                  buttonUppercase: buttonUppercase,
                  buttonText: buttonText,
                  customCTA: customButton != null
                      ? (context) {
                          return customButton!;
                        }
                      : null,
                  onTryAgain: onTryAgain,
                ),
    );
  }
}
