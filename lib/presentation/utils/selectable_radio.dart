import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class SelectableRadio<T> extends StatefulWidget {
  const SelectableRadio({
    Key? key,
    required this.value,
    required this.groupValue,
    this.onChanged,
    this.filled = false,
    this.disabled = false,
    this.enableSplashColor = false,
    this.size,
    this.filledColor,
    this.border,
    this.activeColor,
    this.inactiveColor,
    this.disabledColor,
    this.activeChild,
    this.label,
    this.labelStyle,
  }) : super(key: key);

  final T value;
  final T groupValue;
  final void Function(T)? onChanged;
  final bool filled;
  final bool disabled;
  final bool enableSplashColor;
  final double? size;
  final Color? filledColor;
  final Color? activeColor;
  final Color? inactiveColor;
  final Color? disabledColor;
  final Border? border;
  final Widget? activeChild;
  final String? label;
  final TextStyle? labelStyle;

  @override
  State<SelectableRadio<T>> createState() => _SelectableRadioState<T>();
}

class _SelectableRadioState<T> extends State<SelectableRadio<T>> {
  late ValueNotifier<T> _currentValueNotifier;

  @override
  void initState() {
    _currentValueNotifier = ValueNotifier(widget.groupValue);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant SelectableRadio<T> oldWidget) {
    if (oldWidget.groupValue != widget.groupValue) {
      _currentValueNotifier.value = widget.groupValue;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _currentValueNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final themeData = GBTheme.of(context);
    final Color activeColor = widget.activeColor ?? themeData.accentColor;
    final Color inactiveColor = widget.inactiveColor ?? themeData.disabledColor;

    return ValueListenableBuilder<T>(
      valueListenable: _currentValueNotifier,
      builder: (context, value, _) {
        final isSelected = value == widget.value;

        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              splashColor: (widget.enableSplashColor) ? activeColor.withOpacity(0.5) : Colors.transparent,
              highlightColor: activeColor.withOpacity(0.3),
              customBorder: const CircleBorder(),
              onTap: _onTap,
              child: AnimatedContainer(
                duration: const Duration(
                  milliseconds: 200,
                ),
                height: widget.size ?? 30,
                width: widget.size ?? 30,
                decoration: BoxDecoration(
                  color: (widget.filled) ? widget.filledColor ?? activeColor : null,
                  border: widget.border ??
                      Border.all(
                        color: (isSelected) ? activeColor : inactiveColor,
                      ),
                  shape: BoxShape.circle,
                ),
                child: AnimatedScale(
                  duration: const Duration(
                    milliseconds: 200,
                  ),
                  scale: (isSelected) ? 1 : 0,
                  child: widget.activeChild ??
                      Icon(
                        Icons.check,
                        color: (!widget.filled) ? activeColor : widget.disabledColor,
                        size: (widget.size != null) ? widget.size! - 7 : null,
                      ),
                ),
              ),
            ),
            if (widget.label != null) ...[
              const SizedBox(
                width: 10,
              ),
              GestureDetector(
                onTap: _onTap,
                child: GBText.body(
                  widget.label!,
                  style: widget.labelStyle,
                ),
              ),
            ],
          ],
        );
      },
    );
  }

  void _onTap() {
    if (!widget.disabled) {
      widget.onChanged?.call(widget.value);
      _currentValueNotifier.value = widget.value;
    }
  }
}
