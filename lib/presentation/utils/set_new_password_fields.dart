import 'package:flutter/material.dart';

import '../presentation.dart';

class NewPasswordSetFieldsStyle {
  /// If should center the fields
  final bool centerFields;

  /// Define if field labes should be disabled
  /// Defaults to [false]
  final bool hideFieldsLabels;

  /// New password field hint custom text
  final String? newPasswordHintText;

  /// Confirm password field hint custom text
  final String? confirmPasswordHintText;

  /// Password field feedback
  final List<InputFeedbackText> passwordFeedbacks;

  /// Confirm password field feedback
  final List<InputFeedbackText> confirmPasswordFeedbacks;

  final double fieldsSpace;

  /// The expression pattern that will validate the password,
  /// if this property is not provided `r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!#%*?&])[A-Za-z\d@$!#%*?&]*"`
  /// will be used instead
  final RegExp? passwordRegExp;

  /// The mandatory length of password
  final int passwordLength;

  /// Defines a custom builder widget that will wrap the TexField inner box
  /// This is useful to for example wrap your widget inside a Material elevation, etc
  final WrapWidgetBuilder? textfieldBoxWrapper;

  /// Provides a way to change the [PasswordField] label
  final String? newPasswordLabel;

  /// Provides a way to change the confirm [PasswordField] label
  final String? confirmPasswordLabel;

  /// Provides a way to change the style of both [PasswordField] labels
  final TextStyle? labelStyle;

  /// Override default input decoration with your own implementation for new password field
  /// Provides a way to override default input decoration styles
  /// by default if you only provide this value it will merge your style definition with default
  /// if you want to relay completely on your styling set [overrideDecoration] to `true`
  /// This function provides you access to default obscure icon
  final InputDecoration Function(BuildContext context, Widget icon)? newPasswordFieldInputDecoration;

  /// Override default input decoration with your own implementation for confirm password field
  final InputDecoration? confirmPasswordFieldInputDecoration;

  NewPasswordSetFieldsStyle({
    this.centerFields = false,
    this.hideFieldsLabels = false,
    this.newPasswordHintText,
    this.confirmPasswordHintText,
    this.passwordFeedbacks = const [],
    this.confirmPasswordFeedbacks = const [],
    this.fieldsSpace = 32,
    this.passwordRegExp,
    this.passwordLength = 8,
    this.textfieldBoxWrapper,
    this.newPasswordLabel,
    this.confirmPasswordLabel,
    this.labelStyle,
    this.newPasswordFieldInputDecoration,
    this.confirmPasswordFieldInputDecoration,
  });
}

class NewPasswordSetFieldsState {
  /// Indicates if the regex provided passes the check of the password field
  final bool meetsRegex;

  /// Indicates if the length of the password is met
  final bool meetsLength;

  /// Indicates if the password field match the confirm password
  final bool meetsEquality;

  final String password;
  final String confirmPassword;

  bool get meetsAll {
    return meetsRegex && meetsLength && meetsEquality;
  }

  const NewPasswordSetFieldsState({
    required this.meetsRegex,
    required this.meetsLength,
    required this.meetsEquality,
    required this.password,
    required this.confirmPassword,
  });

  @override
  bool operator ==(covariant NewPasswordSetFieldsState other) {
    if (identical(this, other)) return true;

    return other.meetsRegex == meetsRegex &&
        other.meetsLength == meetsLength &&
        other.meetsEquality == meetsEquality &&
        other.password == password &&
        other.confirmPassword == confirmPassword;
  }

  @override
  int get hashCode {
    return meetsRegex.hashCode ^
        meetsLength.hashCode ^
        meetsEquality.hashCode ^
        password.hashCode ^
        confirmPassword.hashCode;
  }
}

class NewPasswordSetFields extends StatefulWidget {
  /// Called when state changes
  final void Function(NewPasswordSetFieldsState state) onFieldStateChange;

  // Define the style configuration for this widget
  final NewPasswordSetFieldsStyle style;

  const NewPasswordSetFields({
    Key? key,
    required this.onFieldStateChange,
    required this.style,
  }) : super(key: key);

  @override
  State<NewPasswordSetFields> createState() => _NewPasswordSetFieldsState();
}

class _NewPasswordSetFieldsState extends State<NewPasswordSetFields> {
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();
  late RegExp _passwordRegExp;

  bool _passwordAccomplishLength = false;
  bool _passwordMatchRegExp = false;
  bool _passwordsAreEqual = false;

  NewPasswordSetFieldsState get fieldsState {
    return NewPasswordSetFieldsState(
      confirmPassword: _confirmPasswordController.text,
      password: _passwordController.text,
      meetsEquality: _passwordsAreEqual,
      meetsLength: _passwordAccomplishLength,
      meetsRegex: _passwordMatchRegExp,
    );
  }

  @override
  void initState() {
    _passwordRegExp =
        widget.style.passwordRegExp ?? RegExp(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!#%*?&])[A-Za-z\d@$!#%*?&]*");
    _passwordController.addListener(_passwordListener);
    _confirmPasswordController.addListener(_confirmPasswordListener);
    super.initState();
  }

  @override
  void dispose() {
    _passwordController.removeListener(_passwordListener);
    _confirmPasswordController.removeListener(_confirmPasswordListener);
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  void _passwordListener() {
    final password = _passwordController.text;

    if (password.length >= widget.style.passwordLength) {
      _passwordAccomplishLength = true;
    } else if (_passwordAccomplishLength) {
      _passwordAccomplishLength = false;
    }

    if (_passwordRegExp.hasMatch(password)) {
      _passwordMatchRegExp = true;
    } else if (_passwordMatchRegExp) {
      _passwordMatchRegExp = false;
    }

    _notifyStateChange();
    setState(() {});
  }

  void _confirmPasswordListener() {
    if (_confirmPasswordController.text == _passwordController.text) {
      _passwordsAreEqual = true;
    } else if (_passwordsAreEqual) {
      _passwordsAreEqual = false;
    }

    _notifyStateChange();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);
    return Column(
      children: [
        Column(
          children: [
            Center(
              widthFactor: (widget.style.centerFields) ? null : 1,
              child: PasswordField(
                controller: _passwordController,
                label:
                    (widget.style.hideFieldsLabels) ? null : widget.style.newPasswordLabel ?? localizations.newPassword,
                decoration: widget.style.newPasswordFieldInputDecoration,
                overrideDecoration: widget.style.newPasswordFieldInputDecoration != null,
                hintText: widget.style.newPasswordHintText,
                feedback: widget.style.passwordFeedbacks,
                wrapInnerBox: widget.style.textfieldBoxWrapper,
                labelStyle: widget.style.labelStyle,
              ),
            ),
          ],
        ),
        SizedBox(
          height: widget.style.fieldsSpace,
        ),
        Column(
          crossAxisAlignment: (widget.style.centerFields) ? CrossAxisAlignment.center : CrossAxisAlignment.start,
          children: [
            Center(
              widthFactor: (widget.style.centerFields) ? null : 1,
              child: PasswordField(
                controller: _confirmPasswordController,
                decoration: widget.style.newPasswordFieldInputDecoration,
                overrideDecoration: widget.style.newPasswordFieldInputDecoration != null,
                label: (widget.style.hideFieldsLabels)
                    ? null
                    : widget.style.confirmPasswordLabel ?? localizations.confirmPassword,
                hintText: widget.style.confirmPasswordHintText,
                feedback: widget.style.confirmPasswordFeedbacks,
                wrapInnerBox: widget.style.textfieldBoxWrapper,
                labelStyle: widget.style.labelStyle,
              ),
            ),
          ],
        ),
      ],
    );
  }

  void _notifyStateChange() {
    widget.onFieldStateChange.call(fieldsState);
  }
}
