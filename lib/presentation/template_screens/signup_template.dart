import 'package:flutter/material.dart' hide TextField;
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class SignUpTemplate extends StatefulWidget {
  final EdgeInsets padding;
  final Widget? icon;
  final bool canSubmit;
  final bool submitting;

  /// Center `icon`
  final bool centerIcon;

  /// Center `H1` and `H3` widgets
  final bool centerHead;

  /// Hide the texts sections
  final bool hideTitleAndSubtitleSection;

  /// Center sign up fields
  final bool centerFields;

  /// Empty space between `icon` and title/subtitle texts
  final double iconBottomSpace;

  /// Empty space between `H1` and `H3`
  final double headTextsSpace;

  /// Empty space between title/subtitle and `fields`
  final double headBottomSpace;

  /// Empty space between `fields` and sign up button
  final double buttonTopSpace;

  /// Custom text for `H1`
  final String? titleText;

  /// Custom text for `H3`
  final String? subtitleText;

  /// Provides a way to customize the way the title
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? titleBuilder;

  /// Provides a way to customize the way the subtitle
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? subtitleBuilder;

  /// The text shown in the button
  final String? buttonText;

  /// Custom text to show in sign in legend
  final String? signInLegendText;

  /// The `H1` text color
  final Color? titleTextColor;

  /// The `H3` text color
  final Color? subtitleTextColor;

  /// The color applied to the haveAccount legend action color
  final TextStyle? legendActionTextStyle;

  /// The Style to apply to the haveAccount legend normal text
  final TextStyle? legendNormalTextStyle;

  /// Content placed between the button and `fields`
  final Widget? additionalRequirement;
  final List<Widget> fields;

  /// Specify type of main CTA button size
  /// default [ButtonSize.large]
  final ButtonSize buttonSize;

  /// If Provided, this function will be used to inject a widget at the end of the screen
  final Widget Function(GBUiKitLocalizations localizations)? footerBuilder;

  final void Function()? onHaveAccountLegendTap;
  final void Function() onSubmit;
  final double spaceBetweenFields;

  const SignUpTemplate({
    Key? key,
    this.padding = const EdgeInsets.only(
      top: 24,
      bottom: 16,
      left: 16,
      right: 16,
    ),
    this.icon,
    this.additionalRequirement,
    this.centerIcon = false,
    this.centerHead = false,
    this.centerFields = false,
    this.iconBottomSpace = 25,
    this.headTextsSpace = 0,
    this.headBottomSpace = 40,
    this.buttonTopSpace = 34,
    String? titleText,
    @Deprecated("Please use instead: titleText") String? welcomeText,
    @Deprecated("Please use instead: subtitleText") String? descriptionText,
    String? subtitleText,
    this.titleBuilder,
    this.subtitleBuilder,
    this.buttonText,
    this.signInLegendText,
    this.titleTextColor,
    this.hideTitleAndSubtitleSection = false,
    this.subtitleTextColor,
    this.legendActionTextStyle,
    this.legendNormalTextStyle,
    required this.canSubmit,
    required this.submitting,
    required this.fields,
    required this.onSubmit,
    this.onHaveAccountLegendTap,
    this.buttonSize = ButtonSize.large,
    this.spaceBetweenFields = 24,
    this.footerBuilder,
  })  : titleText = titleText ?? welcomeText,
        subtitleText = subtitleText ?? descriptionText,
        super(key: key);

  @override
  State<SignUpTemplate> createState() => _SignUpTemplateState();

  static List<Widget> defaultFields(
    BuildContext context, {
    Function(String value)? onFirstNameChange,
    Function(String value)? onLastNameChange,
    Function(String value)? onUsernameChange,
    Function(String value)? onEmailChange,
    Function(String value)? onPasswordChange,
    Function(String value)? onConfirmPasswordChange,
    List<InputFeedbackText>? passwordFeedback,
    List<InputFeedbackText>? confirmPasswordFeedback,
    String? emailLabel,
    String? passwordLabel,
    String? confirmPasswordLabel,
  }) {
    final GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);
    return [
      if (onFirstNameChange != null)
        TextField(
          label: localizations.firstName,
          hintText: localizations.enterYourFirstName,
          onChanged: onFirstNameChange,
        ),
      if (onLastNameChange != null)
        TextField(
          label: localizations.lastName,
          hintText: localizations.enterYourLastName,
          onChanged: onLastNameChange,
        ),
      if (onUsernameChange != null)
        TextField(
          label: localizations.username,
          hintText: localizations.enterYourUsername,
          onChanged: onUsernameChange,
        ),
      if (onEmailChange != null)
        TextField(
          label: emailLabel ?? localizations.email,
          hintText: localizations.enterYourEmail,
          keyboardType: TextInputType.emailAddress,
          onChanged: onEmailChange,
        ),
      if (onPasswordChange != null)
        PasswordField(
          label: passwordLabel ?? localizations.password,
          hintText: localizations.enterYourPassword,
          onChanged: onPasswordChange,
          feedback: [
            if (passwordFeedback != null) ...passwordFeedback
            // InputFeedbackText.check(
            //   key: ValueKey("chars count"),
            //   active: state.passwordCharCountPass,
            //   text: Text(
            //     localizations.xCharacters(6),
            //   ),
            //   color: GBTheme.backgroundContrast(context),
            // ),
            // InputFeedbackText.check(
            //   key: ValueKey("chars symbols"),
            //   active: state.passwordSymbolsPass,
            //   text: Text(
            //     localizations.mixOfLettersNumbersAndSymbols,
            //   ),
            //   color: GBTheme.backgroundContrast(context),
            // ),
          ],
        ),
      if (onConfirmPasswordChange != null)
        PasswordField(
          label: confirmPasswordLabel ?? localizations.confirmPassword,
          hintText: localizations.confirmYourPassword,
          onChanged: onConfirmPasswordChange,
          feedback: [if (confirmPasswordFeedback != null) ...confirmPasswordFeedback],
        ),
    ];
  }
}

class _SignUpTemplateState extends State<SignUpTemplate> {
  @override
  Widget build(BuildContext context) {
    GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);
    final fieldMaxWidth = Theme.of(context).inputDecorationTheme.constraints?.maxWidth ?? 343;
    return Padding(
      padding: widget.padding,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // MARK: Icon
              if (widget.icon != null) ...[
                Container(
                  constraints: BoxConstraints(minHeight: 32),
                  alignment: (widget.centerIcon) ? Alignment.center : null,
                  child: widget.icon,
                ),
                SizedBox(
                  height: widget.iconBottomSpace,
                ),
              ],
              // MARK: Title & Subtitle
              if (!widget.hideTitleAndSubtitleSection)
                Center(
                  widthFactor: (widget.centerHead) ? null : 1,
                  child: Column(
                    crossAxisAlignment: (widget.centerHead) ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                    children: [
                      _buildTitle(localizations, context),
                      SizedBox(
                        height: widget.headTextsSpace,
                      ),
                      _buildSubtitle(localizations, context),
                    ],
                  ),
                ),
              SizedBox(
                height: widget.headBottomSpace,
              ),
              // MARK: Fields
              LayoutBuilder(
                builder: (context, constraints) {
                  return ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: constraints.maxWidth,
                    ),
                    child: Column(
                      crossAxisAlignment: (widget.centerFields) ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                      children: [
                        ...widget.fields.map(
                          (e) => Padding(
                            padding: EdgeInsets.only(bottom: widget.spaceBetweenFields),
                            child: e,
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
          if (widget.additionalRequirement != null) widget.additionalRequirement!,
          SizedBox(
            height: widget.buttonTopSpace,
          ),
          Center(
            child: Column(
              children: [
                if (widget.buttonSize == ButtonSize.free)
                  ContainedButton(
                    disabled: !widget.canSubmit,
                    busy: widget.submitting,
                    onPressed: () {
                      widget.onSubmit();
                    },
                    child: Text(
                      widget.buttonText ?? localizations.signUp,
                    ),
                  )
                else if (widget.buttonSize == ButtonSize.large)
                  ContainedButton.large(
                    disabled: !widget.canSubmit,
                    busy: widget.submitting,
                    onPressed: () {
                      widget.onSubmit();
                    },
                    child: Text(
                      widget.buttonText ?? localizations.signUp,
                    ),
                  )
                else
                  ContainedButton.small(
                    disabled: !widget.canSubmit,
                    busy: widget.submitting,
                    onPressed: () {
                      widget.onSubmit();
                    },
                    child: Text(
                      widget.buttonText ?? localizations.signUp,
                    ),
                  ),
                SizedBox(
                  height: 22,
                ),
                if (widget.onHaveAccountLegendTap != null)
                  HaveAnAccountLegend(
                    alreadyHave: true,
                    signInText: widget.signInLegendText,
                    actionStyle: widget.legendActionTextStyle,
                    normalStyle: widget.legendNormalTextStyle,
                    onAction: widget.onHaveAccountLegendTap!,
                  ),
                if (widget.footerBuilder != null) widget.footerBuilder!.call(localizations)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSubtitle(GBUiKitLocalizations localizations, BuildContext context) {
    return widget.subtitleBuilder?.call(localizations) ??
        Text(
          widget.subtitleText ?? localizations.fillTheFormAndSignUp,
          textAlign: (widget.centerHead) ? TextAlign.center : null,
          style: Theme.of(context).textTheme.displaySmall?.copyWith(
                color: widget.subtitleTextColor,
              ),
        );
  }

  Widget _buildTitle(GBUiKitLocalizations localizations, BuildContext context) {
    return widget.titleBuilder?.call(localizations) ??
        Text(
          widget.titleText ?? localizations.welcome,
          textAlign: (widget.centerHead) ? TextAlign.center : null,
          style: Theme.of(context).textTheme.displayLarge?.copyWith(
                color: widget.titleTextColor,
              ),
        );
  }
}
