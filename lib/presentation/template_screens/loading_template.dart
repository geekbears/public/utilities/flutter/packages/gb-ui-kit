import 'package:flutter/material.dart';

class LoadingTemplate extends StatelessWidget {
  const LoadingTemplate({
    Key? key,
    required this.icon,
    required this.loading,
    this.customIconBuilder,
    this.progressIndicatorColor,
    this.progressIndicatorStrokeWidth = 5.0,
  })  : assert(
          progressIndicatorColor == null || customIconBuilder == null,
          "When using 'customIconBuilder', you shouldn't define progressIndicator properties as they won't have any effect",
        ),
        super(key: key);

  final Widget icon;
  final bool loading;
  final Color? progressIndicatorColor;
  final double progressIndicatorStrokeWidth;
  final Widget Function(BuildContext context)? customIconBuilder;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: [
          Positioned(
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            child: IconTheme(
              data: IconThemeData(
                color: Theme.of(context).colorScheme.secondary,
                size: 75,
              ),
              child: icon,
            ),
          ),
          AnimatedPositioned(
            duration: const Duration(milliseconds: 200),
            left: 0,
            right: 0,
            bottom: loading ? 40 : -10,
            child: AnimatedOpacity(
              duration: const Duration(milliseconds: 200),
              opacity: loading ? 1 : 0,
              child: Center(
                child: Builder(builder: (context) {
                  if (customIconBuilder != null) {
                    return customIconBuilder!(context);
                  }
                  return SizedBox(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(
                      color: progressIndicatorColor,
                      strokeWidth: progressIndicatorStrokeWidth,
                    ),
                  );
                }),
              ),
            ),
          )
        ],
      ),
    );
  }
}
