// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import '../../flutter_gb_ui_kit.dart';

class ErrorTemplate extends StatelessWidget {
  final String? title;
  final String? description;
  final bool busy;
  final EdgeInsets padding;

  final Color? titleColor;
  final Color? messageColor;

  final void Function() onTryAgain;

  const ErrorTemplate({
    Key? key,
    this.title,
    this.description,
    this.busy = false,
    this.padding = const EdgeInsets.symmetric(horizontal: 16.0),
    this.titleColor,
    this.messageColor,
    required this.onTryAgain,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);
    return Padding(
      padding: padding,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GBText.h1(localizations.error, color: titleColor),
            SizedBox(
              height: 124,
            ),
            GBText.body2(
              (description ?? ""),
              color: messageColor ?? GBTheme.of(context).textBlack,
            ),
            SizedBox(
              height: 80,
            ),
            ContainedButton.text(
              localizations.tryAgain,
              busy: busy,
              onPressed: () {
                onTryAgain();
              },
            ),
          ],
        ),
      ),
    );
  }
}
