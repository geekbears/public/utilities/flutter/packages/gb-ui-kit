import 'package:flutter/material.dart';

import '../presentation.dart';

class AccountCodeConfirmationTemplate extends StatefulWidget {
  final void Function(String code)? onCodeChange;
  final void Function()? onResendCode;
  final void Function(String code) onSubmit;

  final EdgeInsets padding;
  final bool? canSubmit;
  final bool submitting;
  final bool resendingCode;
  final bool centerCodeField;
  final bool hideCodeFieldLabel;
  final String? codeFieldLabel;
  final int codeLength;

  /// The keyboard styling for the code input field
  final TextInputType? keyboardType;

  /// if should wrap body in safe are
  /// defaults to true
  final bool wrapInSafeArea;

  /// Space between `confirmText` and `descriptionText`
  final double headerTextsSpace;

  /// Space between head and code field
  final double headerBottomSpace;

  /// `H1` custom text
  final String? confirmText;

  /// `H3` custom text
  final String? descriptionText;

  /// The `H1` text color
  final Color? confirmTextColor;

  /// The `H3` text color
  final Color? descriptionTextColor;

  /// The style applied to the legend action
  final TextStyle? legendActionStyle;

  /// Text shown in the button
  final String? buttonText;

  /// The position where the button will be placed
  final ButtonTemplatePosition buttonPosition;

  /// The position where the legend will be placed
  final ButtonTemplatePosition legendPosition;

  /// Code field feedback
  final List<InputFeedbackText> codeFieldFeedback;

  /// Remove the confirm text and description section
  final bool hideConfirmAndDescriptionSection;

  /// Should force text to be uppercase
  /// defaults to [false]
  final bool forceUppercase;

  /// Provide a way to change code field input value
  final String? codeValue;

  const AccountCodeConfirmationTemplate({
    Key? key,
    this.onCodeChange,
    this.onResendCode,
    required this.onSubmit,
    this.padding = const EdgeInsets.only(
      top: 24,
      bottom: 16,
      left: 16,
      right: 16,
    ),
    this.canSubmit,
    this.submitting = false,
    this.resendingCode = false,
    this.centerCodeField = false,
    this.hideCodeFieldLabel = false,
    this.hideConfirmAndDescriptionSection = false,
    this.codeFieldLabel,
    this.codeLength = 6,
    this.headerTextsSpace = 0,
    this.headerBottomSpace = 40,
    this.confirmText,
    this.descriptionText,
    this.buttonText,
    this.confirmTextColor,
    this.descriptionTextColor,
    this.legendActionStyle,
    this.buttonPosition = ButtonTemplatePosition.END_OF_SCREEN,
    this.legendPosition = ButtonTemplatePosition.END_OF_SCREEN,
    this.codeFieldFeedback = const [],
    this.wrapInSafeArea = true,
    this.forceUppercase = false,
    this.keyboardType,
    this.codeValue,
  }) : super(key: key);

  @override
  State<AccountCodeConfirmationTemplate> createState() => _AccountCodeConfirmationTemplateState();
}

class _AccountCodeConfirmationTemplateState extends State<AccountCodeConfirmationTemplate> {
  String _code = "";

  bool get hasCompletedCode {
    return _code.isNotEmpty && _code.length == widget.codeLength;
  }

  @override
  void initState() {
    if (widget.codeValue != null) {
      _code = widget.codeValue!;
    }
    super.initState();
  }

  @override
  void didUpdateWidget(covariant AccountCodeConfirmationTemplate oldWidget) {
    if (oldWidget.codeValue != widget.codeValue) {
      _code = widget.codeValue ?? "";
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);
    final containedButton = ContainedButton.large(
      disabled: !hasCompletedCode || !(widget.canSubmit ?? true),
      busy: widget.submitting,
      onPressed: () {
        widget.onSubmit(_code);
      },
      text: widget.buttonText ?? localizations.send,
    );

    final resendCodeLegend = ComposedActionText(
      normalText: localizations.didntReceiveCode,
      actionText: " " + localizations.sendAgain,
      actionStyle: widget.legendActionStyle,
      busy: widget.resendingCode,
      onAction: widget.onResendCode ?? () {},
    );

    Widget child = LayoutBuilder(builder: (context, constrains) {
      final screenHeight = constrains.maxHeight.isFinite ? constrains.maxHeight : MediaQuery.of(context).size.height;
      final height = (screenHeight < (500 - kToolbarHeight)) ? (500 - kToolbarHeight) : (screenHeight - kToolbarHeight);
      return SizedBox.expand(
        child: SingleChildScrollView(
          child: Container(
            padding: widget.padding,
            constraints: BoxConstraints(minHeight: screenHeight),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: widget.buttonPosition == ButtonTemplatePosition.END_OF_SCREEN ||
                      widget.legendPosition == ButtonTemplatePosition.END_OF_SCREEN
                  ? MainAxisAlignment.spaceBetween
                  : MainAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Icon(
                    //   SermonsAppIcon.sermons_icon,
                    //   size: 32,
                    // ),
                    // SizedBox(
                    //   height: 25,
                    // ),
                    if (!widget.hideConfirmAndDescriptionSection)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.confirmText ?? localizations.confirmAccount,
                            style: Theme.of(context).textTheme.displayLarge?.copyWith(
                                  color: widget.confirmTextColor,
                                ),
                          ),
                          SizedBox(
                            height: widget.headerTextsSpace,
                          ),
                          Text(
                            widget.descriptionText ?? localizations.insertTheVerificationCodeYouReceiveInYourMail,
                            style: Theme.of(context).textTheme.displaySmall?.copyWith(
                                  color: widget.descriptionTextColor,
                                ),
                          ),
                        ],
                      ),
                    SizedBox(
                      height: widget.headerBottomSpace,
                    ),
                    Center(
                      widthFactor: (widget.centerCodeField) ? null : 1,
                      child: Column(
                        crossAxisAlignment:
                            (widget.centerCodeField) ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                        children: [
                          FittedBox(
                            child: CodeField(
                              value: _code,
                              forceUppercase: widget.forceUppercase,
                              keyboardType: widget.keyboardType,
                              label: (widget.hideCodeFieldLabel) ? null : widget.codeFieldLabel ?? localizations.code,
                              onChanged: (code) {
                                setState(() {
                                  _code = code;
                                });
                                widget.onCodeChange?.call(code);
                              },
                              length: widget.codeLength,
                            ),
                          ),
                          if (widget.codeFieldFeedback.isNotEmpty) ...[
                            const SizedBox(
                              height: 8,
                            ),
                            ...widget.codeFieldFeedback.map((feedback) {
                              return Container(
                                width: 330,
                                child: feedback,
                              );
                            }).toList(),
                          ],
                          if (widget.onResendCode != null)
                            if (widget.legendPosition == ButtonTemplatePosition.BELOW_CODE_FIELD) ...[
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                alignment: Alignment.centerRight,
                                width: 350,
                                child: resendCodeLegend,
                              ),
                            ]
                        ],
                      ),
                    ),
                  ],
                ),
                if (widget.buttonPosition == ButtonTemplatePosition.BELOW_CODE_FIELD) ...[
                  const SizedBox(
                    height: 60,
                  ),
                  Center(
                    child: containedButton,
                  ),
                ],
                // const Spacer(),
                if (widget.onResendCode != null)
                  Center(
                    child: Column(
                      children: [
                        if (widget.buttonPosition == ButtonTemplatePosition.END_OF_SCREEN) containedButton,
                        SizedBox(
                          height: 27,
                        ),
                        if (widget.legendPosition == ButtonTemplatePosition.END_OF_SCREEN) resendCodeLegend,
                      ],
                    ),
                  ),
              ],
            ),
          ),
        ),
      );
    });

    if (widget.wrapInSafeArea) {
      return SafeArea(child: child);
    }
    return child;
  }
}
