import 'package:flutter/material.dart' hide TextField;

import '../presentation.dart';

class ResetPasswordRequestTemplate extends StatefulWidget {
  // A flag to indicate if the main container should force its expansion to the parents full height
  // This is true by default and will accommodate the sections with a spaceBetween alignment
  final bool forceFullHeight;

  final EdgeInsets padding;
  final bool loading;
  final bool centerEmailField;

  /// Provides a way to hide the labels, useful when implementing custom [InputDecoration]
  final bool hideFieldLabel;

  /// Space between `forgotText` and `descriptionText`
  final double headerTextsSpace;

  /// Space between head and code field
  final double headerBottomSpace;

  /// `H1` custom text
  final String? titleText;

  /// `H3` custom text
  /// @Deprecated("Please use instead: 'subtitleText'") String? descriptionText,
  final String? subtitleText;

  /// Email field hint custom text
  final String? fieldHintText;

  /// Email field hint custom text
  final String? buttonText;

  /// The `H1` text color
  final Color? titleTextColor;

  /// The `H3` text color
  final Color? subtitleTextColor;

  /// The color applied to the haveAccount legend action color
  final TextStyle? legendActionTextStyle;

  /// The Style to apply to the haveAccount legend normal text
  final TextStyle? legendNormalTextStyle;

  /// Specify type of main CTA button size
  /// default [ButtonSize.large]
  final ButtonSize buttonSize;

  /// The position where the button will be placed
  final ButtonTemplatePosition buttonPosition;

  /// Specify the spacing that should be between
  /// This only applies when [ButtonTemplatePosition.BELOW_CODE_FIELD]
  /// Defaults to [60]
  final double fieldButtonSpacing;

  /// Provides a way to Inject a widget at the header of the template (top)
  final Widget Function(BuildContext context)? headerBuilder;

  /// Customize the alignment of objects placed on the alignment
  /// Defaults to [CrossAxisAlignment.start]
  final CrossAxisAlignment headerHorizontalAlignment;

  /// Provides a way to customize the way the title
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? titleBuilder;

  /// Provides a way to customize the way the subtitle
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? subtitleBuilder;

  /// Provides a way to customize the way the main button is rendered
  /// [defaultCta] provides you access to the default widget (useful if you just want to wrap or place other widgets aside the default)
  /// [callback] provides a callback to be used on your custom button 'onTap' implementation, this way the widget can trigger [onLogin] callback properly
  /// []
  /// [localizations] provides you access to default localizations
  final Widget Function(Widget defaultCta, Function() callback, GBUiKitLocalizations localizations)? mainButtonBuilder;

  /// Defines a custom builder widget that will wrap the TexField inner box
  /// This is useful to for example wrap your widget inside a Material elevation, etc
  final WrapWidgetBuilder? textfieldBoxWrapper;

  final void Function()? onHaveAccountLegendTap;

  final void Function(String email) onRequestPassword;

  /// If should wrap in safe area
  /// By default true
  final bool wrapInSafeArea;

  /// Provide a way to change email field input value
  final String? emailValue;

  /// Override default input decoration with your own implementation for email field
  final InputDecoration? emailFieldInputDecoration;

  const ResetPasswordRequestTemplate({
    Key? key,
    this.forceFullHeight = true,
    this.padding = const EdgeInsets.only(
      top: 24,
      bottom: 16,
      left: 16,
      right: 16,
    ),
    this.loading = false,
    this.onHaveAccountLegendTap,
    required this.onRequestPassword,
    this.centerEmailField = false,
    this.hideFieldLabel = false,
    this.headerTextsSpace = 16,
    this.headerBottomSpace = 40,
    @Deprecated("Please use instead: 'titleText'") forgotText,
    String? titleText,
    @Deprecated("Please use instead: 'subtitleText'") String? descriptionText,
    String? subtitleText,
    this.fieldHintText,
    @Deprecated("Please use instead: 'titleTextColor") Color? forgotTextColor,
    Color? titleTextColor,
    this.buttonText,
    @Deprecated("Please use instead: 'descriptionTextColor'") Color? descriptionTextColor,
    Color? subtitleTextColor,
    this.legendActionTextStyle,
    this.legendNormalTextStyle,
    this.buttonSize = ButtonSize.large,
    this.buttonPosition = ButtonTemplatePosition.END_OF_SCREEN,
    this.fieldButtonSpacing = 60,
    this.titleBuilder,
    this.subtitleBuilder,
    this.mainButtonBuilder,
    this.headerBuilder,
    this.headerHorizontalAlignment = CrossAxisAlignment.center,
    this.wrapInSafeArea = true,
    this.textfieldBoxWrapper,
    this.emailValue,
    this.emailFieldInputDecoration,
  })  : subtitleText = subtitleText ?? descriptionText,
        subtitleTextColor = subtitleTextColor ?? descriptionTextColor,
        titleText = titleText ?? forgotText,
        titleTextColor = titleTextColor ?? forgotTextColor,
        super(key: key);

  @override
  State<ResetPasswordRequestTemplate> createState() => _ResetPasswordRequestTemplateState();
}

class _ResetPasswordRequestTemplateState extends State<ResetPasswordRequestTemplate> {
  TextEditingController _emailController = TextEditingController();

  @override
  void initState() {
    _emailController.addListener(() {
      setState(() {});
    });
    if (widget.emailValue != null) {
      _emailController.text = widget.emailValue!;
    }
    super.initState();
  }

  @override
  void didUpdateWidget(covariant ResetPasswordRequestTemplate oldWidget) {
    if (oldWidget.emailValue != widget.emailValue) {
      _emailController.text = widget.emailValue ?? "";
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);
    // final screenHeight = MediaQuery.of(context).size.height;

    final containedButton = _buildCTA(localizations);

    // final height = (screenHeight < (400 - kToolbarHeight)) ? (400 - kToolbarHeight) : (screenHeight - kToolbarHeight);

    Widget child = LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          child: Container(
            padding: widget.padding,
            constraints: widget.forceFullHeight ? BoxConstraints(minHeight: constraints.maxHeight) : null,
            child: Column(
              mainAxisAlignment: (widget.buttonPosition == ButtonTemplatePosition.END_OF_SCREEN)
                  ? MainAxisAlignment.spaceBetween
                  : MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: widget.headerHorizontalAlignment,
                      children: [
                        ...widget.headerBuilder != null
                            ? [
                                Builder(builder: (context) {
                                  return widget.headerBuilder!.call(context);
                                })
                              ]
                            : [],
                        _buildTitle(localizations),
                        SizedBox(
                          height: widget.headerTextsSpace,
                        ),
                        _buildSubtitle(localizations),
                      ],
                    ),
                    SizedBox(
                      height: widget.headerBottomSpace,
                    ),
                    Center(
                      widthFactor: (widget.centerEmailField) ? null : 1,
                      child: Column(
                        crossAxisAlignment: (widget.centerEmailField) ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                        children: [
                          TextField(
                            label: (widget.hideFieldLabel) ? null : localizations.email,
                            hintText: widget.fieldHintText ?? localizations.enterYourEmail,
                            controller: _emailController,
                            keyboardType: TextInputType.emailAddress,
                            innerBoxWrapper: widget.textfieldBoxWrapper,
                            decoration: widget.emailFieldInputDecoration,
                            overrideDecoration: widget.emailFieldInputDecoration != null,
                          ),
                          SizedBox(
                            height: widget.fieldButtonSpacing,
                          ),
                          if (widget.buttonPosition == ButtonTemplatePosition.BELOW_CODE_FIELD)
                            Center(
                              child: containedButton,
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
                // if (widget.buttonPosition == ButtonTemplatePosition.BELOW_CODE_FIELD) const Spacer(),
                if (widget.buttonPosition == ButtonTemplatePosition.END_OF_SCREEN || widget.onHaveAccountLegendTap != null)
                  Center(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 8,
                        ),
                        if (widget.buttonPosition == ButtonTemplatePosition.END_OF_SCREEN) containedButton,
                        if (widget.onHaveAccountLegendTap != null) ...[
                          SizedBox(
                            height: 27,
                          ),
                          HaveAnAccountLegend(
                            alreadyHave: false,
                            actionStyle: widget.legendActionTextStyle,
                            normalStyle: widget.legendNormalTextStyle,
                            onAction: widget.onHaveAccountLegendTap!,
                          )
                        ],
                      ],
                    ),
                  )
              ],
            ),
          ),
        );
      },
    );

    if (widget.wrapInSafeArea) {
      return SafeArea(
        child: child,
      );
    }

    return child;
  }

  Widget _buildTitle(GBUiKitLocalizations localizations) {
    return widget.titleBuilder?.call(localizations) ??
        Text(
          widget.titleText ?? localizations.forgotPassword,
          style: Theme.of(context).textTheme.displayLarge?.copyWith(
                color: widget.titleTextColor,
              ),
        );
  }

  Widget _buildSubtitle(GBUiKitLocalizations localizations) {
    if (widget.subtitleBuilder != null) {
      return widget.subtitleBuilder!.call(localizations);
    }
    if (widget.subtitleText == null || widget.subtitleText!.isNotEmpty) {
      return Text(
        widget.subtitleText ?? localizations.resetPasswordDescription,
        style: Theme.of(context).textTheme.displaySmall?.copyWith(
              color: widget.subtitleTextColor,
            ),
      );
    }
    return SizedBox.shrink();
  }

  Widget _buildCTA(GBUiKitLocalizations localizations) {
    final containedButton = ContainedButton.large(
      disabled: _emailController.text.isEmpty,
      busy: widget.loading,
      onPressed: _onSubmit,
      text: widget.buttonText ?? localizations.send,
    );

    return widget.mainButtonBuilder?.call(containedButton, _onSubmit, localizations) ?? containedButton;
  }

  _onSubmit() {
    widget.onRequestPassword.call(_emailController.text);
  }
}
