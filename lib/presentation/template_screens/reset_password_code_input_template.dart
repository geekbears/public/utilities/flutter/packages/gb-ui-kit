import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class ResetPasswordCodeInputTemplate extends StatefulWidget {
  // A flag to indicate if the main container should force its expansion to the parents full height
  // This is true by default and will accommodate the sections with a spaceBetween alignment
  final bool forceFullHeight;

  final EdgeInsets padding;
  final bool resendingCode;
  final bool centerCodeField;
  final bool hideFieldLabel;
  final bool alignResendCodeToRight;

  /// Space between `forgotText` and `descriptionText`
  final double headerTextsSpace;

  /// Space between head and code field
  final double headerBottomSpace;

  /// `H1` custom text
  final String? titleText;

  /// `H3` custom text
  final String? subtitleText;

  /// Email field hint custom text
  final String? buttonText;

  /// The `H1` text color
  final Color? titleTextColor;

  /// The `H3` text color
  final Color? subtitleTextColor;

  /// Customize the alignment of objects placed on the alignment
  /// Defaults to [CrossAxisAlignment.start]
  final CrossAxisAlignment headerHorizontalAlignment;

  /// Provides a way to Inject a widget at the header of the template (top)
  final Widget Function(BuildContext context)? headerBuilder;

  /// Provides a way to customize the way the title
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? titleBuilder;

  /// Provides a way to customize the way the subtitle
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? subtitleBuilder;

  /// The position where the button will be placed
  final ButtonTemplatePosition buttonPosition;

  /// The color applied to the haveAccount legend action color
  final TextStyle? legendActionTextStyle;

  /// The Style to apply to the haveAccount legend normal text
  final TextStyle? legendNormalTextStyle;

  /// Specify type of main CTA button size
  /// default [ButtonSize.large]
  final ButtonSize buttonSize;

  /// Code field feedback
  final List<InputFeedbackText> codeFieldFeedback;

  /// Provides a way to customize the way the main button is rendered
  /// [defaultCta] provides you access to the default widget (useful if you just want to wrap or place other widgets aside the default)
  /// [callback] provides a callback to be used on your custom button 'onTap' implementation, this way the widget can trigger [onContinue] callback properly
  /// []
  /// [localizations] provides you access to default localizations
  final Widget Function(
    Widget defaultCta,
    Function() callback,
    GBUiKitLocalizations localizations,
  )? mainButtonBuilder;

  /// Defines a custom builder widget that will wrap the TexField inner box
  /// This is useful to for example wrap your widget inside a Material elevation, etc
  final WrapWidgetBuilder? textfieldBoxWrapper;

  final void Function()? onChangeEmailAddress;
  final void Function()? onSendAgain;
  final void Function()? onResendCode;
  final void Function()? onSignUp;
  final void Function(String code) onContinue;

  /// If should wrap in safe area
  /// By default true
  final bool wrapInSafeArea;

  /// Called whenever the code field change
  final void Function(String code)? onCodeChange;

  /// If true this template will also show the fields needed for entering the new password
  final bool showNewPasswordFields;

  /// Define password set fields style
  final NewPasswordSetFieldsStyle? newPasswordFieldsStyle;

  final void Function(NewPasswordSetFieldsState state)? onSetNewPasswordFieldsChange;

  /// Set busy state of the continue CTA
  final bool mainButtonBusy;

  /// Set disabled state of the continue CTA
  final bool mainButtonDisabled;

  /// If true this will make the codeField to automatically set its chars as uppercase
  final bool forceCodeUppercase;

  /// Provide a way to change code field input value
  final String? codeValue;

  /// Provide a way to add new widget below the [CodeField] section
  final Widget? extraContent;

  final String? inputsHintText;

  /// Provides a way to force error style on input field (normally red colored)
  final bool forceErrorStyle;

  /// Provide a wat to customize the length of the code input field
  /// Default is [6]
  final int codeFieldLength;

  // Customize the type of keyboard show to user when typing on codefield
  final TextInputType? codeFieldKeyboardType;

  const ResetPasswordCodeInputTemplate({
    Key? key,
    this.forceFullHeight = true,
    this.padding = const EdgeInsets.only(
      top: 24,
      bottom: 16,
      left: 16,
      right: 16,
    ),
    this.onResendCode,
    this.onChangeEmailAddress,
    this.onSendAgain,
    this.onSignUp,
    required this.onContinue,
    this.resendingCode = false,
    this.centerCodeField = false,
    this.hideFieldLabel = false,
    this.alignResendCodeToRight = false,
    this.headerTextsSpace = 0,
    this.headerBottomSpace = 40,
    this.buttonText,
    this.buttonSize = ButtonSize.large,
    this.legendActionTextStyle,
    this.legendNormalTextStyle,
    this.buttonPosition = ButtonTemplatePosition.END_OF_SCREEN,
    this.codeFieldFeedback = const [],
    this.mainButtonBuilder,
    this.wrapInSafeArea = true,
    this.textfieldBoxWrapper,
    this.forceCodeUppercase = true,
    this.titleText,
    this.subtitleText,
    this.titleTextColor,
    this.subtitleTextColor,
    this.headerHorizontalAlignment = CrossAxisAlignment.start,
    this.headerBuilder,
    this.titleBuilder,
    this.subtitleBuilder,
    this.onCodeChange,
    this.showNewPasswordFields = false,
    this.newPasswordFieldsStyle,
    this.onSetNewPasswordFieldsChange,
    this.mainButtonBusy = false,
    this.mainButtonDisabled = false,
    this.codeValue,
    this.extraContent,
    this.inputsHintText,
    this.forceErrorStyle = false,
    this.codeFieldLength = 6,
    this.codeFieldKeyboardType,
  })  : assert(
          (showNewPasswordFields == true && onSetNewPasswordFieldsChange != null) || showNewPasswordFields == false,
          "If you are enabling password set fields, you should provide 'onSetNewPasswordFieldsChange'",
        ),
        assert(
          (showNewPasswordFields == true && newPasswordFieldsStyle != null) || showNewPasswordFields == false,
          "If you are enabling password set fields, you should provide 'newPasswordFieldsStyle'",
        ),
        super(key: key);

  @override
  State<ResetPasswordCodeInputTemplate> createState() => _ResetPasswordCodeInputTemplateState();
}

class _ResetPasswordCodeInputTemplateState extends State<ResetPasswordCodeInputTemplate> {
  String _code = "";

  @override
  void initState() {
    if (widget.codeValue != null) {
      _code = widget.codeValue!;
    }
    super.initState();
  }

  @override
  void didUpdateWidget(covariant ResetPasswordCodeInputTemplate oldWidget) {
    if (oldWidget.codeValue != widget.codeValue) {
      _code = widget.codeValue ?? "";
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);

    late Widget containedButton = _buildCTA(localizations);

    final resendCodeLegend = ComposedActionText(
      normalText: localizations.didntReceiveCode,
      actionText: " " + localizations.sendAgain,
      busy: widget.resendingCode,
      actionStyle: widget.legendActionTextStyle,
      normalStyle: widget.legendNormalTextStyle,
      onAction: widget.onResendCode ?? () {},
    );

    Widget child = LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          child: Container(
            padding: widget.padding,
            constraints: widget.forceFullHeight
                ? BoxConstraints(
                    minHeight: constraints.maxHeight,
                  )
                : null,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: widget.headerHorizontalAlignment,
                      children: [
                        ...widget.headerBuilder != null
                            ? [
                                Builder(builder: (context) {
                                  return widget.headerBuilder!.call(context);
                                })
                              ]
                            : [],
                        _buildTitle(localizations, context),
                        SizedBox(
                          height: widget.headerTextsSpace,
                        ),
                        _buildSubtitle(localizations, context),
                      ],
                    ),
                    SizedBox(
                      height: widget.headerBottomSpace,
                    ),
                    Center(
                      widthFactor: (widget.centerCodeField) ? null : 1,
                      child: Column(
                        crossAxisAlignment: (widget.centerCodeField) ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                        children: [
                          FittedBox(
                            child: CodeField(
                              value: _code,
                              innerBoxWrapper: widget.textfieldBoxWrapper,
                              keyboardType: widget.codeFieldKeyboardType,
                              label: (widget.hideFieldLabel) ? null : localizations.code,
                              forceUppercase: widget.forceCodeUppercase,
                              forceErrorStyle: widget.forceErrorStyle,
                              hintText: widget.inputsHintText,
                              onChanged: (code) {
                                _code = code;
                                widget.onCodeChange?.call(code);
                              },
                              length: widget.codeFieldLength,
                            ),
                          ),
                          if (widget.codeFieldFeedback.isNotEmpty) ...[
                            const SizedBox(
                              height: 8,
                            ),
                            ...widget.codeFieldFeedback.map((feedback) {
                              return Container(
                                width: 330,
                                child: feedback,
                              );
                            }).toList(),
                          ],
                          if (widget.extraContent != null) ...[widget.extraContent!],
                          if (widget.onChangeEmailAddress != null) ...[
                            const SizedBox(
                              height: 8,
                            ),
                            Container(
                              width: 330,
                              alignment: AlignmentDirectional.centerStart,
                              child: GestureDetector(
                                onTap: widget.onChangeEmailAddress!.call,
                                child: GBText.caption(
                                  localizations.changeTheEmailAddress,
                                  color: GBTheme.accentColorOf(context),
                                ),
                              ),
                            ),
                          ],
                          if (widget.onResendCode != null) ...[
                            const SizedBox(
                              height: 8,
                            ),
                            Container(
                              width: (widget.centerCodeField) ? 330 : null,
                              alignment: (widget.alignResendCodeToRight) ? Alignment.centerRight : Alignment.centerLeft,
                              child: resendCodeLegend,
                            )
                          ],
                          if (widget.showNewPasswordFields)
                            Column(
                              children: [
                                SizedBox(
                                  height: 35,
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  child: GBText.h3(
                                    "Insert new password and then confirm it to continue.",
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                SizedBox(
                                  height: 35,
                                ),
                                NewPasswordSetFields(
                                  onFieldStateChange: widget.onSetNewPasswordFieldsChange!,
                                  style: widget.newPasswordFieldsStyle!,
                                ),
                              ],
                            ),
                          if (widget.buttonPosition == ButtonTemplatePosition.BELOW_CODE_FIELD) ...[
                            SizedBox(
                              height: widget.showNewPasswordFields ? 34 : 60,
                            ),
                            Center(
                              child: containedButton,
                            ),
                          ]
                        ],
                      ),
                    ),
                  ],
                ),
                Center(
                  child: Column(
                    children: [
                      if (widget.buttonPosition == ButtonTemplatePosition.END_OF_SCREEN) containedButton,
                      if (widget.onSendAgain != null)
                        Column(
                          children: [
                            SizedBox(
                              height: 16,
                            ),
                            resendCodeLegend,
                          ],
                        ),
                      if (widget.onSignUp != null)
                        Column(
                          children: [
                            SizedBox(
                              height: 16,
                            ),
                            ComposedActionText(
                              normalText: localizations.dontHaveAnAccount,
                              actionText: " " + localizations.signUp,
                              actionStyle: widget.legendActionTextStyle,
                              normalStyle: widget.legendNormalTextStyle,
                              onAction: widget.onSignUp!,
                            ),
                          ],
                        ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );

    if (widget.wrapInSafeArea) {
      return SafeArea(
        child: child,
      );
    }

    return child;
  }

  Widget _buildSubtitle(GBUiKitLocalizations localizations, BuildContext context) {
    return widget.subtitleBuilder?.call(localizations) ??
        Text(
          widget.subtitleText ?? localizations.insertTheVerificationCodeYouReceiveInYourMail,
          style: Theme.of(context).textTheme.displaySmall?.copyWith(
                color: widget.subtitleTextColor,
              ),
        );
  }

  Widget _buildTitle(GBUiKitLocalizations localizations, BuildContext context) {
    return widget.titleBuilder?.call(localizations) ??
        Text(
          widget.titleText ?? localizations.resetPassword,
          style: Theme.of(context).textTheme.displayLarge?.copyWith(
                color: widget.titleTextColor,
              ),
        );
  }

  Widget _buildCTA(GBUiKitLocalizations localizations) {
    late ContainedButton containedButton;
    switch (widget.buttonSize) {
      case ButtonSize.free:
        containedButton = ContainedButton(
          busy: widget.mainButtonBusy,
          disabled: widget.mainButtonDisabled,
          onPressed: _onSubmit,
          child: Text(
            widget.buttonText ?? localizations.continuee,
          ),
        );
        break;
      case ButtonSize.large:
        containedButton = ContainedButton.large(
          busy: widget.mainButtonBusy,
          disabled: widget.mainButtonDisabled,
          onPressed: _onSubmit,
          child: Text(
            widget.buttonText ?? localizations.continuee,
          ),
        );
        break;
      case ButtonSize.small:
      default:
        containedButton = ContainedButton.small(
          busy: widget.mainButtonBusy,
          disabled: widget.mainButtonDisabled,
          onPressed: _onSubmit,
          child: Text(
            widget.buttonText ?? localizations.continuee,
          ),
        );
    }

    return widget.mainButtonBuilder?.call(containedButton, _onSubmit, localizations) ?? containedButton;
  }

  _onSubmit() {
    widget.onContinue.call(_code);
  }
}
