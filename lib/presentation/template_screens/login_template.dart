import 'dart:math';

import 'package:flutter/material.dart' hide TextField;
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class LoginTemplate extends StatefulWidget {
  final EdgeInsets padding;

  // A flag to indicate if the main container should force its expansion to the parents full height
  // This is true by default and will accommodate the sections with a spaceBetween alignment
  final bool forceFullHeight;

  /// Flag to specify if the UI should show a loading CTA
  final bool authenticating;

  /// Specify whether to disable or not the CTA
  final bool canSubmit;

  /// If `icon`, `welcomeText` and `descriptionText` will be center
  final bool centerHead;

  /// Will center the `email` and `password` fields
  final bool centerFields;

  /// Define the `Forgot your password?` legend
  final Alignment forgotPasswordAlignment;

  /// Define a custom builder for the forgot passwords builder
  /// Provides acces to the default widget definition
  final Widget Function(BuildContext context, Widget child)? forgotPasswordBuilder;

  /// Define a custom builder for the have an account legend builder
  /// Provides access to the default widget definition
  final WrapWidgetBuilder? haveAnAccountBuilder;

  /// If `true`, `Email` and `Password` labels will be hidden
  final bool hideFieldsLabel;

  /// Empty space between `icon` and `welcomeText`
  final double spaceBetweenIconAndTitle;

  /// Empty space between `welcomeText` and `descriptionText`
  /// @Deprecated("Please use ispaceBetweenWelcomeAndSubtitlenstead ''") double? spaceBetweenWelcomeAndDescription,
  final double spaceBetweenWelcomeAndSubtitle;

  /// Empty space between the head content and the rest of content
  final double spaceBetweenHeadAndFields;

  /// Empty space between `email` and `password` fields
  final double spaceBetweenFields;

  /// Empty space between fields and sign in button
  final double spaceBetweenFieldsAndButton;

  /// Sign in button text
  final String? buttonText;

  /// The `H1` text
  final String? titleText;

  /// The hint text shown in the email field
  final String? emailFieldHintText;

  /// The hint text shown in the password field
  final String? passwordFieldHintText;

  /// The `H3` text
  final String? subtitleText;

  /// Custom text to show in sign up legend
  final String? signUpLegendText;

  /// The `H1` text color
  final Color? titleTextColor;

  /// The `H3` text color
  final Color? subtitleTextColor;

  /// The color applied to the haveAccount legend action color
  final Color? legendTextColor;

  /// The Style to apply to the haveAccount legend normal text
  final TextStyle? legendNormalStyle;

  /// The Style to apply to the haveAccount legend normal text
  final TextStyle? legendActionStyle;

  /// Widget which is placed at the top
  final Widget? icon;

  /// Widget to allow sign in with other options
  final Widget? loginOptions;

  /// Specify type of main CTA button size
  /// default [ButtonSize.large]
  final ButtonSize? buttonSize;

  /// Specify custom color for main CTA button
  final List<WidgetStateProperty<Color>>? buttonColors;

  /// Specify if should center titles
  final bool centerTitles;

  /// Provides a way to customize the way the main button is rendered
  /// [defaultCta] provides you access to the default widget (useful if you just want to wrap or place other widgets aside the default)
  /// [callback] provides a callback to be used on your custom button 'onTap' implementation, this way the widget can trigger [onLogin] callback properly
  /// [submitting] bool to indicate if the widget is busy
  /// [localizations] provides you access to default localizations
  final Widget Function(Widget defaultCta, Function() callback, bool submitting, GBUiKitLocalizations localizations)?
      mainButtonBuilder;

  /// Provides a way to customize the way the title
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? titleBuilder;

  /// Provides a way to customize the way the subtitle
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? subtitleBuilder;

  /// If should wrap in safe area
  /// By default true
  final bool wrapInSafeArea;

  /// Pass a custom email string label
  final String? emailLabel;

  /// Pass a custom password string label
  final String? passwordLabel;

  /// Provide a way to change the email field input value
  final String? emailValue;

  /// Provide a way to change the password field input value
  final String? passwordValue;

  final void Function()? onForgotPasswordTap;
  final void Function()? onHaveAccountLegendTap;
  final void Function(String email, String password) onLogin;

  /// Override default input decoration with your own implementation for email field
  final InputDecoration? emailFieldInputDecoration;

  /// Override default input decoration with your own implementation for password field
  final InputDecoration Function(BuildContext context, Widget icon)? passwordFieldInputDecoration;

  /// Define if have an account action text should be uppercase
  /// Defaults to [true]
  final bool haveAnAccountActionUppercase;

  const LoginTemplate({
    Key? key,
    this.padding = const EdgeInsets.only(
      top: 24,
      bottom: 16,
      left: 16,
      right: 16,
    ),
    this.forceFullHeight = true,
    required this.authenticating,
    required this.onLogin,
    this.canSubmit = true,
    this.centerHead = false,
    this.centerFields = true,
    this.forgotPasswordAlignment = Alignment.centerLeft,
    this.forgotPasswordBuilder,
    this.hideFieldsLabel = false,
    @Deprecated("Please use instead: 'spaceBetweenIconAndTitle'") double? spaceBetweenIconAndWelcome,
    double spaceBetweenIconAndTitle = 25,
    @Deprecated("Please use instead: 'spaceBetweenWelcomeAndSubtitle'") double? spaceBetweenWelcomeAndDescription,
    double spaceBetweenWelcomeAndSubtitle = 0,
    this.spaceBetweenHeadAndFields = 40,
    this.spaceBetweenFields = 40,
    this.spaceBetweenFieldsAndButton = 50,
    @Deprecated("Please use instead: 'titleText'") String? welcomeText,
    String? titleText,
    @Deprecated("Please use instead: 'descriptionText'") String? descriptionText,
    String? subtitleText,
    this.buttonText,
    this.signUpLegendText,
    this.emailFieldHintText,
    this.passwordFieldHintText,
    this.icon,
    @Deprecated("Please use instead 'titleTextColor'") Color? welcomeTextColor,
    Color? titleTextColor,
    @Deprecated("Please use instead 'descriptionTextColor'") Color? descriptionTextColor,
    Color? subtitleTextColor,
    this.legendTextColor,
    this.legendNormalStyle,
    this.legendActionStyle,
    this.loginOptions,
    this.onForgotPasswordTap,
    this.onHaveAccountLegendTap,
    this.buttonSize,
    this.buttonColors,
    this.mainButtonBuilder,
    this.titleBuilder,
    this.subtitleBuilder,
    this.wrapInSafeArea = true,
    this.centerTitles = false,
    this.emailLabel,
    this.passwordLabel,
    this.haveAnAccountBuilder,
    this.emailValue,
    this.passwordValue,
    this.emailFieldInputDecoration,
    this.passwordFieldInputDecoration,
    this.haveAnAccountActionUppercase = true,
  })  : titleText = titleText ?? welcomeText,
        subtitleText = subtitleText ?? descriptionText,
        titleTextColor = titleTextColor ?? welcomeTextColor,
        subtitleTextColor = subtitleTextColor ?? descriptionTextColor,
        spaceBetweenIconAndTitle = spaceBetweenIconAndWelcome ?? spaceBetweenIconAndTitle,
        spaceBetweenWelcomeAndSubtitle = spaceBetweenWelcomeAndDescription ?? spaceBetweenWelcomeAndSubtitle,
        assert(
          buttonSize == null || mainButtonBuilder == null,
          "You can just define either 'buttonSize' or 'mainButtonBuilder' but not both",
        ),
        assert(
          (titleTextColor == null && titleText == null) || titleBuilder == null,
          "You can just define either 'welcomeText|titleText' or 'titleBuilder' but not both",
        ),
        assert(
          (subtitleTextColor == null && subtitleText == null) || subtitleBuilder == null,
          "You can just define either 'descriptionText|subtitleText' or 'subtitleBuilder' but not both",
        ),
        super(key: key);

  @override
  State<LoginTemplate> createState() => _LoginTemplateState();
}

class _LoginTemplateState extends State<LoginTemplate> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    if (widget.emailValue != null) {
      _emailController.text = widget.emailValue!;
    }
    if (widget.passwordValue != null) {
      _passwordController.text = widget.passwordValue!;
    }
    super.initState();
  }

  @override
  void didUpdateWidget(covariant LoginTemplate oldWidget) {
    if (oldWidget.emailValue != widget.emailValue) {
      _emailController.text = widget.emailValue ?? '';
    }
    if (oldWidget.passwordValue != widget.passwordValue) {
      _passwordController.text = widget.passwordValue ?? '';
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);
    final haveAnAccountLegend = HaveAnAccountLegend(
      alreadyHave: false,
      upperCase: widget.haveAnAccountActionUppercase,
      signUpText: widget.signUpLegendText,
      normalStyle: widget.legendNormalStyle,
      actionStyle: widget.legendActionStyle,
      onAction: widget.onHaveAccountLegendTap ?? () {},
    );
    final fieldMaxWidth = Theme.of(context).inputDecorationTheme.constraints?.maxWidth ?? 343;
    Widget child;
    child = LayoutBuilder(
      builder: (context, outerConstraints) {
        final outerPadding = EdgeInsets.zero.copyWith(
          top: widget.padding.top,
          bottom: widget.padding.bottom,
          left: widget.padding.left,
          right: widget.padding.right,
        );
        return SingleChildScrollView(
          child: Container(
            padding: outerPadding,
            constraints: widget.forceFullHeight
                ? BoxConstraints(
                    minHeight: outerConstraints.maxHeight - outerPadding.vertical,
                  )
                : null,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LayoutBuilder(
                  builder: (context, nestedConstraints) {
                    return Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: (widget.centerHead) ? MainAxisAlignment.center : MainAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: (widget.centerHead) ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                          children: [
                            if (widget.icon != null) ...[
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                  minHeight: 32,
                                  minWidth: min(fieldMaxWidth, nestedConstraints.maxWidth),
                                  maxWidth: min(nestedConstraints.maxWidth, fieldMaxWidth),
                                ),
                                child: widget.icon,
                              ),
                              SizedBox(
                                height: widget.spaceBetweenIconAndTitle,
                              ),
                            ],
                          ],
                        ),
                      ],
                    );
                  },
                ),

                // title and fields
                Column(
                  children: [
                    /// titles
                    LayoutBuilder(
                      builder: (context, nestedConstraints) {
                        return Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: widget.centerTitles ? MainAxisAlignment.center : MainAxisAlignment.start,
                          children: [
                            Container(
                              constraints: BoxConstraints(
                                minWidth: min(fieldMaxWidth, nestedConstraints.maxWidth),
                                maxWidth: nestedConstraints.maxWidth,
                              ),
                              child: Column(
                                crossAxisAlignment:
                                    widget.centerTitles ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                                children: [
                                  _buildTitle(localizations),
                                  SizedBox(
                                    height: widget.spaceBetweenWelcomeAndSubtitle,
                                  ),
                                  _buildSubtitle(localizations),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                    SizedBox(
                      height: widget.spaceBetweenHeadAndFields,
                    ),

                    /// fields
                    LayoutBuilder(
                      builder: (context, nestedConstraints) {
                        return Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: widget.centerFields ? MainAxisAlignment.center : MainAxisAlignment.start,
                          children: [
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth: min(fieldMaxWidth, nestedConstraints.maxWidth),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        (widget.centerFields) ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                                    children: [
                                      TextField(
                                        overrideDecoration: widget.emailFieldInputDecoration != null,
                                        decoration: widget.emailFieldInputDecoration,
                                        label: (widget.hideFieldsLabel) ? null : widget.emailLabel ?? localizations.email,
                                        hintText: widget.emailFieldHintText,
                                        controller: _emailController,
                                        keyboardType: TextInputType.emailAddress,
                                      ),
                                      SizedBox(
                                        height: widget.spaceBetweenFields,
                                      ),
                                      PasswordField(
                                        decoration: widget.passwordFieldInputDecoration,
                                        overrideDecoration: widget.passwordFieldInputDecoration != null,
                                        label:
                                            (widget.hideFieldsLabel) ? null : widget.passwordLabel ?? localizations.password,
                                        hintText: widget.passwordFieldHintText,
                                        controller: _passwordController,
                                        // obscureText: true,
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Builder(builder: (context) {
                                        final defaultForgotPassword = GestureDetector(
                                          onTap: widget.onForgotPasswordTap,
                                          child: Text(
                                            localizations.forgotYourPassword,
                                            style: GBTheme.of(context).textTheme.bodySmall,
                                          ),
                                        );
                                        final alignment = widget.forgotPasswordAlignment;
                                        return Container(
                                          alignment: widget.forgotPasswordAlignment,
                                          constraints: BoxConstraints(
                                            maxWidth: min(fieldMaxWidth, outerConstraints.maxWidth),
                                          ),
                                          child: widget.forgotPasswordBuilder != null
                                              ? widget.forgotPasswordBuilder!(
                                                  context,
                                                  defaultForgotPassword,
                                                )
                                              : Padding(
                                                  padding: EdgeInsets.only(
                                                    left: {Alignment.bottomLeft, Alignment.topLeft, Alignment.centerLeft}
                                                            .contains(alignment)
                                                        ? 4
                                                        : 0,
                                                    right: {Alignment.bottomRight, Alignment.topRight, Alignment.centerRight}
                                                            .contains(alignment)
                                                        ? 4
                                                        : 0,
                                                  ),
                                                  child: defaultForgotPassword,
                                                ),
                                        );
                                      }),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: widget.spaceBetweenFieldsAndButton,
                ),
                LayoutBuilder(
                  builder: (context, nestedConstraints) {
                    return Container(
                      constraints: BoxConstraints(
                        minWidth: min(fieldMaxWidth, nestedConstraints.maxWidth),
                      ),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 27,
                            ),
                            _buildMainCTA(localizations),
                            if (widget.loginOptions != null) widget.loginOptions!,
                            if (widget.onHaveAccountLegendTap != null || widget.haveAnAccountBuilder != null)
                              widget.haveAnAccountBuilder?.call(context, haveAnAccountLegend) ?? haveAnAccountLegend
                          ],
                        ),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        );
      },
    );

    if (widget.wrapInSafeArea) {
      return SafeArea(
        child: child,
      );
    }
    return child;
  }

  _onCTATap() {
    widget.onLogin(
      _emailController.text.trim(),
      _passwordController.text.trim(),
    );
  }

  Widget _buildSubtitle(GBUiKitLocalizations localizations) {
    return widget.subtitleBuilder?.call(localizations) ??
        Text(
          widget.subtitleText ?? localizations.loginToYourAccount,
          textAlign: (widget.centerTitles) ? TextAlign.center : null,
          style: Theme.of(context).textTheme.displaySmall?.copyWith(
                color: widget.subtitleTextColor,
              ),
        );
  }

  Widget _buildTitle(GBUiKitLocalizations localizations) {
    return widget.titleBuilder?.call(localizations) ??
        Text(
          widget.titleText ?? localizations.hello,
          textAlign: (widget.centerTitles) ? TextAlign.center : null,
          style: Theme.of(context).textTheme.displayLarge?.copyWith(
                color: widget.titleTextColor,
              ),
        );
  }

  Widget _buildMainCTA(GBUiKitLocalizations localizations) {
    Widget defaultBtn;
    ButtonSize buttonSize = widget.buttonSize ?? ButtonSize.large;
    if (buttonSize == ButtonSize.free) {
      defaultBtn = ContainedButton.text(
        widget.buttonText ?? localizations.signIn,
        colors: widget.buttonColors,
        onPressed: _onCTATap,
        busy: widget.authenticating,
      );
    } else if (buttonSize == ButtonSize.large) {
      defaultBtn = ContainedButton.large(
        onPressed: _onCTATap,
        busy: widget.authenticating,
        text: widget.buttonText ?? localizations.signIn,
      );
    } else {
      defaultBtn = ContainedButton.small(
        onPressed: _onCTATap,
        busy: widget.authenticating,
        text: widget.buttonText ?? localizations.signIn,
      );
    }

    if (widget.mainButtonBuilder != null) {
      return widget.mainButtonBuilder!(
        defaultBtn,
        _onCTATap,
        widget.authenticating,
        localizations,
      );
    }
    return defaultBtn;
  }
}
