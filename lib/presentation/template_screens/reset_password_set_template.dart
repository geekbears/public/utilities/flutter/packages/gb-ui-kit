// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class ResetPasswordSetTemplate extends StatefulWidget {
  // A flag to indicate if the main container should force its expansion to the parents full height
  // This is true by default and will accommodate the sections with a spaceBetween alignment
  final bool forceFullHeight;

  /// Define the styling of password fields
  final NewPasswordSetFieldsStyle fieldsStyle;

  /// Called when state changes
  final void Function(NewPasswordSetFieldsState state) onFieldStateChange;

  final EdgeInsets padding;

  /// Defines if CTA should be flagged as busy
  final bool settingNewPassword;

  /// Space between `newPasswordText` and `descriptionText`
  final double headerTextsSpace;

  /// Space between head and passwords field
  final double headerBottomSpace;

  /// Space between newPassword field and confirmPassword field
  final double fieldsSpace;

  /// `H1` custom text
  final String? titleText;

  /// `H3` custom text
  final String? subtitleText;

  /// Provides a way to customize the way the title
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? titleBuilder;

  /// Provides a way to customize the way the subtitle
  /// [localizations] provides you access to default localizations
  final Widget Function(GBUiKitLocalizations localizations)? subtitleBuilder;

  /// The text shown in the button
  final String? buttonText;

  /// The color applied to the haveAccount legend action color
  final TextStyle? legendActionTextStyle;

  /// The Style to apply to the haveAccount legend normal text
  final TextStyle? legendNormalTextStyle;

  /// Specify type of main CTA button size
  /// default [ButtonSize.large]
  final ButtonSize buttonSize;

  /// The `H1` text color
  final Color? titleTextColor;

  /// The `H3` text color
  final Color? subtitleTextColor;

  /// The position where the button will be placed
  final ButtonTemplatePosition buttonPosition;

  /// Called when main CTA is pressed
  final void Function(String, String) onSendPassword;

  final void Function()? onHaveAnAccountLegend;

  /// Provides a way to customize the way the main button is rendered
  /// [defaultCta] provides you access to the default widget (useful if you just want to wrap or place other widgets aside the default)
  /// [callback] provides a callback to be used on your custom button 'onTap' implementation, this way the widget can trigger [onSendPassword] callback properly
  /// [submitting] bool to indicate if the widget is busy
  /// [shouldDisable] indicates if you should disable the button
  /// [localizations] provides you access to default localizations
  final Widget Function(
    Widget defaultCta,
    Function() callback,
    bool submitting,
    bool shouldDisable,
    GBUiKitLocalizations localizations,
  )? mainButtonBuilder;

  /// If should wrap in safe area
  /// By default true
  final bool wrapInSafeArea;

  /// Center fields
  final bool centerFields;

  const ResetPasswordSetTemplate({
    Key? key,
    this.forceFullHeight = true,
    this.padding = const EdgeInsets.only(
      top: 24,
      left: 16,
      right: 16,
      bottom: 16,
    ),
    this.settingNewPassword = false,
    this.headerTextsSpace = 0,
    this.headerBottomSpace = 40,
    this.fieldsSpace = 32,
    this.buttonText,
    this.buttonPosition = ButtonTemplatePosition.END_OF_SCREEN,
    this.onHaveAnAccountLegend,
    this.legendActionTextStyle,
    this.legendNormalTextStyle,
    this.buttonSize = ButtonSize.large,
    this.mainButtonBuilder,
    this.wrapInSafeArea = true,
    required this.fieldsStyle,
    this.titleText,
    this.titleTextColor,
    this.titleBuilder,
    this.subtitleBuilder,
    this.subtitleText,
    this.subtitleTextColor,
    required this.onSendPassword,
    required this.onFieldStateChange,
    this.centerFields = false,
  }) : super(key: key);

  @override
  State<ResetPasswordSetTemplate> createState() => _ResetPasswordSetTemplateState();
}

class _ResetPasswordSetTemplateState extends State<ResetPasswordSetTemplate> {
  NewPasswordSetFieldsState? _fieldsState;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final GBUiKitLocalizations localizations = GBUiKitLocalizations.of(context);

    late Widget containedButton = _buildCTA(localizations);

    Widget child = LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          child: Container(
            padding: widget.padding,
            constraints: widget.forceFullHeight
                ? BoxConstraints(
                    minHeight: constraints.maxHeight,
                  )
                : null,
            child: Column(
              mainAxisAlignment: (widget.buttonPosition == ButtonTemplatePosition.END_OF_SCREEN)
                  ? MainAxisAlignment.spaceBetween
                  : MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _buildTitle(localizations, context),
                        if (widget.subtitleText != null || widget.subtitleBuilder != null) ...[
                          SizedBox(
                            height: widget.headerTextsSpace,
                          ),
                          _buildSubtitle(localizations, context),
                        ],
                      ],
                    ),
                    SizedBox(
                      height: widget.headerBottomSpace,
                    ),
                    NewPasswordSetFields(
                      style: widget.fieldsStyle,
                      onFieldStateChange: (state) {
                        setState(() {
                          _fieldsState = state;
                        });
                        widget.onFieldStateChange(state);
                      },
                    ),
                    if (widget.buttonPosition == ButtonTemplatePosition.BELOW_CODE_FIELD) ...[
                      const SizedBox(
                        height: 60,
                      ),
                      Center(
                        child: containedButton,
                      )
                    ],
                  ],
                ),
                Center(
                  child: Column(
                    children: [
                      if (widget.buttonPosition == ButtonTemplatePosition.END_OF_SCREEN) containedButton,
                      if (widget.onHaveAnAccountLegend != null) ...[
                        SizedBox(
                          height: 27,
                        ),
                        ComposedActionText(
                          normalText: localizations.dontHaveAnAccount,
                          actionText: " " + localizations.signUp,
                          actionStyle: widget.legendActionTextStyle,
                          normalStyle: widget.legendNormalTextStyle,
                          onAction: widget.onHaveAnAccountLegend!,
                        )
                      ],
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );

    if (widget.wrapInSafeArea) {
      return SafeArea(
        child: child,
      );
    }

    return child;
  }

  Widget _buildSubtitle(GBUiKitLocalizations localizations, BuildContext context) {
    return widget.subtitleBuilder?.call(localizations) ??
        Text(
          widget.subtitleText ?? localizations.insertTheVerificationCodeYouReceiveInYourMail,
          style: Theme.of(context).textTheme.displaySmall?.copyWith(
                color: widget.subtitleTextColor,
              ),
        );
  }

  Widget _buildTitle(GBUiKitLocalizations localizations, BuildContext context) {
    return widget.titleBuilder?.call(localizations) ??
        Text(
          widget.titleText ?? localizations.resetPassword,
          style: Theme.of(context).textTheme.displayLarge?.copyWith(
                color: widget.titleTextColor,
              ),
        );
  }

  Widget _buildCTA(GBUiKitLocalizations localizations) {
    late ContainedButton containedButton;
    bool shouldDisabled = (widget.settingNewPassword ||
        !((_fieldsState?.meetsRegex ?? false) &&
            (_fieldsState?.meetsLength ?? false) &&
            (_fieldsState?.meetsEquality ?? false)));
    switch (widget.buttonSize) {
      case ButtonSize.free:
        containedButton = ContainedButton(
          disabled: shouldDisabled,
          busy: widget.settingNewPassword,
          onPressed: _onSubmit,
          child: Text(
            widget.buttonText ?? localizations.confirm,
          ),
        );
        break;
      case ButtonSize.large:
        containedButton = ContainedButton.large(
          disabled: shouldDisabled,
          busy: widget.settingNewPassword,
          onPressed: _onSubmit,
          child: Text(
            widget.buttonText ?? localizations.confirm,
          ),
        );
        break;
      case ButtonSize.small:
      default:
        containedButton = ContainedButton.small(
          disabled: shouldDisabled,
          busy: widget.settingNewPassword,
          onPressed: _onSubmit,
          child: Text(
            widget.buttonText ?? localizations.confirm,
          ),
        );
    }

    return widget.mainButtonBuilder
            ?.call(containedButton, _onSubmit, widget.settingNewPassword, shouldDisabled, localizations) ??
        containedButton;
  }

  _onSubmit() {
    widget.onSendPassword.call(_fieldsState!.password, _fieldsState!.confirmPassword);
  }
}
