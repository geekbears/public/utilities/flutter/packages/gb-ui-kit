import 'dart:math';

import 'package:flutter/material.dart' as material;
import 'package:flutter/material.dart';

import '../presentation.dart';

class SliverAppBar extends StatelessWidget {
  final String? title;
  final Color? textColor;
  final List<Widget>? actions;
  final bool automaticallyImplyLeading;
  final Widget? leading;
  final Color? backgroundColor;
  final double minHeight;
  final double maxHeight;
  final Widget? flexibleSpace;

  const SliverAppBar({
    Key? key,
    this.title,
    this.textColor,
    this.actions,
    this.automaticallyImplyLeading = true,
    this.leading,
    this.backgroundColor,
    this.minHeight = 44,
    this.maxHeight = 96,
    this.flexibleSpace,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GBThemeData gbTheme = GBTheme.of(context);
    final Color textBlackColor = textColor ?? gbTheme.textBlack;
    final Color backgroundColor = this.backgroundColor ?? gbTheme.backgroundBase;
    return material.SliverAppBar(
      // collapsedHeight: height,
      toolbarHeight: minHeight,
      expandedHeight: 100,
      backgroundColor: backgroundColor,
      stretchTriggerOffset: 1,
      leading: leading,
      stretch: true,
      onStretchTrigger: () {
        print("object");
        return;
      } as Future<void> Function()?,
      flexibleSpace: SafeArea(
        child: material.LayoutBuilder(
          builder: (context, constraints) {
            final double currentHeight = constraints.maxHeight;
            final double center = constraints.maxWidth / 2;
            // final x = currentHeight / (center * 100);
            // center X = 187
            // 0x = 0
            final double positionX =
                max(0.0, ((center * (currentHeight - minHeight)) / (minHeight - maxHeight)) + center);
            final double translationCenter = max(0, (.5 * (currentHeight - maxHeight)) / (minHeight - maxHeight));
            final double padLeft = 16;
            final double fPad =
                min(((-padLeft * (currentHeight - maxHeight)) / (minHeight - maxHeight)) + padLeft, padLeft);
            // print("current Max Height: ${currentHeight}, x: $positionX, center: $center");
            // print("Translation Center: ${translationCenter}");
            // print("Left Pad: ${fPad}");
            return material.Stack(
              children: [
                // Container(
                //   color: Colors.yellow,
                //   width: 187.5,
                //   height: 2,
                // ),
                material.Positioned(
                  bottom: 0,
                  left: positionX,
                  child: material.FractionalTranslation(
                    translation: material.Offset(-(translationCenter), 0),
                    child: Container(
                      padding: material.EdgeInsets.only(left: fPad),
                      child: title != null ? GBText.h4(title, color: textBlackColor) : null,
                    ),
                  ),
                ),
                material.FlexibleSpaceBar(
                  stretchModes: <StretchMode>[
                    StretchMode.zoomBackground,
                    StretchMode.blurBackground,
                    // StretchMode.fadeTitle,
                  ],
                  collapseMode: material.CollapseMode.pin,
                  background: material.Stack(
                    fit: StackFit.expand,
                    children: [
                      material.LayoutBuilder(builder: (context, constraints) {
                        // print(constraints.maxHeight);
                        return Container(
                          // color: material.Colors.amber,
                          padding: material.EdgeInsets.only(left: 16),
                          alignment: material.Alignment.bottomLeft,
                        );
                      }),
                    ],
                  ),
                  centerTitle: false,
                  // title: GBText.h5("Title"),
                ),
              ],
            );
          },
        ),
      ),
      // flexibleSpace: material.FlexibleSpaceBar(
      //   stretchModes: <StretchMode>[
      //     StretchMode.zoomBackground,
      //     StretchMode.blurBackground,
      //     // StretchMode.fadeTitle,
      //   ],
      //   collapseMode: material.CollapseMode.pin,
      //   background: material.Stack(
      //     fit: StackFit.expand,
      //     children: [
      //       material.LayoutBuilder(builder: (context, constraints) {
      //         print(constraints.maxHeight);
      //         return Container(
      //           // color: material.Colors.amber,
      //           padding: material.EdgeInsets.only(left: 16),
      //           alignment: material.Alignment.bottomLeft,
      //           child: GBText.h4("Title"),
      //         );
      //       }),
      //     ],
      //   ),
      //   centerTitle: false,
      //   // title: GBText.h5("Title"),
      // ),
      automaticallyImplyLeading: automaticallyImplyLeading,
      elevation: 0,
      pinned: true,
      // title: title != null ? Text(title) : null,
    );
  }
}
