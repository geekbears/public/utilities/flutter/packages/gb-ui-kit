
import 'package:flutter/material.dart' as material;
import 'package:flutter/material.dart' hide AppBar;
import 'package:flutter/services.dart';

import '../presentation.dart';

export 'sliver_app_bar.dart';

class AppBar extends StatefulWidget implements PreferredSizeWidget {
  final String? title;
  final Color? textColor;
  final List<Widget>? actions;
  final bool automaticallyImplyLeading;
  final Widget? leading;
  final Color? backgroundColor;
  final double height;
  final Widget? flexibleSpace;

  /// {@template flutter.material.appbar.systemOverlayStyle}
  /// Specifies the style to use for the system overlays that overlap the AppBar.
  ///
  /// This property is only used if [backwardsCompatibility] is false (the default).
  ///
  /// If this property is null, then [AppBarTheme.systemOverlayStyle] of
  /// [ThemeData.appBarTheme] is used. If that is also null, an appropriate
  /// [SystemUiOverlayStyle] is calculated based on the [backgroundColor].
  ///
  /// The AppBar's descendants are built within a
  /// `AnnotatedRegion<SystemUiOverlayStyle>` widget, which causes
  /// [SystemChrome.setSystemUIOverlayStyle] to be called
  /// automatically.  Apps should not enclose an AppBar with their
  /// own [AnnotatedRegion].
  /// {@endtemplate}
  //
  /// See also:
  ///  * [SystemChrome.setSystemUIOverlayStyle]
  final SystemUiOverlayStyle? systemOverlayStyle;

  const AppBar({
    Key? key,
    this.title,
    this.textColor,
    this.actions,
    this.automaticallyImplyLeading = true,
    this.leading,
    this.backgroundColor,
    this.height = 44,
    this.flexibleSpace,
    this.systemOverlayStyle,
  }) : super(key: key);

  factory AppBar.expanded({
    String? title,
    Color? textColor,
    List<Widget>? actions,
    bool automaticallyImplyLeading = true,
    Widget? leading,
    Color? backgroundColor,
    SystemUiOverlayStyle? systemOverlayStyle,
  }) {
    return AppBar(
      height: 96,
      title: null,
      textColor: textColor,
      actions: actions,
      automaticallyImplyLeading: automaticallyImplyLeading,
      leading: leading,
      backgroundColor: backgroundColor,
      systemOverlayStyle: systemOverlayStyle,
      flexibleSpace: material.SafeArea(
        child: material.Builder(builder: (context) {
          final GBThemeData gbThemeData = GBTheme.of(context);
          final Color titleColor = gbThemeData.textBlack;
          return Container(
            alignment: material.Alignment.bottomLeft,
            padding: material.EdgeInsets.only(
              left: 16,
            ),
            child: GBText.h4(
              "Title",
              color: titleColor,
            ),
          );
        }),
      ),
    );
  }

  @override
  _AppBarState createState() => _AppBarState();

  @override
  Size get preferredSize => Size.fromHeight(height);
}

class _AppBarState extends State<AppBar> {
  @override
  Widget build(BuildContext context) {
    final GBThemeData gbTheme = GBTheme.of(context);
    final Color textBlackColor = widget.textColor ?? gbTheme.textBlack;
    final Color backgroundColor = widget.backgroundColor ?? gbTheme.backgroundBase;
    return material.AppBar(
      backgroundColor: backgroundColor,
      leading: widget.leading,
      flexibleSpace: widget.flexibleSpace,
      automaticallyImplyLeading: widget.automaticallyImplyLeading,
      elevation: 0,
      systemOverlayStyle: widget.systemOverlayStyle,
      title: widget.title != null
          ? GBText(
              widget.title,
              style: material.TextStyle(
                inherit: true,
                fontSize: 16,
              ),
            )
          : null,
      iconTheme: material.IconThemeData(
        size: 20,
        color: textBlackColor,
      ),
      centerTitle: true,
      actions: widget.actions
          ?.map(
            (e) => material.Material(
              color: Colors.transparent,
              child: e,
            ),
          )
          .toList(),
    );
  }
}
