import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../presentation.dart';

const EdgeInsets _defaultInsetPadding = EdgeInsets.symmetric(horizontal: 26.0, vertical: 24.0);

Future<T?> showAppModal<T>(
  BuildContext context, {
  Widget? title,
  Widget? message,
  List<Widget>? actions,
  Widget? customCloseIcon,
  Widget? icon,
  Color? backgroundColor,
  ShapeBorder? shape,
  bool barrierDismissible = false,
  EdgeInsetsGeometry? contentPadding,
  EdgeInsets? insetPadding,
  bool useRootNavigator = true,
  Axis actionsDirection = Axis.vertical,
  /**
   * Define the spacing betweenn the titles-message -> actions
   */
  double titlesActionsSpacing = 60,
  /**
   * Define the spacing between icon -> title
   */
  double iconTitlesSpacing = 16,
  /**
   * Define the spacing between title -> message
   */
  double textsSpacing = 16,
  /**
   * Define spacing between each action
   */
  double actionsSpacing = 16,
}) {
  return showDialog<T>(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (context) {
      return SimpleDialog(
        contentPadding: EdgeInsets.zero,
        titlePadding: EdgeInsets.zero,
        backgroundColor: backgroundColor,
        shape: shape,
        alignment: AlignmentDirectional.center,
        insetPadding: insetPadding ?? _defaultInsetPadding,
        // title: _AppModalTitle(
        //   child: title,
        // ),
        children: [
          _AppModalContent(
            contentPadding: contentPadding,
            message: message,
            customCloseIcon: customCloseIcon,
            icon: icon,
            title: title,
            actions: actions,
            actionsDirection: actionsDirection,
            titlesActionsSpacing: titlesActionsSpacing,
            iconTitlesSpacing: iconTitlesSpacing,
            textsSpacing: textsSpacing,
            actionsSpacing: actionsSpacing,
          ),
        ],
      );
    },
    useRootNavigator: useRootNavigator,
  );
}

void showCupertinoAlert(
  BuildContext context, {
  String? title,
  String? content,
  List<Widget>? actions,
}) async {
  await showDialog(
    context: context,
    builder: (_) => CupertinoAlertDialog(
      title: (title != null) ? Text(title) : null,
      content: (content != null) ? Text(content) : null,
      actions: actions ?? [],
    ),
  );
}

class _AppModalTitle extends StatelessWidget {
  final Widget? title;
  final Widget? icon;
  final double iconTitleSpacing;

  const _AppModalTitle({
    Key? key,
    this.title,
    this.icon,
    required this.iconTitleSpacing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (icon != null) ...[
          IconTheme(
            data: IconThemeData(
              size: 60,
            ),
            child: icon!,
          ),
          SizedBox(
            height: iconTitleSpacing,
          ),
        ],
        DefaultTextStyle(
          style: GBTheme.of(context).textTheme.titleLarge!.copyWith(
                color: Colors.black,
              ),
          textAlign: TextAlign.center,
          child: title ?? SizedBox.shrink(),
        ),
      ],
    );
  }
}

class _AppModalContent extends StatelessWidget {
  final Widget? message;
  final Widget? customCloseIcon;
  final Widget? icon;
  final Widget? title;
  final List<Widget>? actions;
  final EdgeInsetsGeometry? contentPadding;
  final Axis actionsDirection;
  /**
   * Define the spacing betweenn the titles-message -> actions
   */
  final double titlesActionsSpacing;
  final double iconTitlesSpacing;
  final double textsSpacing;
  final double actionsSpacing;

  const _AppModalContent({
    Key? key,
    required this.message,
    required this.customCloseIcon,
    required this.icon,
    required this.title,
    required this.actions,
    this.contentPadding,
    required this.actionsDirection,
    required this.titlesActionsSpacing,
    required this.iconTitlesSpacing,
    required this.textsSpacing,
    required this.actionsSpacing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      // fit: StackFit.expand,
      children: [
        Positioned(
          left: 25,
          top: 25,
          // height: w20,
          child: customCloseIcon ??
              IconButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.close,
                  color: Colors.black,
                ),
              ),
        ),
        Container(
          width: double.infinity,
          padding: contentPadding ??
              EdgeInsets.only(
                top: 120,
                left: 40,
                bottom: 72,
                right: 40,
              ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _AppModalTitle(
                title: title,
                icon: icon,
                iconTitleSpacing: iconTitlesSpacing,
              ),
              SizedBox(
                height: textsSpacing,
              ),
              if (message != null) message!,
              if (actions != null)
                Padding(
                  padding: EdgeInsets.only(top: titlesActionsSpacing),
                  child: Wrap(
                    spacing: actionsSpacing,
                    direction: actionsDirection,
                    alignment: WrapAlignment.center,
                    children: [
                      ...actions!,
                    ],
                  ),
                )
            ],
          ),
        ),
      ],
    );
  }
}
