// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alreadyHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("Already have an account?"),
        "changeTheEmailAddress":
            MessageLookupByLibrary.simpleMessage("Change the email address"),
        "code": MessageLookupByLibrary.simpleMessage("Code"),
        "confirm": MessageLookupByLibrary.simpleMessage("Confirm"),
        "confirmAccount":
            MessageLookupByLibrary.simpleMessage("Confirm Account"),
        "confirmPassword":
            MessageLookupByLibrary.simpleMessage("Confirm Password"),
        "confirmYourPassword":
            MessageLookupByLibrary.simpleMessage("Confirm your password"),
        "continuee": MessageLookupByLibrary.simpleMessage("Continue"),
        "didntReceiveCode":
            MessageLookupByLibrary.simpleMessage("Didn’t receive code?"),
        "dontHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("Don’t have an account?"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "enterYourEmail":
            MessageLookupByLibrary.simpleMessage("Enter your email"),
        "enterYourFirstName":
            MessageLookupByLibrary.simpleMessage("Enter your first name"),
        "enterYourLastName":
            MessageLookupByLibrary.simpleMessage("Enter your last name"),
        "enterYourName":
            MessageLookupByLibrary.simpleMessage("Enter your name"),
        "enterYourPassword":
            MessageLookupByLibrary.simpleMessage("Enter your password"),
        "enterYourUsername":
            MessageLookupByLibrary.simpleMessage("Enter your username"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "fillTheFormAndSignUp":
            MessageLookupByLibrary.simpleMessage("Fill the form and sign up."),
        "firstName": MessageLookupByLibrary.simpleMessage("First Name"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("Forgot password"),
        "forgotYourPassword":
            MessageLookupByLibrary.simpleMessage("Forgot your password?"),
        "hello": MessageLookupByLibrary.simpleMessage("Hello"),
        "insertTheVerificationCodeYouReceiveInYourMail":
            MessageLookupByLibrary.simpleMessage(
                "Insert the verification code you receive in your mail."),
        "lastName": MessageLookupByLibrary.simpleMessage("Last Name"),
        "loginToYourAccount":
            MessageLookupByLibrary.simpleMessage("Login to your account."),
        "newPassword": MessageLookupByLibrary.simpleMessage("New Password"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "paste": MessageLookupByLibrary.simpleMessage("Paste"),
        "resetPassword": MessageLookupByLibrary.simpleMessage("Reset Password"),
        "resetPasswordDescription": MessageLookupByLibrary.simpleMessage(
            "Enter the email associated with your account and we’ll send and email with instructions to reset your password"),
        "save": MessageLookupByLibrary.simpleMessage("Save"),
        "send": MessageLookupByLibrary.simpleMessage("Send"),
        "sendAgain": MessageLookupByLibrary.simpleMessage("Send Again"),
        "signIn": MessageLookupByLibrary.simpleMessage("Sign In"),
        "signUp": MessageLookupByLibrary.simpleMessage("Sign Up"),
        "signup": MessageLookupByLibrary.simpleMessage("Signup"),
        "state": MessageLookupByLibrary.simpleMessage("State"),
        "tryAgain": MessageLookupByLibrary.simpleMessage("Try Again"),
        "typeToFilter":
            MessageLookupByLibrary.simpleMessage("Type to filter ..."),
        "unknownFailure":
            MessageLookupByLibrary.simpleMessage("Unknown Failure"),
        "username": MessageLookupByLibrary.simpleMessage("Username"),
        "welcome": MessageLookupByLibrary.simpleMessage("Welcome")
      };
}
