import 'package:flutter/widgets.dart';

abstract class DefaultColors {
  static const Color accent_accent_blue = Color(0xff3d87f6);
  static const Color background_base = Color(0xfff7f7f7);
  static const Color text_text_white = Color(0xffffffff);
  static const Color primaryPrimaryYellow = Color(0xfff6ce3d);
  static const Color accent_error_red = Color(0xfff63d3d);
  static const Color accent_error_red_light = Color(0xfffeecec);
  static const Color background_disabled = Color(0xffe5e5e5);
  static const Color background_contrast = Color(0xfff2f2f2);
  static const Color background_focus = Color(0xff80d9d9d9);
  static const Color text_gray_disabled = Color(0xff808080);
  static const Color text_gray_base = Color(0xff4d4d4d);
  static const Color text_text_black = Color(0xff000000);
}
