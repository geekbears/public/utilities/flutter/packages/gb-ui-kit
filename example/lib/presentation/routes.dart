import 'package:auto_route/auto_route.dart';
import 'package:ui_kit_example/presentation/presentation.dart';

export 'routes.gr.dart';

@MaterialAutoRouter(
  replaceInRouteName: "Screen,Route",
  routes: [
    AutoRoute(
      path: "home",
      page: MyHomeScreen,
      initial: true,
    ),
    AutoRoute(
      path: "appbar",
      page: AppBarScreen,
    ),
    AutoRoute(
      path: "buttons",
      page: ButtonsScreen,
    ),
    AutoRoute(
      path: "text",
      page: TextScreen,
    ),
    AutoRoute(
      path: "inputs",
      page: InputsScreen,
    ),
    AutoRoute(
      path: 'animations',
      page: AnimationsScreen,
    ),
    /* Template Screens */
    AutoRoute(
      path: "template-screens",
      page: TemplateScreensScreen,
    ),
    AutoRoute(
      path: "authentication-selection",
      page: AuthenticationSelectionScreen,
    ),
    AutoRoute(
      path: "loading",
      page: LoadingScreen,
    ),
    AutoRoute(
      path: "error",
      page: ErrorScreen,
    ),
    AutoRoute(
      path: "login",
      page: LoginScreen,
    ),
    AutoRoute(
      path: "signup",
      page: SignUpScreen,
    ),
    AutoRoute(
      path: "request-password-reset",
      page: RequestPasswordResetScreen,
    ),
    AutoRoute(
      path: "reset-password-code-input",
      page: ResetPasswordCodeInputScreen,
    ),
    AutoRoute(
      path: "reset-password-set-password-screen",
      page: ResetPasswordSetPasswordScreen,
    ),
    AutoRoute(
      path: "account-code-confirmation-screen",
      page: AccountCodeConfirmationScreen,
    ),
    /* Template Screens end */
    AutoRoute(
      path: "modals",
      page: ModalsAndMessagesScreen,
    ),
    AutoRoute(
      path: "material3",
      page: Material3NavBarScreen,
    ),
    AutoRoute(
      path: "navbar",
      page: NavBarScreen,
    ),
    AutoRoute(
      path: "utils",
      page: UtilsScreen,
    ),
  ],
)
class $AppRouter {}
