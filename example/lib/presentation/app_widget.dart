import 'package:flutter/material.dart';
import 'package:ui_kit_example/presentation/presentation.dart';

import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool initialThemeSet = false;

  late AppRouter _appRouter;

  @override
  void initState() {
    _appRouter = AppRouter();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
      localizationsDelegates: const [
        GBUiKitLocalizations.delegate,
      ],
      // theme: theme,
      builder: (context, child) {
        return OverlayCanvas(
          child: ThemeConfigurator(
            builder: (theme) {
              return Builder(
                builder: (context) {
                  if (!initialThemeSet) {
                    initialThemeSet = true;
                    final themeD = theme ?? GBTheme.of(context);
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      ThemeConfigurator.of(context)!.changeTheme(themeD);
                    });
                  }
                  return Theme(
                    data: theme!,
                    child: child!,
                  );
                },
              );
            },
          ),
        );
        // return ThemeConfigurator(
        //   builder: (theme) {
        //     return Builder(
        //       builder: (context) {
        //         if (!initialThemeSet) {
        //           initialThemeSet = true;
        //           final themeD = theme ?? GBTheme.of(context);
        //           WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
        //             ThemeConfigurator.of(context)!.changeTheme(themeD);
        //           });
        //         }
        //         return Theme(
        //           data: theme!,
        //           child: child!,
        //         );
        //       },
        //     );
        //   },
        // );
      },
    );
  }
}
