// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i2;
import 'package:flutter/cupertino.dart' as _i4;
import 'package:flutter/material.dart' as _i3;

import 'presentation.dart' as _i1;

class AppRouter extends _i2.RootStackRouter {
  AppRouter([_i3.GlobalKey<_i3.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i2.PageFactory> pagesMap = {
    MyHomeRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.MyHomeScreen(),
      );
    },
    AppBarRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.AppBarScreen(),
      );
    },
    ButtonsRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.ButtonsScreen(),
      );
    },
    TextRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.TextScreen(),
      );
    },
    InputsRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.InputsScreen(),
      );
    },
    AnimationsRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.AnimationsScreen(),
      );
    },
    TemplateRoutesRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.TemplateScreensScreen(),
      );
    },
    AuthenticationSelectionRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.AuthenticationSelectionScreen(),
      );
    },
    LoadingRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.LoadingScreen(),
      );
    },
    ErrorRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.ErrorScreen(),
      );
    },
    LoginRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.LoginScreen(),
      );
    },
    SignUpRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.SignUpScreen(),
      );
    },
    RequestPasswordResetRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.RequestPasswordResetScreen(),
      );
    },
    ResetPasswordCodeInputRoute.name: (routeData) {
      final args = routeData.argsAs<ResetPasswordCodeInputRouteArgs>();
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i1.ResetPasswordCodeInputScreen(
          key: args.key,
          showSetPasswordFields: args.showSetPasswordFields,
        ),
      );
    },
    ResetPasswordSetPasswordRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.ResetPasswordSetPasswordScreen(),
      );
    },
    AccountCodeConfirmationRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.AccountCodeConfirmationScreen(),
      );
    },
    ModalsAndMessagesRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.ModalsAndMessagesScreen(),
      );
    },
    Material3NavBarRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.Material3NavBarScreen(),
      );
    },
    NavBarRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.NavBarScreen(),
      );
    },
    UtilsRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.UtilsScreen(),
      );
    },
  };

  @override
  List<_i2.RouteConfig> get routes => [
        _i2.RouteConfig(
          '/#redirect',
          path: '/',
          redirectTo: 'home',
          fullMatch: true,
        ),
        _i2.RouteConfig(
          MyHomeRoute.name,
          path: 'home',
        ),
        _i2.RouteConfig(
          AppBarRoute.name,
          path: 'appbar',
        ),
        _i2.RouteConfig(
          ButtonsRoute.name,
          path: 'buttons',
        ),
        _i2.RouteConfig(
          TextRoute.name,
          path: 'text',
        ),
        _i2.RouteConfig(
          InputsRoute.name,
          path: 'inputs',
        ),
        _i2.RouteConfig(
          AnimationsRoute.name,
          path: 'animations',
        ),
        _i2.RouteConfig(
          TemplateRoutesRoute.name,
          path: 'template-screens',
        ),
        _i2.RouteConfig(
          AuthenticationSelectionRoute.name,
          path: 'authentication-selection',
        ),
        _i2.RouteConfig(
          LoadingRoute.name,
          path: 'loading',
        ),
        _i2.RouteConfig(
          ErrorRoute.name,
          path: 'error',
        ),
        _i2.RouteConfig(
          LoginRoute.name,
          path: 'login',
        ),
        _i2.RouteConfig(
          SignUpRoute.name,
          path: 'signup',
        ),
        _i2.RouteConfig(
          RequestPasswordResetRoute.name,
          path: 'request-password-reset',
        ),
        _i2.RouteConfig(
          ResetPasswordCodeInputRoute.name,
          path: 'reset-password-code-input',
        ),
        _i2.RouteConfig(
          ResetPasswordSetPasswordRoute.name,
          path: 'reset-password-set-password-screen',
        ),
        _i2.RouteConfig(
          AccountCodeConfirmationRoute.name,
          path: 'account-code-confirmation-screen',
        ),
        _i2.RouteConfig(
          ModalsAndMessagesRoute.name,
          path: 'modals',
        ),
        _i2.RouteConfig(
          Material3NavBarRoute.name,
          path: 'material3',
        ),
        _i2.RouteConfig(
          NavBarRoute.name,
          path: 'navbar',
        ),
        _i2.RouteConfig(
          UtilsRoute.name,
          path: 'utils',
        ),
      ];
}

/// generated route for
/// [_i1.MyHomeScreen]
class MyHomeRoute extends _i2.PageRouteInfo<void> {
  const MyHomeRoute()
      : super(
          MyHomeRoute.name,
          path: 'home',
        );

  static const String name = 'MyHomeRoute';
}

/// generated route for
/// [_i1.AppBarScreen]
class AppBarRoute extends _i2.PageRouteInfo<void> {
  const AppBarRoute()
      : super(
          AppBarRoute.name,
          path: 'appbar',
        );

  static const String name = 'AppBarRoute';
}

/// generated route for
/// [_i1.ButtonsScreen]
class ButtonsRoute extends _i2.PageRouteInfo<void> {
  const ButtonsRoute()
      : super(
          ButtonsRoute.name,
          path: 'buttons',
        );

  static const String name = 'ButtonsRoute';
}

/// generated route for
/// [_i1.TextScreen]
class TextRoute extends _i2.PageRouteInfo<void> {
  const TextRoute()
      : super(
          TextRoute.name,
          path: 'text',
        );

  static const String name = 'TextRoute';
}

/// generated route for
/// [_i1.InputsScreen]
class InputsRoute extends _i2.PageRouteInfo<void> {
  const InputsRoute()
      : super(
          InputsRoute.name,
          path: 'inputs',
        );

  static const String name = 'InputsRoute';
}

/// generated route for
/// [_i1.AnimationsScreen]
class AnimationsRoute extends _i2.PageRouteInfo<void> {
  const AnimationsRoute()
      : super(
          AnimationsRoute.name,
          path: 'animations',
        );

  static const String name = 'AnimationsRoute';
}

/// generated route for
/// [_i1.TemplateScreensScreen]
class TemplateRoutesRoute extends _i2.PageRouteInfo<void> {
  const TemplateRoutesRoute()
      : super(
          TemplateRoutesRoute.name,
          path: 'template-screens',
        );

  static const String name = 'TemplateRoutesRoute';
}

/// generated route for
/// [_i1.AuthenticationSelectionScreen]
class AuthenticationSelectionRoute extends _i2.PageRouteInfo<void> {
  const AuthenticationSelectionRoute()
      : super(
          AuthenticationSelectionRoute.name,
          path: 'authentication-selection',
        );

  static const String name = 'AuthenticationSelectionRoute';
}

/// generated route for
/// [_i1.LoadingScreen]
class LoadingRoute extends _i2.PageRouteInfo<void> {
  const LoadingRoute()
      : super(
          LoadingRoute.name,
          path: 'loading',
        );

  static const String name = 'LoadingRoute';
}

/// generated route for
/// [_i1.ErrorScreen]
class ErrorRoute extends _i2.PageRouteInfo<void> {
  const ErrorRoute()
      : super(
          ErrorRoute.name,
          path: 'error',
        );

  static const String name = 'ErrorRoute';
}

/// generated route for
/// [_i1.LoginScreen]
class LoginRoute extends _i2.PageRouteInfo<void> {
  const LoginRoute()
      : super(
          LoginRoute.name,
          path: 'login',
        );

  static const String name = 'LoginRoute';
}

/// generated route for
/// [_i1.SignUpScreen]
class SignUpRoute extends _i2.PageRouteInfo<void> {
  const SignUpRoute()
      : super(
          SignUpRoute.name,
          path: 'signup',
        );

  static const String name = 'SignUpRoute';
}

/// generated route for
/// [_i1.RequestPasswordResetScreen]
class RequestPasswordResetRoute extends _i2.PageRouteInfo<void> {
  const RequestPasswordResetRoute()
      : super(
          RequestPasswordResetRoute.name,
          path: 'request-password-reset',
        );

  static const String name = 'RequestPasswordResetRoute';
}

/// generated route for
/// [_i1.ResetPasswordCodeInputScreen]
class ResetPasswordCodeInputRoute
    extends _i2.PageRouteInfo<ResetPasswordCodeInputRouteArgs> {
  ResetPasswordCodeInputRoute({
    _i4.Key? key,
    required bool showSetPasswordFields,
  }) : super(
          ResetPasswordCodeInputRoute.name,
          path: 'reset-password-code-input',
          args: ResetPasswordCodeInputRouteArgs(
            key: key,
            showSetPasswordFields: showSetPasswordFields,
          ),
        );

  static const String name = 'ResetPasswordCodeInputRoute';
}

class ResetPasswordCodeInputRouteArgs {
  const ResetPasswordCodeInputRouteArgs({
    this.key,
    required this.showSetPasswordFields,
  });

  final _i4.Key? key;

  final bool showSetPasswordFields;

  @override
  String toString() {
    return 'ResetPasswordCodeInputRouteArgs{key: $key, showSetPasswordFields: $showSetPasswordFields}';
  }
}

/// generated route for
/// [_i1.ResetPasswordSetPasswordScreen]
class ResetPasswordSetPasswordRoute extends _i2.PageRouteInfo<void> {
  const ResetPasswordSetPasswordRoute()
      : super(
          ResetPasswordSetPasswordRoute.name,
          path: 'reset-password-set-password-screen',
        );

  static const String name = 'ResetPasswordSetPasswordRoute';
}

/// generated route for
/// [_i1.AccountCodeConfirmationScreen]
class AccountCodeConfirmationRoute extends _i2.PageRouteInfo<void> {
  const AccountCodeConfirmationRoute()
      : super(
          AccountCodeConfirmationRoute.name,
          path: 'account-code-confirmation-screen',
        );

  static const String name = 'AccountCodeConfirmationRoute';
}

/// generated route for
/// [_i1.ModalsAndMessagesScreen]
class ModalsAndMessagesRoute extends _i2.PageRouteInfo<void> {
  const ModalsAndMessagesRoute()
      : super(
          ModalsAndMessagesRoute.name,
          path: 'modals',
        );

  static const String name = 'ModalsAndMessagesRoute';
}

/// generated route for
/// [_i1.Material3NavBarScreen]
class Material3NavBarRoute extends _i2.PageRouteInfo<void> {
  const Material3NavBarRoute()
      : super(
          Material3NavBarRoute.name,
          path: 'material3',
        );

  static const String name = 'Material3NavBarRoute';
}

/// generated route for
/// [_i1.NavBarScreen]
class NavBarRoute extends _i2.PageRouteInfo<void> {
  const NavBarRoute()
      : super(
          NavBarRoute.name,
          path: 'navbar',
        );

  static const String name = 'NavBarRoute';
}

/// generated route for
/// [_i1.UtilsScreen]
class UtilsRoute extends _i2.PageRouteInfo<void> {
  const UtilsRoute()
      : super(
          UtilsRoute.name,
          path: 'utils',
        );

  static const String name = 'UtilsRoute';
}
