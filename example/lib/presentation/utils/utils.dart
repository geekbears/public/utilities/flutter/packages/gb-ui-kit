import 'package:flutter/material.dart';

mixin ShowcaseScreenMixin {
  Widget buildHeading(BuildContext context, String text) => Align(
        alignment: AlignmentDirectional.centerStart,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            text,
            style: Theme.of(context).textTheme.titleLarge,
          ),
        ),
      );

  Divider buildDivider() => const Divider(
        height: 30,
      );
}
