import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class ThemeConfigurator extends StatefulWidget {
  final Widget Function(GBThemeData? theme)? builder;

  const ThemeConfigurator({
    Key? key,
    this.builder,
  }) : super(key: key);

  @override
  _ThemeConfiguratorState createState() => _ThemeConfiguratorState();

  static _ThemeConfiguratorState? of(BuildContext context) {
    return _InheritedConfigurator.of(context)!.themeConfig;
  }
}

class _ThemeConfiguratorState extends State<ThemeConfigurator> {
  GBThemeData? themeData;

  @override
  void initState() {
    super.initState();
    themeData = GBThemeData(
      // useMaterial3: false,
      textTheme: const TextTheme(
          // headline1: TextStyle(fontSize: 34, color: Colors.red),
          ),
      // // inputDecorationTheme: InputDecorationTheme(
      // //   border: GBThemeData.textFieldBaseOutlineBorder.copyWith(
      // //     borderSide: GBThemeData.textFieldBaseBorderSide.copyWith(
      // //       width: 3,
      // //     ),
      // //   ),
      // // ),
    );
    Future.delayed(const Duration(seconds: 1)).then((value) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        changeTheme(themeData!.copyWith());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    themeData ??= GBTheme.of(context);

    return _InheritedConfigurator(
      child: widget.builder!(themeData),
      themeConfig: this,
    );
  }

  changeTheme(GBThemeData themeData) {
    setState(() {
      this.themeData = themeData;
    });
  }
}

class _InheritedConfigurator extends InheritedWidget {
  final _ThemeConfiguratorState? themeConfig;

  const _InheritedConfigurator({
    Key? key,
    this.themeConfig,
    required this.child,
  }) : super(key: key, child: child);

  @override
  final Widget child;

  static _InheritedConfigurator? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<_InheritedConfigurator>();
  }

  @override
  bool updateShouldNotify(_InheritedConfigurator oldWidget) {
    return oldWidget.themeConfig!.themeData != themeConfig!.themeData;
  }
}
