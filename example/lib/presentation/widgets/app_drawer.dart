import 'package:flutter/material.dart';
import 'package:flutter_hsvcolor_picker/flutter_hsvcolor_picker.dart';

import 'package:flutter_gb_ui_kit/presentation/presentation.dart';

import 'theme_configurator.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({
    Key? key,
  }) : super(key: key);

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  bool _colorOpen = false;
  bool _radiusOpen = false;

  @override
  Widget build(BuildContext context) {
    final GBThemeData themeData = GBTheme.of(context);
    return Drawer(
      child: SafeArea(
        child: Container(
          color: Colors.blue.shade400,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Column(
                children: [
                  ExpansionPanelList(
                    expansionCallback: (panelIndex, isExpanded) {
                      setState(() {
                        if (panelIndex == 0 && !isExpanded) {
                          _colorOpen = true;
                          _radiusOpen = false;
                        } else if (panelIndex == 1 && !isExpanded) {
                          _colorOpen = false;
                          _radiusOpen = true;
                        }
                      });
                    },
                    children: [
                      ExpansionPanel(
                        isExpanded: _colorOpen,
                        headerBuilder: (context, isExpanded) => const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text("ColorSettings"),
                        ),
                        body: buildColorSettings(context, themeData),
                      ),
                      ExpansionPanel(
                        isExpanded: _radiusOpen,
                        headerBuilder: (context, isExpanded) => const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text("BorderSettings"),
                        ),
                        body: buildBorderSettings(context, themeData),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Column buildColorSettings(BuildContext context, GBThemeData themeData) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        buildColorRow(
          "PrimaryColor:",
          context,
          themeData,
          (tT) => tT.primaryColor,
          (color, cD) => cD.copyWith(primaryColor: color),
        ),
        buildColorRow(
          "AccentColor:",
          context,
          themeData,
          (tD) => tD.accentColor,
          (color, cD) => cD.copyWith(accentColor: color),
        ),
        buildColorRow(
          "AccentErrorColor:",
          context,
          themeData,
          (tD) => tD.accentError,
          (color, cD) => cD.copyWith(accentError: color),
        ),
        buildColorRow(
          "AccentErrorLightColor:",
          context,
          themeData,
          (tD) => tD.accentErrorLight,
          (color, cD) => cD.copyWith(accentErrorLight: color),
        ),
        buildColorRow(
          "TextDisabled:",
          context,
          themeData,
          (tD) => tD.textDisabled,
          (color, cD) => cD.copyWith(textDisabled: color),
        ),
        buildColorRow(
          "TextWhite:",
          context,
          themeData,
          (tD) => tD.textWhite,
          (color, cD) => cD.copyWith(textWhite: color),
        ),
        buildColorRow(
          "BackgroundBase:",
          context,
          themeData,
          (tD) => tD.backgroundBase,
          (color, cD) => cD.copyWith(backgroundBase: color),
        ),
        buildColorRow(
          "BackgroundContrast:",
          context,
          themeData,
          (tD) => tD.backgroundContrast,
          (color, cD) => cD.copyWith(backgroundContrast: color),
        ),
        buildColorRow(
          "BackgroundFocus:",
          context,
          themeData,
          (tD) => tD.backgroundFocus,
          (color, cD) => cD.copyWith(backgroundFocus: color),
        ),
        buildColorRow(
          "BackgroundDisabled:",
          context,
          themeData,
          (tD) => tD.backgroundDisabled,
          (color, cD) => cD.copyWith(backgroundDisabled: color),
        ),
        buildColorRow(
          "TextBase:",
          context,
          themeData,
          (tD) => tD.textBase,
          (color, cD) => cD.copyWith(textBase: color),
        ),
        buildColorRow(
          "TextBlack:",
          context,
          themeData,
          (tD) => tD.textBlack,
          (color, cD) => cD.copyWith(textBlack: color),
        ),
      ],
    );
  }

  Column buildBorderSettings(BuildContext context, GBThemeData themeData) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        buildRadiusRow(
          "Field Radius:",
          context,
          themeData,
          (tT) => tT.textFieldRadius,
          (radius, cD) => cD.copyWith(textFieldRadius: radius),
        ),
        buildRadiusRow(
          "Button Radius:",
          context,
          themeData,
          (tT) => tT.buttonsRadius,
          (radius, cD) => cD.copyWith(buttonsRadius: radius),
        ),
        buildIntSliderRow(
          "Field Border Width:",
          context,
          themeData,
          (tT) => tT.textFieldBorderWidth,
          (width, cD) => cD.copyWith(textFieldBorderWidth: width),
        ),
      ],
    );
  }

  Column buildColorRow(
      String title,
      BuildContext context,
      GBThemeData themeData,
      Color? Function(GBThemeData themeData) colorMapper,
      ThemeData Function(Color color, GBThemeData currentThemeData) newThemeHandler) {
    final color = colorMapper.call(themeData);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateColor.resolveWith((states) {
              return color!;
            }),
          ),
          onPressed: () {
            Navigator.of(context).pop();
            final themeConfigState = ThemeConfigurator.of(context);
            showDialog(
                context: context,
                builder: (ctx) {
                  return AlertDialog(
                    content: SingleChildScrollView(
                      child: ColorPicker(
                        color: color!,
                        onChanged: (value) {
                          final newTheme = newThemeHandler(value, themeData);
                          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                            themeConfigState!.changeTheme(
                              newTheme as GBThemeData,
                            );
                          });
                          // setState(() {
                          // });
                        },
                      ),
                    ),
                  );
                });
          },
          child: const Text("change"),
        ),
      ],
    );
  }

  Column buildRadiusRow(
      String title,
      BuildContext context,
      GBThemeData themeData,
      BorderRadius? Function(GBThemeData themeData) radiusMapper,
      ThemeData Function(BorderRadius radius, GBThemeData currentThemeData) newThemeHandler) {
    final color = radiusMapper.call(themeData);
    final currentRadius = radiusMapper(themeData)?.topLeft.x ?? 0;
    print("current radius" + currentRadius.toString());
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title),
        Slider(
            value: currentRadius,
            min: 0,
            max: 35,
            onChanged: (value) {
              final themeConfigState = ThemeConfigurator.of(context);
              final newTheme = newThemeHandler(
                BorderRadius.all(
                  Radius.circular(value.toInt().toDouble()),
                ),
                themeData,
              );
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                themeConfigState!.changeTheme(
                  newTheme as GBThemeData,
                );
              });
            })
      ],
    );
  }

  Column buildIntSliderRow(
      String title,
      BuildContext context,
      GBThemeData themeData,
      double? Function(GBThemeData themeData) radiusMapper,
      ThemeData Function(double width, GBThemeData currentThemeData) newThemeHandler) {
    final currentWidth = radiusMapper(themeData) ?? 1;
    print("current radius" + currentWidth.toString());
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title),
        Slider(
          value: currentWidth,
          min: 0,
          max: 35,
          onChanged: (value) {
            final themeConfigState = ThemeConfigurator.of(context);
            final newTheme = newThemeHandler(
              value,
              themeData,
            );
            WidgetsBinding.instance.addPostFrameCallback(
              (timeStamp) {
                themeConfigState!.changeTheme(
                  newTheme as GBThemeData,
                );
              },
            );
          },
        )
      ],
    );
  }
}

class ColorWrapper {
  Color color;

  ColorWrapper(
    this.color,
  );
}
