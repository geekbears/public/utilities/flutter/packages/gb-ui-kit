import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/presentation/presentation.dart' hide AppBar;
import 'package:ui_kit_example/presentation/presentation.dart';

class NavBarScreen extends StatelessWidget {
  const NavBarScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: const AppDrawer(),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.accessibility_new_rounded),
        onPressed: () {},
      ),
      bottomNavigationBar: BottomNavBar(
        items: [
          BottomNavItem(
            icon: Icons.ac_unit,
            label: "option 1",
          ),
          BottomNavItem(
            icon: Icons.add_circle_sharp,
            label: "option 2",
          ),
          BottomNavItem(
            icon: Icons.add_location_alt_sharp,
            label: "option 3",
          ),
          BottomNavItem(
            icon: Icons.accessibility_sharp,
            label: "option 4",
          ),
          BottomNavItem(
            icon: Icons.texture,
            label: "option 5",
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              color: Colors.red,
              child: ContainedButton.small(
                onPressed: () {},
                text: "change",
              ),
            ),
          ),
        ],
      ),
    );
  }
}
