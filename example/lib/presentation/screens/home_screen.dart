import 'package:flutter/material.dart';
import 'package:ui_kit_example/presentation/presentation.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart' hide AppBar;

class MyHomeScreen extends StatefulWidget {
  const MyHomeScreen({Key? key}) : super(key: key);
  @override
  _MyHomeScreenState createState() => _MyHomeScreenState();
}

class _MyHomeScreenState extends State<MyHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("GB UI Kit"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            buildScreenButton(
              context,
              "Inputs",
              const InputsRoute(),
            ),
            buildScreenButton(
              context,
              "Text",
              const TextRoute(),
            ),
            buildScreenButton(
              context,
              "Buttons",
              const ButtonsRoute(),
            ),
            buildScreenButton(
              context,
              "Animations",
              const AnimationsRoute(),
            ),
            buildScreenButton(
              context,
              "Template Screens",
              const TemplateRoutesRoute(),
            ),
            buildScreenButton(
              context,
              "Navbar",
              const NavBarRoute(),
            ),
            buildScreenButton(
              context,
              "AppBar",
              const AppBarRoute(),
            ),
            buildScreenButton(
              context,
              "Material 3",
              const Material3NavBarRoute(),
            ),
            buildScreenButton(
              context,
              "Modals and Messages",
              const ModalsAndMessagesRoute(),
            ),
            buildScreenButton(
              context,
              "Utils",
              const UtilsRoute(),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildScreenButton(BuildContext context, String text, PageRouteInfo route) {
    return ContainedButton(
      child: Text(text),
      onPressed: () {
        context.pushRoute(route);
      },
    );
  }
}
