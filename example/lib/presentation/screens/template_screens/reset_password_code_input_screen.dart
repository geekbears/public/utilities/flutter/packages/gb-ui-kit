// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart' hide AppBar;
import 'package:ui_kit_example/presentation/routes.dart';

import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class ResetPasswordCodeInputScreen extends StatelessWidget {
  final bool showSetPasswordFields;

  const ResetPasswordCodeInputScreen({
    Key? key,
    required this.showSetPasswordFields,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBar(
        height: kToolbarHeight,
      ),
      body: ResetPasswordCodeInputTemplate(
        padding: const EdgeInsets.only(top: 0, bottom: 16, left: 16, right: 16),
        headerTextsSpace: 10,
        centerCodeField: true,
        hideFieldLabel: true,
        alignResendCodeToRight: true,
        titleText: "Reset Your Password",
        buttonPosition: ButtonTemplatePosition.END_OF_SCREEN,
        // onChangeEmailAddress: () {
        //   debugPrint("change email");
        // },
        codeFieldFeedback: [
          InputFeedbackText.check(
            key: const Key('code'),
            active: false,
            text: const Text('Valid code'),
          ),
        ],
        onResendCode: () {
          debugPrint("send code again");
        },
        onContinue: (code) {
          context.navigateTo(
            const ResetPasswordSetPasswordRoute(),
          );
        },
        onSignUp: () {},
        showNewPasswordFields: showSetPasswordFields,
        newPasswordFieldsStyle: NewPasswordSetFieldsStyle(
          passwordFeedbacks: [
            InputFeedbackText.check(
              key: const ValueKey("key"),
              active: true,
              text: GBText.caption("something"),
            ),
          ],
        ),
        onSetNewPasswordFieldsChange: (state) {},
      ),
    );
  }
}
