import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';
import 'package:auto_route/auto_route.dart';
import 'package:ui_kit_example/presentation/routes.gr.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GBScaffold(
      body: LoginTemplate(
        centerFields: true,
        centerHead: true,
        authenticating: false,
        spaceBetweenWelcomeAndSubtitle: 20,
        spaceBetweenFields: 20,
        icon: Container(
          height: 100,
          width: 100,
          color: Colors.red,
        ),
        onLogin: (email, password) {
          debugPrint("on login - email: $email, password: $password");
        },
        onForgotPasswordTap: () {
          context.navigateTo(
            const RequestPasswordResetRoute(),
          );
        },
      ),
    );
  }
}
