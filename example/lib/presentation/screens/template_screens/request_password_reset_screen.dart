import 'package:flutter/material.dart' hide AppBar;
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';
import 'package:auto_route/auto_route.dart';
import 'package:ui_kit_example/presentation/routes.dart';

class RequestPasswordResetScreen extends StatelessWidget {
  const RequestPasswordResetScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBar(),
      body: ResetPasswordRequestTemplate(
        padding: const EdgeInsets.only(top: 0, bottom: 16, left: 16, right: 16),
        subtitleText: "You will receive a code to create a new password.",
        hideFieldLabel: true,
        centerEmailField: true,
        fieldHintText: "Type your email",
        buttonText: "Say Hello",
        titleTextColor: Colors.green,
        subtitleTextColor: Colors.blue,
        buttonPosition: ButtonTemplatePosition.END_OF_SCREEN,
        onRequestPassword: (email) {
          debugPrint(email);
          context.navigateTo(
            ResetPasswordCodeInputRoute(
              showSetPasswordFields: false,
            ),
          );
        },
        onHaveAccountLegendTap: () {
          debugPrint("on legend");
        },
      ),
    );
  }
}
