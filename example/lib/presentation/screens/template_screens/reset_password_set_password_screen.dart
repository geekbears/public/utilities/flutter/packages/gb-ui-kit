import 'package:flutter/material.dart' hide AppBar;
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class ResetPasswordSetPasswordScreen extends StatelessWidget {
  const ResetPasswordSetPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBar(
        height: kToolbarHeight,
      ),
      body: ResetPasswordSetTemplate(
        padding: const EdgeInsets.only(top: 0, left: 16, right: 16, bottom: 16),
        fieldsStyle: NewPasswordSetFieldsStyle(
          centerFields: true,
          newPasswordHintText: "Input your new password here",
          confirmPasswordHintText: "Confirm your password",
          passwordFeedbacks: [
            InputFeedbackText.check(
              key: const Key('length'),
              active: false,
              text: const Text('Min length'),
            ),
          ],
          confirmPasswordFeedbacks: [
            InputFeedbackText.check(
              key: const Key('match'),
              active: false,
              text: const Text('Passwords match'),
            ),
          ],
        ),
        buttonPosition: ButtonTemplatePosition.BELOW_CODE_FIELD,
        titleText: "Set Your Password",
        subtitleText: "Set a new password for your account",
        headerTextsSpace: 10,
        onSendPassword: (password, confirmedPassowrd) {
          debugPrint("Password: $password, Confirmed password: $confirmedPassowrd");
        },
        onHaveAnAccountLegend: () {},
        onFieldStateChange: (state) {},
      ),
    );
  }
}
