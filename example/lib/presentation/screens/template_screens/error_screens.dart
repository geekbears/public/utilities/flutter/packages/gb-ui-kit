import 'package:flutter/material.dart';

import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ErrorTemplate(
        description: "Some random error description",
        onTryAgain: () {
          debugPrint("On Try Again");
        },
      ),
    );
  }
}
