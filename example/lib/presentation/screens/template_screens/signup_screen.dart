import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SignUpTemplate(
          canSubmit: true,
          centerHead: true,
          submitting: false,
          centerFields: true,
          centerIcon: true,
          buttonTopSpace: 10,
          additionalRequirement: Column(
            children: [
              Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SelectableRadio(
                      value: true,
                      groupValue: true,
                      size: 25,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    GBText.body("Accept terms"),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
          icon: Container(
            height: 50,
            width: 50,
            color: Colors.blue,
          ),
          onSubmit: () {},
          onHaveAccountLegendTap: () {},
          fields: SignUpTemplate.defaultFields(
            context,
            onFirstNameChange: (value) {},
            onLastNameChange: (value) {},
            onPasswordChange: (value) {},
            onConfirmPasswordChange: (value) {},
            onEmailChange: (value) {},
          ),
        ),
      ),
    );
  }
}
