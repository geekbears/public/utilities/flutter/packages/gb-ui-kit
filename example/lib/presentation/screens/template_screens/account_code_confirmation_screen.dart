import 'package:flutter/material.dart' hide AppBar;
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class AccountCodeConfirmationScreen extends StatelessWidget {
  const AccountCodeConfirmationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBar(
        height: 56,
      ),
      body: AccountCodeConfirmationTemplate(
        padding: const EdgeInsets.only(top: 0, bottom: 16, left: 16, right: 16),
        hideCodeFieldLabel: true,
        headerTextsSpace: 20,
        centerCodeField: true,
        legendPosition: ButtonTemplatePosition.BELOW_CODE_FIELD,
        buttonPosition: ButtonTemplatePosition.END_OF_SCREEN,
        codeFieldFeedback: [
          InputFeedbackText.check(
            key: const Key('code'),
            active: false,
            text: const Text('Valid code'),
          ),
        ],
        buttonText: "Confirm",
        onResendCode: () {
          debugPrint("On resend code");
        },
        onSubmit: (code) {
          debugPrint("On Submit, code: $code");
        },
      ),
    );
  }
}
