import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/presentation/presentation.dart' hide AppBar;
import 'package:ui_kit_example/presentation/presentation.dart';

class Material3NavBarScreen extends StatefulWidget {
  const Material3NavBarScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<Material3NavBarScreen> createState() => _Material3NavBarScreenState();
}

class _Material3NavBarScreenState extends State<Material3NavBarScreen> {
  bool useMaterial3 = false;
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return ThemeConfigurator(
      builder: (theme) => Theme(
        data: theme?.copyWith(
              useMaterial3: useMaterial3,
            ) ??
            GBTheme.of(context).copyWith(
              useMaterial3: useMaterial3,
            ),
        child: Scaffold(
          appBar: AppBar(),
          drawer: const AppDrawer(),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AnimatedContainer(
                  duration: const Duration(
                    milliseconds: 200,
                  ),
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.secondary,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    (useMaterial3)
                        ? "I'm a container using the color `colorScheme.secondary`\nbut applying the `useMaterial3` property c:"
                        : "I'm a container using the color `colorScheme.secondary` c:",
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                TextButton(
                  onPressed: () => setState(
                    () => useMaterial3 = !useMaterial3,
                  ),
                  child: Text(
                    (useMaterial3) ? 'Disable useMaterial3' : 'Enable useMaterial3',
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: NavigationBar(
            selectedIndex: index,
            onDestinationSelected: (value) => setState(() => index = value),
            destinations: const [
              NavigationDestination(
                icon: Icon(Icons.ac_unit),
                label: "option 1",
              ),
              NavigationDestination(
                icon: Icon(Icons.add_circle_sharp),
                label: "option 2",
              ),
              NavigationDestination(
                icon: Icon(Icons.add_location_alt_sharp),
                label: "option 3",
              ),
              NavigationDestination(
                icon: Icon(Icons.accessibility_sharp),
                label: "option 4",
              ),
              NavigationDestination(
                icon: Icon(Icons.texture),
                label: "option 5",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
