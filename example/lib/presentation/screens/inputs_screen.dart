// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart' as gb_ui;

import '../presentation.dart';

class InputsScreen extends StatelessWidget with ShowcaseScreenMixin {
  const InputsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: const AppDrawer(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              buildHeading(context, "TextFields"),
              buildTextFields(),
              buildDivider(),
              buildHeading(context, "PasswordField"),
              buildPasswordField(),
              buildHeading(context, "CodeField"),
              buildCodeField(),
              buildHeading(context, "SelectField"),
              buildSelectField(),
              buildHeading(context, "MultiSelectField"),
              buildSelectField(true),
              buildHeading(context, "Address State Select"),
              buildAddressStateSelectField(),
              buildHeading(context, "Custom Feedback"),
              const _CustomFeedbackTextField(),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildAddressStateSelectField() {
    return gb_ui.AddressStateSelectField(
      onStateChange: (state) {
        debugPrint("Selected State: $state");
      },
      enableSearch: true,
    );
  }

  Widget buildSelectField([bool multi = false]) => _SelectField(
        multi: multi,
      );

  Widget buildMultiSelectField() => const _SelectField();

  Widget buildCodeField() {
    return const _CodeField();
  }

  Widget buildPasswordField() => const _PasswordField();

  Widget buildTextFields() {
    return const _TextField();
  }
}

class _SelectField extends StatefulWidget {
  final bool multi;

  const _SelectField({
    Key? key,
    this.multi = false,
  }) : super(key: key);

  @override
  __SelectFieldState createState() => __SelectFieldState();
}

class __SelectFieldState extends State<_SelectField> {
  bool _disabled = false;
  bool _error = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.multi)
          gb_ui.MultiSelectField(
            // enableSearch: true,
            enabled: !_disabled,
            label: "Label",
            enableSearch: true,
            // closeOnSelect: false,
            errorText: _error ? "Some error here" : null,
            trailingBuilder: (context, index, isSelected) => const Icon(Icons.ac_unit),
            options: [
              ...[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(
                (e) => gb_ui.SelectFieldOption(
                  title: "Option $e",
                  value: e,
                  trailing: e == 3
                      ? Row(
                          children: [
                            const Icon(Icons.access_alarm),
                            IconButton(
                              icon: const Icon(Icons.account_box),
                              onPressed: () {
                                debugPrint("icon tap");
                              },
                            )
                          ],
                        )
                      : null,
                ),
              ),
            ],
            onSelected: (dynamic value) {
              debugPrint("Option selected is $value");
            },
            // enabled: false,
          )
        else
          gb_ui.SelectField(
            enableSearch: true,
            enabled: !_disabled,
            label: "Label",
            // closeOnSelect: false,
            errorText: _error ? "Some error here" : null,
            trailingBuilder: (context, index, isSelected) => const Icon(Icons.ac_unit),
            options: [
              ...[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(
                (e) => gb_ui.SelectFieldOption(
                  title: "Option $e",
                  value: e,
                  trailing: e == 3
                      ? Row(
                          children: [
                            const Icon(Icons.access_alarm),
                            IconButton(
                              icon: const Icon(Icons.account_box),
                              onPressed: () {
                                debugPrint("icon tap");
                              },
                            )
                          ],
                        )
                      : null,
                ),
              ),
            ],
            onSelected: (dynamic value) {
              debugPrint("Option selected is $value");
            },
            // enabled: false,
          ),
        _Switches(
          disabled: _disabled,
          error: _error,
          onDisableChange: (value) => setState(() => _disabled = value),
          onErrorChange: (value) => setState(() => _error = value),
        ),
      ],
    );
  }
}

class _PasswordField extends StatefulWidget {
  const _PasswordField({
    Key? key,
  }) : super(key: key);

  @override
  __PasswordFieldState createState() => __PasswordFieldState();
}

class __PasswordFieldState extends State<_PasswordField> {
  bool _disabled = false;
  bool _error = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        gb_ui.PasswordField(
          label: "Label",
          enabled: !_disabled,
          errorText: _error ? "Error text" : null,
        ),
        _Switches(
          disabled: _disabled,
          error: _error,
          onDisableChange: (value) => setState(() => _disabled = value),
          onErrorChange: (value) => setState(() => _error = value),
        ),
      ],
    );
  }
}

class _TextField extends StatefulWidget {
  const _TextField({
    Key? key,
  }) : super(key: key);

  @override
  __TextFieldState createState() => __TextFieldState();
}

class __TextFieldState extends State<_TextField> {
  bool _disabled = false;
  bool _error = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        gb_ui.TextField(
          label: "Email",
          prefixIcon: const Icon(Icons.mail),
          hintText: 'Hey',
          errorText: _error ? "Assistive text" : null,
          enabled: !_disabled,
        ),
        _Switches(
          disabled: _disabled,
          error: _error,
          onDisableChange: (value) => setState(() => _disabled = value),
          onErrorChange: (value) => setState(() => _error = value),
        ),
      ],
    );
  }
}

class _CustomFeedbackTextField extends StatefulWidget {
  const _CustomFeedbackTextField({
    Key? key,
  }) : super(key: key);

  @override
  __CustomFeedbackTextFieldState createState() => __CustomFeedbackTextFieldState();
}

class __CustomFeedbackTextFieldState extends State<_CustomFeedbackTextField> {
  bool _disabled = false;
  bool _error = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        gb_ui.TextField(
          label: "Email",
          // errorText: _error ? "Assistive text" : null,
          enabled: !_disabled,
        ),
        gb_ui.InputFeedbackContainer(
          feedback: [
            gb_ui.InputFeedbackText.check(
              key: const ValueKey("1"),
              active: _error,
              text: const Text(
                "Some text here",
              ),
            ),
            gb_ui.InputFeedbackText.check(
              key: const ValueKey("2"),
              active: _error,
              text: const Text(
                "Some text here",
              ),
              color: Colors.purple,
            ),
            if (_error)
              gb_ui.InputFeedbackText.error(
                key: const ValueKey("3"),
                context: context,
                active: _error,
                text: const Text(
                  "Some text here",
                ),
              ),
          ],
        ),
        _Switches(
          disabled: _disabled,
          error: _error,
          onDisableChange: (value) => setState(() => _disabled = value),
          onErrorChange: (value) => setState(() => _error = value),
        ),
      ],
    );
  }
}

class _Switches extends StatelessWidget {
  final Function(bool value) onErrorChange;
  final Function(bool value) onDisableChange;

  const _Switches({
    Key? key,
    required bool disabled,
    required bool error,
    required this.onErrorChange,
    required this.onDisableChange,
  })  : _disabled = disabled,
        _error = error,
        super(key: key);

  final bool _disabled;
  final bool _error;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: [
            Switch(
              value: _disabled,
              onChanged: onDisableChange,
            ),
            const Text("Disabled"),
          ],
        ),
        Column(
          children: [
            Switch(
              value: _error,
              onChanged: onErrorChange,
            ),
            const Text("Error"),
          ],
        ),
      ],
    );
  }
}

class _CodeField extends StatefulWidget {
  const _CodeField({
    Key? key,
  }) : super(key: key);

  @override
  __CodeFieldState createState() => __CodeFieldState();
}

class __CodeFieldState extends State<_CodeField> {
  bool _disabled = false;
  bool _error = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        gb_ui.CodeField(
          label: "Code",
          errorText: _error ? "Error text" : null,
          enabled: !_disabled,
          keyboardType: TextInputType.number,
          onChanged: (value) {
            debugPrint("The value is: $value");
          },
        ),
        _Switches(
          disabled: _disabled,
          error: _error,
          onDisableChange: (value) => setState(() => _disabled = value),
          onErrorChange: (value) => setState(() => _error = value),
        ),
      ],
    );
  }
}
