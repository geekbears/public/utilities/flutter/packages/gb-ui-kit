import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/presentation/presentation.dart' hide AppBar;
import 'package:ui_kit_example/presentation/presentation.dart';

class TextScreen extends StatelessWidget with ShowcaseScreenMixin {
  const TextScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: const AppDrawer(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              buildHeading(context, "Headings"),
              buildDivider(),
              buildHeadings(context),
              buildHeading(context, "Subtitle"),
              Column(
                children: [
                  GBText.subtitle("Subtitle/ S1-bold"),
                  GBText.subtitle2("Subtitle/ S2-bold"),
                ],
              ),
              buildHeading(context, "Body"),
              Column(
                children: [
                  GBText.body("Body/B1-regular"),
                  GBText.body2("Body/B2-regular"),
                ],
              ),
              buildHeading(context, "Other"),
              Column(
                children: [
                  GBText.button("Small/ BUTTON-bold"),
                  GBText.caption("Small/Caption-regular"),
                  GBText.overline("Small/overline-regular"),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Column buildHeadings(BuildContext context) {
    return Column(
      children: [
        GBText.h1("Head/H1-regular"),
        GBText.h2("Head/H2-regular"),
        GBText.h3("Head/H3-regular"),
        GBText.h4("Head/H4-regular"),
        GBText.h5("Head/H5-regular"),
        GBText.h6("Head/H6-regular"),
      ],
    );
  }
}
