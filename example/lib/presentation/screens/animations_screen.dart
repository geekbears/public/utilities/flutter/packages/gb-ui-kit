import 'package:flutter/material.dart' hide AppBar;
import '../widgets/app_drawer.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class AnimationsScreen extends StatefulWidget {
  const AnimationsScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<AnimationsScreen> createState() => _AnimationsScreenState();
}

class _AnimationsScreenState extends State<AnimationsScreen> {
  bool _useRandomHeight = true;
  bool _useRandomWidth = true;
  bool _showFirstChild = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBar(
        backgroundColor: Colors.blue,
        title: "Animations",
      ),
      drawer: const AppDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            GBText.h2(
              "AnimatedPlaceholder",
              color: Colors.black,
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 50,
              child: Center(
                child: AnimatedPlaceholder(
                  useRandomHeight: _useRandomHeight,
                  useRandomWidth: _useRandomWidth,
                  randomMaxHeight: 50,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SelectableRadio(
              value: true,
              groupValue: _useRandomHeight,
              label: "Use random height",
              onChanged: (value) => setState(() => _useRandomHeight = !_useRandomHeight),
            ),
            const SizedBox(
              height: 10,
            ),
            SelectableRadio(
              value: true,
              groupValue: _useRandomWidth,
              label: "Use random width",
              onChanged: (value) => setState(() => _useRandomWidth = !_useRandomWidth),
            ),
            const SizedBox(
              height: 30,
            ),
            GBText.h2(
              "AnimatedFadeHeightSwitcher",
              color: Colors.black,
            ),
            const SizedBox(
              height: 20,
            ),
            AnimatedFadeHeightSwitcher(
              child: (_showFirstChild)
                  ? const Center(
                      child: FlutterLogo(
                        size: 100,
                      ),
                    )
                  : const Align(
                      alignment: Alignment.center,
                      child: Text("Another widget"),
                    ),
            ),
            const SizedBox(
              height: 20,
            ),
            ContainedButton.small(
              onPressed: () => setState(() => _showFirstChild = !_showFirstChild),
              text: "Change child",
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              "Audio Visualizer Widget",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(
              width: 30,
              height: 24,
              // color: Colors.blueAccent,
              child: MusicVisualizer(
                linesCount: 4,
                lineWidth: 4,
                colors: [Colors.blue, Colors.black],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
