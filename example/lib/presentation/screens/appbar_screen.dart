import 'package:flutter/material.dart' hide AppBar, SliverAppBar;

import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';
import 'package:ui_kit_example/presentation/presentation.dart';

enum AppBarType { expanded, normal, sliver }

class AppBarScreen extends StatefulWidget {
  const AppBarScreen({
    Key? key,
  }) : super(key: key);

  @override
  _AppBarScreenState createState() => _AppBarScreenState();
}

class _AppBarScreenState extends State<AppBarScreen> {
  AppBarType type = AppBarType.normal;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: type == AppBarType.normal
          ? AppBar(
              leading: const BackButton(),
              automaticallyImplyLeading: false,
              title: "Hello",
              actions: [
                IconButton(
                  icon: const Icon(Icons.ac_unit_outlined),
                  onPressed: () {},
                )
              ],
            )
          : type == AppBarType.expanded
              ? AppBar.expanded(
                  leading: const BackButton(),
                  automaticallyImplyLeading: false,
                  title: "Hello",
                  actions: [
                    IconButton(
                      icon: const Icon(Icons.ac_unit_outlined),
                      onPressed: () {},
                    )
                  ],
                )
              : null,
      drawer: const AppDrawer(),
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: [
          if (type == AppBarType.sliver)
            const SliverAppBar(
              title: "Title",
            ),
          SliverList(
            delegate: SliverChildListDelegate([
              Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Builder(builder: (context) {
                        return ContainedButton.text(
                          "Open Drawer",
                          onPressed: () {
                            Scaffold.of(context).openDrawer();
                          },
                        );
                      }),
                      Builder(builder: (context) {
                        return ContainedButton.text(
                          type == AppBarType.normal
                              ? "normal"
                              : type == AppBarType.expanded
                                  ? "expanded"
                                  : "sliver",
                          onPressed: () {
                            if (type == AppBarType.normal) {
                              type = AppBarType.expanded;
                            } else if (type == AppBarType.expanded) {
                              type = AppBarType.sliver;
                            } else {
                              type = AppBarType.normal;
                            }
                            setState(() {});
                          },
                        );
                      }),
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Container(
                          width: 30,
                          height: 30,
                          color: Colors.red,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Container(
                          width: 30,
                          height: 30,
                          color: Colors.red,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Container(
                          width: 30,
                          height: 30,
                          color: Colors.red,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Container(
                          width: 30,
                          height: 30,
                          color: Colors.red,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Container(
                          width: 30,
                          height: 30,
                          color: Colors.red,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Container(
                          width: 30,
                          height: 30,
                          color: Colors.red,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Container(
                          width: 30,
                          height: 30,
                          color: Colors.red,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Container(
                          width: 30,
                          height: 30,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
