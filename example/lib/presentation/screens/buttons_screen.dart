import 'package:flutter/material.dart' hide OutlinedButton;
import 'package:flutter_gb_ui_kit/presentation/presentation.dart' hide AppBar;
import 'package:ui_kit_example/presentation/presentation.dart';

class ButtonsScreen extends StatefulWidget {
  const ButtonsScreen({Key? key}) : super(key: key);

  @override
  State<ButtonsScreen> createState() => _ButtonsScreenState();
}

class _ButtonsScreenState extends State<ButtonsScreen> with ShowcaseScreenMixin {
  bool _disabled = false;
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: const AppDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            buildHeading(context, "Contained Buttons"),
            Column(
              children: [
                Switcher(
                  value: _disabled,
                  enabledColor: Colors.green,
                  disabledColor: Colors.red,
                  onChanged: (value) {
                    setState(() {
                      _disabled = value;
                    });
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("Disabled:"),
                    Switch(
                      value: _disabled,
                      onChanged: (value) {
                        setState(() {
                          _disabled = value;
                        });
                      },
                    ),
                    const Text("Loading:"),
                    Switch(
                      value: _loading,
                      onChanged: (value) {
                        setState(() {
                          _loading = value;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
            Column(
              children: [
                ContainedButton.text(
                  "Text button",
                  onPressed: () {},
                  disabled: _disabled,
                  busy: _loading,
                ),
                buildDivider(),
                ContainedButton.small(
                  text: "Small button",
                  onPressed: () {},
                  disabled: _disabled,
                  busy: _loading,
                ),
                buildDivider(),
                ContainedButton.large(
                  text: "Large",
                  onPressed: () {},
                  disabled: _disabled,
                  busy: _loading,
                ),
                buildDivider(),
                ContainedButton.large(
                  text: "Large with gradient",
                  colors: [
                    WidgetStateProperty.all(Colors.red),
                    WidgetStateProperty.all(Colors.orange),
                  ],
                  onPressed: () {},
                  disabled: _disabled,
                  busy: _loading,
                ),
              ],
            ),
            buildDivider(),
            buildHeading(context, "Outlined Buttons"),
            Column(
              children: [
                OutlinedButton.text(
                  "Text button",
                  onPressed: () {},
                  disabled: _disabled,
                  busy: _loading,
                ),
                buildDivider(),
                OutlinedButton.small(
                  onPressed: () {},
                  text: "Small button",
                  disabled: _disabled,
                  busy: _loading,
                ),
                buildDivider(),
                OutlinedButton.large(
                  text: "Large",
                  onPressed: () {},
                  disabled: _disabled,
                  busy: _loading,
                ),
              ],
            ),
            buildDivider(),
            buildHeading(context, "Link Button"),
            LinkButton(
              child: const Icon(Icons.ac_unit_outlined),
              onPressed: () {},
              disabled: _disabled,
              busy: _loading,
            ),
            LinkButton.text(
              "label",
              prefix: const Icon(Icons.check),
              trail: const Icon(Icons.check),
              onPressed: () {},
              disabled: _disabled,
              busy: _loading,
            ),
            LinkButton.text(
              "label",
              onPressed: () {},
              disabled: _disabled,
              busy: _loading,
            ),
            buildDivider(),
            buildHeading(context, "Composed action text"),
            ComposedActionText(
              disabled: _disabled,
              onAction: () {},
              normalText: "This is some text:",
              actionText: "Action",
              busy: _loading,
            ),
          ],
        ),
      ),
    );
  }
}
