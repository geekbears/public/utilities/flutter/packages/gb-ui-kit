import 'package:flutter/material.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';
import 'package:auto_route/auto_route.dart';
import 'package:ui_kit_example/presentation/routes.dart';

class TemplateScreensScreen extends StatelessWidget {
  const TemplateScreensScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ContainedButton.text(
              "Loading",
              onPressed: () {
                context.navigateTo(
                  const LoadingRoute(),
                );
              },
            ),
            ContainedButton.text(
              "Login",
              onPressed: () {
                context.navigateTo(
                  const LoginRoute(),
                );
              },
            ),
            ContainedButton.text(
              "SignUp",
              onPressed: () {
                context.navigateTo(
                  const SignUpRoute(),
                );
              },
            ),
            ContainedButton.text(
              "Request Password Reset",
              onPressed: () {
                context.navigateTo(
                  const RequestPasswordResetRoute(),
                );
              },
            ),
            ContainedButton.text(
              "Reset Password Code",
              onPressed: () {
                context.navigateTo(
                  ResetPasswordCodeInputRoute(showSetPasswordFields: false),
                );
              },
            ),
            ContainedButton.text(
              "Reset Password Code With Password Fields",
              onPressed: () {
                context.navigateTo(
                  ResetPasswordCodeInputRoute(showSetPasswordFields: true),
                );
              },
            ),
            ContainedButton.text(
              "Reset Password Set",
              onPressed: () {
                context.navigateTo(
                  const ResetPasswordSetPasswordRoute(),
                );
              },
            ),
            ContainedButton.text(
              "Account Code Confirmation",
              onPressed: () {
                context.navigateTo(
                  const AccountCodeConfirmationRoute(),
                );
              },
            ),
            ContainedButton.text(
              "Error",
              onPressed: () {
                context.navigateTo(
                  const ErrorRoute(),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
