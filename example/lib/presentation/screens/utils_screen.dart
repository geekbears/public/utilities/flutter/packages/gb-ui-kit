import 'package:flutter/material.dart' hide Stepper, Step, AppBar;

import '../widgets/app_drawer.dart';
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';

class UtilsScreen extends StatefulWidget {
  const UtilsScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<UtilsScreen> createState() => _UtilsScreenState();
}

class _UtilsScreenState extends State<UtilsScreen> {
  bool isInFirstStep = true;
  bool rememberPassword = true;
  bool hasError = false;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return GBScaffold(
      appBar: const AppBar(),
      drawer: const AppDrawer(),
      body: Stack(
        children: [
          Image.network(
            'https://upload.wikimedia.org/wikipedia/commons/2/29/1969_Ford_Mustang_Mach_1_%2837901276352%29.jpg',
            color: Colors.white.withOpacity(0.8),
            colorBlendMode: BlendMode.colorDodge,
            fit: BoxFit.fill,
            height: double.infinity,
            width: double.infinity,
          ),
          SingleChildScrollView(
            child: Column(
              children: [
                GBText.h2('Stepper'),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: Stepper(
                    dotsColor: Colors.blue,
                    steps: [
                      Step(
                        title: 'Step 1',
                        isActive: isInFirstStep,
                        isCompleted: !isInFirstStep,
                        activeColor: Colors.red,
                        inactiveColor: Colors.black,
                        completedColor: Colors.green,
                        completedBorderColor: Colors.blue,
                      ),
                      Step(
                        title: 'Step 2',
                        isActive: !isInFirstStep,
                        isCompleted: false,
                        activeColor: Colors.red,
                        inactiveColor: Colors.black,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ContainedButton.small(
                      text: 'Back Step',
                      onPressed: () => setState(() => isInFirstStep = true),
                      disabled: isInFirstStep,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    ContainedButton.small(
                      text: 'Next Step',
                      onPressed: () => setState(() => isInFirstStep = false),
                      disabled: !isInFirstStep,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 40,
                ),
                GBText.h2('Selectable radio'),
                const SizedBox(
                  height: 20,
                ),
                SelectableRadio<bool>(
                  value: true,
                  groupValue: rememberPassword,
                  onChanged: (value) => setState(() => rememberPassword = !rememberPassword),
                  label: 'Remember my password',
                ),
                const SizedBox(
                  height: 20,
                ),
                GBText.h2('GenericDataHandlerWidget'),
                const SizedBox(
                  height: 20,
                ),
                GenericDataHandlerWidget(
                  onTryAgain: () => setState(() => isLoading = !isLoading),
                  isLoading: isLoading,
                  hasError: hasError,
                  errorTitle: "Error Over Here",
                  errorMessage: "Something went wrong while deserializing, you should take in account",
                  loadingChild: Center(
                    child: AnimatedPlaceholder(
                      useRandomHeight: false,
                      useRandomWidth: false,
                      height: 60,
                      width: 60,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: const Center(
                        child: Text(
                          "We have data!",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                SelectableRadio<bool>(
                  value: true,
                  groupValue: isLoading,
                  onChanged: (value) => setState(() => isLoading = !isLoading),
                  label: 'Is loading',
                ),
                const SizedBox(
                  height: 30,
                ),
                ContainedButton.small(
                  onPressed: () => setState(() => hasError = !hasError),
                  text: "Error button has error: $hasError",
                ),
                const SizedBox(
                  height: 30,
                ),
                GBText.h2('TimePicker'),
                const SizedBox(
                  height: 20,
                ),
                TimePicker(
                  // availableAfterHours: 1,
                  // availableAfterMinutes: 20,
                  unavailableTimes: const [
                    UnavailableTime(
                      hour: 8,
                      disableEntireHour: false,
                      disableMinutesFrom: 13,
                      disableMinutesTo: 23,
                    ),
                    UnavailableTime(
                      hour: 14,
                      disableEntireHour: false,
                      disableMinutesFrom: 0,
                      disableMinutesTo: 5,
                    ),
                    UnavailableTime(
                      hour: 14,
                      disableEntireHour: false,
                      disableMinutesFrom: 10,
                      disableMinutesTo: 15,
                    ),
                    UnavailableTime(
                      hour: 16,
                      disableEntireHour: true,
                      disableMinutesFrom: 0,
                      disableMinutesTo: 0,
                    ),
                    UnavailableTime(
                      hour: 12,
                      disableEntireHour: false,
                      disableMinutesFrom: 45,
                      disableMinutesTo: 59,
                    ),
                  ],
                  activeTimeStyle: const TextStyle(
                    fontSize: 23,
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                  ),
                  disabledTimeStyle: const TextStyle(
                    fontSize: 23,
                    color: Colors.red,
                  ),
                  onChangedParsed: (time, isAvailable) => print("$time, isAvailableTime: $isAvailable"),
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
