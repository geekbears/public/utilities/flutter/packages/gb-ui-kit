import 'package:flutter/material.dart' hide AppBar, TextField;
import 'package:flutter_gb_ui_kit/flutter_gb_ui_kit.dart';
import 'package:ui_kit_example/presentation/widgets/widgets.dart';

class ModalProperties {
  String? title;
  String? message;
  bool showConfirmButton;
  bool showIcon;
  bool barrierDismissible;

  ModalProperties({
    this.title,
    this.message,
    this.showConfirmButton = false,
    this.showIcon = false,
    this.barrierDismissible = false,
  });
}

class SnackbardProperties {
  String? title;
  String? message;
  Color? backgroundColor;
  SnackbarAlertType type;
  Duration duration;

  SnackbardProperties({
    this.title,
    this.message,
    this.backgroundColor,
    this.type = SnackbarAlertType.success,
    this.duration = const Duration(seconds: 3),
  });
}

class ModalsAndMessagesScreen extends StatelessWidget {
  const ModalsAndMessagesScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
      ),
      drawer: const AppDrawer(),
      body: const SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ModalPropertiesFields(),
              SizedBox(
                height: 20,
              ),
              SnackbardPropertiesFields(),
            ],
          ),
        ),
      ),
    );
  }
}

class ModalPropertiesFields extends StatefulWidget {
  const ModalPropertiesFields({
    Key? key,
  }) : super(key: key);

  @override
  State<ModalPropertiesFields> createState() => _ModalPropertiesFieldsState();
}

class _ModalPropertiesFieldsState extends State<ModalPropertiesFields> {
  final ModalProperties modalProperties = ModalProperties();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GBText.subtitle('Modal properties'),
        const SizedBox(
          height: 20,
        ),
        GBText.body('Title'),
        const SizedBox(
          height: 10,
        ),
        TextField(
          onChanged: (value) => setState(() => modalProperties.title = value),
        ),
        const SizedBox(
          height: 10,
        ),
        GBText.body('Message'),
        const SizedBox(
          height: 10,
        ),
        TextField(
          onChanged: (value) => setState(() => modalProperties.message = value),
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Flexible(
              fit: FlexFit.loose,
              child: GBText.body('Show confirm button (modal action example)'),
            ),
            Checkbox(
              onChanged: (value) => setState(() => modalProperties.showConfirmButton = value!),
              value: modalProperties.showConfirmButton,
            ),
          ],
        ),
        Row(
          children: [
            Flexible(
              fit: FlexFit.loose,
              child: GBText.body('Show icon (modal icon example)'),
            ),
            Checkbox(
              onChanged: (value) => setState(() => modalProperties.showIcon = value!),
              value: modalProperties.showIcon,
            ),
          ],
        ),
        Row(
          children: [
            Flexible(
              fit: FlexFit.loose,
              child: GBText.body('Enable barrier dismissible'),
            ),
            Checkbox(
              onChanged: (value) => setState(() => modalProperties.barrierDismissible = value!),
              value: modalProperties.barrierDismissible,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Center(
          child: TextButton(
            onPressed: () => showAppModal(
              context,
              backgroundColor: GBTheme.of(context).backgroundBase,
              customCloseIcon: GestureDetector(
                onTap: Navigator.of(context).pop,
                child: Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                    color: GBTheme.of(context).accentColor.withOpacity(0.5),
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 20,
                  ),
                ),
              ),
              icon: (modalProperties.showIcon)
                  ? const Icon(
                      Icons.check_circle,
                      size: 20,
                    )
                  : null,
              title: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(modalProperties.title ?? ''),
              ),
              message: Text(modalProperties.message ?? ''),
              actions: [
                if (modalProperties.showConfirmButton)
                  ContainedButton.small(
                    onPressed: Navigator.of(context).pop,
                    text: 'Confirm',
                  ),
              ],
              barrierDismissible: modalProperties.barrierDismissible,
            ),
            child: const Text(
              'Show modal',
            ),
          ),
        ),
      ],
    );
  }
}

class SnackbardPropertiesFields extends StatefulWidget {
  const SnackbardPropertiesFields({
    Key? key,
  }) : super(key: key);

  @override
  State<SnackbardPropertiesFields> createState() => _SnackbardPropertiesFieldsState();
}

class _SnackbardPropertiesFieldsState extends State<SnackbardPropertiesFields> {
  final SnackbardProperties snackbardProperties = SnackbardProperties();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GBText.subtitle('Snackbar properties'),
        const SizedBox(
          height: 20,
        ),
        GBText.body('Title'),
        const SizedBox(
          height: 10,
        ),
        TextField(
          onChanged: (value) => setState(() => snackbardProperties.title = value),
        ),
        const SizedBox(
          height: 10,
        ),
        GBText.body('Message'),
        const SizedBox(
          height: 10,
        ),
        TextField(
          onChanged: (value) => setState(() => snackbardProperties.message = value),
        ),
        const SizedBox(
          height: 10,
        ),
        const SizedBox(
          height: 10,
        ),
        GBText.body('Duration'),
        SelectField<int>(
          onSelected: (value) => setState(() => snackbardProperties.duration = Duration(seconds: value)),
          options: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map<SelectFieldOption<int>>((e) {
            return SelectFieldOption(
              title: e.toString(),
              value: e,
            );
          }).toList(),
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Flexible(
              fit: FlexFit.loose,
              child: GBText.body('Is an error? '),
            ),
            Checkbox(
              value: snackbardProperties.type == SnackbarAlertType.failure,
              onChanged: (value) => setState(
                () => snackbardProperties.type = value! ? SnackbarAlertType.failure : SnackbarAlertType.success,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Center(
          child: TextButton(
            onPressed: () => showSnackBarAlert(
              context,
              title: snackbardProperties.title,
              message: snackbardProperties.message,
              type: snackbardProperties.type,
              duration: snackbardProperties.duration,
            ),
            child: const Text(
              'Show snackbar',
            ),
          ),
        ),
      ],
    );
  }
}
